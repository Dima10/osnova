<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplCommandContent;

/**
 * ApplCommandContentSearch represents the model behind the search form about `app\models\ApplCommandContent`.
 */
class ApplCommandContentSearch extends ApplCommandContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applcmdc_id', 'applcmdc_applcmd_id', 'applcmdc_prs_id', 'applcmdc_trp_id'], 'integer'],
            [['applcmdc_create_user', 'applcmdc_create_time', 'applcmdc_create_ip', 'applcmdc_update_user', 'applcmdc_update_time', 'applcmdc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplCommandContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applcmdc_id' => $this->applcmdc_id,
            'applcmdc_applcmd_id' => $this->applcmdc_applcmd_id,
            'applcmdc_prs_id' => $this->applcmdc_prs_id,
            'applcmdc_trp_id' => $this->applcmdc_trp_id,
            'applcmdc_create_time' => $this->applcmdc_create_time,
            'applcmdc_update_time' => $this->applcmdc_update_time,
        ]);

        $query->andFilterWhere(['like', 'applcmdc_create_user', $this->applcmdc_create_user])
            ->andFilterWhere(['like', 'applcmdc_create_ip', $this->applcmdc_create_ip])
            ->andFilterWhere(['like', 'applcmdc_update_user', $this->applcmdc_update_user])
            ->andFilterWhere(['like', 'applcmdc_update_ip', $this->applcmdc_update_ip]);

        return $dataProvider;
    }
}
