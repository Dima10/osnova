<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement}}".
 *
 * @property integer $agr_id
 * @property string $agr_number
 * @property string $agr_date
 * @property integer $agr_ab_id
 * @property integer $agr_prs_id
 * @property integer $agr_comp_id
 * @property integer $agr_acc_id
 * @property integer $agr_ast_id
 * @property string $agr_comment
 * @property string $agr_sum
 * @property string $agr_tax
 * @property integer $agr_pat_id
 * @property string $agr_fdata
 * @property string $agr_fdata_sign
 * @property resource $agr_data
 * @property resource $agr_data_sign
 * @property string $agr_create_time
 * @property string $agr_create_ip
 * @property string $agr_update_user
 * @property string $agr_update_time
 * @property string $agr_update_ip
 * @property string $agr_create_user
 *
 * @property Ab $agrAb
 * @property Company $agrComp
 * @property Account $agrAcc
 * @property Person $agrPrs
 * @property AgreementStatus $agrAst 
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnex[] $agreementAnnexes
 * @property AgreementAdd[] $agreementAdds
 * @property AgreementAcc[] $agreementAccs
 */
class Agreement extends \yii\db\ActiveRecord
{
    public $agr_act;
    public $agr_annex;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['agr_number', 'agr_date', 'agr_ab_id', 'agr_comp_id'], 'required'],
            [['agr_prs_id', 'agr_ab_id', 'agr_pat_id', 'agr_comp_id', 'agr_acc_id', 'agr_prs_id', 'agr_ast_id'], 'integer'],
            [['agr_sum', 'agr_tax'], 'number'],
            [['agr_number', 'agr_date'], 'string'],

            [['agr_data', 'agr_data_sign'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, docx, pdf'],

            [['agr_comment'], 'string', 'max' => 1024],
            [['agr_fdata', 'agr_fdata_sign'], 'string'],
            [['agr_create_time', 'agr_update_time'], 'safe'],
            [['agr_create_ip', 'agr_update_user', 'agr_update_ip', 'agr_create_user'], 'string', 'max' => 64],

            [['agr_number', 'agr_comp_id'], 'unique', 'targetAttribute' => ['agr_number', 'agr_comp_id'], 'message' => Yii::t('app', 'The combination of Agr Number and Agr Comp ID has already been taken.')],

            [['agr_ab_id'],     'exist', 'skipOnError' => true, 'targetClass' => Ab::class,               'targetAttribute' => ['agr_ab_id' => 'ab_id']],
            [['agr_comp_id'],   'exist', 'skipOnError' => true, 'targetClass' => Company::class,          'targetAttribute' => ['agr_comp_id' => 'comp_id']],
            [['agr_acc_id'],    'exist', 'skipOnError' => true, 'targetClass' => Account::class,          'targetAttribute' => ['agr_acc_id' => 'acc_id']],
            [['agr_ast_id'],    'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class,  'targetAttribute' => ['agr_ast_id' => 'ast_id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agr_id' => Yii::t('app', 'Agr ID'),
            'agr_number' => Yii::t('app', 'Agr Number'),
            'agr_date' => Yii::t('app', 'Agr Date'),
            'agr_ab_id' => Yii::t('app', 'Agr Ab ID'),
            'agr_comp_id' => Yii::t('app', 'Agr Comp ID'),
            'agr_acc_id' => Yii::t('app', 'Agr Acc ID'),
            'agr_prs_id' => Yii::t('app', 'Agr Prs ID'),
            'agr_pat_id' => Yii::t('app', 'Agr Pat ID'), 
            'agr_ast_id' => Yii::t('app', 'Agr Ast ID'),
            'agr_comment' => Yii::t('app', 'Agr Comment'),
            'agr_sum' => Yii::t('app', 'Agr Sum'),
            'agr_tax' => Yii::t('app', 'Agr Tax'),
            'agr_data' => Yii::t('app', 'Agr Data'),
            'agr_data_sign' => Yii::t('app', 'Agr Data Sign'),
            'agr_fdata' => Yii::t('app', 'Agr Fdata'),
            'agr_fdata_sign' => Yii::t('app', 'Agr Fdata Sign'),
            'agr_act' => Yii::t('app', 'Agr Act'),
            'agr_annex' => Yii::t('app', 'Agr Annex'),
            'agr_create_time' => Yii::t('app', 'Agr Create Time'),
            'agr_create_ip' => Yii::t('app', 'Agr Create Ip'),
            'agr_update_user' => Yii::t('app', 'Agr Update User'),
            'agr_update_time' => Yii::t('app', 'Agr Update Time'),
            'agr_update_ip' => Yii::t('app', 'Agr Update Ip'),
            'agr_create_user' => Yii::t('app', 'Agr Create User'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAdds()
    {
        return $this->hasMany(AgreementAdd::class, ['agadd_agr_id' => 'agr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrAst() 
    { 
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'agr_ast_id']); 
    } 
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'agr_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'agr_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'agr_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgrPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'agr_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_agr_id' => 'agr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_agr_id' => 'agr_id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getAgreementAccs() 
    { 
        return $this->hasMany(AgreementAcc::class, ['aga_agr_id' => 'agr_id']); 
    }


    /**
     * @inheritdoc
     * @return AgreementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->agr_create_user = Yii::$app->user->identity->username;
            $this->agr_create_ip = Yii::$app->request->userIP;
            $this->agr_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->agr_update_user = Yii::$app->user->identity->username;
            $this->agr_update_ip = Yii::$app->request->userIP;
            $this->agr_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
