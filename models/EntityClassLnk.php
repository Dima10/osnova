<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%entity_class_lnk}}".
 *
 * @property integer $entcl_id
 * @property integer $entcl_ent_id
 * @property integer $entcl_entc_id
 * @property string $entc_create_user
 * @property string $entc_create_time
 * @property string $entc_create_ip
 * @property string $entc_update_user
 * @property string $entc_update_time
 * @property string $entc_update_ip
 *
 * @property Entity $entclEnt
 * @property EntityClass $entclEntc
 */
class EntityClassLnk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_class_lnk}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entcl_ent_id', 'entcl_entc_id'], 'required'],
            [['entcl_ent_id', 'entcl_entc_id'], 'integer'],
            [['entcl_create_time', 'entcl_update_time'], 'safe'],
            [['entcl_create_user', 'entcl_create_ip', 'entcl_update_user', 'entcl_update_ip'], 'string', 'max' => 64],
            [['entcl_ent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::class, 'targetAttribute' => ['entcl_ent_id' => 'ent_id']],
            [['entcl_entc_id'], 'exist', 'skipOnError' => true, 'targetClass' => EntityClass::class, 'targetAttribute' => ['entcl_entc_id' => 'entc_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entcl_id' => Yii::t('app', 'Entcl ID'),
            'entcl_ent_id' => Yii::t('app', 'Entcl Ent ID'),
            'entcl_entc_id' => Yii::t('app', 'Entcl Entc ID'),
            'entc_create_user' => Yii::t('app', 'Entc Create User'),
            'entc_create_time' => Yii::t('app', 'Entc Create Time'),
            'entc_create_ip' => Yii::t('app', 'Entc Create Ip'),
            'entc_update_user' => Yii::t('app', 'Entc Update User'),
            'entc_update_time' => Yii::t('app', 'Entc Update Time'),
            'entc_update_ip' => Yii::t('app', 'Entc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntclEnt()
    {
        return $this->hasOne(Entity::class, ['ent_id' => 'entcl_ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntclEntc()
    {
        return $this->hasOne(EntityClass::class, ['entc_id' => 'entcl_entc_id']);
    }

    /**
     * @inheritdoc
     * @return EntityClassLnkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntityClassLnkQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->entcl_create_user = Yii::$app->user->identity->username;
            $this->entcl_create_ip = Yii::$app->request->userIP;
            $this->entcl_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->entcl_update_user = Yii::$app->user->identity->username;
            $this->entcl_update_ip = Yii::$app->request->userIP;
            $this->entcl_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
