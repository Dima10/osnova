<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2017-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_add}}".
 *
 * @property integer $agadd_id
 * @property integer $agadd_ab_id
 * @property string $agadd_number
 * @property string $agadd_date
 * @property integer $agadd_agr_id
 * @property integer $agadd_pat_id
 * @property integer $agadd_acc_id
 * @property integer $agadd_add_id
 * @property integer $agadd_prs_id
 * @property integer $agadd_ast_id
 * @property string $agadd_file_name
 * @property resource $agadd_file_data
 * @property string $agadd_create_user
 * @property string $agadd_create_time
 * @property string $agadd_create_ip
 * @property string $agadd_update_user
 * @property string $agadd_update_time
 * @property string $agadd_update_ip
 *
 * @property Agreement $agaddAgr
 * @property AgreementStatus $agaddAst
 * @property Address $agaddAdd
 * @property Account $agaddAcc
 * @property Staff $agaddStf
 * @property Ab $agaddAb
 * @property Pattern $agaddPat
 */
class AgreementAdd extends \yii\db\ActiveRecord
{

    public $number;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_add}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agadd_ab_id', 'agadd_number', 'agadd_date', 'agadd_agr_id'], 'required'],
            [['agadd_date', 'agadd_create_time', 'agadd_update_time'], 'safe'],
            [['agadd_ab_id', 'agadd_agr_id', 'agadd_ast_id', 'agadd_pat_id', 'agadd_pat_id', 'agadd_acc_id', 'agadd_add_id', 'agadd_prs_id'], 'integer'],
            [['agadd_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, pdf'],
            [['agadd_file_name'], 'string', 'max' => 256],

            [['agadd_number', 'agadd_create_user', 'agadd_create_ip', 'agadd_update_user', 'agadd_update_ip'], 'string', 'max' => 64],
            [['agadd_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['agadd_ab_id' => 'ab_id']],
            [['agadd_agr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agadd_agr_id' => 'agr_id']],
            [['agadd_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['agadd_pat_id' => 'pat_id']],
            [['number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agadd_id' => Yii::t('app', 'Agadd ID'),
            'agadd_ab_id' => Yii::t('app','Agadd Ab ID'),
            'agadd_number' => Yii::t('app', 'Agadd Number'),
            'agadd_date' => Yii::t('app', 'Agadd Date'),
            'agadd_agr_id' => Yii::t('app', 'Agadd Agr ID'),
            'agadd_pat_id' => Yii::t('app', 'Agadd Pat ID'),
            'agadd_acc_id' => Yii::t('app', 'Agadd Acc ID'),
            'agadd_add_id' => Yii::t('app', 'Agadd Add ID'),
            'agadd_prs_id' => Yii::t('app', 'Agadd Prs ID'),
            'agadd_ast_id' => Yii::t('app', 'Agadd Ast ID'),
            'agadd_file_name' => Yii::t('app', 'Agadd File Name'),
            'agadd_file_data' => Yii::t('app', 'Agadd File Data'),
            'agadd_create_user' => Yii::t('app', 'Agadd Create User'),
            'agadd_create_time' => Yii::t('app', 'Agadd Create Time'),
            'agadd_create_ip' => Yii::t('app', 'Agadd Create Ip'),
            'agadd_update_user' => Yii::t('app', 'Agadd Update User'),
            'agadd_update_time' => Yii::t('app', 'Agadd Update Time'),
            'agadd_update_ip' => Yii::t('app', 'Agadd Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'agadd_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'agadd_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddAst()
    {
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'agadd_ast_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddAdd()
    {
        return $this->hasOne(Address::class, ['add_id' => 'agadd_add_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'agadd_prs_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getAgaddAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'agadd_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgaddAgr()
    {
        return $this->hasOne(Agreement::class, ['agr_id' => 'agadd_agr_id']);
    }

    /**
     * @inheritdoc
     * @return AgreementAddQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementAddQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->agadd_create_user = Yii::$app->user->identity->username;
            $this->agadd_create_ip = Yii::$app->request->userIP;
            $this->agadd_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->agadd_update_user = Yii::$app->user->identity->username;
            $this->agadd_update_ip = Yii::$app->request->userIP;
            $this->agadd_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
