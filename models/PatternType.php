<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%pattern_type}}".
 *
 * @property integer $patt_id
 * @property string $patt_name
 * @property string $patt_note
 * @property string $patt_create_user
 * @property string $patt_create_time
 * @property string $patt_create_ip
 * @property string $patt_update_user
 * @property string $patt_update_time
 * @property string $patt_update_ip
 *
 * @property Pattern[] $patterns
 */
class PatternType extends \yii\db\ActiveRecord
{

    const  FINAL_DOCUMENT_TYPE_ID = 16;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pattern_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patt_name'], 'required'],
            [['patt_note'], 'string'],
            [['patt_create_time', 'patt_update_time'], 'safe'],
            [['patt_name', 'patt_create_user', 'patt_create_ip', 'patt_update_user', 'patt_update_ip'], 'string', 'max' => 64],
            [['patt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'patt_id' => Yii::t('app', 'Patt ID'),
            'patt_name' => Yii::t('app', 'Patt Name'),
            'patt_note' => Yii::t('app', 'Patt Note'),
            'patt_create_user' => Yii::t('app', 'Patt Create User'),
            'patt_create_time' => Yii::t('app', 'Patt Create Time'),
            'patt_create_ip' => Yii::t('app', 'Patt Create Ip'),
            'patt_update_user' => Yii::t('app', 'Patt Update User'),
            'patt_update_time' => Yii::t('app', 'Patt Update Time'),
            'patt_update_ip' => Yii::t('app', 'Patt Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatterns()
    {
        return $this->hasMany(Pattern::class, ['pat_patt_id' => 'patt_id']);
    }

    /**
     * @inheritdoc
     * @return PatternTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PatternTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->patt_create_user = Yii::$app->user->identity->username;
            $this->patt_create_ip = Yii::$app->request->userIP;
            $this->patt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->patt_update_user = Yii::$app->user->identity->username;
            $this->patt_update_ip = Yii::$app->request->userIP;
            $this->patt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
