<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;


class Constant
{
    const MONTHS = [
        'January' => 'января',
        'February' => 'февраля',
        'March' => 'марта',
        'April' => 'апрель',
        'May' => 'Мая',
        'June' => 'июня',
        'July' => 'июля',
        'August' => 'августа',
        'September' => 'сентября',
        'October' => 'октября',
        'November' => 'ноября',
        'December' => 'декабря'
    ];

    const YES_NO = [
        '' => '',
        0 => 'Нет',
        1 => 'Да',
    ];

    /**
     * Возвращаем Да/Нет
     * @param boolean $var
     * @return array
     */
    public static function yes_no($var = false): array
    {
        if ($var)
            return [0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')];
        else
            return ['' => '', 0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')];
    }


    /**
     * Возвращаем доступные реестры
     * @param boolean $var
     * @return array
     */
    public static function reestr_val($var = false): array
    {
        if ($var) {
            $reesters = [0 => Yii::t('app', 'Main Reestr')];
        } else {
            $reesters = ['' => '', 0 => Yii::t('app', 'Main Reestr')];
        }

        if (!User::isSpecAdmin()) {
            $reesters[1] = Yii::t('app', 'Add Reestr');
        }

        return $reesters;
    }
}
