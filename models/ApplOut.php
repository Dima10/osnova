<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_out}}".
 *
 * @property integer $applout_id
 * @property string $applout_number
 * @property string $applout_date
 * @property integer $applout_trt_id
 * @property integer $applout_svdt_id
 * @property integer $applout_pat_id
 * @property string $applout_file_name
 * @property resource $applout_file_data
 * @property string $applout_create_user
 * @property string $applout_create_time
 * @property string $applout_create_ip
 * @property string $applout_update_user
 * @property string $applout_update_time
 * @property string $applout_update_ip
 *
 * @property SvcDocType $apploutSvdt
 * @property Pattern $apploutPat
 * @property TrainingType $apploutTrt
 * @property ApplOutContent[] $applOutContents
 */
class ApplOut extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_out}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applout_number', 'applout_date'], 'required'],
            [['applout_date', 'applout_create_time', 'applout_update_time'], 'safe'],
            [['applout_pat_id', 'applout_trt_id', 'applout_svdt_id'], 'integer'],
            [['applout_file_data'], 'string'],
            [['applout_number', 'applout_create_user', 'applout_create_ip', 'applout_update_user', 'applout_update_ip'], 'string', 'max' => 64],
            [['applout_file_name'], 'string', 'max' => 256],
            [['applout_number'], 'unique'],
            [['applout_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['applout_pat_id' => 'pat_id']],
            [['applout_svdt_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applout_svdt_id' => 'svdt_id']],
            [['applout_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applout_trt_id' => 'trt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applout_id' => Yii::t('app', 'Applout ID'),
            'applout_number' => Yii::t('app', 'Applout Number'),
            'applout_date' => Yii::t('app', 'Applout Date'),
            'applout_trt_id' => Yii::t('app', 'Applout Trt ID'),
            'applout_svdt_id' => Yii::t('app', 'Applout Svdt ID'),
            'applout_pat_id' => Yii::t('app', 'Applout Pat ID'),
            'applout_file_name' => Yii::t('app', 'Applout File Name'),
            'applout_file_data' => Yii::t('app', 'Applout File Data'),
            'applout_create_user' => Yii::t('app', 'Applout Create User'),
            'applout_create_time' => Yii::t('app', 'Applout Create Time'),
            'applout_create_ip' => Yii::t('app', 'Applout Create Ip'),
            'applout_update_user' => Yii::t('app', 'Applout Update User'),
            'applout_update_time' => Yii::t('app', 'Applout Update Time'),
            'applout_update_ip' => Yii::t('app', 'Applout Update Ip'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applout_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applout_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApploutPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applout_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplOutContents()
    {
        return $this->hasMany(ApplOutContent::class, ['apploutc_applout_id' => 'applout_id']);
    }

    /**
     * @inheritdoc
     * @return ApplOutQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplOutQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applout_create_user = Yii::$app->user->identity->username;
            $this->applout_create_ip = Yii::$app->request->userIP;
            $this->applout_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applout_update_user = Yii::$app->user->identity->username;
            $this->applout_update_ip = Yii::$app->request->userIP;
            $this->applout_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
