<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_final}}".
 *
 * Итоговый документ
 *
 * @property integer $applf_id
 * @property integer $applf_reestr
 * @property string $applf_prs_id
 * @property string $applf_applr_id
 * @property string $applf_name_first
 * @property string $applf_name_last
 * @property string $applf_name_middle
 * @property string $applf_name_full
 * @property integer $applf_applm_id
 * @property integer $applf_svdt_id
 * @property integer $applf_trt_id
 * @property integer $applf_trp_id
 * @property integer $applf_trp_hour
 * @property string $applf_cmd_date
 * @property string $applf_end_date
 * @property string $applf_number
 * @property integer $applf_pat_id
 * @property resource $applf_file_data
 * @property string $applf_file_name
 * @property resource $applf_file0_data
 * @property string $applf_file0_name
 * @property string $applf_create_user
 * @property string $applf_create_time
 * @property string $applf_create_ip
 * @property string $applf_update_user
 * @property string $applf_update_time
 * @property string $applf_update_ip
 *
 * @property ApplRequest $applfApplr
 * @property Person $applfPrs
 * @property Pattern $applfPat
 * @property TrainingProg $applfTrp
 * @property SvcDocType $applfSvdt
 * @property TrainingType $applfTrt
 */
class ApplFinal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_final}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applf_prs_id', 'applf_name_first', 'applf_name_last', 'applf_name_full', 'applf_trp_hour', 'applf_cmd_date', 'applf_end_date', 'applf_number'], 'required'],
            [['applf_prs_id', 'applf_applr_id', 'applf_reestr', 'applf_applm_id', 'applf_svdt_id', 'applf_trt_id', 'applf_trp_id', 'applf_trp_hour'], 'integer'],
            [['applf_cmd_date', 'applf_end_date', 'applf_create_time', 'applf_update_time'], 'safe'],
            [['applf_file_data', 'applf_file0_data'], 'string'],
            [['applf_name_first', 'applf_name_last', 'applf_name_middle', 'applf_number', 'applf_create_user', 'applf_create_ip', 'applf_update_user', 'applf_update_ip'], 'string', 'max' => 64],
            [['applf_name_full', 'applf_file_name', 'applf_file0_name'], 'string', 'max' => 256],
            [['applf_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applf_trp_id' => 'trp_id']],
            [['applf_svdt_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applf_svdt_id' => 'svdt_id']],
            [['applf_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applf_trt_id' => 'trt_id']],
            [['applf_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['applf_pat_id' => 'pat_id']],
            [['applf_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applf_prs_id' => 'prs_id']],
            //[['applf_applr_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplRequest::class, 'targetAttribute' => ['applf_applr_id' => 'applr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applf_id' => Yii::t('app', 'Applf ID'),
            'applf_reestr' => Yii::t('app', 'Applf Reestr'),
            'applf_applr_id' => Yii::t('app', 'Applf Applr ID'),
            'applf_prs_id' => Yii::t('app', 'Applf Prs ID'),
            'applf_name_first' => Yii::t('app', 'Applf Name First'),
            'applf_name_last' => Yii::t('app', 'Applf Name Last'),
            'applf_name_middle' => Yii::t('app', 'Applf Name Middle'),
            'applf_name_full' => Yii::t('app', 'Applf Name Full'),
            'applf_svdt_id' => Yii::t('app', 'Applf Svdt ID'),
            'applf_trt_id' => Yii::t('app', 'Applf Trt ID'),
            'applf_trp_id' => Yii::t('app', 'Applf Trp ID'),
            'applf_trp_hour' => Yii::t('app', 'Applf Trp Hour'),
            'applf_cmd_date' => Yii::t('app', 'Applf Cmd Date'),
            'applf_end_date' => Yii::t('app', 'Applf End Date'),
            'applf_number' => Yii::t('app', 'Applf Number'),
            'applf_pat_id' => Yii::t('app', 'Applf Pat ID'),
            'applf_file_data' => Yii::t('app', 'Applf File Data'),
            'applf_file_name' => Yii::t('app', 'Applf File Name'),
            'applf_file0_data' => Yii::t('app', 'Applf File0 Data'),
            'applf_file0_name' => Yii::t('app', 'Applf File0 Name'),
            'applf_create_user' => Yii::t('app', 'Applf Create User'),
            'applf_create_time' => Yii::t('app', 'Applf Create Time'),
            'applf_create_ip' => Yii::t('app', 'Applf Create Ip'),
            'applf_update_user' => Yii::t('app', 'Applf Update User'),
            'applf_update_time' => Yii::t('app', 'Applf Update Time'),
            'applf_update_ip' => Yii::t('app', 'Applf Update Ip'),
            'applf_applm_id' => Yii::t('app', 'Applf Applm ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfApplr()
    {
        return $this->hasOne(ApplRequest::class, ['applr_id' => 'applf_applr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applf_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applf_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applf_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applf_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplfTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applf_trt_id']);
    }

    /**
     * @inheritdoc
     * @return ApplFinalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplFinalQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applf_create_user = Yii::$app->user->identity->username;
            $this->applf_create_ip = Yii::$app->request->userIP;
            $this->applf_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applf_update_user = Yii::$app->user->identity->username;
            $this->applf_update_ip = Yii::$app->request->userIP;
            $this->applf_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (
            $applr = ApplRequestContent::find()
                ->where(['applrc_applf_id' => $this->applf_id])
                ->one()
        ) {
            $applr->applrc_applf_id = null;
            return $applr->save();
        } else {
            return true;
        }
    }

}
