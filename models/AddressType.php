<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%address_type}}".
 *
 * @property integer $addt_id
 * @property string $addt_name
 * @property string $addt_create_user
 * @property string $addt_create_time
 * @property string $addt_create_ip
 * @property string $addt_update_user
 * @property string $addt_update_time
 * @property string $addt_update_ip
 *
 * @property Address[] $addresses
 */
class AddressType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%address_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['addt_name'], 'required'],
            [['addt_create_time', 'addt_update_time'], 'safe'],
            [['addt_name', 'addt_create_user', 'addt_create_ip', 'addt_update_user', 'addt_update_ip'], 'string', 'max' => 64],
            [['addt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'addt_id' => Yii::t('app', 'Addt ID'),
            'addt_name' => Yii::t('app', 'Addt Name'),
            'addt_create_user' => Yii::t('app', 'Create User'),
            'addt_create_time' => Yii::t('app', 'Create Time'),
            'addt_create_ip' => Yii::t('app', 'Create Ip'),
            'addt_update_user' => Yii::t('app', 'Update User'),
            'addt_update_time' => Yii::t('app', 'Update Time'),
            'addt_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['add_addt_id' => 'addt_id']);
    }

    /**
     * @inheritdoc
     * @return AddressTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddressTypeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->addt_create_user = Yii::$app->user->identity->username;
            $this->addt_create_ip = Yii::$app->request->userIP;
            $this->addt_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->addt_update_user = Yii::$app->user->identity->username;
            $this->addt_update_ip = Yii::$app->request->userIP;
            $this->addt_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
