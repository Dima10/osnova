<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "{{%address}}".
 *
 * @property integer $add_id
 * @property integer $add_ab_id
 * @property integer $add_active
 * @property integer $add_addt_id
 * @property integer $add_cou_id
 * @property integer $add_reg_id
 * @property integer $add_city_id
 * @property string $add_index
 * @property string $add_data
 * @property string $add_create_user
 * @property string $add_create_time
 * @property string $add_create_ip
 * @property string $add_update_user
 * @property string $add_update_time
 * @property string $add_update_ip
 *
 * @property Ab $addAb
 * @property Country $addCou
 * @property City $addCity
 * @property AddressType $addAddt
 * @property Region $addReg
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_ab_id', 'add_addt_id', 'add_reg_id', 'add_data'], 'required'],
            [['add_ab_id', 'add_addt_id', 'add_reg_id', 'add_city_id', 'add_cou_id', 'add_active'], 'integer'],
            [['add_create_time', 'add_update_time'], 'safe'],
            [['add_index'], 'string', 'max' => 16],
            [['add_data'], 'string', 'max' => 256],
            [['add_create_user', 'add_create_ip', 'add_update_user', 'add_update_ip'], 'string', 'max' => 64],
            [['add_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['add_ab_id' => 'ab_id']],
            [['add_cou_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['add_cou_id' => 'cou_id']],
            //[['add_city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['add_city_id' => 'city_id']],
            [['add_addt_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressType::class, 'targetAttribute' => ['add_addt_id' => 'addt_id']],
            [['add_reg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['add_reg_id' => 'reg_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'add_id' => Yii::t('app', 'Add ID'),
            'add_ab_id' => Yii::t('app', 'Add Ab ID'),
            'add_active' => Yii::t('app', 'Add Active'),
            'add_addt_id' => Yii::t('app', 'Add Addt ID'),
            'add_reg_id' => Yii::t('app', 'Add Reg ID'),
            'add_city_id' => Yii::t('app', 'Add City ID'),
            'add_cou_id' => Yii::t('app', 'Add Cou ID'),
            'add_index' => Yii::t('app', 'Add Index'),
            'add_data' => Yii::t('app', 'Add Data'),
            'add_create_user' => Yii::t('app', 'Create User'),
            'add_create_time' => Yii::t('app', 'Create Time'),
            'add_create_ip' => Yii::t('app', 'Create Ip'),
            'add_update_user' => Yii::t('app', 'Update User'),
            'add_update_time' => Yii::t('app', 'Update Time'),
            'add_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'add_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddCity()
    {
        return $this->hasOne(City::class, ['city_id' => 'add_city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddCou()
    {
        return $this->hasOne(Country::class, ['cou_id' => 'add_cou_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddAddt()
    {
        return $this->hasOne(AddressType::class, ['addt_id' => 'add_addt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddReg()
    {
        return $this->hasOne(Region::class, ['reg_id' => 'add_reg_id']);
    }

    /**
     * @inheritdoc
     * @return AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddressQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            if ($this->add_city_id == 0)
                $this->add_city_id = null;

            $this->add_create_user = Yii::$app->user->identity->username;
            $this->add_create_ip = Yii::$app->request->userIP;
            $this->add_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            if ($this->add_city_id == 0)
                $this->add_city_id = null;

            $this->add_update_user = Yii::$app->user->identity->username;
            $this->add_update_ip = Yii::$app->request->userIP;
            $this->add_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        try {
            Yii::$app->db->createCommand()
                ->update(Address::tableName(), ['add_active' => 0], "add_ab_id = {$this->add_ab_id} and add_id <> {$this->add_id}")
                ->execute();
        } catch (Exception $e) {
        }

    }
}
