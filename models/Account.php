<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\db\Command;
use yii\db\Exception;
use yii\db\Query;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $acc_id
 * @property integer $acc_ab_id
 * @property integer $acc_active
 * @property integer $acc_bank_id
 * @property string $acc_number
 * @property string $acc_create_user
 * @property string $acc_create_time
 * @property string $acc_create_ip
 * @property string $acc_update_user
 * @property string $acc_update_time
 * @property string $acc_update_ip
 *
 * @property Ab $accAb
 * @property Bank $accBank
 * @property Agreement[] $accAgreements
 * @property AgreementAct[] $accAgreementActs
 * @property AgreementAnnex[] $accAgreementAnnexes
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_ab_id', 'acc_bank_id', 'acc_number'], 'required'],
            [['acc_ab_id', 'acc_bank_id', 'acc_active'], 'integer'],
            [['acc_create_time', 'acc_update_time'], 'safe'],
            [['acc_number'], 'string', 'max' => 32],
            [['acc_create_user', 'acc_create_ip', 'acc_update_user', 'acc_update_ip'], 'string', 'max' => 64],
            [['acc_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['acc_ab_id' => 'ab_id']],
            [['acc_bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::class, 'targetAttribute' => ['acc_bank_id' => 'bank_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'acc_id' => Yii::t('app', 'Acc ID'),
            'acc_ab_id' => Yii::t('app', 'Acc Ab ID'),
            'acc_active' => Yii::t('app', 'Acc Active'),
            'acc_bank_id' => Yii::t('app', 'Acc Bank ID'),
            'acc_number' => Yii::t('app', 'Acc Number'),
            'acc_create_user' => Yii::t('app', 'Acc Create User'),
            'acc_create_time' => Yii::t('app', 'Acc Create Time'),
            'acc_create_ip' => Yii::t('app', 'Acc Create Ip'),
            'acc_update_user' => Yii::t('app', 'Acc Update User'),
            'acc_update_time' => Yii::t('app', 'Acc Update Time'),
            'acc_update_ip' => Yii::t('app', 'Acc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'acc_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccBank()
    {
        return $this->hasOne(Bank::class, ['bank_id' => 'acc_bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccAgreements()
    {
        return $this->hasMany(Agreement::class, ['agr_acc_id' => 'acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_acc_id' => 'acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_acc_id' => 'acc_id']);
    }

    /**
     * @inheritdoc
     * @return AccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->acc_create_user = Yii::$app->user->identity->username;
            $this->acc_create_ip = Yii::$app->request->userIP;
            $this->acc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->acc_update_user = Yii::$app->user->identity->username;
            $this->acc_update_ip = Yii::$app->request->userIP;
            $this->acc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        try {
            Yii::$app->db->createCommand()
                ->update(Account::tableName(), ['acc_active' => 0], "acc_ab_id = {$this->acc_ab_id} and acc_id <> {$this->acc_id}")
                ->execute();
        } catch (Exception $e) {
        }

    }
}
