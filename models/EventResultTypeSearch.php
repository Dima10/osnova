<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventResultType;

/**
 * EventResultTypeSearch represents the model behind the search form about `app\models\EventResultType`.
 */
class EventResultTypeSearch extends EventResultType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evrt_id'], 'integer'],
            [['evrt_name', 'evrt_note', 'evrt_create_user', 'evrt_create_time', 'evrt_create_ip', 'evrt_update_user', 'evrt_update_time', 'evrt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventResultType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'evrt_id' => $this->evrt_id,
            'evrt_create_time' => $this->evrt_create_time,
            'evrt_update_time' => $this->evrt_update_time,
        ]);

        $query->andFilterWhere(['like', 'evrt_name', $this->evrt_name])
            ->andFilterWhere(['like', 'evrt_note', $this->evrt_note])
            ->andFilterWhere(['like', 'evrt_create_user', $this->evrt_create_user])
            ->andFilterWhere(['like', 'evrt_create_ip', $this->evrt_create_ip])
            ->andFilterWhere(['like', 'evrt_update_user', $this->evrt_update_user])
            ->andFilterWhere(['like', 'evrt_update_ip', $this->evrt_update_ip]);

        return $dataProvider;
    }
}
