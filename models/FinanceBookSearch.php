<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceBook;

/**
 * FinanceBookSearch represents the model behind the search form about `app\models\FinanceBook`.
 */
class FinanceBookSearch extends FinanceBook
{
    public $ab_name;
    public $aga_number;
    public $svc_name;
    public $acc_number;
    public $pt_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fb_id', 'fb_svc_id', 'fb_ab_id', 'fb_comp_id', 'fb_fbt_id', 'fb_aga_id', 'fb_acc_id', 'fb_fbs_id'], 'integer'],
            [['fb_sum', 'fb_tax', 'fb_pay'], 'number'],

            [['ab_name', 'aga_number', 'svc_name'], 'string'],

            [['fb_date', 'fb_payment', 'fb_comment', 'fb_create_user', 'fb_create_ip', 'fb_create_time', 'fb_update_user', 'fb_update_ip', 'fb_update_time'], 'safe'],
            [['acc_number'], 'safe'],
            [['pt_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {

        if (isset($id)) {
            $query = FinanceBook::find()
                ->leftJoin(Svc::tableName(), 'fb_svc_id = svc_id')
                ->leftJoin(Ab::tableName(), 'fb_ab_id = ab_id')
                ->leftJoin(AgreementAcc::tableName(), 'fb_aga_id = aga_id')
                ->leftJoin(FinanceBookType::tableName(), 'fbt_id = fb_fbt_id')
                ->leftJoin(Company::tableName(), 'comp_id = fb_comp_id')
                ->leftJoin(FinanceBookStatus::tableName(), 'fbs_id = fb_fbs_id')
                ->joinWith(['fbAcc' => function($query) { $query->from(['fbAcc' => 'account']);}])
                ->joinWith(['fbPt' => function($query) { $query->from(['fbPt' => 'payment_type']);}])
                ->where(['fb_ab_id' => $id])
            ;

        } else {
            $query = FinanceBook::find()
                ->leftJoin(Svc::tableName(), 'fb_svc_id = svc_id')
                ->leftJoin(Ab::tableName(), 'fb_ab_id = ab_id')
                ->leftJoin(AgreementAcc::tableName(), 'fb_aga_id = aga_id')
                ->leftJoin(FinanceBookType::tableName(), 'fbt_id = fb_fbt_id')
                ->leftJoin(Company::tableName(), 'comp_id = fb_comp_id')
                ->leftJoin(FinanceBookStatus::tableName(), 'fbs_id = fb_fbs_id')
                ->joinWith(['fbAcc' => function($query) { $query->from(['fbAcc' => 'account']);}])
                ->joinWith(['fbPt' => function($query) { $query->from(['fbPt' => 'payment_type']);}])
            ;

        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['ab_name'] = [
            'asc' => ['ab_name' => SORT_ASC],
            'desc' => ['ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['aga_number'] = [
            'asc' => ['aga_number' => SORT_ASC],
            'desc' => ['aga_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svc_name'] = [
            'asc' => ['svc_name' => SORT_ASC],
            'desc' => ['svc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['acc_number'] = [
            'asc' => ['acc_number' => SORT_ASC],
            'desc' => ['acc_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['pt_id'] = [
            'asc' => ['pt_id' => SORT_ASC],
            'desc' => ['pt_id' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fb_id' => $this->fb_id,
            'fb_svc_id' => $this->fb_svc_id,
            'fb_fbt_id' => $this->fb_fbt_id,
            'fb_ab_id' => $this->fb_ab_id,
            'fb_comp_id' => $this->fb_comp_id,
            'fb_aca_id' => $this->fb_aca_id,
            'fb_aga_id' => $this->fb_aga_id,
            'fb_acc_id' => $this->fb_acc_id,
            'fb_fbs_id' => $this->fb_fbs_id,
            'fb_pt_id' => $this->fb_pt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'aga_number', $this->aga_number])

            ->andFilterWhere(['like', 'fbAcc.acc_number', $this->acc_number])
            ->andFilterWhere(['like', 'fbPt.pt_id', $this->pt_id])

            ->andFilterWhere(['like', 'fb_sum', $this->fb_sum])
            ->andFilterWhere(['like', 'fb_tax', $this->fb_tax])
            ->andFilterWhere(['like', 'fb_pay', $this->fb_pay])
            ->andFilterWhere(['like', 'fb_date', $this->fb_date])
            ->andFilterWhere(['like', 'fb_payment', $this->fb_payment])
            ->andFilterWhere(['like', 'fb_comment', $this->fb_comment])
            ->andFilterWhere(['like', 'fb_create_user', $this->fb_create_user])
            ->andFilterWhere(['like', 'fb_create_time', $this->fb_create_time])
            ->andFilterWhere(['like', 'fb_create_ip', $this->fb_create_ip])
            ->andFilterWhere(['like', 'fb_update_user', $this->fb_update_user])
            ->andFilterWhere(['like', 'fb_update_time', $this->fb_update_time])
            ->andFilterWhere(['like', 'fb_update_ip', $this->fb_update_ip])
        ;

        return $dataProvider;
    }
}
