<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SvcProg;

/**
 * SvcProgSearch represents the model behind the search form about `app\models\SvcProg`.
 */
class SvcProgSearch extends SvcProg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sprog_id', 'sprog_svc_id', 'sprog_trp_id'], 'integer'],
            [['sprog_create_user', 'sprog_create_time', 'sprog_create_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcProg::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sprog_id' => $this->sprog_id,
            'sprog_svc_id' => $this->sprog_svc_id,
            'sprog_trp_id' => $this->sprog_trp_id,
        ]);

        $query
            ->andFilterWhere(['like', 'sprog_create_user', $this->sprog_create_user])
            ->andFilterWhere(['like', 'sprog_create_time', $this->sprog_create_time])
            ->andFilterWhere(['like', 'sprog_create_ip', $this->sprog_create_ip])
            ;

        return $dataProvider;
    }
}
