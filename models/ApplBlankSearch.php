<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplBlank;

/**
 * ApplBlankSearch represents the model behind the search form about `app\models\ApplBlank`.
 */
class ApplBlankSearch extends ApplBlank
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applblk_id', 'applblk_qty_1', 'applblk_qty_2', 'applblk_qty_3', 'applblk_qty_5'], 'integer'],
            [['applblk_number', 'applblk_date', 'applblk_qty_4', 'applblk_file_name', 'applblk_file_data', 'applblk_create_user', 'applblk_create_time', 'applblk_create_ip', 'applblk_update_user', 'applblk_update_time', 'applblk_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplBlank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['applblk_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applblk_id' => $this->applblk_id,
//            'applblk_date' => $this->applblk_date,
            'applblk_qty_1' => $this->applblk_qty_1,
            'applblk_qty_2' => $this->applblk_qty_2,
            'applblk_qty_3' => $this->applblk_qty_3,
            'applblk_qty_5' => $this->applblk_qty_5,
        ]);

        $query
            ->andFilterWhere(['like', 'applblk_date', $this->applblk_date])
            ->andFilterWhere(['like', 'applblk_update_time', $this->applblk_update_time])
            ->andFilterWhere(['like', 'applblk_create_time', $this->applblk_create_time])
            ->andFilterWhere(['like', 'applblk_number', $this->applblk_number])
            ->andFilterWhere(['like', 'applblk_qty_4', $this->applblk_qty_4])
            ->andFilterWhere(['like', 'applblk_file_name', $this->applblk_file_name])
            ->andFilterWhere(['like', 'applblk_file_data', $this->applblk_file_data])
            ->andFilterWhere(['like', 'applblk_create_user', $this->applblk_create_user])
            ->andFilterWhere(['like', 'applblk_create_ip', $this->applblk_create_ip])
            ->andFilterWhere(['like', 'applblk_update_user', $this->applblk_update_user])
            ->andFilterWhere(['like', 'applblk_update_ip', $this->applblk_update_ip])
        ;

        return $dataProvider;
    }
}
