<?php

namespace app\models;

use app\models\AgreementAcc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ReportFinancesSearch extends AgreementAccA
{

    public $id;
    public $ent_name;
    public $agra_number;
    public $sum;
    public $aca_id;
    public $entc_id;
    public $svc_svdt_id;
    public $fb_fbt_id;
    public $fb_pt_id;
    public $svc_name;
    public $trp_name;
    public $price;
    public $qty;
    public $aca_sum;
    public $ana_cost_a;
    public $fb_payment;
    public $fb_date;
    public $aga_number;
    public $aga_date;
    public $aga_ast_id;
    public $fb_sum;
    public $sum_ss;
    public $marzha;
    public $agent_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'aca_id', 'entc_id', 'svc_svdt_id', 'fb_fbt_id', 'fb_pt_id', 'aga_ast_id'], 'integer'],
            [['aga_number', 'aga_date', 'fb_payment'], 'safe'],
            [['ent_name', 'agent_name', 'agra_number', 'svc_name', 'trp_name', 'fb_date'], 'string'],
            [['price', 'marzha', 'sum_ss', 'qty', 'sum', 'fb_sum', 'ana_cost_a'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new \yii\db\Query())->select('*')
            ->from(FinanceReport::tableName())
        ;

        $dataProvider = new ActiveDataProvider([
           'query' => $query,
           'pagination' => [
               'pageSize' => 50,
           ],
       ]);

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['id' => SORT_ASC],
            'desc' => ['id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ent_name'] = [
            'asc' => ['ent_name' => SORT_ASC],
            'desc' => ['ent_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agent_name'] = [
            'asc' => ['agent_name' => SORT_ASC],
            'desc' => ['agent_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['trp_name' => SORT_ASC],
            'desc' => ['trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svc_name'] = [
            'asc' => ['svc_name' => SORT_ASC],
            'desc' => ['svc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agra_number'] = [
            'asc' => ['agra_number' => SORT_ASC],
            'desc' => ['agra_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['price'] = [
            'asc' => ['price' => SORT_ASC],
            'desc' => ['price' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['marzha'] = [
            'asc' => ['marzha' => SORT_ASC],
            'desc' => ['marzha' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sum_ss'] = [
            'asc' => ['sum_ss' => SORT_ASC],
            'desc' => ['sum_ss' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['qty'] = [
            'asc' => ['qty' => SORT_ASC],
            'desc' => ['qty' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['sum'] = [
            'asc' => ['sum' => SORT_ASC],
            'desc' => ['sum' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ana_cost_a'] = [
            'asc' => ['ana_cost_a' => SORT_ASC],
            'desc' => ['ana_cost_a' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['aga_number'] = [
            'asc' => ['aga_number' => SORT_ASC],
            'desc' => ['aga_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['aga_date'] = [
            'asc' => ['aga_date' => SORT_ASC],
            'desc' => ['aga_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['fb_payment'] = [
            'asc' => ['fb_payment' => SORT_ASC],
            'desc' => ['fb_payment' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['fb_date'] = [
            'asc' => ['fb_date' => SORT_ASC],
            'desc' => ['fb_date' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
           'id' => $this->id,
           'fb_pt_id' => $this->fb_pt_id,
           'fb_fbt_id' => $this->fb_fbt_id,
           'entc_id' => $this->entc_id,
           'svc_svdt_id' => $this->svc_svdt_id,
           'aga_ast_id' => $this->aga_ast_id,
       ]);

        $query
            ->andFilterWhere(['like', 'ent_name', $this->ent_name])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'agra_number', $this->agra_number])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'marzha', $this->marzha])
            ->andFilterWhere(['like', 'sum_ss', $this->sum_ss])
            ->andFilterWhere(['like', 'qty', $this->qty])
            ->andFilterWhere(['like', 'sum', $this->sum])
//            ->andFilterWhere(['like', 'ana_cost_a', $this->ana_cost_a])
//            ->andFilterWhere(['like', 'aga_number', $this->aga_number])
//            ->andFilterWhere(['like', 'aga_date', $this->aga_date])
//            ->andFilterWhere(['like', 'fb_payment', $this->fb_payment])
//            ->andFilterWhere(['like', 'fb_date', $this->fb_date])
        ;

        if ($this->ana_cost_a == 'не задано') {
            $query->andWhere(['is', 'ana_cost_a', null]);
        } else {
            $query->andFilterWhere(['like', 'ana_cost_a', $this->ana_cost_a]);
        }

        if ($this->aga_number == 'не задано') {
            $query->andWhere(['is', 'aga_number', null]);
        } else {
            $query->andFilterWhere(['like', 'aga_number', $this->aga_number]);
        }

        if ($this->aga_date == 'не задано') {
            $query->andWhere(['is', 'aga_date', null]);
        } else {
            $query->andFilterWhere(['like', 'aga_date', $this->aga_date]);
        }

        if ($this->fb_date == 'не задано') {
            $query->andWhere(['is', 'fb_date', null]);
        } else {
            $query->andFilterWhere(['like', 'fb_date', $this->fb_date]);
        }

        if ($this->fb_payment == 'не задано') {
            $query->andWhere(['is', 'fb_payment', null]);
        } else {
            $query->andFilterWhere(['like', 'fb_payment', $this->fb_payment]);
        }

        return $dataProvider;
    }

    public function getQueryBySearch($params)
    {
        $query = (new \yii\db\Query())->select('*')
            ->from(FinanceReport::tableName())
        ;

        $this->load($params);

        $query->andFilterWhere([
                                   'id' => $this->id,
                                   'fb_pt_id' => $this->fb_pt_id,
                                   'fb_fbt_id' => $this->fb_fbt_id,
                                   'entc_id' => $this->entc_id,
                                   'svc_svdt_id' => $this->svc_svdt_id,
                                   'aga_ast_id' => $this->aga_ast_id,
                               ]);

        $query
            ->andFilterWhere(['like', 'ent_name', $this->ent_name])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'agra_number', $this->agra_number])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'marzha', $this->marzha])
            ->andFilterWhere(['like', 'sum_ss', $this->sum_ss])
            ->andFilterWhere(['like', 'qty', $this->qty])
            ->andFilterWhere(['like', 'sum', $this->sum])
//            ->andFilterWhere(['like', 'ana_cost_a', $this->ana_cost_a])
//            ->andFilterWhere(['like', 'aga_number', $this->aga_number])
//            ->andFilterWhere(['like', 'aga_date', $this->aga_date])
//            ->andFilterWhere(['like', 'fb_payment', $this->fb_payment])
//            ->andFilterWhere(['like', 'fb_date', $this->fb_date])
        ;

        if ($this->ana_cost_a == 'не задано') {
            $query->andWhere(['is', 'ana_cost_a', null]);
        } else {
            $query->andFilterWhere(['like', 'ana_cost_a', $this->ana_cost_a]);
        }

        if ($this->aga_number == 'не задано') {
            $query->andWhere(['is', 'aga_number', null]);
        } else {
            $query->andFilterWhere(['like', 'aga_number', $this->aga_number]);
        }

        if ($this->aga_date == 'не задано') {
            $query->andWhere(['is', 'aga_date', null]);
        } else {
            $query->andFilterWhere(['like', 'aga_date', $this->aga_date]);
        }

        if ($this->fb_date == 'не задано') {
            $query->andWhere(['is', 'fb_date', null]);
        } else {
            $query->andFilterWhere(['like', 'fb_date', $this->fb_date]);
        }

        if ($this->fb_payment == 'не задано') {
            $query->andWhere(['is', 'fb_payment', null]);
        } else {
            $query->andFilterWhere(['like', 'fb_payment', $this->fb_payment]);
        }

        return $query;
    }

    public function attributeLabels()
    {
        return [
            'ent_name' => 'Юр Лицо',
            'entc_id' => 'Тип юрлица',
            'fb_fbt_id' => 'Тип',
            'svc_name' => 'Наименование услуги',
            'trp_name' => 'Наименование программы',
            'svc_svdt_id' => 'Вид выдаваемого документа',
            'agra_number' => '№ приложения',
            'price' => 'Цена',
            'qty' => 'Кол-во',
            'sum' => 'Сумма',
            'ana_cost_a' => 'Себестоимость',
            'aga_number' => '№ счета',
            'aga_date' => 'Дата счета',
            'aga_ast_id' => 'Статус счета',
            'fb_pt_id' => 'Тип оплаты',
            'fb_payment' => '№ платежного поручения',
            'fb_date' => 'Дата платежного поручения/дата оплаты',
            'sum_ss' => 'Сумма с/c',
            'marzha' => 'Маржа',
            'agent_name' => 'Менеджер',
        ];
    }
}
