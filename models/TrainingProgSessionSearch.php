<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingProgSession;

/**
 * TrainingProgSessionSearch represents the model behind the search form about `app\models\TrainingProgSession`.
 */
class TrainingProgSessionSearch extends TrainingProgSession
{
    public $trp_name;
    public $prs_full_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tps_id', 'tps_trp_id', 'tps_prs_id'], 'integer'],
            [['tps_session_id', 'tps_create_user', 'tps_create_time', 'tps_create_ip', 'trp_name', 'prs_full_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingProgSession::find();
        $query->joinWith(['tpsTrp', 'tpsPrs']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['training_prog.trp_name' => SORT_ASC],
            'desc' => ['training_prog.trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_full_name'] = [
            'asc' => ['person.prs_full_name' => SORT_ASC],
            'desc' => ['person.prs_full_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tps_id' => $this->tps_id,
        ]);

        $query->andFilterWhere(['like', 'tps_session_id', $this->tps_session_id])
            ->andFilterWhere(['like', 'tps_create_user', $this->tps_create_user])
            ->andFilterWhere(['like', 'tps_create_time', $this->tps_create_time])
            ->andFilterWhere(['like', 'tps_create_ip', $this->tps_create_ip])
            ->andFilterWhere(['like', 'training_prog.trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'person.prs_full_name', $this->prs_full_name]);
        return $dataProvider;
    }
}
