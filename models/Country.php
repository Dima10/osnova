<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%country}}".
 *
 * @property integer $cou_id
 * @property string $cou_name
 * @property string $cou_create_user
 * @property string $cou_create_time
 * @property string $cou_create_ip
 * @property string $cou_update_user
 * @property string $cou_update_time
 * @property string $cou_update_ip
 *
 * @property Region[] $regions
 * @property City[] $cities
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cou_name'], 'required'],
            [['cou_create_time', 'cou_update_time'], 'safe'],
            [['cou_name'], 'string', 'max' => 128],
            [['cou_create_user', 'cou_create_ip', 'cou_update_user', 'cou_update_ip'], 'string', 'max' => 64],
            [['cou_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cou_id' => Yii::t('app', 'Cou ID'),
            'cou_name' => Yii::t('app', 'Cou Name'),
            'cou_create_user' => Yii::t('app', 'Create User'),
            'cou_create_time' => Yii::t('app', 'Create Time'),
            'cou_create_ip' => Yii::t('app', 'Create Ip'),
            'cou_update_user' => Yii::t('app', 'Update User'),
            'cou_update_time' => Yii::t('app', 'Update Time'),
            'cou_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::class, ['reg_cou_id' => 'cou_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['city_cou_id' => 'cou_id']);
    }

    /**
     * @inheritdoc
     * @return CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CountryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->cou_create_user = Yii::$app->user->identity->username;
            $this->cou_create_ip = Yii::$app->request->userIP;
            $this->cou_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->cou_update_user = Yii::$app->user->identity->username;
            $this->cou_update_ip = Yii::$app->request->userIP;
            $this->cou_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
