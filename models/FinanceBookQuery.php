<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FinanceBook]].
 *
 * @see FinanceBook
 */
class FinanceBookQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return FinanceBook[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FinanceBook|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
