<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplRequestContent;

/**
 * ApplRequestContentSearch represents the model behind the search form about `app\models\ApplRequestContent`.
 */
class ApplRequestContentSearch extends ApplRequestContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applrc_id', 'applrc_applr_id', 'applrc_prs_id', 'applrc_svc_id'], 'integer'],
            [['applrc_date_upk'], 'string'],
            [['applrc_create_user', 'applrc_create_time', 'applrc_create_ip', 'applrc_update_user', 'applrc_update_time', 'applrc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = 0)
    {
        $query = ApplRequestContent::find();

        if (isset($params['applr_id']))
            $query = $query->where(['applrc_applr_id' => $params['applr_id']]);

        if ($id > 0) {
            $query = ApplRequestContent::find()
                ->leftJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
                ->where(['applr_ab_id' => $id])
                ->orderBy('applr_date');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applrc_id' => $this->applrc_id,
            'applrc_applr_id' => $this->applrc_applr_id,
            'applrc_prs_id' => $this->applrc_prs_id,
            'applrc_svc_id' => $this->applrc_svc_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applrc_date_upk', $this->applrc_date_upk])
            ->andFilterWhere(['like', 'applrc_create_user', $this->applrc_create_user])
            ->andFilterWhere(['like', 'applrc_create_user', $this->applrc_create_user])
            ->andFilterWhere(['like', 'applrc_create_time', $this->applrc_create_time])
            ->andFilterWhere(['like', 'applrc_create_ip', $this->applrc_create_ip])
            ->andFilterWhere(['like', 'applrc_update_user', $this->applrc_update_user])
            ->andFilterWhere(['like', 'applrc_update_time', $this->applrc_update_time])
            ->andFilterWhere(['like', 'applrc_update_ip', $this->applrc_update_ip])
            ;

        return $dataProvider;
    }
}
