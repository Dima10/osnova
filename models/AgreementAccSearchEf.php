<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAcc;

/**
 * AgreementAccSearch represents the model behind the search form about `app\models\AgreementAcc`.
 */
class AgreementAccSearchEf extends AgreementAcc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aga_id', 'aga_ab_id', 'aga_comp_id', 'aga_agr_id', 'aga_agra_id', 'aga_pat_id', 'aga_ast_id'], 'integer'],
            [['aga_number', 'aga_date', 'aga_comment', 'aga_create_user', 'aga_create_time', 'aga_create_ip', 'aga_update_user', 'aga_update_time', 'aga_update_ip'], 'safe'],
            [['aga_sum', 'aga_tax'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (isset($id)) {
            $query = AgreementAcc::find()->where(['aga_ab_id' => $id]);
        } else {
            $query = AgreementAcc::find();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => ['aga_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'aga_id' => $this->aga_id,
            'aga_ab_id' => $this->aga_ab_id,
            'aga_comp_id' => $this->aga_comp_id,
            'aga_agr_id' => $this->aga_agr_id,
            'aga_agra_id' => $this->aga_agra_id,
            'aga_pat_id' => $this->aga_pat_id,
            'aga_ast_id' => $this->aga_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'aga_number', $this->aga_number])
            ->andFilterWhere(['like', 'aga_date', $this->aga_date])
            ->andFilterWhere(['like', 'aga_sum', $this->aga_sum])
            ->andFilterWhere(['like', 'aga_tax', $this->aga_tax])
            ->andFilterWhere(['like', 'aga_comment', $this->aga_comment])
            ->andFilterWhere(['like', 'aga_fdata', $this->aga_fdata])
            ->andFilterWhere(['like', 'aga_fdata_sign', $this->aga_fdata_sign])
            ->andFilterWhere(['like', 'aga_create_user', $this->aga_create_user])
            ->andFilterWhere(['like', 'aga_create_time', $this->aga_create_time])
            ->andFilterWhere(['like', 'aga_create_ip', $this->aga_create_ip])
            ->andFilterWhere(['like', 'aga_update_user', $this->aga_update_user])
            ->andFilterWhere(['like', 'aga_update_time', $this->aga_update_time])
            ->andFilterWhere(['like', 'aga_update_ip', $this->aga_update_ip])
        ;

        return $dataProvider;
    }
}
