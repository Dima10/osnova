<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_report_date_marker".
 *
 * @property int $id
 * @property string $marker
 */
class FinanceReportDateMarker extends \yii\db\ActiveRecord
{

    const DATE_MARKER_ID = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_report_date_marker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['marker'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marker' => 'Marker',
        ];
    }

    public function getDateMarker(){
        return FinanceReportDateMarker::findOne(self::DATE_MARKER_ID);
    }

    public function setCurrentDateMarker()
    {
        $dateMarker = FinanceReportDateMarker::findOne(self::DATE_MARKER_ID);
        $dateTime =  new \DateTime();
        $dateMarker->marker = $dateTime->format('Y-m-d H:i:s');
        $dateMarker->save();
    }

    public function expired($hours = 2)
    {
        $dateCurrentTime =  new \DateTime('NOW');

        $dateMarker = $this->getDateMarker();
        $dateMarkerTime =  new \DateTime($dateMarker->marker);

        $interval = $dateCurrentTime->diff($dateMarkerTime);

        $intervalInSeconds = (new \DateTime())->setTimeStamp(0)->add($interval)->getTimeStamp();
        $intervalInMinutes = $intervalInSeconds/60;
        $intervalInHours = $intervalInMinutes/60;

        return abs($intervalInHours) > $hours;
    }
}
