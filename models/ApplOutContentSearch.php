<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplOutContent;

/**
 * ApplOutContentSearch represents the model behind the search form about `app\models\ApplOutContent`.
 */
class ApplOutContentSearch extends ApplOutContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apploutc_id', 'apploutc_applout_id', 'apploutc_prs_id', 'apploutc_trp_id', 'apploutc_ab_id', 'apploutc_applcmd_id', 'apploutc_applsx_id', 'apploutc_apple_id'], 'integer'],
            [['apploutc_number_upk', 'apploutc_create_user', 'apploutc_create_time', 'apploutc_create_ip', 'apploutc_update_user', 'apploutc_update_time', 'apploutc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplOutContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'apploutc_id' => $this->apploutc_id,
            'apploutc_applout_id' => $this->apploutc_applout_id,
            'apploutc_prs_id' => $this->apploutc_prs_id,
            'apploutc_trp_id' => $this->apploutc_trp_id,
            'apploutc_ab_id' => $this->apploutc_ab_id,
            'apploutc_applcmd_id' => $this->apploutc_applcmd_id,
            'apploutc_applsx_id' => $this->apploutc_applsx_id,
            'apploutc_apple_id' => $this->apploutc_apple_id,
            'apploutc_create_time' => $this->apploutc_create_time,
            'apploutc_update_time' => $this->apploutc_update_time,
        ]);

        $query->andFilterWhere(['like', 'apploutc_number_upk', $this->apploutc_number_upk])
            ->andFilterWhere(['like', 'apploutc_create_user', $this->apploutc_create_user])
            ->andFilterWhere(['like', 'apploutc_create_ip', $this->apploutc_create_ip])
            ->andFilterWhere(['like', 'apploutc_update_user', $this->apploutc_update_user])
            ->andFilterWhere(['like', 'apploutc_update_ip', $this->apploutc_update_ip]);

        return $dataProvider;
    }
}
