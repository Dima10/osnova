<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementStatus;

/**
 * AgreementStatusSearch represents the model behind the search form about `app\models\AgreementStatus`.
 */
class AgreementStatusSearch extends AgreementStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ast_id'], 'integer'],
            [['ast_name', 'ast_create_time', 'ast_create_ip', 'ast_update_user', 'ast_update_time', 'ast_update_ip', 'ast_create_user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgreementStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ast_id' => $this->ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'ast_name', $this->ast_name])
            ->andFilterWhere(['like', 'ast_create_time', $this->ast_create_time])
            ->andFilterWhere(['like', 'ast_create_ip', $this->ast_create_ip])
            ->andFilterWhere(['like', 'ast_update_time', $this->ast_update_time])
            ->andFilterWhere(['like', 'ast_update_user', $this->ast_update_user])
            ->andFilterWhere(['like', 'ast_update_ip', $this->ast_update_ip])
            ->andFilterWhere(['like', 'ast_create_user', $this->ast_create_user])
       ;

        return $dataProvider;
    }
}
