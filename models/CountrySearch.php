<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Country;

/**
 * CountrySearch represents the model behind the search form about `app\models\Country`.
 */
class CountrySearch extends Country
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cou_id'], 'integer'],
            [['cou_name', 'cou_create_user', 'cou_create_time', 'cou_create_ip', 'cou_update_user', 'cou_update_time', 'cou_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cou_id' => $this->cou_id
        ]);

        $query->andFilterWhere(['like', 'cou_name', $this->cou_name])
            ->andFilterWhere(['like', 'cou_create_user', $this->cou_create_user])
            ->andFilterWhere(['like', 'cou_create_ip', $this->cou_create_ip])
            ->andFilterWhere(['like', 'cou_update_user', $this->cou_update_user])
            ->andFilterWhere(['like', 'cou_update_ip', $this->cou_update_ip]);

        if($this->cou_create_time != ''){
            $query->andFilterWhere(['like', 'cou_create_time', $this->cou_create_time]);
        }

        if($this->cou_update_time != ''){
            $query->andFilterWhere(['like', 'cou_update_time', $this->cou_update_time]);
        }

        return $dataProvider;
    }
}
