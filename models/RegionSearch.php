<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Region;

/**
 * RegionSearch represents the model behind the search form about `app\models\Region`.
 */
class RegionSearch extends Region
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_id', 'reg_cou_id'], 'integer'],
            [['reg_name', 'reg_create_user', 'reg_create_time', 'reg_create_ip', 'reg_update_user', 'reg_update_time', 'reg_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Region::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'reg_id' => $this->reg_id,
            'reg_cou_id' => $this->reg_cou_id,
        ]);

        $query->andFilterWhere(['like', 'reg_name', $this->reg_name])
            ->andFilterWhere(['like', 'reg_create_user', $this->reg_create_user])
            ->andFilterWhere(['like', 'reg_create_ip', $this->reg_create_ip])
            ->andFilterWhere(['like', 'reg_update_user', $this->reg_update_user])
            ->andFilterWhere(['like', 'reg_create_time', $this->reg_create_time])
            ->andFilterWhere(['like', 'reg_update_time', $this->reg_update_time])
            ->andFilterWhere(['like', 'reg_update_ip', $this->reg_update_ip]);

        return $dataProvider;
    }
}
