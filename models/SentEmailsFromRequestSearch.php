<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "sent_emails_from_request".
 *
 * @property int $id
 * @property int $applr_id
 * @property int $applrc_prs_id
 * @property string $fio
 * @property string $login
 * @property int $applrc_trp_id
 * @property int $applr_ab_id
 * @property int $applr_manager_id
 * @property string $recipient_email
 * @property string $sender_email
 * @property int $status
 * @property string $send_date
 * @property int $applr_reestr
 * @property int $applr_flag
 * @property string $portal
 */
class SentEmailsFromRequestSearch extends SentEmailsFromRequest
{

    public $applr_id;
    public $applrc_prs_id;
    public $fio;
    public $login;
    public $recipient_email;
    public $sender_email;
    public $send_date;
    public $portal;
    public $status;
    public $applr_flag;
    public $applr_reestr;
    public $trp_name;
    public $ab_id;
    public $ab_name;
    public $manager_ab_name;
    public $appls_sh_passed;
    public $appl_sh_date_end;
    public $appls_sheet_passed;
    public $appl_sheet_date_end;
    public $applf_end_date;
    public $applf_file_name;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['applr_id', 'applrc_prs_id', 'applrc_trp_id', 'applr_ab_id', 'applr_manager_id', 'status', 'applr_reestr', 'applr_flag'], 'integer'],
//            [['send_date', 'portal'], 'required'],
            [['send_date', 'portal'], 'safe'],
            [['fio', 'login', 'recipient_email', 'sender_email', 'trp_name', 'ab_id', 'ab_name', 'manager_ab_name', 'appls_sh_passed', 'appl_sh_date_end', 'appls_sheet_passed', 'appl_sheet_date_end', 'applf_file_name', 'applf_end_date'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SentEmailsFromRequest::find()
            ->leftJoin(ApplSh::tableName(). ' as sh', '(sh.appls_prs_id = applrc_prs_id AND sh.appls_trp_id = applrc_trp_id)' )
            ->leftJoin(ApplSheet::tableName(). ' as sheet', '(sheet.appls_prs_id = applrc_prs_id AND sheet.appls_trp_id = applrc_trp_id)' )
            ->leftJoin(TrainingProg::tableName(), 'applrc_trp_id = trp_id')
            ->leftJoin(Ab::tableName().' as ab1', 'applr_ab_id = ab1.ab_id')
            ->leftJoin(Ab::tableName().' as ab2', 'applr_manager_id = ab2.ab_id')
            ->leftJoin(ApplFinal::tableName(), '(applf_prs_id = applrc_prs_id AND applf_trp_id = applrc_trp_id)')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['training_prog.trp_name' => SORT_ASC],
            'desc' => ['training_prog.trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ab_id'] = [
            'asc' => ['ab1.ab_id' => SORT_ASC],
            'desc' => ['ab1.ab_id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ab_name'] = [
            'asc' => ['ab1.ab_name' => SORT_ASC],
            'desc' => ['ab1.ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['manager_ab_name'] = [
            'asc' => ['ab2.ab_name' => SORT_ASC],
            'desc' => ['ab2.ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['appls_sh_passed'] = [
            'asc' => ['sh.appls_passed' => SORT_ASC],
            'desc' => ['sh.appls_passed' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['appl_sh_date_end'] = [
            'asc' => ['sh.appls_date' => SORT_ASC],
            'desc' => ['sh.appls_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['appls_sheet_passed'] = [
            'asc' => ['sheet.appls_passed' => SORT_ASC],
            'desc' => ['sheet.appls_passed' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['appl_sheet_date_end'] = [
            'asc' => ['sheet.appls_date' => SORT_ASC],
            'desc' => ['sheet.appls_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applf_file_name'] = [
            'asc' => ['appl_final.applf_file_name' => SORT_ASC],
            'desc' => ['appl_final.applf_file_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applf_end_date'] = [
            'asc' => ['appl_final.applf_end_date' => SORT_ASC],
            'desc' => ['appl_final.applf_end_date' => SORT_DESC],
        ];

        $this->load($params);

        $reestr = $this->applr_reestr;
        if(User::isSpecAdmin()) {
            $reestr = 0;
        }

//        $query->andFilterWhere([
//            'applr_reestr' => $reestr
//        ]);


        $query
            ->andFilterWhere(['=', 'applr_id', $this->applr_id])
            ->andFilterWhere(['=', 'applrc_prs_id', $this->applrc_prs_id])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['=', 'ab1.ab_id', $this->ab_id])
            ->andFilterWhere(['like', 'ab1.ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'ab2.ab_name', $this->manager_ab_name])
            ->andFilterWhere(['=', 'sh.appls_passed', $this->appls_sh_passed])
            ->andFilterWhere(['like', 'sh.appls_date', $this->appl_sh_date_end])
            ->andFilterWhere(['=', 'sheet.appls_passed', $this->appls_sheet_passed])
            ->andFilterWhere(['like', 'sheet.appls_date', $this->appl_sheet_date_end])
            ->andFilterWhere(['like', 'applf_end_date', $this->applf_end_date])
            ->andFilterWhere(['like', 'recipient_email', $this->recipient_email])
            ->andFilterWhere(['like', 'sender_email', $this->sender_email])
            ->andFilterWhere(['like', 'send_date', $this->send_date])
            ->andFilterWhere(['like', 'portal', $this->portal])
            ->andFilterWhere(['=', 'status', $this->status])
            ->andFilterWhere(['=', 'applr_flag', $this->applr_flag])
            ->andFilterWhere(['=', 'applr_reestr', $reestr]);

        if(isset($params['SentEmailsFromRequestSearch']['applf_file_name']) && $params['SentEmailsFromRequestSearch']['applf_file_name'] === ''){
        }
        elseif (isset($params['SentEmailsFromRequestSearch']['applf_file_name']) && $params['SentEmailsFromRequestSearch']['applf_file_name'] == 1){
            $query->andFilterWhere(['is not', 'applf_file_name', new \yii\db\Expression('null')]);
        } else if (isset($params['SentEmailsFromRequestSearch']['applf_file_name']) && $params['SentEmailsFromRequestSearch']['applf_file_name'] == 0) {
            $query->andFilterWhere(['is', 'applf_file_name', new \yii\db\Expression('null')]);
        }

        return $dataProvider;
    }
}
