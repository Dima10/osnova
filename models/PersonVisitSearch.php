<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonVisit;

/**
 * PersonVisitSearch represents the model behind the search form about `app\models\PersonVisit`.
 */
class PersonVisitSearch extends PersonVisit
{

    public $prs_full_name;
    public $prs_connect_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pv_id', 'pv_prs_id'], 'integer'],
            [['pv_addr', 'pv_create_user', 'pv_create_time', 'pv_update_user', 'pv_update_time', 'pv_time'], 'safe'],
            [['prs_full_name', 'prs_connect_user'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonVisit::find()
            ->leftJoin(Person::tableName(), 'pv_prs_id = prs_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['prs_full_name'] = [
            'asc' => ['person.prs_full_name' => SORT_ASC],
            'desc' => ['person.prs_full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_connect_user'] = [
            'asc' => ['person.prs_connect_user' => SORT_ASC],
            'desc' => ['person.prs_connect_user' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pv_id' => $this->pv_id,
            'pv_prs_id' => $this->pv_prs_id,
            'pv_time' => $this->pv_time
        ]);

        $query
            ->andFilterWhere(['like', 'prs_full_name', $this->prs_full_name])
            ->andFilterWhere(['like', 'prs_connect_user', $this->prs_connect_user])
            ->andFilterWhere(['like', 'pv_addr', $this->pv_addr])
            ->andFilterWhere(['like', 'pv_create_user', $this->pv_create_user])
            ->andFilterWhere(['like', 'pv_create_time', $this->pv_create_time])
            ->andFilterWhere(['like', 'pv_update_user', $this->pv_update_user])
            ->andFilterWhere(['like', 'pv_update_time', $this->pv_update_time])
        ;

        return $dataProvider;
    }
}
