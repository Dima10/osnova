<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_request_content}}".
 *
 * Заявка на обучение: контент
 *
 * @property integer $applrc_id
 * @property integer $applrc_applr_id
 * @property integer $applrc_applcmd_id
 * @property integer $applrc_applf_id
 * @property integer $applrc_prs_id
 * @property string $applrc_position
 * @property integer $applrc_svc_id
 * @property integer $applrc_trp_id
 * @property string $applrc_date_upk
 * @property string $applrc_flag
 * @property integer $applrc_notify
 * @property string $applrc_create_user
 * @property string $applrc_create_time
 * @property string $applrc_create_ip
 * @property string $applrc_update_user
 * @property string $applrc_update_time
 * @property string $applrc_update_ip
 *
 * @property ApplRequest $applrcApplr
 * @property Person $applrcPrs
 * @property Svc $applrcSvc
 * @property TrainingProg $applrcTrp
 * @property ApplCommand $applrcApplcmd
 * @property ApplFinal $applrcApplf
 */
class ApplRequestContent extends \yii\db\ActiveRecord
{
    public $check;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_request_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $data = [];
        if ($this->applrcApplr->applr_flag > 0) {
            $data = [[['applrc_date_upk'], 'required']];
        }
        return $data + [
            [['applrc_applr_id', 'applrc_prs_id', 'applrc_svc_id', 'applrc_trp_id'], 'required'],
            [['applrc_applf_id', 'applrc_applr_id', 'applrc_prs_id', 'applrc_svc_id', 'applrc_trp_id', 'applrc_applcmd_id', 'applrc_flag', 'applrc_notify'], 'integer'],
            [['applrc_create_time', 'applrc_update_time'], 'safe'],
            [['applrc_date_upk'], 'string'],
            [['applrc_position'], 'string', 'max' => 64],
            [['applrc_create_user', 'applrc_create_ip', 'applrc_update_user', 'applrc_update_ip'], 'string', 'max' => 64],
            [['applrc_applr_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplRequest::class, 'targetAttribute' => ['applrc_applr_id' => 'applr_id']],
            [['applrc_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['applrc_svc_id' => 'svc_id']],
            [['applrc_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applrc_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applrc_id' => Yii::t('app', 'Applrc ID'),
            'applrc_applr_id' => Yii::t('app', 'Applrc Applr ID'),
            'applrc_applcmd_id' => Yii::t('app', 'Applrc Applcmd ID'),
            'applrc_prs_id' => Yii::t('app', 'Applrc Prs ID'),
            'applrc_position' => Yii::t('app', 'Applrc Position'),
            'applrc_svc_id' => Yii::t('app', 'Applrc Svc ID'),
            'applrc_trp_id' => Yii::t('app', 'Applrc Trp ID'),
            'applrc_date_upk' => Yii::t('app', 'Applrc Date Upk'),
            'applrc_flag' => Yii::t('app', 'Applrc Flag'),
            'applrc_notify' => Yii::t('app', 'Applrc Notify'),
            'applrc_create_user' => Yii::t('app', 'Applrc Create User'),
            'applrc_create_time' => Yii::t('app', 'Applrc Create Time'),
            'applrc_create_ip' => Yii::t('app', 'Applrc Create Ip'),
            'applrc_update_user' => Yii::t('app', 'Applrc Update User'),
            'applrc_update_time' => Yii::t('app', 'Applrc Update Time'),
            'applrc_update_ip' => Yii::t('app', 'Applrc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcApplr()
    {
        return $this->hasOne(ApplRequest::class, ['applr_id' => 'applrc_applr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcApplf()
    {
        return $this->hasOne(ApplFinal::class, ['applf_id' => 'applrc_applf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcApplcmd()
    {
        return $this->hasOne(ApplCommand::class, ['applcmd_id' => 'applrc_applcmd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'applrc_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applrc_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplrcPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applrc_prs_id']);
    }

    /**
     * @inheritdoc
     * @return ApplRequestContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplRequestContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        if (!$this->check) {
            if ($appl = ApplRequest::find()->innerJoin(AgreementStatus::tableName(), 'applr_ast_id = ast_id')->where(['applr_id' => $this->applrc_applr_id])->andWhere(['like', 'ast_sys', 'M'])->one()) {
                return false;
            }
        }

        if ($insert) {
            $this->applrc_flag = is_null($this->applrc_flag) ? 0 : $this->applrc_flag;

            $this->applrc_create_user = Yii::$app->user->identity->username;
            $this->applrc_create_ip = Yii::$app->request->userIP;
            $this->applrc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applrc_update_user = Yii::$app->user->identity->username;
            $this->applrc_update_ip = Yii::$app->request->userIP;
            $this->applrc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
