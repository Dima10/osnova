<?php
/**
 * @author Ruslan Bondarenko r.i.bondarenko@gmail.com (Dnipro)
 * @copyright Copyright (c) 2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 *
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * ListenerSearch represents the model behind the search form about `app\models\ApplRequestContent`.
 */
class ListenerSearch extends Listener
{

    public $svc_name;
    public $trp_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prs_last_name', 'prs_first_name', 'prs_middle_name', 'applr_number',
                'applr_date', 'applr_ast_name', 'svc_name', 'trp_name'], 'string'],
            [['applrc_id', 'applr_flag', 'applr_ast_id', 'applrc_applf_id', 'trp_id', 'pv_time', 'svc_id', 'applr_reestr'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $id
     *
     * @return ArrayDataProvider
     */
    public function search($params, $id)
    {
        $query = ApplRequestContent::find()
                ->leftJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
                ->leftJoin(Svc::tableName(), 'applrc_svc_id = svc_id')
                ->leftJoin(TrainingProg::tableName(), 'applrc_trp_id = trp_id')
                ->where(['applr_ab_id' => $id])
                ->andWhere('svc.svc_name like "%'.($params['ListenerSearch']['svc_name'] ?? '').'%"')
                ->andWhere('training_prog.trp_name like "%'.($params['ListenerSearch']['trp_name'] ?? '').'%"')
                ->orderBy('applr_date')
                ->all()
        ;

        $array = [];
        foreach ($query as $data) {
            $array[] = [
                'applrc_id' => $data->applrc_id,
                'pv_time' => $data->applrcPrs->visit->pv_time ?? '',
                'prs_last_name' => $data->applrcPrs->prs_last_name ?? '',
                'prs_first_name' => $data->applrcPrs->prs_first_name ?? '',
                'prs_middle_name' => $data->applrcPrs->prs_middle_name ?? '',
                'applr_number' => $data->applrcApplr->applr_number,
                'applr_date' => $data->applrcApplr->applr_date,
                'applr_ast_name' => $data->applrcApplr->applrAst->ast_name,
                'applr_reestr' => $data->applrcApplr->applr_reestr,
                'svc_name' => $data->applrcSvc->svc_name,
                'svc_id' => $data->applrcSvc->svc_id,
                'trp_name' => $data->applrcTrp->trp_name,
                'trp_id' => $data->applrcTrp->trp_id,
                'applr_flag' => $data->applrcApplr->applr_flag,
                'applrc_applf_id' => $data->applrc_applf_id,
                'applr_ast_id' => $data->applrcApplr->applr_ast_id,
            ];
        }

        $this->load($params);

        if (!$this->validate()) {
            return new ArrayDataProvider([
                'key' => 'applrc_id',
                'allModels' => $array,
                'sort' => [
                    'attributes' => ['prs_last_name', 'prs_first_name', 'prs_middle_name', 'applr_number',
                        'applr_date', 'applr_ast_id', 'applr_reestr', 'svc_id', 'trp_id', 'pv_time',
                        'applrc_id', 'applr_flag', 'applr_ast_id', 'applrc_applf_id'
                    ],
                ],

            ]);
        }

        foreach ($array as $key => &$data) {
            if (!empty($this->prs_last_name)) {
                if (mb_stripos($data['prs_last_name'], $this->prs_last_name) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (!empty($this->prs_first_name)) {
                if (mb_stripos($data['prs_first_name'], $this->prs_first_name) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (!empty($this->prs_middle_name)) {
                if (mb_stripos($data['prs_middle_name'], $this->prs_middle_name) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (!empty($this->applr_number)) {
                if (mb_stripos($data['applr_number'], $this->applr_number) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (!empty($this->pv_time)) {
                if (mb_stripos($data['pv_time'], $this->pv_time) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (!empty($this->applr_date)) {
                if (mb_stripos($data['applr_date'], $this->applr_date) === false) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (is_numeric($this->applr_ast_id)) {
                if ((int)$data['applr_ast_id'] != $this->applr_ast_id) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (is_numeric($this->applr_reestr)) {
                if ((int)$data['applr_reestr'] != $this->applr_reestr) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (is_numeric($this->svc_id)) {
                if ($data['svc_id'] != $this->svc_id) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (is_numeric($this->trp_id)) {
                if ($data['trp_id'] != $this->trp_id) {
                    unset($array[$key]);
                    continue;
                }
            }
            if (is_numeric($this->applr_flag)) {
                if ($data['applr_flag'] != $this->applr_flag) {
                    unset($array[$key]);
                    continue;
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'key' => 'applrc_id',
            'allModels' => $array,
            'sort' => [
                'attributes' => ['prs_last_name', 'prs_first_name', 'prs_middle_name', 'applr_number', 'pv_time',
                    'applr_date', 'ast_name', 'applr_reestr', 'svc_name', 'trp_name',
                    'applrc_id', 'applr_flag', 'applr_ast_id', 'applrc_applf_id', 'svc_id', 'trp_id'
                ],
                'defaultOrder' => ['applr_date' => SORT_DESC],
            ],

        ]);

        $dataProvider->sort->attributes['svc_name'] = [
            'asc' => ['svc_name' => SORT_ASC],
            'desc' => ['svc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['trp_name' => SORT_ASC],
            'desc' => ['trp_name' => SORT_DESC],
        ];

        return $dataProvider;
    }
}
