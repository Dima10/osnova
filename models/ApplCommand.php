<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_command}}".
 *
 * Приказ о зачислении
 *
 * @property integer $applcmd_id
 * @property integer $applcmd_reestr
 * @property string $applcmd_number
 * @property string $applcmd_date
 * @property integer $applcmd_trt_id
 * @property integer $applcmd_svdt_id
 * @property integer $applcmd_ab_id
 * @property integer $applcmd_comp_id
 *
 * @property integer $applcmd_pat_id
 * @property string $applcmd_file_name
 * @property resource $applcmd_file_data

 * @property string $applcmd_create_user
 * @property string $applcmd_create_time
 * @property string $applcmd_create_ip
 * @property string $applcmd_update_user
 * @property string $applcmd_update_time
 * @property string $applcmd_update_ip
 *
 * @property TrainingType $applcmdTrt
 * @property SvcDocType $applcmdSvdt
 * @property Ab $applcmdAb
 * @property Pattern $applcmdPat
 * @property Company $applcmdComp
 * @property ApplCommandContent[] $applCommandContents
 */
class ApplCommand extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_command}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applcmd_number', 'applcmd_date', 'applcmd_reestr'], 'required'],
            [['applcmd_date', 'applcmd_create_time', 'applcmd_update_time'], 'safe'],
            [['applcmd_reestr', 'applcmd_ab_id', 'applcmd_comp_id', 'applcmd_trt_id', 'applcmd_pat_id', 'applcmd_svdt_id'], 'integer'],

            [['applcmd_file_name'], 'string'],
            [['applcmd_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, pdf'],

            [['applcmd_number'], 'unique'],

            [['applcmd_number', 'applcmd_create_user', 'applcmd_create_ip', 'applcmd_update_user', 'applcmd_update_ip'], 'string', 'max' => 64],

            [['applcmd_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['applcmd_ab_id' => 'ab_id']],
            [['applcmd_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['applcmd_comp_id' => 'comp_id']],
            [['applcmd_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applcmd_trt_id' => 'trt_id']],
            [['applcmd_svdt_id'], 'exist', 'skipOnEmpty' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applcmd_svdt_id' => 'svdt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applcmd_id' => Yii::t('app', 'Applcmd ID'),
            'applcmd_reestr' => Yii::t('app', 'Applcmd Reestr'),
            'applcmd_number' => Yii::t('app', 'Applcmd Number'),
            'applcmd_date' => Yii::t('app', 'Applcmd Date'),
            'applcmd_trt_id' => Yii::t('app', 'Applcmd Trt ID'),
            'applcmd_svdt_id' => Yii::t('app', 'Applcmd Svdt ID'),
            'applcmd_ab_id' => Yii::t('app', 'Applcmd Ab ID'),
            'applcmd_comp_id' => Yii::t('app', 'Applcmd Comp ID'),
            'applcmd_pat_id' => Yii::t('app', 'Applcmd Pat ID'),
            'applcmd_file_name' => Yii::t('app', 'Applcmd File Name'),
            'applcmd_file_data' => Yii::t('app', 'Applcmd File Data'),
            'applcmd_create_user' => Yii::t('app', 'Applcmd Create User'),
            'applcmd_create_time' => Yii::t('app', 'Applcmd Create Time'),
            'applcmd_create_ip' => Yii::t('app', 'Applcmd Create Ip'),
            'applcmd_update_user' => Yii::t('app', 'Applcmd Update User'),
            'applcmd_update_time' => Yii::t('app', 'Applcmd Update Time'),
            'applcmd_update_ip' => Yii::t('app', 'Applcmd Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'applcmd_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applcmd_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'applcmd_comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applcmd_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applcmd_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplCommandContents()
    {
        return $this->hasMany(ApplCommandContent::class, ['applcmdc_applcmd_id' => 'applcmd_id']);
    }

    /**
     * @inheritdoc
     * @return ApplCommandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplCommandQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applcmd_create_user = Yii::$app->user->identity->username;
            $this->applcmd_create_ip = Yii::$app->request->userIP;
            $this->applcmd_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applcmd_update_user = Yii::$app->user->identity->username;
            $this->applcmd_update_ip = Yii::$app->request->userIP;
            $this->applcmd_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        foreach ($this->applCommandContents as $obj) {
            $obj->delete();
        }
        return parent::beforeDelete();
    }
}
