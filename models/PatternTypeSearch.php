<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PatternType;

/**
 * PatternTypeSearch represents the model behind the search form about `app\models\PatternType`.
 */
class PatternTypeSearch extends PatternType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patt_id'], 'integer'],
            [['patt_name', 'patt_note', 'patt_create_user', 'patt_create_time', 'patt_create_ip', 'patt_update_user', 'patt_update_time', 'patt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PatternType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'patt_id' => $this->patt_id
        ]);

        $query->andFilterWhere(['like', 'patt_name', $this->patt_name])
            ->andFilterWhere(['like', 'patt_note', $this->patt_note])
            ->andFilterWhere(['like', 'patt_create_user', $this->patt_create_user])
            ->andFilterWhere(['like', 'patt_create_ip', $this->patt_create_ip])
            ->andFilterWhere(['like', 'patt_update_user', $this->patt_update_user])
            ->andFilterWhere(['like', 'patt_update_ip', $this->patt_update_ip])
            ->andFilterWhere(['like', 'patt_create_time', $this->patt_create_time])
            ->andFilterWhere(['like', 'patt_update_time', $this->patt_update_time]);

        return $dataProvider;
    }
}
