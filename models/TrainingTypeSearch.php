<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingType;

/**
 * TrainingTypeSearch represents the model behind the search form about `app\models\TrainingType`.
 */
class TrainingTypeSearch extends TrainingType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trt_id'], 'integer'],
            [['trt_name', 'trt_create_user', 'trt_create_time', 'trt_create_ip', 'trt_update_user', 'trt_update_time', 'trt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trt_id' => $this->trt_id,
//            'trt_create_time' => $this->trt_create_time,
//            'trt_update_time' => $this->trt_update_time,
        ]);

        $query->andFilterWhere(['like', 'trt_name', $this->trt_name])
            ->andFilterWhere(['like', 'trt_create_user', $this->trt_create_user])
            ->andFilterWhere(['like', 'trt_create_ip', $this->trt_create_ip])
            ->andFilterWhere(['like', 'trt_update_user', $this->trt_update_user])
            ->andFilterWhere(['like', 'trt_create_time', $this->trt_create_time])
            ->andFilterWhere(['like', 'trt_update_time', $this->trt_update_time])
            ->andFilterWhere(['like', 'trt_update_ip', $this->trt_update_ip]);

        return $dataProvider;
    }
}
