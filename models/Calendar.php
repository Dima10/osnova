<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%calendar}}".
 *
 * @property integer $cal_id
 * @property string $cal_date
 * @property integer $cal_holiday
 * @property string $cal_create_user
 * @property string $cal_create_time
 * @property string $cal_create_ip
 * @property string $cal_update_user
 * @property string $cal_update_time
 * @property string $cal_update_ip
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%calendar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cal_date', 'cal_holiday'], 'required'],
            [['cal_date', 'cal_create_time', 'cal_update_time'], 'safe'],
            [['cal_holiday'], 'integer'],
            [['cal_create_user', 'cal_create_ip', 'cal_update_user', 'cal_update_ip'], 'string', 'max' => 64],
            [['cal_date'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cal_id' => Yii::t('app', 'Cal ID'),
            'cal_date' => Yii::t('app', 'Cal Date'),
            'cal_holiday' => Yii::t('app', 'Cal Holiday'),
            'cal_create_user' => Yii::t('app', 'Cal Create User'),
            'cal_create_time' => Yii::t('app', 'Cal Create Time'),
            'cal_create_ip' => Yii::t('app', 'Cal Create Ip'),
            'cal_update_user' => Yii::t('app', 'Cal Update User'),
            'cal_update_time' => Yii::t('app', 'Cal Update Time'),
            'cal_update_ip' => Yii::t('app', 'Cal Update Ip'),
        ];
    }

    /**
     * @inheritdoc
     * @return CalendarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CalendarQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->cal_create_user = Yii::$app->user->identity->username;
            $this->cal_create_ip = Yii::$app->request->userIP;
            $this->cal_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->cal_update_user = Yii::$app->user->identity->username;
            $this->cal_update_ip = Yii::$app->request->userIP;
            $this->cal_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * Return next work date
     * @param string $date
     * @param boolean $next
     * @param boolean $eq
     * @return string
     */
    public static function workDate($date, $next = true, $eq = true) {
        if ($next) {
            $order = SORT_ASC;
            if ($eq) {
                $where = '>=';
            } else {
                $where = '>';
            }
        } else {
            $order = SORT_DESC;
            if ($eq) {
                $where = '<=';
            } else {
                $where = '<';
            }
        }
        $cal =
            Calendar::find()
                ->where([$where, 'cal_date', $date])
                ->andWhere(['cal_holiday' => '0'])
                ->orderBy(['cal_date' => $order])
                ->one();
        if ($cal) {
            return $cal->cal_date;
        }
        return $date;
    }

    /**
     * Return next work date
     * @param string $date
     * @param int $count
     * @param boolean $next
     * @param boolean $eq
     * @return string
     */
    public static function workDatePeriod($date, $count, $next = true, $eq = true) {
        if ($next) {
            $order = SORT_ASC;
            if ($eq) {
                $where = '>=';
            } else {
                $where = '>';
            }
        } else {
            $order = SORT_DESC;
            if ($eq) {
                $where = '<=';
            } else {
                $where = '<';
            }
        }
        $cal =
            Calendar::find()
                ->where([$where, 'cal_date', $date])
                ->andWhere(['cal_holiday' => '0'])
                ->orderBy(['cal_date' => $order])
                ->limit($count)
                ->all();
        if (!empty($cal)) {
            return $cal[count($cal)-1]->cal_date;
        }
        return $date;
    }

    /**
     * Check work date
     * @param string $date
     * @return boolean
     */
    public static function workDateCheck($date) {
        if (Calendar::find()
            ->where(['cal_date' => $date])
            ->andWhere(['cal_holiday' => '0'])
            ->one()) {
            return true;
        }
        return false;
    }
}
