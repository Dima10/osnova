<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person_visit_event}}".
 *
 * @property int $pve_id
 * @property string $pve_name
 *
 * @property PersonVisit1[] $personVisit1s
 */
class PersonVisitEvent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%person_visit_event}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pve_name'], 'required'],
            [['pve_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pve_id' => Yii::t('app', 'Pve ID'),
            'pve_name' => Yii::t('app', 'Pve Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonVisit1s()
    {
        return $this->hasMany(PersonVisit1::class, ['pv1_pve_id' => 'pve_id']);
    }

    /**
     * {@inheritdoc}
     * @return PersonVisitEventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonVisitEventQuery(get_called_class());
    }
}
