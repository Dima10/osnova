<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_mail_type}}".
 *
 * @property integer $umt_id
 * @property string $umt_name
 *
 * @property UserMail[] $userMails
 */
class UserMailType extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_mail_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['umt_name'], 'required'],
            [['umt_name'], 'string', 'max' => 128],
            [['unt_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'umt_id' => Yii::t('app', 'Umt ID'),
            'umt_name' => Yii::t('app', 'Umt Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMails()
    {
        return $this->hasMany(UserMail::class, ['um_umt_id' => 'umt_id']);
    }

    /**
     * @inheritdoc
     * @return UserMailTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserMailTypeQuery(get_called_class());
    }
}
