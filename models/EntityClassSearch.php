<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EntityClass;

/**
 * EntityClassSearch represents the model behind the search form about `app\models\EntityClass`.
 */
class EntityClassSearch extends EntityClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entc_id'], 'integer'],
            [['entc_name', 'entc_create_user', 'entc_create_time', 'entc_create_ip', 'entc_update_user', 'entc_update_time', 'entc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EntityClass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entc_id' => $this->entc_id,
        ]);

        $query->andFilterWhere(['like', 'entc_name', $this->entc_name])
            ->andFilterWhere(['like', 'entc_create_user', $this->entc_create_user])
            ->andFilterWhere(['like', 'entc_create_ip', $this->entc_create_ip])
            ->andFilterWhere(['like', 'entc_update_user', $this->entc_update_user])
            ->andFilterWhere(['like', 'entc_create_time', $this->entc_create_time])
            ->andFilterWhere(['like', 'entc_update_time', $this->entc_update_time])
            ->andFilterWhere(['like', 'entc_update_ip', $this->entc_update_ip]);

        return $dataProvider;
    }
}
