<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%ab}}".
 *
 * @property integer $ab_id
 * @property string $ab_name
 * @property integer $ab_type
 * @property string $ab_create_user
 * @property string $ab_create_time
 * @property string $ab_create_ip
 * @property string $ab_update_user
 * @property string $ab_update_time
 * @property string $ab_update_ip
 *
 * @property Account[] $accounts
 * @property Address[] $addresses
 * @property Agreement[] $agreements
 * @property Contact[] $contacts
 * @property FinanceBook[] $financeBooks
 * @property Entity $entity
 * @property Event[] $events
 * @property Person $person
 * @property Task[] $tasks
 * @property Svc[] $svcs
 */
class Ab extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ab}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ab_name', 'ab_type'], 'required'],
            [['ab_type'], 'integer'],
            [['ab_create_time', 'ab_update_time'], 'safe'],
            [['ab_name'], 'string', 'max' => 128],
            [['ab_create_user', 'ab_create_ip', 'ab_update_user', 'ab_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ab_id' => Yii::t('app', 'Ab ID'),
            'ab_name' => Yii::t('app', 'Ab Name'),
            'ab_type' => Yii::t('app', 'Ab Type'),
            'ab_create_user' => Yii::t('app', 'Create User'),
            'ab_create_time' => Yii::t('app', 'Create Time'),
            'ab_create_ip' => Yii::t('app', 'Create Ip'),
            'ab_update_user' => Yii::t('app', 'Update User'),
            'ab_update_time' => Yii::t('app', 'Update Time'),
            'ab_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::class, ['acc_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::class, ['add_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['agr_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::class, ['con_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::class, ['ent_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::class, ['event_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['task_ab_id' => 'ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSvcs()
    {
        return $this->hasMany(Svc::class, ['svc_ab_id' => 'ab_id']);
    }

    /**
     * @inheritdoc
     * @return AbQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AbQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->ab_create_user = Yii::$app->user->identity->username;
            $this->ab_create_ip = Yii::$app->request->userIP;
            $this->ab_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->ab_update_user = Yii::$app->user->identity->username;
            $this->ab_update_ip = Yii::$app->request->userIP;
            $this->ab_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
