<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_module}}".
 *
 * @property integer $trm_id
 * @property integer $trm_trm_id
 * @property string $trm_name
 * @property string $trm_code
 * @property resource $trm_dataA
 * @property string $trm_dataA_name
 * @property resource $trm_dataB
 * @property string $trm_dataB_name
 * @property integer $trm_test_question
 * @property string $trm_create_user
 * @property string $trm_create_time
 * @property string $trm_create_ip
 * @property string $trm_update_user
 * @property string $trm_update_time
 * @property string $trm_update_ip
 *
 * @property TrainingLecture[] $trainingLectures
 * @property TrainingModule $trmTrm
 * @property TrainingModule[] $trainingModules
 * @property TrainingQuestion[] $trainingQuestions
 * @property TrainingModulePerson[] $trainingModulePeople
 * @property TrainingProgModule[] $trainingProgModules
 */
class TrainingModule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trm_trm_id', 'trm_test_question'], 'integer'],
            [['trm_name', 'trm_code'], 'required'],
            [['trm_create_time', 'trm_update_time'], 'safe'],
            [['trm_name'], 'string', 'max' => 512],
            [['trm_code'], 'string', 'max' => 128],
            [['trm_dataA_name', 'trm_dataB_name'], 'string', 'max' => 256],
            [['trm_dataA', 'trm_dataB'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, doc, pdf'],


            [['trm_create_user', 'trm_create_ip', 'trm_update_user', 'trm_update_ip'], 'string', 'max' => 64],
            [['trm_trm_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingModule::class, 'targetAttribute' => ['trm_trm_id' => 'trm_id']],
    ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trm_id' => Yii::t('app', 'Trm ID'),
            'trm_trm_id' => Yii::t('app', 'Trm Trm ID'),
            'trm_name' => Yii::t('app', 'Trm Name'),
            'trm_code' => Yii::t('app', 'Trm Code'),
            'trm_dataA' => Yii::t('app', 'Trm Data A'),
            'trm_dataA_name' => Yii::t('app', 'Trm Data A Name'),
            'trm_dataB' => Yii::t('app', 'Trm Data B'),
            'trm_dataB_name' => Yii::t('app', 'Trm Data B Name'),
            'trm_test_question' => Yii::t('app', 'Trm Test Question'),
            'trm_create_user' => Yii::t('app', 'Trm Create User'),
            'trm_create_time' => Yii::t('app', 'Trm Create Time'),
            'trm_create_ip' => Yii::t('app', 'Trm Create Ip'),
            'trm_update_user' => Yii::t('app', 'Trm Update User'),
            'trm_update_time' => Yii::t('app', 'Trm Update Time'),
            'trm_update_ip' => Yii::t('app', 'Trm Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingLectures()
    {
        return $this->hasMany(TrainingLecture::class, ['tlec_trm_id' => 'trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrmTrm()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'trm_trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingModules()
    {
        return $this->hasMany(TrainingModule::class, ['trm_trm_id' => 'trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingQuestions()
    {
        return $this->hasMany(TrainingQuestion::class, ['trq_trm_id' => 'trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingModulePeople()
    {
        if ($prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username])) {
            return $this->hasMany(TrainingModulePerson::class, ['tmp_trm_id' => 'trm_id'])->onCondition(['tmp_prs_id' => $prs->prs_id]);
        } else {
            return $this->hasMany(TrainingModulePerson::class, ['tmp_trm_id' => 'trm_id'])->onCondition(['tmp_prs_id' => 0]);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingProgModules()
    {
        return $this->hasMany(TrainingProgModule::class, ['trpl_trm_id' => 'trm_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingModuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingModuleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->trm_create_user = Yii::$app->user->identity->username;
            $this->trm_create_ip = Yii::$app->request->userIP;
            $this->trm_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->trm_update_user = Yii::$app->user->identity->username;
            $this->trm_update_ip = Yii::$app->request->userIP;
            $this->trm_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
