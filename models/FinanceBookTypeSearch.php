<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceBookType;

/**
 * FinanceBookTypeSearch represents the model behind the search form about `app\models\FinanceBookType`.
 */
class FinanceBookTypeSearch extends FinanceBookType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fbt_id'], 'integer'],
            [['fbt_name', 'fbt_create_user', 'fbt_create_ip', 'fbt_create_time', 'fbt_update_user', 'fbt_update_ip', 'fbt_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBookType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fbt_id' => $this->fbt_id,
            'fbt_create_time' => $this->fbt_create_time,
            'fbt_update_time' => $this->fbt_update_time,
        ]);

        $query->andFilterWhere(['like', 'fbt_name', $this->fbt_name])
            ->andFilterWhere(['like', 'fbt_create_user', $this->fbt_create_user])
            ->andFilterWhere(['like', 'fbt_create_ip', $this->fbt_create_ip])
            ->andFilterWhere(['like', 'fbt_update_user', $this->fbt_update_user])
            ->andFilterWhere(['like', 'fbt_update_ip', $this->fbt_update_ip]);

        return $dataProvider;
    }
}
