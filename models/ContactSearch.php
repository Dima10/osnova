<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contact;

/**
 * ContactSearch represents the model behind the search form about `app\models\Contact`.
 */
class ContactSearch extends Contact
{
    public $contragent;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['con_id', 'con_ab_id', 'con_cont_id'], 'integer'],
            [['con_text', 'con_create_user', 'con_create_time', 'con_create_ip', 'con_update_user', 'con_update_time', 'con_update_ip'], 'safe'],
            [['contragent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contact::find()->joinWith(['conAb' => function($query) { $query->from(['conAb' => 'ab']);}]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['contragent'] = [
            'asc' => ['addAb.ab_name' => SORT_ASC],
            'desc' => ['addAb.ab_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'con_id' => $this->con_id,
            'con_ab_id' => $this->con_ab_id,
            'con_cont_id' => $this->con_cont_id,
        ]);

        $query->andFilterWhere(['like', 'con_text', $this->con_text])
            ->andFilterWhere(['like', 'con_create_user', $this->con_create_user])
            ->andFilterWhere(['like', 'conAb.ab_name', $this->contragent])
            ->andFilterWhere(['like', 'con_create_ip', $this->con_create_ip])
            ->andFilterWhere(['like', 'con_update_user', $this->con_update_user])
            ->andFilterWhere(['like', 'con_create_time', $this->con_create_time])
            ->andFilterWhere(['like', 'con_update_time', $this->con_update_time])
            ->andFilterWhere(['like', 'con_update_ip', $this->con_update_ip]);

        return $dataProvider;
    }
}
