<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class ApplFinalFile
{

    public function createFinalDocumentFile($applfId)
    {
        $model = $model = ApplFinal::findOne($applfId);

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $model->applf_svdt_id])
                ->andWhere(['patt_id' => 16])
                ->andWhere(['pat_trt_id' => $model->applf_trt_id])
                ->one();
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->applf_cmd_date . '_' . $model->applf_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            try {
                $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            } catch (CopyFileException $e) {
            } catch (CreateTemporaryFileException $e) {
            }

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applf_cmd_date)->getTimestamp();
            $document->setValue('PRIKAZ_IN_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('PRIKAZ_IN_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('PRIKAZ_IN_DATE_YYYY', strftime('%Y', $dateVal));

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applf_end_date)->getTimestamp();
            $document->setValue('PRIKAZ_OUT_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('PRIKAZ_OUT_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('PRIKAZ_OUT_DATE_YYYY', strftime('%Y', $dateVal));

            $data = $model->applf_name_full;
            $document->setValue('LISTENER_FIO', $data);
            $document->setValue('PERSONA_FIO', $data);

            $data = $model->applfTrp->trp_name;
            $document->setValue('PROG', $data);

            $data = $model->applfTrp->trp_hour;
            $document->setValue('PROG_TIME', $data);

            $data = $model->applf_number;
            $document->setValue('DOC_NUMBER', $data);

            $document->saveAs($file_name);

            $model->applf_file_name = $model->applf_cmd_date . '_' . $model->applf_trt_id . '_' . $pattern->pat_fname;
            $model->applf_file_data = file_get_contents($file_name);
            $model->applf_pat_id = $pattern->pat_id;

            unlink($file_name);
            unlink($tmpl_name);

           $model->save();

    }

}
