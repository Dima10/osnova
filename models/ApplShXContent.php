<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_sh_x_content}}".
 *
 * Ведомость тестов
 *
 * @property integer $applsxc_id
 * @property integer $applsxc_applsx_id
 * @property integer $applsxc_prs_id
 * @property integer $applsxc_trp_id
 * @property integer $applsxc_score
 * @property integer $applsxc_passed
 * @property integer $applsxc_applcmd_id
 * @property string $applsxc_create_user
 * @property string $applsxc_create_time
 * @property string $applsxc_create_ip
 * @property string $applsxc_update_user
 * @property string $applsxc_update_time
 * @property string $applsxc_update_ip
 *
 * @property Person $applsxcPrs
 * @property ApplShX $applsxcApplsx
 * @property ApplCommand $applsxcApplcmd
 * @property TrainingProg $applsxcTrp
 */
class ApplShXContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_sh_x_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsxc_applsx_id', 'applsxc_prs_id', 'applsxc_trp_id', 'applsxc_score', 'applsxc_passed'], 'required'],
            [['applsxc_applcmd_id', 'applsxc_applsx_id', 'applsxc_prs_id', 'applsxc_trp_id', 'applsxc_score', 'applsxc_passed'], 'integer'],
            [['applsxc_create_time', 'applsxc_update_time'], 'safe'],
            [['applsxc_create_user', 'applsxc_create_ip', 'applsxc_update_user', 'applsxc_update_ip'], 'string', 'max' => 64],
            [['applsxc_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applsxc_prs_id' => 'prs_id']],
            [['applsxc_applsx_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplShX::class, 'targetAttribute' => ['applsxc_applsx_id' => 'applsx_id']],
            [['applsxc_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applsxc_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applsxc_id' => Yii::t('app', 'Applsxc ID'),
            'applsxc_applsx_id' => Yii::t('app', 'Applsxc Applsx ID'),
            'applsxc_prs_id' => Yii::t('app', 'Applsxc Prs ID'),
            'applsxc_trp_id' => Yii::t('app', 'Applsxc Trp ID'),
            'applsxc_score' => Yii::t('app', 'Applsxc Score'),
            'applsxc_passed' => Yii::t('app', 'Applsxc Passed'),
            'applsxc_create_user' => Yii::t('app', 'Applsxc Create User'),
            'applsxc_create_time' => Yii::t('app', 'Applsxc Create Time'),
            'applsxc_create_ip' => Yii::t('app', 'Applsxc Create Ip'),
            'applsxc_update_user' => Yii::t('app', 'Applsxc Update User'),
            'applsxc_update_time' => Yii::t('app', 'Applsxc Update Time'),
            'applsxc_update_ip' => Yii::t('app', 'Applsxc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxcPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applsxc_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxcApplsx()
    {
        return $this->hasOne(ApplShX::class, ['applsx_id' => 'applsxc_applsx_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxcApplcmd()
    {
        return $this->hasOne(ApplCommand::class, ['applcmd_id' => 'applsxc_applcmd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applsxc_trp_id']);
    }

    /**
     * @inheritdoc
     * @return ApplShXContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplShXContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applsxc_create_user = Yii::$app->user->identity->username;
            $this->applsxc_create_ip = Yii::$app->request->userIP;
            $this->applsxc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applsxc_update_user = Yii::$app->user->identity->username;
            $this->applsxc_update_ip = Yii::$app->request->userIP;
            $this->applsxc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
