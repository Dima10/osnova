<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property integer $event_id
 * @property integer $event_ab_id
 * @property integer $event_evt_id
 * @property integer $event_evrt_id
 * @property string $event_date
 * @property integer $event_user_id
 * @property string $event_note
 * @property string $event_create_user
 * @property string $event_create_time
 * @property string $event_create_ip
 * @property string $event_update_user
 * @property string $event_update_time
 * @property string $event_update_ip
 *
 * @property EventResultType $eventEvrt
 * @property Ab $eventAb
 * @property User $eventUser
 * @property EventType $eventEvt
 * @property Task[] $tasks
 * @property TaskTmpl[] $taskTmpls
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_ab_id', 'event_evt_id', 'event_evrt_id', 'event_date'], 'required'],
            [['event_ab_id', 'event_evt_id', 'event_evrt_id', 'event_user_id'], 'integer'],
            [['event_date', 'event_create_time', 'event_update_time'], 'safe'],
            [['event_note'], 'string'],
            [['event_create_user', 'event_create_ip', 'event_update_user', 'event_update_ip'], 'string', 'max' => 64],
            [['event_evrt_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventResultType::class, 'targetAttribute' => ['event_evrt_id' => 'evrt_id']],
            [['event_ab_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['event_ab_id' => 'ab_id']],
            [['event_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['event_user_id' => 'user_id']],
            [['event_evt_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventType::class, 'targetAttribute' => ['event_evt_id' => 'evt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_id' => Yii::t('app', 'Event ID'),
            'event_ab_id' => Yii::t('app', 'Event Ab ID'),
            'event_evt_id' => Yii::t('app', 'Event Evt ID'),
            'event_evrt_id' => Yii::t('app', 'Event Evrt ID'),
            'event_date' => Yii::t('app', 'Event Date'),
            'event_user_id' => Yii::t('app', 'Event User ID'),
            'event_note' => Yii::t('app', 'Event Note'),
            'event_create_user' => Yii::t('app', 'Event Create User'),
            'event_create_time' => Yii::t('app', 'Event Create Time'),
            'event_create_ip' => Yii::t('app', 'Event Create Ip'),
            'event_update_user' => Yii::t('app', 'Event Update User'),
            'event_update_time' => Yii::t('app', 'Event Update Time'),
            'event_update_ip' => Yii::t('app', 'Event Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventEvrt()
    {
        return $this->hasOne(EventResultType::class, ['evrt_id' => 'event_evrt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'event_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'event_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventEvt()
    {
        return $this->hasOne(EventType::class, ['evt_id' => 'event_evt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['task_event_id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTmpls()
    {
        return $this->hasMany(TaskTmpl::class, ['tasktp_event_id' => 'event_id']);
    }

    /**
     * @inheritdoc
     * @return EventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventQuery(get_called_class());
    }
}
