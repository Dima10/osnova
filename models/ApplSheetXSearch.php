<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplSheetX;

/**
 * ApplSheetXSearch represents the model behind the search form about `app\models\ApplSheetX`.
 */
class ApplSheetXSearch extends ApplSheetX
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsx_id', 'applsx_trt_id'], 'integer'],
            [['applsx_number', 'applsx_date', 'applsx_create_user', 'applsx_create_time', 'applsx_create_ip', 'applsx_update_user', 'applsx_update_time', 'applsx_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplSheetX::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['applsx_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applsx_id' => $this->applsx_id,
            'applsx_trt_id' => $this->applsx_trt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applsx_date', $this->applsx_date])
            ->andFilterWhere(['like', 'applsx_create_time', $this->applsx_create_time])
            ->andFilterWhere(['like', 'applsx_update_time', $this->applsx_update_time])
            ->andFilterWhere(['like', 'applsx_number', $this->applsx_number])
            ->andFilterWhere(['like', 'applsx_create_user', $this->applsx_create_user])
            ->andFilterWhere(['like', 'applsx_create_ip', $this->applsx_create_ip])
            ->andFilterWhere(['like', 'applsx_update_user', $this->applsx_update_user])
            ->andFilterWhere(['like', 'applsx_update_ip', $this->applsx_update_ip])
        ;

        return $dataProvider;
    }
}
