<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplEndContent;

/**
 * ApplEndContentSearch represents the model behind the search form about `app\models\ApplEndContent`.
 */
class ApplEndContentSearch extends ApplEndContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applec_id', 'applec_apple_id', 'applec_prs_id', 'applec_trp_id'], 'integer'],
            [['applec_number', 'applec_create_user', 'applec_create_time', 'applec_create_ip', 'applec_update_user', 'applec_update_time', 'applec_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplEndContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applec_id' => $this->applec_id,
            'applec_apple_id' => $this->applec_apple_id,
            'applec_prs_id' => $this->applec_prs_id,
            'applec_trp_id' => $this->applec_trp_id,
            'applec_create_time' => $this->applec_create_time,
            'applec_update_time' => $this->applec_update_time,
        ]);

        $query->andFilterWhere(['like', 'applec_number', $this->applec_number])
            ->andFilterWhere(['like', 'applec_create_user', $this->applec_create_user])
            ->andFilterWhere(['like', 'applec_create_ip', $this->applec_create_ip])
            ->andFilterWhere(['like', 'applec_update_user', $this->applec_update_user])
            ->andFilterWhere(['like', 'applec_update_ip', $this->applec_update_ip]);

        return $dataProvider;
    }
}
