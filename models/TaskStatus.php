<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%task_status}}".
 *
 * @property integer $tasks_id
 * @property string $tasks_name
 * @property string $tasks_note
 * @property string $tasks_create_user
 * @property string $tasks_create_time
 * @property string $tasks_create_ip
 * @property string $tasks_update_user
 * @property string $tasks_update_time
 * @property string $tasks_update_ip
 *
 * @property Task[] $tasks
 */
class TaskStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tasks_name', 'tasks_note'], 'required'],
            [['tasks_note'], 'string'],
            [['tasks_create_time', 'tasks_update_time'], 'safe'],
            [['tasks_name', 'tasks_create_user', 'tasks_create_ip', 'tasks_update_user', 'tasks_update_ip'], 'string', 'max' => 64],
            [['tasks_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tasks_id' => Yii::t('app', 'Tasks ID'),
            'tasks_name' => Yii::t('app', 'Tasks Name'),
            'tasks_note' => Yii::t('app', 'Tasks Note'),
            'tasks_create_user' => Yii::t('app', 'Tasks Create User'),
            'tasks_create_time' => Yii::t('app', 'Tasks Create Time'),
            'tasks_create_ip' => Yii::t('app', 'Tasks Create Ip'),
            'tasks_update_user' => Yii::t('app', 'Tasks Update User'),
            'tasks_update_time' => Yii::t('app', 'Tasks Update Time'),
            'tasks_update_ip' => Yii::t('app', 'Tasks Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::class, ['task_tasks_id' => 'tasks_id']);
    }

    /**
     * @inheritdoc
     * @return TaskStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskStatusQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tasks_create_user = Yii::$app->user->identity->username;
            $this->tasks_create_ip = Yii::$app->request->userIP;
            $this->tasks_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->tasks_update_user = Yii::$app->user->identity->username;
            $this->tasks_update_ip = Yii::$app->request->userIP;
            $this->tasks_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
