<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentType;

/**
 * PaymentTypeSearch represents the model behind the search form about `app\models\PaymentType`.
 */
class PaymentTypeSearch extends PaymentType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pt_id'], 'integer'],
            [['pt_name', 'pt_create_user', 'pt_create_ip', 'pt_create_time', 'pt_update_user', 'pt_update_ip', 'pt_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pt_id' => $this->pt_id,
            'pt_create_time' => $this->pt_create_time,
            'pt_update_time' => $this->pt_update_time,
        ]);

        $query->andFilterWhere(['like', 'pt_name', $this->pt_name])
            ->andFilterWhere(['like', 'pt_create_user', $this->pt_create_user])
            ->andFilterWhere(['like', 'pt_create_ip', $this->pt_create_ip])
            ->andFilterWhere(['like', 'pt_update_user', $this->pt_update_user])
            ->andFilterWhere(['like', 'pt_update_ip', $this->pt_update_ip]);

        return $dataProvider;
    }
}
