<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_act}}".
 *
 * @property integer $act_id
 * @property string $act_number
 * @property string $act_date
 * @property integer $act_ab_id 
 * @property integer $act_comp_id 
 * @property integer $act_agr_id 
 * @property integer $act_agra_id
 * @property integer $act_acc_id
 * @property integer $act_add_id
 * @property integer $act_prs_id
 * @property string $act_comment
 * @property string $act_sum
 * @property string $act_tax
 * @property integer $act_pat_id 
 * @property integer $act_ast_id
 * @property resource $act_data
 * @property string $act_fdata
 * @property resource $act_data_sign
 * @property string $act_fdata_sign
 * @property string $act_create_user
 * @property string $act_create_time
 * @property string $act_create_ip
 * @property string $act_update_user
 * @property string $act_update_time
 * @property string $act_update_ip
 *
 * @property Agreement $actAgr
 * @property AgreementAnnex $actAgra
 * @property Ab $actAb
 * @property Pattern $actPat
 * @property Account $actAcc
 * @property Address $actAdd
 * @property Person $actPrs
 * @property Company $actComp
 * @property AgreementStatus $actAst
 *
 */
class AgreementAct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_act}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_number', 'act_date', 'act_agr_id', 'act_ab_id', 'act_comp_id'], 'required'],
            [['act_date', 'act_create_time', 'act_update_time'], 'safe'],
            [['act_ab_id', 'act_comp_id', 'act_agr_id', 'act_agra_id', 'act_acc_id', 'act_add_id', 'act_prs_id', 'act_ast_id', 'act_pat_id'], 'integer'],
            [['act_sum', 'act_tax'], 'number'],

            [['act_comment'], 'string', 'max' => 1024],

            [['act_number', 'act_date'], 'string'],

            [['act_data', 'act_data_sign'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, docx, pdf'],

            [['act_create_time', 'act_update_time'], 'safe'],
            [['act_create_user', 'act_create_ip', 'act_update_user', 'act_update_ip'], 'string', 'max' => 64],

            [['act_fdata', 'act_fdata_sign'], 'string', 'max' => 256],

            [['act_agr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['act_agr_id' => 'agr_id']],
            [['act_ab_id'],   'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['act_ab_id' => 'ab_id']],
            [['act_pat_id'],  'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['act_pat_id' => 'pat_id']],
            [['act_acc_id'],  'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['act_acc_id' => 'acc_id']],
            [['act_comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['act_comp_id' => 'comp_id']],
            [['act_ast_id'],  'exist', 'skipOnError' => true, 'targetClass' => AgreementStatus::class, 'targetAttribute' => ['act_ast_id' => 'ast_id']],
            //[['act_agra_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAnnex::class, 'targetAttribute' => ['act_agra_id' => 'agra_id']],

            [['act_number', 'act_agr_id', 'act_comp_id'], 'unique', 'targetAttribute' => ['act_number', 'act_agr_id', 'act_comp_id'], 'message' => Yii::t('app', 'The combination of Act Number and Act Agr ID and Act Comp ID has already been taken.')],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'act_id' => Yii::t('app', 'Act ID'),
            'act_number' => Yii::t('app', 'Act Number'),
            'act_date' => Yii::t('app', 'Act Date'),
            'act_ab_id' => Yii::t('app', 'Act Ab ID'),
            'act_comp_id' => Yii::t('app', 'Act Comp ID'),
            'act_agr_id' => Yii::t('app', 'Act Agr ID'),
            'act_agra_id' => Yii::t('app', 'Act Agra ID'),
            'act_acc_id' => Yii::t('app', 'Act Acc ID'),
            'act_add_id' => Yii::t('app', 'Act Add ID'),
            'act_prs_id' => Yii::t('app', 'Act Prs ID'),
            'act_comment' => Yii::t('app', 'Act Comment'),
            'act_sum' => Yii::t('app', 'Act Sum'),
            'act_tax' => Yii::t('app', 'Act Tax'),
            'act_ast_id' => Yii::t('app', 'Act Ast ID'), 
            'act_fdata' => Yii::t('app', 'Act Fdata'),
            'act_data' => Yii::t('app', 'Act Data'),
            'act_fdata_sign' => Yii::t('app', 'Act Fdata Sign'),
            'act_data_sign' => Yii::t('app', 'Act Data Sign'),
            'act_pat_id' => Yii::t('app', 'Act Pat ID'),
            'act_create_user' => Yii::t('app', 'Act Create User'),
            'act_create_time' => Yii::t('app', 'Act Create Time'),
            'act_create_ip' => Yii::t('app', 'Act Create Ip'),
            'act_update_user' => Yii::t('app', 'Act Update User'), 
            'act_update_time' => Yii::t('app', 'Act Update Time'),
            'act_update_ip' => Yii::t('app', 'Act Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAb()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'act_ab_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAgr()
    {
        return $this->hasOne(Agreement::class, ['agr_id' => 'act_agr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAgra()
    {
        return $this->hasOne(AgreementAnnex::class, ['agra_id' => 'act_agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAcc()
    {
        return $this->hasOne(Account::class, ['acc_id' => 'act_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAdd()
    {
        return $this->hasOne(Address::class, ['add_id' => 'act_add_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'act_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActComp()
    {
        return $this->hasOne(Company::class, ['comp_id' => 'act_comp_id']);
    }

    /** 
     * @return \yii\db\ActiveQuery 
    */ 
    public function getActPat() 
    { 
        return $this->hasOne(Pattern::class, ['pat_id' => 'act_pat_id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActAst()
    {
        return $this->hasOne(AgreementStatus::class, ['ast_id' => 'act_ast_id']);
    }


    /**
     * @inheritdoc
     * @return AgreementActQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementActQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            if ($this->act_agr_id == 0) {
                $this->act_agr_id = null;
            }
            if ($this->act_agra_id == 0) {
                $this->act_agra_id = null;
            }

            if (isset($this->act_agr_id)) {
                $this->act_ab_id = Agreement::findOne($this->act_agr_id)->agr_ab_id;
            } else
            if (isset($this->act_agra_id)) {
                $this->act_ab_id = AgreementAnnex::findOne($this->act_agra_id)->agra_ab_id;
            }

            $this->act_create_user = Yii::$app->user->identity->username;
            $this->act_create_ip = Yii::$app->request->userIP;
            $this->act_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            if ($this->act_agr_id == 0) {
                $this->act_agr_id = null;
            }
            if ($this->act_agra_id == 0) {
                $this->act_agra_id = null;
            }

            if (isset($this->act_agr_id)) {
                $this->act_ab_id = Agreement::findOne($this->act_agr_id)->agr_ab_id;
            } else
            if (isset($this->act_agra_id)) {
                $this->act_ab_id = AgreementAnnex::findOne($this->act_agra_id)->agra_ab_id;
            }

            $this->act_update_user = Yii::$app->user->identity->username;
            $this->act_update_ip = Yii::$app->request->userIP;
            $this->act_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach (AgreementActA::find()->where(['acta_act_id' => $this->act_id])->all() as $key => $value) {
                $value->delete();
            }
            return true;
        } else {
            return false;
        }
    }


}
