<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%finance_book_status}}".
 *
 * @property integer $fbs_id
 * @property string $fbs_name
 * @property string $fbs_create_user
 * @property string $fbs_create_ip
 * @property string $fbs_create_time
 * @property string $fbs_update_user
 * @property string $fbs_update_ip
 * @property string $fbs_update_time
 *
 * @property FinanceBook[] $financeBooks
 */
class FinanceBookStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%finance_book_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fbs_name'], 'required'],
            [['fbs_create_time', 'fbs_update_time'], 'safe'],
            [['fbs_name', 'fbs_create_user', 'fbs_create_ip', 'fbs_update_user', 'fbs_update_ip'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fbs_id' => Yii::t('app', 'Fbs ID'),
            'fbs_name' => Yii::t('app', 'Fbs Name'),
            'fbs_create_user' => Yii::t('app', 'Fbs Create User'),
            'fbs_create_ip' => Yii::t('app', 'Fbs Create Ip'),
            'fbs_create_time' => Yii::t('app', 'Fbs Create Time'),
            'fbs_update_user' => Yii::t('app', 'Fbs Update User'),
            'fbs_update_ip' => Yii::t('app', 'Fbs Update Ip'),
            'fbs_update_time' => Yii::t('app', 'Fbs Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_fbs_id' => 'fbs_id']);
    }

    /**
     * @inheritdoc
     * @return FinanceBookStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FinanceBookStatusQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->fbs_create_user = Yii::$app->user->identity->username;
            $this->fbs_create_ip = Yii::$app->request->userIP;
            $this->fbs_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->fbs_update_user = Yii::$app->user->identity->username;
            $this->fbs_update_ip = Yii::$app->request->userIP;
            $this->fbs_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
