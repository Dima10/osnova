<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pattern;

/**
 * PatternSearch represents the model behind the search form about `app\models\Pattern`.
 */
class PatternSearch extends Pattern
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pat_id', 'pat_patt_id', 'pat_svdt_id', 'pat_trt_id'], 'integer'],
            [['pat_name', 'pat_code', 'pat_fname', 'pat_fdata', 'pat_note', 'pat_create_user', 'pat_create_time', 'pat_create_ip', 'pat_update_user', 'pat_update_time', 'pat_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pattern::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pat_id' => $this->pat_id,
            'pat_patt_id' => $this->pat_patt_id,
            'pat_svdt_id' => $this->pat_svdt_id,
            'pat_trt_id' => $this->pat_trt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'pat_create_time', $this->pat_create_time])
            ->andFilterWhere(['like', 'pat_update_time', $this->pat_update_time])
            ->andFilterWhere(['like', 'pat_name', $this->pat_name])
            ->andFilterWhere(['like', 'pat_code', $this->pat_code])
            ->andFilterWhere(['like', 'pat_fname', $this->pat_fname])
            ->andFilterWhere(['like', 'pat_fdata', $this->pat_fdata])
            ->andFilterWhere(['like', 'pat_note', $this->pat_note])
            ->andFilterWhere(['like', 'pat_create_user', $this->pat_create_user])
            ->andFilterWhere(['like', 'pat_create_ip', $this->pat_create_ip])
            ->andFilterWhere(['like', 'pat_update_user', $this->pat_update_user])
            ->andFilterWhere(['like', 'pat_update_ip', $this->pat_update_ip]);

        return $dataProvider;
    }
}
