<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAnnex;

/**
 * AgreementAnnexSearch represents the model behind the search form about `app\models\AgreementAnnex`.
 */
class AgreementAnnexSearch extends AgreementAnnex
{
    public $yurLico;
    public $dogovor;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agra_id', 'agra_ab_id', 'agra_comp_id', 'agra_agr_id', 'agra_ast_id'], 'integer'],
            [['agra_number', 'agra_date', 'agra_comment', /*'agra_fdata', 'agra_fdata_sign'*/], 'string'],
            [['agra_create_user', 'agra_create_time', 'agra_create_ip', 'agra_update_user', 'agra_update_time', 'agra_update_ip'], 'safe'],
            [['agra_sum', 'agra_tax'], 'number'],
            [['yurLico'], 'safe'],
            [['dogovor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id=null)
    {
        if (isset($id)) {
            $query = AgreementAnnex::find()
                ->joinWith(['agraEnt' => function($query) { $query->from(['agraEnt' => 'entity']);}])
                ->joinWith(['agraAgr' => function($query) { $query->from(['agraAgr' => 'agreement']);}])
                ->where(['agra_ab_id' => $id]);
        } else {
            $query = AgreementAnnex::find()
                ->joinWith(['agraEnt' => function($query) { $query->from(['agraEnt' => 'entity']);}])
                ->joinWith(['agraAgr' => function($query) { $query->from(['agraAgr' => 'agreement']);}]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => ['agra_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['yurLico'] = [
            'asc' => ['agraEnt.ent_name' => SORT_ASC],
            'desc' => ['agraEnt.ent_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dogovor'] = [
            'asc' => ['agraAgr.agr_number' => SORT_ASC],
            'desc' => ['agraAgr.agr_number' => SORT_DESC],
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'agra_comp_id' => $this->agra_comp_id,
            'agra_agr_id' => $this->agra_agr_id,
            'agra_ast_id' => $this->agra_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'agra_id', $this->agra_id])
            ->andFilterWhere(['like', 'agra_number', $this->agra_number])
            ->andFilterWhere(['like', 'agra_date', $this->agra_date])
            ->andFilterWhere(['like', 'agra_sum', $this->agra_sum])
            ->andFilterWhere(['like', 'agra_tax', $this->agra_tax])
            ->andFilterWhere(['like', 'agra_date', $this->agra_date])
            ->andFilterWhere(['like', 'agra_comment', $this->agra_comment])
            ->andFilterWhere(['like', 'agraEnt.ent_name', $this->yurLico])
            ->andFilterWhere(['like', 'agraAgr.agr_number', $this->dogovor])
            //->andFilterWhere(['like', 'agra_fdata', $this->agra_fdata])
            //->andFilterWhere(['like', 'agra_fdata_sign', $this->agra_fdata_sign])
            ->andFilterWhere(['like', 'agra_create_user', $this->agra_create_user])
            ->andFilterWhere(['like', 'agra_create_time', $this->agra_create_time])
            ->andFilterWhere(['like', 'agra_create_ip', $this->agra_create_ip])
            ->andFilterWhere(['like', 'agra_update_user', $this->agra_update_user])
            ->andFilterWhere(['like', 'agra_update_time', $this->agra_update_time])
            ->andFilterWhere(['like', 'agra_update_ip', $this->agra_update_ip])
        ;

        return $dataProvider;
    }
}
