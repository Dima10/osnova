<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_report".
 *
 * @property int $id
 * @property string|null $ent_name
 * @property int|null $entc_id
 * @property int|null $fb_fbt_id
 * @property string|null $svc_name
 * @property string|null $trp_name
 * @property int|null $svc_svdt_id
 * @property string|null $agra_number
 * @property float|null $price
 * @property int|null $qty
 * @property float|null $sum
 * @property float|null $ana_cost_a
 * @property string|null $aga_number
 * @property string|null $aga_date
 * @property int|null $aga_ast_id
 * @property int|null $fb_pt_id
 * @property int|null $fb_payment
 * @property string|null $fb_date
 * @property int|null $aca_id
 * @property int|null $aca_ana_id
 * @property int|null $aca_aga_id
 */
class FinanceReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entc_id', 'fb_fbt_id', 'svc_svdt_id', 'qty', 'aga_ast_id', 'fb_pt_id', 'fb_payment', 'aca_id', 'aca_ana_id', 'aca_aga_id'], 'integer'],
            [['price', 'sum', 'ana_cost_a'], 'number'],
            [['aga_date', 'fb_date'], 'safe'],
            [['ent_name', 'svc_name', 'trp_name', 'agra_number', 'aga_number'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ent_name' => 'Ent Name',
            'entc_id' => 'Entc ID',
            'fb_fbt_id' => 'Fb Fbt ID',
            'svc_name' => 'Svc Name',
            'trp_name' => 'Trp Name',
            'svc_svdt_id' => 'Svc Svdt ID',
            'agra_number' => 'Agra Number',
            'price' => 'Price',
            'qty' => 'Qty',
            'sum' => 'Sum',
            'ana_cost_a' => 'Ana Cost A',
            'aga_number' => 'Aga Number',
            'aga_date' => 'Aga Date',
            'aga_ast_id' => 'Aga Ast ID',
            'fb_pt_id' => 'Fb Pt ID',
            'fb_payment' => 'Fb Payment',
            'fb_date' => 'Fb Date',
            'aca_id' => 'Aca ID',
            'aca_ana_id' => 'Aca Ana ID',
            'aca_aga_id' => 'Aca Aga ID',
            'sum_ss' => 'Sum SS',
            'marzha' => 'Marzha',
            'agent_name' => 'Agent Name',
        ];
    }
}
