<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_command_content}}".
 *
 * @property integer $applcmdc_id
 * @property integer $applcmdc_applcmd_id
 * @property integer $applcmdc_applrc_id
 * @property integer $applcmdc_prs_id
 * @property integer $applcmdc_trp_id
 * @property string $applcmdc_agr_number
 * @property string $applcmdc_create_user
 * @property string $applcmdc_create_time
 * @property string $applcmdc_create_ip
 * @property string $applcmdc_update_user
 * @property string $applcmdc_update_time
 * @property string $applcmdc_update_ip
 *
 * @property ApplCommand $applcmdc
 * @property ApplRequestContent $applcmdcApplrc
 * @property Person $applcmdcPrs
 * @property TrainingProg $applcmdcTrp
 */
class ApplCommandContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_command_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applcmdc_applcmd_id', 'applcmdc_prs_id', 'applcmdc_trp_id'], 'required'],
            [['applcmdc_applcmd_id', 'applcmdc_applrc_id', 'applcmdc_prs_id', 'applcmdc_trp_id'], 'integer'],
            [['applcmdc_create_time', 'applcmdc_update_time'], 'safe'],
            [['applcmdc_agr_number'], 'string'],
            [['applcmdc_create_user', 'applcmdc_create_ip', 'applcmdc_update_user', 'applcmdc_update_ip'], 'string', 'max' => 64],
            [['applcmdc_applcmd_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplCommand::class, 'targetAttribute' => ['applcmdc_applcmd_id' => 'applcmd_id']],
            [['applcmdc_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['applcmdc_prs_id' => 'prs_id']],
            [['applcmdc_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['applcmdc_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applcmdc_id' => Yii::t('app', 'Applcmdc ID'),
            'applcmdc_applcmd_id' => Yii::t('app', 'Applcmdc Applcmd ID'),
            'applcmdc_applrc_id' => Yii::t('app', 'Applcmdc Applrc ID'),
            'applcmdc_prs_id' => Yii::t('app', 'Applcmdc Prs ID'),
            'applcmdc_trp_id' => Yii::t('app', 'Applcmdc Trp ID'),
            'applcmdc_agr_number' => Yii::t('app', 'Applcmdc Agr Number'),
            'applcmdc_create_user' => Yii::t('app', 'Applcmdc Create User'),
            'applcmdc_create_time' => Yii::t('app', 'Applcmdc Create Time'),
            'applcmdc_create_ip' => Yii::t('app', 'Applcmdc Create Ip'),
            'applcmdc_update_user' => Yii::t('app', 'Applcmdc Update User'),
            'applcmdc_update_time' => Yii::t('app', 'Applcmdc Update Time'),
            'applcmdc_update_ip' => Yii::t('app', 'Applcmdc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdc()
    {
        return $this->hasOne(ApplCommand::class, ['applcmd_id' => 'applcmdc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdcApplrc()
    {
        return $this->hasOne(ApplRequestContent::class, ['applrc_id' => 'applcmdc_applrc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdcPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'applcmdc_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplcmdcTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'applcmdc_trp_id']);
    }

    /**
     * @inheritdoc
     * @return ApplCommandContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplCommandContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applcmdc_create_user = Yii::$app->user->identity->username;
            $this->applcmdc_create_ip = Yii::$app->request->userIP;
            $this->applcmdc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applcmdc_update_user = Yii::$app->user->identity->username;
            $this->applcmdc_update_ip = Yii::$app->request->userIP;
            $this->applcmdc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
