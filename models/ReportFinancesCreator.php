<?php

namespace app\models;

use app\models\AgreementAcc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ReportFinancesCreator
{
    /**
     * @var \DateTime
     */
    private $dateStart;
    private $dateEnd;

    public function __construct(\DateTime $dateStart = null, \DateTime $dateEnd = null)
    {
        $this->dateStart = $dateStart;
        $this->dateEnd = $dateEnd;
    }

    public function getQueryWithPayments()
    {
        $query = (new \yii\db\Query())->select([
               'aca_id', 'aca_ana_id', 'aca_aga_id', 'aca_price as price', 'aca_qty as qty', 'fb_sum as sum', '(aca_price * aca_qty) as aca_sum',
               'ana_cost_a', 'agra_number', 'aga_number', 'aga_date', 'aga_ast_id', '(aca_qty * ana_cost_a) as sum_ss', '(fb_sum - (aca_qty * ana_cost_a)) as marzha',
               'ent_name', 'prs_full_name as agent_name', 'entc_id', 'entc_name', 'fbt_name', 'trp_name', 'svc_name', 'svc_svdt_id',
               'fb_fbt_id', 'fb_pt_id', 'fb_payment', 'fb_date'
           ])
            ->from('agreement_acc_a')
            ->leftJoin(AgreementAcc::tableName(), 'aca_aga_id = aga_id')
            ->leftJoin(Entity::tableName(), 'ent_id = aga_ab_id')
            ->leftJoin(EntityClassLnk::tableName(), 'entcl_ent_id = ent_id')
            ->leftJoin(EntityClass::tableName(), 'entc_id = entcl_entc_id')
            ->leftJoin(Person::tableName(), 'prs_id = ent_agent_id')
            ->leftJoin(FinanceBook::tableName(), 'fb_aca_id = aca_id')
            ->leftJoin(FinanceBookType::tableName(), 'fbt_id = fb_fbt_id')
            ->leftJoin(Svc::tableName(), 'fb_svc_id = svc_id')
            ->leftJoin(AgreementAnnexA::tableName(), 'aca_ana_id = ana_id')
            ->leftJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
            ->leftJoin(TrainingProg::tableName(), 'ana_trp_id = trp_id')
        ;

        $dateTime = new \DateTime();
        $startDay = $dateTime->format('Y-m-d 00:00:00');
        $endDay = $dateTime->format('Y-m-d 23:59:59');

        if ($this->dateStart != null && $this->dateEnd != null) {
            $query->andFilterWhere(['between', 'aca_create_time', $this->dateStart->format('Y-m-d 00:00:00'), $this->dateEnd->format('Y-m-d 23:59:59')]);
        } else if ($this->dateStart != null && $this->dateEnd === null) {
            $query->andFilterWhere(['between', 'aca_create_time', $this->dateStart->format('Y-m-d 00:00:00'), $endDay]);
        } else if ($this->dateStart === null && $this->dateEnd != null) {
            $query->andFilterWhere(['between', 'aca_create_time', $startDay, $this->dateEnd->format('Y-m-d 23:59:59')]);
        } else if ($this->dateStart === null && $this->dateEnd != null) {
            $query->andFilterWhere(['between', 'aca_create_time', $startDay, $endDay]);
        }

        return $query;
    }

    public function getQueryWithoutPayment()
    {
        $query = (new \yii\db\Query())->select([
               'aca_id as null',
               'ana_id as aca_ana_id',
               'aca_aga_id as null',
               'ana_price as price',
               '(ana_qty - (ifnull(finance_report.qty, 0))) as qty',
               '(ana_price * ana_qty) as sum',
               'agreement_annex_a.ana_cost_a',
               'agreementAnnex.agra_number',
               'agreementAcc.aga_number as null',
               'agreementAcc.aga_date as null',
               'agreementAcc.aga_ast_id as null',
               'prs.prs_full_name as agent_name',
               '(qty * agreement_annex_a.ana_cost_a) as sum_ss',
               '(sum - sum_ss) as marzha',
               'entity.ent_name',
               'entityClass.entc_id',
               'entityClass.entc_name',
               'fbt_name as null',
               'prog.trp_name',
               'svc.svc_name',
               'svc.svc_svdt_id',
               'fb.fb_fbt_id as null',
               'fb.fb_pt_id as null',
               'fb.fb_payment as null',
               'fb.fb_date as null',
               'finance_report.qty as fr_qty'
           ])
            ->from('agreement_annex_a')
            ->leftJoin(FinanceReport::tableName(), 'aca_ana_id = ana_id')
            ->leftJoin(AgreementAcc::tableName().' agreementAcc', 'aca_aga_id = aga_id')
            ->leftJoin(AgreementAnnex::tableName().' agreementAnnex', 'ana_agra_id = agra_id')
            ->leftJoin(Entity::tableName().' entity', 'ent_id = agra_ab_id')
            ->leftJoin(EntityClassLnk::tableName(), 'entcl_ent_id = ent_id')
            ->leftJoin(EntityClass::tableName(). ' entityClass', 'entityClass.entc_id = entcl_entc_id')
            ->leftJoin(Person::tableName().' prs', 'prs_id = ent_agent_id')
            ->leftJoin(FinanceBook::tableName().' fb', 'fb_aca_id = aca_id')
            ->leftJoin(FinanceBookType::tableName(), 'finance_book_type.fbt_id = fb.fb_fbt_id')
            ->leftJoin(Svc::tableName().' svc', 'ana_svc_id = svc_id')
            ->leftJoin(TrainingProg::tableName().' prog', 'ana_trp_id = trp_id')
            ->where('finance_report.qty!=agreement_annex_a.ana_qty || finance_report.aca_ana_id is null')
        ;

        return $query;
    }

    public function addDataWithPayments()
    {

        $report = new FinanceReport();
        $fields = array_keys($report->attributeLabels());
        unset($fields[array_search('id', $fields)]);
        $query = $this->getQueryWithPayments();

        $financeReportRows = [];

        foreach ($query->all() as $item){
            $financeReportRows[] = $this->prepareToSaveReportRow($fields, $item);
        }

        foreach (array_chunk($financeReportRows, 100) as $chunk){
            Yii::$app->db->createCommand()->batchInsert(FinanceReport::tableName(), $fields, $chunk)->execute();
        }
    }

    public function addDataWithoutPayments()
    {

        $report = new FinanceReport();
        $fields = array_keys($report->attributeLabels());
        unset($fields[array_search('id', $fields)]);
        $query = $this->getQueryWithoutPayment();

        $financeReportRows = [];

        foreach ($query->all() as $item){
            $financeReportRows[] = $this->prepareToSaveReportRow($fields, $item);
        }

        foreach (array_chunk($financeReportRows, 100) as $chunk){
            Yii::$app->db->createCommand()->batchInsert(FinanceReport::tableName(), $fields, $chunk)->execute();
        }

    }

    public function prepareToSaveReportRow($fields, array $item)
    {

        $result = [];
        foreach ($fields as $field) {
            if (isset($item[$field]) && !empty($item[$field])) {
                $result[$field] = $item[$field];
            } else {
                $result[$field] = null;
            }
        }

        return $result;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @param \DateTime $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }
}
