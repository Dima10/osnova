<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserMail;

/**
 * UserMailSearch represents the model behind the search form about `app\models\UserMail`.
 */
class UserMailSearch extends UserMail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['um_id', 'um_sm_id', 'um_umt_id', 'um_reestr', 'um_flag'], 'integer'],
            [['um_name', 'um_subj', 'um_text', 'um_to', 'um_create_user', 'um_create_time', 'um_create_ip', 'um_update_user', 'um_update_time', 'um_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserMail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'um_id' => $this->um_id,
            'um_sm_id' => $this->um_sm_id,
            'um_umt_id' => $this->um_umt_id,
            'um_reestr' => $this->um_reestr,
            'um_flag' => $this->um_flag,
        ]);

        $query->andFilterWhere(['like', 'um_name', $this->um_name])
            ->andFilterWhere(['like', 'um_subj', $this->um_subj])
            ->andFilterWhere(['like', 'um_text', $this->um_text])
            ->andFilterWhere(['like', 'um_to', $this->um_to])
            ->andFilterWhere(['like', 'um_create_user', $this->um_create_user])
            ->andFilterWhere(['like', 'um_create_time', $this->um_create_time])
            ->andFilterWhere(['like', 'um_create_ip', $this->um_create_ip])
            ->andFilterWhere(['like', 'um_update_user', $this->um_update_user])
            ->andFilterWhere(['like', 'um_update_time', $this->um_update_time])
            ->andFilterWhere(['like', 'um_update_ip', $this->um_update_ip])
        ;

        return $dataProvider;
    }
}
