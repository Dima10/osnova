<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SvcValue;

/**
 * SvcValueSearch represents the model behind the search form about `app\models\SvcValue`.
 */
class SvcValueSearch extends SvcValue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svcv_id', 'svcv_svc_id', 'svcv_svcp_id'], 'integer'],
            [['svcv_value', 'svcv_create_user', 'svcv_create_time', 'svcv_create_ip', 'svcv_update_user', 'svcv_update_time', 'svcv_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcValue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'svcv_id' => $this->svcv_id,
            'svcv_svc_id' => $this->svcv_svc_id,
            'svcv_svcp_id' => $this->svcv_svcp_id,
//            'svcv_create_time' => $this->svcv_create_time,
//            'svcv_update_time' => $this->svcv_update_time,
        ]);

        $query->andFilterWhere(['like', 'svcv_value', $this->svcv_value])
            ->andFilterWhere(['like', 'svcv_create_user', $this->svcv_create_user])
            ->andFilterWhere(['like', 'svcv_create_ip', $this->svcv_create_ip])
            ->andFilterWhere(['like', 'svcv_update_user', $this->svcv_update_user])
            ->andFilterWhere(['like', 'svcv_create_time', $this->svcv_create_time])
            ->andFilterWhere(['like', 'svcv_update_time', $this->svcv_update_time])
            ->andFilterWhere(['like', 'svcv_update_ip', $this->svcv_update_ip]);

        return $dataProvider;
    }
}
