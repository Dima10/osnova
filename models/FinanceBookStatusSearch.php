<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceBookStatus;

/**
 * FinanceBookStatusSearch represents the model behind the search form about `app\models\FinanceBookStatus`.
 */
class FinanceBookStatusSearch extends FinanceBookStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fbs_id'], 'integer'],
            [['fbs_name', 'fbs_create_user', 'fbs_create_ip', 'fbs_create_time', 'fbs_update_user', 'fbs_update_ip', 'fbs_update_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBookStatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fbs_id' => $this->fbs_id,
        ]);

        $query->andFilterWhere(['like', 'fbs_name', $this->fbs_name])
            ->andFilterWhere(['like', 'fbs_create_user', $this->fbs_create_user])
            ->andFilterWhere(['like', 'fbs_create_ip', $this->fbs_create_ip])
            ->andFilterWhere(['like', 'fbs_update_user', $this->fbs_update_user])
            ->andFilterWhere(['like', 'fbs_create_time', $this->fbs_create_time])
            ->andFilterWhere(['like', 'fbs_update_time', $this->fbs_update_time])
            ->andFilterWhere(['like', 'fbs_update_ip', $this->fbs_update_ip]);

        return $dataProvider;
    }
}
