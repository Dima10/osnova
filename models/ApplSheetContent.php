<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_sheet_content}}".
 *
 * @property integer $applsc_id
 * @property integer $applsc_appls_id
 * @property integer $applsc_trq_id
 * @property string $applsc_trq_question
 * @property integer $applsc_tra_id
 * @property string $applsc_tra_answer
 * @property integer $applsc_tra_variant
 * @property string $applsc_create_user
 * @property string $applsc_create_time
 * @property string $applsc_create_ip
 * @property string $applsc_update_user
 * @property string $applsc_update_time
 * @property string $applsc_update_ip
 *
 * @property ApplSheet $applscAppls
 * @property TrainingAnswer $applscTra
 * @property TrainingQuestion $applscTrq
 */
class ApplSheetContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_sheet_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsc_appls_id', 'applsc_trq_id'], 'required'],
            [['applsc_appls_id', 'applsc_trq_id', 'applsc_tra_id', 'applsc_tra_variant'], 'integer'],
            [['applsc_trq_question', 'applsc_tra_answer'], 'string'],
            [['applsc_create_time', 'applsc_update_time'], 'safe'],
            [['applsc_create_user', 'applsc_create_ip', 'applsc_update_user', 'applsc_update_ip'], 'string', 'max' => 64],
            [['applsc_appls_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplSheet::class, 'targetAttribute' => ['applsc_appls_id' => 'appls_id']],
            [['applsc_tra_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingAnswer::class, 'targetAttribute' => ['applsc_tra_id' => 'tra_id']],
            [['applsc_trq_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingQuestion::class, 'targetAttribute' => ['applsc_trq_id' => 'trq_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applsc_id' => Yii::t('app', 'Applsc ID'),
            'applsc_appls_id' => Yii::t('app', 'Applsc Appls ID'),
            'applsc_trq_id' => Yii::t('app', 'Applsc Trq ID'),
            'applsc_trq_question' => Yii::t('app', 'Applsc Trq Question'),
            'applsc_tra_id' => Yii::t('app', 'Applsc Tra ID'),
            'applsc_tra_answer' => Yii::t('app', 'Applsc Tra Answer'),
            'applsc_tra_variant' => Yii::t('app', 'Applsc Tra Variant'),
            'applsc_create_user' => Yii::t('app', 'Applsc Create User'),
            'applsc_create_time' => Yii::t('app', 'Applsc Create Time'),
            'applsc_create_ip' => Yii::t('app', 'Applsc Create Ip'),
            'applsc_update_user' => Yii::t('app', 'Applsc Update User'),
            'applsc_update_time' => Yii::t('app', 'Applsc Update Time'),
            'applsc_update_ip' => Yii::t('app', 'Applsc Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplscAppls()
    {
        return $this->hasOne(ApplSheet::class, ['appls_id' => 'applsc_appls_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplscTra()
    {
        return $this->hasOne(TrainingAnswer::class, ['tra_id' => 'applsc_tra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplscTrq()
    {
        return $this->hasOne(TrainingQuestion::class, ['trq_id' => 'applsc_trq_id']);
    }

    /**
     * @inheritdoc
     * @return ApplSheetContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplSheetContentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {

            if ($q = TrainingQuestion::findOne($this->applsc_trq_id)) {
                $this->applsc_trq_question = $q->trq_question;
            }

            if ($ans = TrainingAnswer::findOne($this->applsc_tra_id)) {
                $this->applsc_tra_answer = $ans->tra_answer;
                $this->applsc_tra_variant= $ans->tra_variant;
                }

            $this->applsc_create_user = Yii::$app->user->identity->username;
            $this->applsc_create_ip = Yii::$app->request->userIP;
            $this->applsc_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applsc_update_user = Yii::$app->user->identity->username;
            $this->applsc_update_ip = Yii::$app->request->userIP;
            $this->applsc_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * Return string test result
     * @return string
     */
    public function testResult()
    {
        return $this->applsc_tra_variant == 1 ? Yii::t('app', 'Correct Answer') : Yii::t('app', 'Incorrect Answer');
    }
}
