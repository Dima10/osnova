<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgreementAct;

/**
 * AgreementActSearch represents the model behind the search form about `app\models\AgreementAct`.
 */
class AgreementActSearch extends AgreementAct
{
    public $yurLico;
    public $dogovor;
    public $appNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_id', 'act_agr_id', 'act_agra_id', 'act_ab_id', 'act_comp_id', 'act_ast_id'], 'integer'],
            [['act_number', 'act_date', 'act_comment', 'act_create_user', 'act_create_time', 'act_create_ip', 'act_update_user', 'act_update_time', 'act_update_ip'], 'safe'],
            [['act_sum', 'act_tax'], 'number'],
            [['yurLico'], 'safe'],
            [['dogovor'], 'safe'],
            [['appNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $pageSize
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pageSize = 10, $id = null)
    {
        if (isset($id)) {
            $query = AgreementAct::find()
                ->joinWith(['actAb' => function($query) { $query->from(['actAb' => 'ab']);}])
                ->joinWith(['actAgr' => function($query) { $query->from(['actAgr' => 'agreement']);}])
                ->joinWith(['actAgra' => function($query) { $query->from(['actAgra' => 'agreement_annex']);}])
                ->where(['act_ab_id' => $id]);
        } else {
            $query = AgreementAct::find()
                ->joinWith(['actAb' => function($query) { $query->from(['actAb' => 'ab']);}])
                ->joinWith(['actAgr' => function($query) { $query->from(['actAgr' => 'agreement']);}])
                ->joinWith(['actAgra' => function($query) { $query->from(['actAgra' => 'agreement_annex']);}])
                ;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
            'sort' => [
                'defaultOrder' => ['act_date' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['yurLico'] = [
            'asc' => ['actAb.ab_name' => SORT_ASC],
            'desc' => ['actAb.ab_name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['dogovor'] = [
            'asc' => ['actAgr.agr_number' => SORT_ASC],
            'desc' => ['actAgr.agr_number' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['appNumber'] = [
            'asc' => ['actAgra.agra_number' => SORT_ASC],
            'desc' => ['actAgra.agra_number' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'act_id' => $this->act_id,
            'act_ab_id' => $this->act_ab_id,
            'act_comp_id' => $this->act_comp_id,
            'act_agr_id' => $this->act_agr_id,
            'act_agra_id' => $this->act_agra_id,
            'act_ast_id' => $this->act_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'act_number', $this->act_number])
            ->andFilterWhere(['like', 'act_date', $this->act_date])
            ->andFilterWhere(['like', 'actAb.ab_name', $this->yurLico])
            ->andFilterWhere(['like', 'actAgr.agr_number', $this->dogovor])
            ->andFilterWhere(['like', 'actAgra.agra_number', $this->appNumber])
            ->andFilterWhere(['like', 'act_sum', $this->act_sum])
            ->andFilterWhere(['like', 'act_tax', $this->act_tax])
            ->andFilterWhere(['like', 'act_number', $this->act_number])
            ->andFilterWhere(['like', 'act_comment', $this->act_comment])
            //->andFilterWhere(['like', 'act_fdata_sign', $this->act_data_sign])
            //->andFilterWhere(['like', 'act_create_user', $this->act_create_user])
            ->andFilterWhere(['like', 'act_create_time', $this->act_create_time])
            ->andFilterWhere(['like', 'act_create_ip', $this->act_create_ip])
            ->andFilterWhere(['like', 'act_update_user', $this->act_update_user])
            ->andFilterWhere(['like', 'act_update_time', $this->act_update_time])
            ->andFilterWhere(['like', 'act_update_ip', $this->act_update_ip])
        ;

        return $dataProvider;
    }
}
