<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TaskType;

/**
 * TaskTypeSearch represents the model behind the search form about `app\models\TaskType`.
 */
class TaskTypeSearch extends TaskType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskt_id'], 'integer'],
            [['taskt_name', 'taskt_note', 'taskt_create_user', 'taskt_create_time', 'taskt_create_ip', 'taskt_update_user', 'taskt_update_time', 'taskt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'taskt_id' => $this->taskt_id,
            'taskt_create_time' => $this->taskt_create_time,
            'taskt_update_time' => $this->taskt_update_time,
        ]);

        $query->andFilterWhere(['like', 'taskt_name', $this->taskt_name])
            ->andFilterWhere(['like', 'taskt_note', $this->taskt_note])
            ->andFilterWhere(['like', 'taskt_create_user', $this->taskt_create_user])
            ->andFilterWhere(['like', 'taskt_create_ip', $this->taskt_create_ip])
            ->andFilterWhere(['like', 'taskt_update_user', $this->taskt_update_user])
            ->andFilterWhere(['like', 'taskt_update_ip', $this->taskt_update_ip]);

        return $dataProvider;
    }
}
