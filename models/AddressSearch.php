<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Address;

/**
 * AddressSearch represents the model behind the search form about `app\models\Address`.
 */
class AddressSearch extends Address
{
    public $contragent;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_id', 'add_ab_id', 'add_addt_id', 'add_reg_id', 'add_cou_id', 'add_city_id'], 'integer'],
            [['add_index', 'add_data', 'add_create_user', 'add_create_time', 'add_create_ip', 'add_update_user', 'add_update_time', 'add_update_ip'], 'safe'],
            [['contragent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Address::find()->joinWith(['addAb' => function($query) { $query->from(['addAb' => 'ab']);}]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['contragent'] = [
            'asc' => ['addAb.ab_name' => SORT_ASC],
            'desc' => ['addAb.ab_name' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ($this->add_cou_id === '') {
            $this->add_reg_id = '';
            $this->add_city_id = '';
        }
        if ($this->add_reg_id === '') {
            $this->add_city_id = '';
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'add_id' => $this->add_id,
            'add_ab_id' => $this->add_ab_id,
            'add_addt_id' => $this->add_addt_id,
            'add_cou_id' => $this->add_cou_id,
            'add_reg_id' => $this->add_reg_id,
            'add_city_id' => $this->add_city_id,
        ]);

        $query
            ->andFilterWhere(['like', 'add_index', $this->add_index])
            ->andFilterWhere(['like', 'add_data', $this->add_data])
            ->andFilterWhere(['like', 'addAb.ab_name', $this->contragent])
            ->andFilterWhere(['like', 'add_create_user', $this->add_create_user])
            ->andFilterWhere(['like', 'add_create_ip', $this->add_create_ip])
            ->andFilterWhere(['like', 'add_update_user', $this->add_update_user])
            ->andFilterWhere(['like', 'add_create_time', $this->add_create_time])
            ->andFilterWhere(['like', 'add_update_time', $this->add_update_time])
            ->andFilterWhere(['like', 'add_update_ip', $this->add_update_ip]);

        return $dataProvider;
    }
}
