<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%svc_prog}}".
 *
 * @property integer $sprog_id
 * @property integer $sprog_svc_id
 * @property integer $sprog_trp_id
 * @property string $sprog_create_user
 * @property string $sprog_create_time
 * @property string $sprog_create_ip
 *
 * @property Svc $sprogSvc
 * @property TrainingProg $sprogTrp
 */
class SvcProg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%svc_prog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sprog_svc_id', 'sprog_trp_id'], 'required'],
            [['sprog_svc_id', 'sprog_trp_id'], 'integer'],
            [['sprog_create_time'], 'safe'],
            [['sprog_create_user', 'sprog_create_ip'], 'string', 'max' => 64],
            [['sprog_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['sprog_svc_id' => 'svc_id']],
            [['sprog_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['sprog_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sprog_id' => Yii::t('app', 'Sprog ID'),
            'sprog_svc_id' => Yii::t('app', 'Sprog Svc ID'),
            'sprog_trp_id' => Yii::t('app', 'Sprog Trp ID'),
            'sprog_create_user' => Yii::t('app', 'Sprog Create User'),
            'sprog_create_time' => Yii::t('app', 'Sprog Create Time'),
            'sprog_create_ip' => Yii::t('app', 'Sprog Create Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprogSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'sprog_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprogTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'sprog_trp_id']);
    }

    /**
     * @inheritdoc
     * @return SvcProgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SvcProgQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->sprog_create_user = Yii::$app->user->identity->username;
            $this->sprog_create_ip = Yii::$app->request->userIP;
            $this->sprog_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            return true;
        }
    }

}
