<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonVisit1;

/**
 * PersonVisit1Search represents the model behind the search form about `app\models\PersonVisit1`.
 */
class PersonVisit1Search extends PersonVisit1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pv1_id', 'pv1_pv_id', 'pv1_pve_id'], 'integer'],
            [['pv1_create_user', 'pv1_create_time', 'pv1_create_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonVisit1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pv1_id' => $this->pv1_id,
            'pv1_pv_id' => $this->pv1_pv_id,
            'pv1_pve_id' => $this->pv1_pve_id,
            'pv1_create_time' => $this->pv1_create_time,
        ]);

        $query->andFilterWhere(['like', 'pv1_create_user', $this->pv1_create_user])
            ->andFilterWhere(['like', 'pv1_create_ip', $this->pv1_create_ip]);

        return $dataProvider;
    }
}
