<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%company}}".
 *
 * @property integer $comp_id
 * @property integer $comp_ent_id
 * @property string $comp_name
 * @property string $comp_note
 * @property string $comp_create_user
 * @property string $comp_create_time
 * @property string $comp_create_ip
 * @property string $comp_update_user
 * @property string $comp_update_time
 * @property string $comp_update_ip
 *
 * @property Agreement[] $agreements
 * @property AgreementAcc[] $agreementAccs
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnex[] $agreementAnnexes
 * @property FinanceBook[] $financeBooks
 * @property Entity $compEnt
 *
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_ent_id'], 'required'],
            [['comp_ent_id'], 'integer'],
            [['comp_name'], 'string', 'max' => 192],
            [['comp_note'], 'string'],
            [['comp_create_time', 'comp_update_time'], 'safe'],
            [['comp_create_user', 'comp_create_ip', 'comp_update_user', 'comp_update_ip'], 'string', 'max' => 64],
            [['comp_ent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::class, 'targetAttribute' => ['comp_ent_id' => 'ent_id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comp_id' => Yii::t('app', 'Comp ID'),
            'comp_ent_id' => Yii::t('app', 'Comp Ent ID'),
            'comp_name' => Yii::t('app', 'Comp Name'),
            'comp_note' => Yii::t('app', 'Comp Note'),
            'comp_create_user' => Yii::t('app', 'Comp Create User'),
            'comp_create_time' => Yii::t('app', 'Comp Create Time'),
            'comp_create_ip' => Yii::t('app', 'Comp Create Ip'),
            'comp_update_user' => Yii::t('app', 'Comp Update User'),
            'comp_update_time' => Yii::t('app', 'Comp Update Time'),
            'comp_update_ip' => Yii::t('app', 'Comp Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['agr_comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccs()
    {
        return $this->hasMany(AgreementAcc::class, ['aga_comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBooks()
    {
        return $this->hasMany(FinanceBook::class, ['fb_comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompEnt()
    {
        return $this->hasOne(Entity::class, ['ent_id' => 'comp_ent_id']);
    }

    /**
     * @inheritdoc
     * @return CompanyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->comp_name = Entity::findOne($this->comp_ent_id)->ent_name;

        if (!parent::beforeSave($insert)) return false;

        if ($insert) {

            $this->comp_create_user = Yii::$app->user->identity->username;
            $this->comp_create_ip = Yii::$app->request->userIP;
            $this->comp_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->comp_update_user = Yii::$app->user->identity->username;
            $this->comp_update_ip = Yii::$app->request->userIP;
            $this->comp_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
