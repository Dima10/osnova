<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SvcDocType;

/**
 * SvcDocTypeSearch represents the model behind the search form about `app\models\SvcDocType`.
 */
class SvcDocTypeSearch extends SvcDocType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['svdt_id'], 'integer'],
            [['svdt_name', 'svdt_create_user', 'svdt_create_time', 'svdt_create_ip', 'svdt_update_user', 'svdt_update_time', 'svdt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SvcDocType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'svdt_id' => $this->svdt_id,
//            'svdt_create_time' => $this->svdt_create_time,
//            'svdt_update_time' => $this->svdt_update_time,
        ]);

        $query->andFilterWhere(['like', 'svdt_name', $this->svdt_name])
            ->andFilterWhere(['like', 'svdt_create_user', $this->svdt_create_user])
            ->andFilterWhere(['like', 'svdt_create_ip', $this->svdt_create_ip])
            ->andFilterWhere(['like', 'svdt_update_user', $this->svdt_update_user])
            ->andFilterWhere(['like', 'svdt_create_time', $this->svdt_create_time])
            ->andFilterWhere(['like', 'svdt_update_time', $this->svdt_update_time])
            ->andFilterWhere(['like', 'svdt_update_ip', $this->svdt_update_ip]);

        return $dataProvider;
    }
}
