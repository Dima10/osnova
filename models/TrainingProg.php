<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_prog}}".
 *
 * @property integer $trp_id
 * @property string $trp_reestr
 * @property string $trp_name
 * @property string $trp_code
 * @property integer $trp_hour
 * @property string $trp_comment
 * @property int $trp_flag [int(11)]
 * @property resource $trp_dataA1
 * @property resource $trp_dataA2
 * @property string $trp_dataA1_name
 * @property string $trp_dataA2_name
 * @property resource $trp_dataB1
 * @property resource $trp_dataB2
 * @property string $trp_dataB1_name
 * @property string $trp_dataB2_name
 * @property integer $trp_test_question
 * @property string $trp_create_user
 * @property string $trp_create_time
 * @property string $trp_create_ip
 * @property string $trp_update_user
 * @property string $trp_update_time
 * @property string $trp_update_ip
 *
 * @property AgreementAnnexA[] $agreementAnnexAs
 * @property SvcProg[] $svcProgs
 * @property TrainingProgModule[] $trainingProgModules
 *
 */
class TrainingProg extends \yii\db\ActiveRecord
{

    public $applrcApplrId;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_prog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trp_name', 'trp_code', 'trp_hour', 'trp_reestr'], 'required'],
            [['trp_hour', 'trp_test_question', 'trp_flag'], 'integer'],
            [['trp_dataA1', 'trp_dataA2', 'trp_dataB1', 'trp_dataB2'], 'string'],
            [['trp_create_time', 'trp_update_time'], 'safe'],
            [['trp_code'], 'string', 'max' => 128],
            [['trp_name'], 'string', 'max' => 512],
            [['trp_comment'], 'string', 'max' => 1024],
            [['trp_dataA1_name', 'trp_dataA2_name', 'trp_dataB1_name', 'trp_dataB2_name'], 'string', 'max' => 256],
            [['trp_create_user', 'trp_create_ip', 'trp_update_user', 'trp_update_ip'], 'string', 'max' => 64],
            [['trp_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trp_id' => Yii::t('app', 'Trp ID'),
            'trp_reestr' => Yii::t('app', 'Trp Reestr'),
            'trp_name' => Yii::t('app', 'Trp Name'),
            'trp_code' => Yii::t('app', 'Trp Code'),
            'trp_hour' => Yii::t('app', 'Trp Hour'),
            'trp_comment' => Yii::t('app', 'Trp Comment'),
            'trp_dataA1' => Yii::t('app', 'Trp Data A1'),
            'trp_dataA2' => Yii::t('app', 'Trp Data A2'),
            'trp_dataA1_name' => Yii::t('app', 'Trp Data A1 Name'),
            'trp_dataA2_name' => Yii::t('app', 'Trp Data A2 Name'),
            'trp_dataB1' => Yii::t('app', 'Trp Data B1'),
            'trp_dataB2' => Yii::t('app', 'Trp Data B2'),
            'trp_dataB1_name' => Yii::t('app', 'Trp Data B1 Name'),
            'trp_dataB2_name' => Yii::t('app', 'Trp Data B2 Name'),
            'trp_test_question' => Yii::t('app', 'Trp Test Question'),
            'trp_create_user' => Yii::t('app', 'Trp Create User'),
            'trp_create_time' => Yii::t('app', 'Trp Create Time'),
            'trp_create_ip' => Yii::t('app', 'Trp Create Ip'),
            'trp_update_user' => Yii::t('app', 'Trp Update User'),
            'trp_update_time' => Yii::t('app', 'Trp Update Time'),
            'trp_update_ip' => Yii::t('app', 'Trp Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexAs()
    {
        return $this->hasMany(AgreementAnnexA::class, ['ana_trp_id' => 'trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestContent()
    {
        return $this->hasOne(ApplRequestContent::class, ['applrc_trp_id' => 'trp_id']);
    }

    /**
     * @inheritdoc
     * @return \yii\db\ActiveQuery
     */
    public function getSvcProgs()
    {
        return $this->hasMany(SvcProg::class, ['sprog_trp_id' => 'trp_id']);
    }

    /**
     * @inheritdoc
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingProgModules()
    {
        return $this->hasMany(TrainingProgModule::class, ['trpl_trp_id' => 'trp_id']);
    }

    /**
     *
     * @return string
     */
    public function getProgName()
    {
        return $this->trp_name .' ('.$this->trp_hour.')';
    }

    public static function find()
    {
        return new TrainingProgQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->trp_create_user = Yii::$app->user->identity->username;
            $this->trp_create_ip = Yii::$app->request->userIP;
            $this->trp_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->trp_update_user = Yii::$app->user->identity->username;
            $this->trp_update_ip = Yii::$app->request->userIP;
            $this->trp_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
