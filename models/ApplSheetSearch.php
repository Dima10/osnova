<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplSheet;

/**
 * ApplSheetSearch represents the model behind the search form about `app\models\ApplSheet`.
 */
class ApplSheetSearch extends ApplSheet
{
    public $prs_full_name;
    public $prs_connect_user;
    public $applr_number;
//    public $applr_reestr;
    public $applr_flag;
    public $person_prs_full_name;
    public $trp_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appls_id', 'appls_prs_id', 'appls_trp_id', 'appls_trt_id', 'appls_score_max',
                'appls_score', 'appls_passed', 'appls_reestr', 'applr_flag'], 'integer'],
            [['appls_magic', 'prs_full_name', 'trp_name', 'prs_connect_user', 'applr_number'], 'string'],
            [['appls_number', 'appls_date', 'appls_create_user', 'appls_create_time', 'appls_create_ip', 'appls_update_user', 'appls_update_time', 'appls_update_ip'], 'safe'],
            [['person_prs_full_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (!is_null($id)) {
            $query = ApplSheet::find()
//                ->leftJoin(ApplRequestContent::tableName(), 'applrc_prs_id = appls_prs_id')
                ->leftJoin(ApplMain::tableName(), 'appls_applm_id = applm_id')
                ->leftJoin(ApplRequest::tableName().' as request ', 'appls_applr_id = applr_id')
                ->innerJoin(Person::tableName().' as person', 'appls_prs_id = prs_id')
                ->innerJoin(TrainingProg::tableName().' as training', 'appls_trp_id = trp_id')
                ->where(['applm_ab_id' => $id]);
        } else {
            $query = ApplSheet::find()
                ->leftJoin(ApplRequest::tableName().' as request ', 'appls_applr_id = applr_id')
                ->innerJoin(Person::tableName().' as person', 'appls_prs_id = prs_id')
                ->innerJoin(TrainingProg::tableName().' as training', 'appls_trp_id = trp_id')
            ;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['appls_date' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['training.trp_name' => SORT_ASC],
            'desc' => ['training.trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applr_flag'] = [
            'asc' => ['applr_flag' => SORT_ASC],
            'desc' => ['applr_flag' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['person_prs_full_name'] = [
            'asc' => ['person.prs_full_name' => SORT_ASC],
            'desc' => ['person.prs_full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applr_number'] = [
            'asc' => ['applr_number' => SORT_ASC],
            'desc' => ['applr_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_full_name'] = [
            'asc' => ['prs_full_name' => SORT_ASC],
            'desc' => ['prs_full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['appls_reestr'] = [
            'asc' => ['appls_reestr' => SORT_ASC],
            'desc' => ['appls_reestr' => SORT_DESC],
        ];


        $dataProvider->sort->attributes['prs_connect_user'] = [
            'asc' => ['prs_connect_user' => SORT_ASC],
            'desc' => ['prs_connect_user' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->appls_reestr;
        if (User::isSpecAdmin()) {
            $reestr = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'appls_id' => $this->appls_id,
            'appls_prs_id' => $this->appls_prs_id,
//            'appls_trp_id' => $this->appls_trp_id,
            'appls_score_max' => $this->appls_score_max,
            'appls_score' => $this->appls_score,
            'appls_passed' => $this->appls_passed,
            'appls_reestr' => $reestr,
            'applr_flag' => $this->applr_flag,
            'appls_trt_id' => $this->appls_trt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'appls_magic', $this->appls_magic])
            ->andFilterWhere(['like', 'prs_connect_user', $this->prs_connect_user])
            ->andFilterWhere(['like', 'applr_number', $this->applr_number])
            ->andFilterWhere(['like', 'person.prs_full_name', $this->person_prs_full_name])
            ->andFilterWhere(['like', 'training.trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'appls_date', $this->appls_date])
            ->andFilterWhere(['like', 'appls_create_time', $this->appls_create_time])
            ->andFilterWhere(['like', 'appls_update_time', $this->appls_update_time])
            ->andFilterWhere(['like', 'appls_number', $this->appls_number])
            ->andFilterWhere(['like', 'appls_create_user', $this->appls_create_user])
            ->andFilterWhere(['like', 'appls_create_time', $this->appls_create_time])
            ->andFilterWhere(['like', 'appls_create_ip', $this->appls_create_ip])
            ->andFilterWhere(['like', 'appls_update_user', $this->appls_update_user])
            ->andFilterWhere(['like', 'appls_update_time', $this->appls_update_time])
            ->andFilterWhere(['like', 'appls_update_ip', $this->appls_update_ip])
        ;

        return $dataProvider;
    }
}
