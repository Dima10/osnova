<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_module_person}}".
 *
 * @property integer $tmp_id
 * @property integer $tmp_prs_id
 * @property integer $tmp_trm_id
 * @property string $tmp_create_user
 * @property string $tmp_create_time
 * @property string $tmp_create_ip
 * @property string $tmp_update_user
 * @property string $tmp_update_time
 * @property string $tmp_update_ip
 *
 * @property TrainingModule $tmpTrm
 * @property Person $tmpPrs
 */
class TrainingModulePerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_module_person}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tmp_prs_id', 'tmp_trm_id'], 'required'],
            [['tmp_prs_id', 'tmp_trm_id'], 'integer'],
            [['tmp_create_time', 'tmp_update_time'], 'safe'],
            [['tmp_create_user', 'tmp_create_ip', 'tmp_update_user', 'tmp_update_ip'], 'string', 'max' => 64],
            [['tmp_trm_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingModule::class, 'targetAttribute' => ['tmp_trm_id' => 'trm_id']],
            [['tmp_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['tmp_prs_id' => 'prs_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tmp_id' => Yii::t('app', 'Tmp ID'),
            'tmp_prs_id' => Yii::t('app', 'Tmp Prs ID'),
            'tmp_trm_id' => Yii::t('app', 'Tmp Trm ID'),
            'tmp_create_user' => Yii::t('app', 'Tmp Create User'),
            'tmp_create_time' => Yii::t('app', 'Tmp Create Time'),
            'tmp_create_ip' => Yii::t('app', 'Tmp Create Ip'),
            'tmp_update_user' => Yii::t('app', 'Tmp Update User'),
            'tmp_update_time' => Yii::t('app', 'Tmp Update Time'),
            'tmp_update_ip' => Yii::t('app', 'Tmp Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmpTrm()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'tmp_trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmpPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'tmp_prs_id']);
    }

    /**
     * @inheritdoc
     * @return TrainingModulePersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingModulePersonQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tmp_create_user = Yii::$app->user->identity->username;
            $this->tmp_create_ip = Yii::$app->request->userIP;
            $this->tmp_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->tmp_update_user = Yii::$app->user->identity->username;
            $this->tmp_update_ip = Yii::$app->request->userIP;
            $this->tmp_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
