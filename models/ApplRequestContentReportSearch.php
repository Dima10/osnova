<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplRequestContent;

/**
 * ApplRequestContentSearch represents the model behind the search form about `app\models\ApplRequestContent`.
 */
class ApplRequestContentReportSearch extends ApplRequestContent
{
    public $applr_number;
    public $applr_date;
    public $prs_id;

    public $prs_connect_user;
    public $prs_full_name;
    public $prs_last_name;
    public $prs_first_name;
    public $prs_middle_name;
    public $stf_position;
    public $svc_name;
    public $trp_name;
    public $ab_name;
    public $svc_hour;
    public $svdt_name;
    public $agr_number;
    public $agra_number;
    public $ast_name;
    public $applr_flag;
    public $applr_reestr;
    public $manager;
    public $entc_name;
    public $agra_comment;
    public $aga_number;
    public $applf_number;
    public $applf_end_date;
    public $applf_cmd_date;
    public $contacts;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applrc_id', 'applrc_applr_id', 'applrc_prs_id', 'applrc_svc_id', 'prs_id', 'applr_reestr'], 'integer'],
            [['applrc_date_upk', 'applr_number', 'applr_date', 'prs_connect_user', 'prs_full_name',
                'prs_last_name', 'prs_first_name', 'prs_middle_name', 'stf_position',
                'applf_cmd_date', 'entc_name', 'agra_number'], 'string'],
            [['svc_name', 'trp_name', 'ab_name', 'svc_hour', 'svdt_name', 'agr_number', 'ast_name', 'applr_flag', 'manager', 'aga_number', 'agra_comment', 'applf_number', 'applf_end_date', 'contacts'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new \yii\db\Query())->select([
            'applr_number', 'applr_date', 'prs_id', 'prs_connect_user', 'prs_connect_pwd',
            '(SELECT GROUP_CONCAT(con_text) FROM contact AS cont WHERE cont.con_ab_id = prs_id AND cont.con_cont_id = 1) as contacts',
            'prs_full_name', 'prs_last_name', 'prs_first_name', 'prs_middle_name',
            '(SELECT GROUP_CONCAT(stf_position) FROM staff AS stff WHERE stff.stf_prs_id = prs_id) as stf_position',
            'ab1.ab_name as ab_name',
            '(SELECT GROUP_CONCAT(ec.entc_name) FROM entity_class_lnk ecl LEFT JOIN entity_class ec ON ecl.entcl_entc_id = ec.entc_id WHERE ecl.entcl_ent_id = ent_id) as entc_name',
            'agra_comment', 'ab2.ab_name as manager', 'agr_number', 'agra_number',
            '(SELECT GROUP_CONCAT(CONCAT(aga_number, " -", (SELECT ast_name FROM agreement_status AS agrsts WHERE ast_id = aga_ast_id)) ORDER BY aga_number) FROM agreement_acc AS agacc WHERE agacc.aga_agr_id = agr_id AND agacc.aga_prs_id = applrc_prs_id) as aga_number',
            'svdt_name', 'svc_name', 'trp_name',
            'svc_hour', 'applf_cmd_date', 'applf_end_date', 'applf_number', 'applr_reestr', 'applr_flag', 'ast_name' ])
            ->from('appl_request_content')
            ->leftJoin(Person::tableName(), 'applrc_prs_id = prs_id')
            ->leftJoin(Svc::tableName(), 'applrc_svc_id = svc_id')
            ->leftJoin(TrainingProg::tableName(), 'applrc_trp_id = trp_id')
            ->leftJoin(SvcDocType::tableName(), 'svc_svdt_id = svdt_id')
            ->leftJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
            ->leftJoin(AgreementAnnex::tableName(), 'applr_agra_id = agra_id')
            ->leftJoin(Agreement::tableName(), 'agr_id =agra_agr_id')
            ->leftJoin(Ab::tableName().' as ab1', 'applr_ab_id = ab1.ab_id')
            ->leftJoin(Entity::tableName(), 'ab_id = ent_id')
            ->leftJoin(AgreementStatus::tableName(), 'applr_ast_id = ast_id')
            ->leftJoin(ApplFinal::tableName(), 'applrc_applf_id = applf_id')
            ->leftJoin(Ab::tableName().' as ab2', 'applr_manager_id = ab2.ab_id')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['applr_number'] = [
            'asc' => ['applr_number' => SORT_ASC],
            'desc' => ['applr_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applr_date'] = [
            'asc' => ['applr_date' => SORT_ASC],
            'desc' => ['applr_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_id'] = [
            'asc' => ['person.prs_id' => SORT_ASC],
            'desc' => ['person.prs_id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_connect_user'] = [
            'asc' => ['prs_connect_user' => SORT_ASC],
            'desc' => ['prs_connect_user' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_connect_pwd'] = [
            'asc' => ['prs_connect_pwd' => SORT_ASC],
            'desc' => ['prs_connect_pwd' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['contacts'] = [
            'asc' => ['contacts' => SORT_ASC],
            'desc' => ['contacts' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_full_name'] = [
            'asc' => ['prs_full_name' => SORT_ASC],
            'desc' => ['prs_full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_last_name'] = [
            'asc' => ['prs_last_name' => SORT_ASC],
            'desc' => ['prs_last_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_first_name'] = [
            'asc' => ['prs_first_name' => SORT_ASC],
            'desc' => ['prs_first_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['prs_middle_name'] = [
            'asc' => ['prs_middle_name' => SORT_ASC],
            'desc' => ['prs_middle_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['stf_position'] = [
            'asc' => ['stf_position' => SORT_ASC],
            'desc' => ['stf_position' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ab_name'] = [
            'asc' => ['ab_name' => SORT_ASC],
            'desc' => ['ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['entc_name'] = [
            'asc' => ['entc_name' => SORT_ASC],
            'desc' => ['entc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agra_comment'] = [
            'asc' => ['agra_comment' => SORT_ASC],
            'desc' => ['agra_comment' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['manager'] = [
            'asc' => ['manager' => SORT_ASC],
            'desc' => ['manager' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agr_number'] = [
            'asc' => ['agr_number' => SORT_ASC],
            'desc' => ['agr_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['agra_number'] = [
            'asc' => ['agra_number' => SORT_ASC],
            'desc' => ['agra_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['aga_number'] = [
            'asc' => ['aga_number' => SORT_ASC],
            'desc' => ['aga_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svdt_name'] = [
            'asc' => ['svdt_name' => SORT_ASC],
            'desc' => ['svdt_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svc_name'] = [
            'asc' => ['svc_name' => SORT_ASC],
            'desc' => ['svc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['trp_name' => SORT_ASC],
            'desc' => ['trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svc_hour'] = [
            'asc' => ['svc_hour' => SORT_ASC],
            'desc' => ['svc_hour' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applf_cmd_date'] = [
            'asc' => ['applf_cmd_date' => SORT_ASC],
            'desc' => ['applf_cmd_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applf_end_date'] = [
            'asc' => ['applf_end_date' => SORT_ASC],
            'desc' => ['applf_end_date' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applf_number'] = [
            'asc' => ['applf_number' => SORT_ASC],
            'desc' => ['applf_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applr_reestr'] = [
            'asc' => ['applr_reestr' => SORT_ASC],
            'desc' => ['applr_reestr' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applr_flag'] = [
            'asc' => ['applr_flag' => SORT_ASC],
            'desc' => ['applr_flag' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ast_name'] = [
            'asc' => ['ast_name' => SORT_ASC],
            'desc' => ['ast_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->applr_reestr;
        if(User::isSpecAdmin()) {
            $reestr = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applrc_id' => $this->applrc_id,
            'applrc_applr_id' => $this->applrc_applr_id,
            'applrc_prs_id' => $this->applrc_prs_id,
            'applrc_svc_id' => $this->applrc_svc_id,
            'prs_id' => $this->prs_id,
            'applr_flag' => $this->applr_flag,
            'applr_reestr' => $reestr,
            'svdt_id' => $this->svdt_name,
            'applr_ast_id' => $this->ast_name
        ]);

        $query
            ->andFilterWhere(['like', 'applrc_date_upk', $this->applrc_date_upk])
            ->andFilterWhere(['like', 'applrc_create_user', $this->applrc_create_user])
            ->andFilterWhere(['like', 'applrc_create_time', $this->applrc_create_time])
            ->andFilterWhere(['like', 'applrc_create_ip', $this->applrc_create_ip])
            ->andFilterWhere(['like', 'applrc_update_user', $this->applrc_update_user])
            ->andFilterWhere(['like', 'applrc_update_time', $this->applrc_update_time])
            ->andFilterWhere(['like', 'applrc_update_ip', $this->applrc_update_ip])
            ->andFilterWhere(['like', 'applr_number', $this->applr_number])
            ->andFilterWhere(['like', 'applr_date', $this->applr_date])
            ->andFilterHaving(['like', 'aga_number', $this->aga_number])
            ->andFilterWhere(['like', 'agra_number', $this->agra_number])
            ->andFilterWhere(['like', 'prs_connect_user', $this->prs_connect_user])
            ->andFilterHaving(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'prs_full_name', $this->prs_full_name])
            ->andFilterWhere(['like', 'prs_last_name', $this->prs_last_name])
            ->andFilterWhere(['like', 'prs_first_name', $this->prs_first_name])
            ->andFilterWhere(['like', 'prs_middle_name', $this->prs_middle_name])
            ->andFilterHaving(['like', 'stf_position', $this->stf_position])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'ab1.ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'svc_hour', $this->svc_hour])
            ->andFilterWhere(['like', 'agr_number', $this->agr_number])
            ->andFilterWhere(['like', 'ab2.ab_name', $this->manager])
            ->andFilterWhere(['like', 'agra_comment', $this->agra_comment])
            ->andFilterWhere(['like', 'applf_number', $this->applf_number])
            ->andFilterWhere(['like', 'applf_end_date', $this->applf_end_date])
            ->andFilterWhere(['like', 'applf_cmd_date', $this->applf_cmd_date])
            ->andFilterHaving(['like', 'entc_name', $this->entc_name])
        ;

        return $dataProvider;
    }
}
