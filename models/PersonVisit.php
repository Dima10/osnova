<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%person_visit}}".
 *
 * @property int $pv_id
 * @property int $pv_prs_id
 * @property string $pv_addr
 * @property int $pv_time
 * @property string $pv_session_id
 * @property string $pv_create_user
 * @property string $pv_create_time
 * @property string $pv_update_user
 * @property string $pv_update_time
 *
 * @property Person $pvPrs
 * @property PersonVisit1[] $personVisit1s
 */
class PersonVisit extends \yii\db\ActiveRecord
{
    const EVENT_PERSON_VISIT = 'event_person_visit';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%person_visit}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pv_prs_id'], 'required'],
            [['pv_prs_id', 'pv_time'], 'integer'],
            [['pv_create_time', 'pv_update_time'], 'safe'],
            [['pv_session_id', 'pv_addr', 'pv_create_user', 'pv_update_user'], 'string', 'max' => 64],
            [['pv_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['pv_prs_id' => 'prs_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pv_id' => Yii::t('app', 'Pv ID'),
            'pv_prs_id' => Yii::t('app', 'Pv Prs ID'),
            'pv_addr' => Yii::t('app', 'Pv Addr'),
            'pv_time' => Yii::t('app', 'Pv Time'),
            'pv_session_id' => Yii::t('app', 'Pv Session Id'),
            'pv_create_user' => Yii::t('app', 'Pv Create User'),
            'pv_create_time' => Yii::t('app', 'Pv Create Time'),
            'pv_update_user' => Yii::t('app', 'Pv Update User'),
            'pv_update_time' => Yii::t('app', 'Pv Update Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPvPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'pv_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonVisit1s()
    {
        return $this->hasMany(PersonVisit1::class, ['pv1_pv_id' => 'pv_id']);
    }

    /**
     * {@inheritdoc}
     * @return PersonVisitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersonVisitQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->pv_create_user = Yii::$app->user->identity->username;
            $this->pv_create_time = date('Y-m-d H:i:s');
            $this->pv_update_time = date('Y-m-d H:i:s');
            $this->pv_addr = Yii::$app->request->userIP;
            $this->pv_time = 0;
            $this->pv_session_id = yii::$app->session->id;
            return true;
        } else {
            $this->pv_update_user = Yii::$app->user->identity->username;
            $this->pv_update_time = date('Y-m-d H:i:s');
            $this->pv_addr = Yii::$app->request->userIP;

            if ($interval = \DateTime::createFromFormat("Y-m-d H:i:s", $this->pv_create_time)->diff(\DateTime::createFromFormat("Y-m-d H:i:s", $this->pv_update_time))) {
                $this->pv_time =
                  ($interval->i)
                + ($interval->h * 60)
                + ($interval->d * 60 * 24)
                + ($interval->m * 60 * 24 * 30)
                + ($interval->y * 60 * 24 * 365);

            } else {
                $this->pv_time = 0;
            }

            return true;
        }
    }

    /**
     * Insert|Update present time
     * @param \yii\base\Event $event
     */
    public static function updateState($event)
    {
        if (Yii::$app->user->isGuest)
            return;
        if ($prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]))
        {
            if (isset(Yii::$app->session[PersonVisit::class]))
            {
                if ($pv = PersonVisit::findOne(Yii::$app->session[PersonVisit::class])) {
                    if (!$pv->save()) {
                        Yii::$app->session->setFlash('PersonVisit', json_encode($pv->errors, JSON_UNESCAPED_UNICODE));
                    } else {
                        Yii::$app->session[PersonVisit::class] = $pv->pv_id;
                    }
                    return;
                } else {
                    $pv = new PersonVisit;
                    $pv->pv_prs_id = $prs->prs_id;
                    if (!$pv->save()) {
                        Yii::$app->session->setFlash('PersonVisit', json_encode($pv->errors, JSON_UNESCAPED_UNICODE));
                    } else {
                        Yii::$app->session[PersonVisit::class] = $pv->pv_id;
                    }
                    return;
                }
            } else {
                $pv = new PersonVisit;
                $pv->pv_prs_id = $prs->prs_id;
                if (!$pv->save()) {
                    Yii::$app->session->setFlash('PersonVisit', json_encode($pv->errors, JSON_UNESCAPED_UNICODE));
                } else {
                    Yii::$app->session[PersonVisit::class] = $pv->pv_id;
                }
                return;
            }

        }
    }
}
