<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%agreement_annex_a}}".
 *
 * Приложения Детали
 *
 * @property integer $ana_id
 * @property integer $ana_agra_id
 * @property integer $ana_svc_id
 * @property integer $ana_trp_id
 * @property string $ana_cost_a
 * @property string $ana_price_a
 * @property integer $ana_qty
 * @property string $ana_price
 * @property string $ana_tax
 * @property string $ana_create_user
 * @property string $ana_create_time
 * @property string $ana_create_ip
 * @property string $ana_update_user
 * @property string $ana_update_time
 * @property string $ana_update_ip
 *
 * @property AgreementAccA[] $agreementAccAs
 * @property AgreementAnnex $anaAgra
 * @property Svc $anaSvc
 * @property TrainingProg $anaTrp
 *
 */
class AgreementAnnexA extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agreement_annex_a}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ana_agra_id', 'ana_svc_id'], 'required'],
            [['ana_agra_id', 'ana_svc_id', 'ana_trp_id', 'ana_qty'], 'integer'],
            [['ana_cost_a', 'ana_price_a', 'ana_price', 'ana_tax'], 'number'],
            [['ana_create_time', 'ana_update_time'], 'safe'],
            [['ana_create_user', 'ana_create_ip', 'ana_update_user', 'ana_update_ip'], 'string', 'max' => 64],
            [['ana_agra_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAnnex::class, 'targetAttribute' => ['ana_agra_id' => 'agra_id']],
            [['ana_svc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Svc::class, 'targetAttribute' => ['ana_svc_id' => 'svc_id']],
            [['ana_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['ana_trp_id' => 'trp_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ana_id' => Yii::t('app', 'Ana ID'),
            'ana_agra_id' => Yii::t('app', 'Ana Agra ID'),
            'ana_svc_id' => Yii::t('app', 'Ana Svc ID'),
            'ana_trp_id' => Yii::t('app', 'Ana Trp ID'),
            'ana_cost_a' => Yii::t('app', 'Ana Cost A'),
            'ana_price_a' => Yii::t('app', 'Ana Price A'),
            'ana_qty' => Yii::t('app', 'Ana Qty'),
            'ana_price' => Yii::t('app', 'Ana Price'),
            'ana_tax' => Yii::t('app', 'Ana Tax'),
            'ana_create_user' => Yii::t('app', 'Ana Create User'),
            'ana_create_time' => Yii::t('app', 'Ana Create Time'),
            'ana_create_ip' => Yii::t('app', 'Ana Create Ip'),
            'ana_update_user' => Yii::t('app', 'Ana Update User'),
            'ana_update_time' => Yii::t('app', 'Ana Update Time'),
            'ana_update_ip' => Yii::t('app', 'Ana Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAccAs()
    {
        return $this->hasMany(AgreementAccA::class, ['aca_ana_id' => 'ana_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnaAgra()
    {
        return $this->hasOne(AgreementAnnex::class, ['agra_id' => 'ana_agra_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnaSvc()
    {
        return $this->hasOne(Svc::class, ['svc_id' => 'ana_svc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnaTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'ana_trp_id']);
    }

    /**
     * @inheritdoc
     * @return AgreementAnnexAQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgreementAnnexAQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $svc = Svc::findOne($this->ana_svc_id);
            $this->ana_price_a = $svc->svc_price;
            $this->ana_cost_a = $svc->svc_cost;

            $this->ana_create_user = Yii::$app->user->identity->username;
            $this->ana_create_ip = Yii::$app->request->userIP;
            $this->ana_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->ana_update_user = Yii::$app->user->identity->username;
            $this->ana_update_ip = Yii::$app->request->userIP;
            $this->ana_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
