<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_sh_x}}".
 *
 * Ведомость тестов
 *
 * @property integer $applsx_id
 * @property string $applsx_number
 * @property string $applsx_date
 * @property integer $applsx_trt_id
 * @property integer $applsx_svdt_id

 * @property integer $applsx_pat_id
 * @property string $applsx_file_name
 * @property resource $applsx_file_data

 * @property string $applsx_create_user
 * @property string $applsx_create_time
 * @property string $applsx_create_ip
 * @property string $applsx_update_user
 * @property string $applsx_update_time
 * @property string $applsx_update_ip
 *
 * @property ApplShXContent[] $applShXContents
 * @property TrainingType $applsTrt
 * @property SvcDocType $applsSvdt
 * @property Pattern $applsPat
 *
 */
class ApplShX extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_sh_x}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsx_number', 'applsx_date', 'applsx_trt_id'], 'required'],
            [['applsx_date', 'applsx_create_time', 'applsx_update_time'], 'safe'],
            [['applsx_trt_id', 'applsx_pat_id', 'applsx_svdt_id'], 'integer'],
            [['applsx_file_name'], 'string'],
            [['applsx_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, pdf'],

            [['applsx_number'], 'unique'],

            [['applsx_number', 'applsx_create_user', 'applsx_create_ip', 'applsx_update_user', 'applsx_update_ip'], 'string', 'max' => 64],
            [['applsx_number'], 'unique'],
            [['applsx_trt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['applsx_trt_id' => 'trt_id']],
            [['applsx_svdt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['applsx_svdt_id' => 'svdt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applsx_id' => Yii::t('app', 'Applsx ID'),
            'applsx_number' => Yii::t('app', 'Applsx Number'),
            'applsx_date' => Yii::t('app', 'Applsx Date'),
            'applsx_trt_id' => Yii::t('app', 'Applsx Trt ID'),
            'applsx_svdt_id' => Yii::t('app', 'Applsx Svdt ID'),
            'applsx_pat_id' => Yii::t('app', 'Applsx Pat ID'),
            'applsx_file_name' => Yii::t('app', 'Applsx File Name'),
            'applsx_file_data' => Yii::t('app', 'Applsx File Data'),
            'applsx_create_user' => Yii::t('app', 'Applsx Create User'),
            'applsx_create_time' => Yii::t('app', 'Applsx Create Time'),
            'applsx_create_ip' => Yii::t('app', 'Applsx Create Ip'),
            'applsx_update_user' => Yii::t('app', 'Applsx Update User'),
            'applsx_update_time' => Yii::t('app', 'Applsx Update Time'),
            'applsx_update_ip' => Yii::t('app', 'Applsx Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'applsx_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'applsx_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsxPat()
    {
        return $this->hasOne(Pattern::class, ['pat_id' => 'applsx_pat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplShXContents()
    {
        return $this->hasMany(ApplShXContent::class, ['applsxc_applsx_id' => 'applsx_id']);
    }

    /**
     * @inheritdoc
     * @return ApplShXQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplShXQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->applsx_create_user = Yii::$app->user->identity->username;
            $this->applsx_create_ip = Yii::$app->request->userIP;
            $this->applsx_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->applsx_update_user = Yii::$app->user->identity->username;
            $this->applsx_update_ip = Yii::$app->request->userIP;
            $this->applsx_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ApplShXContent::deleteAll(['applsxc_applsx_id' => $this->applsx_id]);
            return true;
        }
        return false;
    }

}
