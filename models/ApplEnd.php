<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_end}}".
 *
 * Приказ о окончании
 *
 * @property integer $apple_id
 * @property integer $apple_applsx_id
 * @property string $apple_number
 * @property string $apple_date
 * @property integer $apple_trt_id
 * @property integer $apple_svdt_id
 *
 * @property integer $apple_pat_id
 * @property string $apple_file_name
 * @property resource $apple_file_data
 * @property string $apple_create_user
 * @property string $apple_create_time
 * @property string $apple_create_ip
 * @property string $apple_update_user
 * @property string $apple_update_time
 * @property string $apple_update_ip
 *
 * @property ApplSheetX $appleApplsx
 * @property ApplEndContent[] $applEndContents
 * @property TrainingType $appleTrt
 * @property SvcDocType $appleSvdt
 *
 */
class ApplEnd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_end}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['apple_number', 'apple_date'], 'required'],
            [['apple_applsx_id', 'apple_trt_id', 'apple_pat_id', 'apple_svdt_id'], 'integer'],
            [['apple_date', 'apple_create_time', 'apple_update_time'], 'safe'],
            [['apple_number', 'apple_create_user', 'apple_create_ip', 'apple_update_user', 'apple_update_ip'], 'string', 'max' => 64],

            [['apple_number'], 'unique'],

            [['apple_file_name'], 'string', 'max' => 256],
            [['apple_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'rtf, docx'],

            [['apple_applsx_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => ApplSheetX::class, 'targetAttribute' => ['apple_applsx_id' => 'applsx_id']],
            [['apple_trt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['apple_trt_id' => 'trt_id']],
            [['apple_pat_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['apple_pat_id' => 'pat_id']],
            [['apple_svdt_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['apple_svdt_id' => 'svdt_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'apple_id' => Yii::t('app', 'Apple ID'),
            'apple_applsx_id' => Yii::t('app', 'Apple Applsx ID'),
            'apple_number' => Yii::t('app', 'Apple Number'),
            'apple_date' => Yii::t('app', 'Apple Date'),
            'apple_trt_id' => Yii::t('app', 'Apple Trt ID'),
            'apple_svdt_id' => Yii::t('app', 'Apple Svdt ID'),
            'apple_file_name' => Yii::t('app', 'Apple File Name'),
            'apple_file_data' => Yii::t('app', 'Apple File Data'),
            'apple_create_user' => Yii::t('app', 'Apple Create User'),
            'apple_create_time' => Yii::t('app', 'Apple Create Time'),
            'apple_create_ip' => Yii::t('app', 'Apple Create Ip'),
            'apple_update_user' => Yii::t('app', 'Apple Update User'),
            'apple_update_time' => Yii::t('app', 'Apple Update Time'),
            'apple_update_ip' => Yii::t('app', 'Apple Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppleTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'apple_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppleSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'apple_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppleApplsx()
    {
        return $this->hasOne(ApplSheetX::class, ['applsx_id' => 'apple_applsx_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplEndContents()
    {
        return $this->hasMany(ApplEndContent::class, ['applec_apple_id' => 'apple_id']);
    }

    /**
     * @inheritdoc
     * @return ApplEndQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplEndQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->apple_create_user = Yii::$app->user->identity->username;
            $this->apple_create_ip = Yii::$app->request->userIP;
            $this->apple_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->apple_update_user = Yii::$app->user->identity->username;
            $this->apple_update_ip = Yii::$app->request->userIP;
            $this->apple_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        foreach ($this->applEndContents as $obj) {
            $obj->delete();
        }
        return parent::beforeDelete();
    }
}
