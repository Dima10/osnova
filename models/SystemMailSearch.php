<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SystemMail;

/**
 * SystemMailSearch represents the model behind the search form of `app\models\SystemMail`.
 */
class SystemMailSearch extends SystemMail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sm_id'], 'integer'],
            [['sm_name', 'sm_server', 'sm_imap_server', 'sm_imap_user', 'sm_imap_password', 'sm_encryption', 'sm_user', 'sm_password', 'sm_create_user', 'sm_create_time', 'sm_create_ip', 'sm_update_user', 'sm_update_time', 'sm_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SystemMail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sm_id' => $this->sm_id,
        ]);

        $query
            ->andFilterWhere(['like', 'sm_name', $this->sm_name])
            ->andFilterWhere(['like', 'sm_server', $this->sm_server])
            ->andFilterWhere(['like', 'sm_encryption', $this->sm_encryption])
            ->andFilterWhere(['like', 'sm_user', $this->sm_user])
            ->andFilterWhere(['like', 'sm_password', $this->sm_password])
            ->andFilterWhere(['like', 'sm_imap_server', $this->sm_imap_server])
            ->andFilterWhere(['like', 'sm_imap_user', $this->sm_imap_user])
            ->andFilterWhere(['like', 'sm_imap_password', $this->sm_imap_password])
            ->andFilterWhere(['like', 'sm_create_user', $this->sm_create_user])
            ->andFilterWhere(['like', 'sm_create_time', $this->sm_create_time])
            ->andFilterWhere(['like', 'sm_create_ip', $this->sm_create_ip])
            ->andFilterWhere(['like', 'sm_update_user', $this->sm_update_user])
            ->andFilterWhere(['like', 'sm_update_time', $this->sm_update_time])
            ->andFilterWhere(['like', 'sm_update_ip', $this->sm_update_ip]);

        return $dataProvider;
    }
}
