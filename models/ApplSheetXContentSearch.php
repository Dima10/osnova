<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplSheetXContent;

/**
 * ApplSheetXContentSearch represents the model behind the search form about `app\models\ApplSheetXContent`.
 */
class ApplSheetXContentSearch extends ApplSheetXContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applsxc_id', 'applsxc_applsx_id', 'applsxc_prs_id', 'applsxc_trp_id', 'applsxc_score', 'applsxc_passed'], 'integer'],
            [['applsxc_create_user', 'applsxc_create_time', 'applsxc_create_ip', 'applsxc_update_user', 'applsxc_update_time', 'applsxc_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplSheetXContent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applsxc_id' => $this->applsxc_id,
            'applsxc_applsx_id' => $this->applsxc_applsx_id,
            'applsxc_prs_id' => $this->applsxc_prs_id,
            'applsxc_trp_id' => $this->applsxc_trp_id,
            'applsxc_score' => $this->applsxc_score,
            'applsxc_passed' => $this->applsxc_passed,
            'applsxc_create_time' => $this->applsxc_create_time,
            'applsxc_update_time' => $this->applsxc_update_time,
        ]);

        $query->andFilterWhere(['like', 'applsxc_create_user', $this->applsxc_create_user])
            ->andFilterWhere(['like', 'applsxc_create_ip', $this->applsxc_create_ip])
            ->andFilterWhere(['like', 'applsxc_update_user', $this->applsxc_update_user])
            ->andFilterWhere(['like', 'applsxc_update_ip', $this->applsxc_update_ip]);

        return $dataProvider;
    }
}
