<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tag}}".
 *
 * @property integer $tag_id
 * @property string $tag_name
 * @property string $tag_comment
 * @property string $tag_create_user
 * @property string $tag_create_time
 * @property string $tag_create_ip
 * @property string $tag_update_user
 * @property string $tag_update_time
 * @property string $tag_update_ip
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_name', 'tag_comment'], 'required'],
            [['tag_create_time', 'tag_update_time'], 'safe'],
            [['tag_name', 'tag_create_user', 'tag_create_ip', 'tag_update_user', 'tag_update_ip'], 'string', 'max' => 64],
            [['tag_comment'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => Yii::t('app', 'Tag ID'),
            'tag_name' => Yii::t('app', 'Tag Name'),
            'tag_comment' => Yii::t('app', 'Tag Comment'),
            'tag_create_user' => Yii::t('app', 'Tag Create User'),
            'tag_create_time' => Yii::t('app', 'Tag Create Time'),
            'tag_create_ip' => Yii::t('app', 'Tag Create Ip'),
            'tag_update_user' => Yii::t('app', 'Tag Update User'),
            'tag_update_time' => Yii::t('app', 'Tag Update Time'),
            'tag_update_ip' => Yii::t('app', 'Tag Update Ip'),
        ];
    }

    /**
     * @inheritdoc
     * @return TagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TagQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->tag_create_user = Yii::$app->user->identity->username;
            $this->tag_create_ip = Yii::$app->request->userIP;
            $this->tag_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->tag_update_user = Yii::$app->user->identity->username;
            $this->tag_update_ip = Yii::$app->request->userIP;
            $this->tag_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

}
