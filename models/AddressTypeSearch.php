<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddressType;

/**
 * AddressTypeSearch represents the model behind the search form about `app\models\AddressType`.
 */
class AddressTypeSearch extends AddressType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['addt_id'], 'integer'],
            [['addt_name', 'addt_create_user', 'addt_create_time', 'addt_create_ip', 'addt_update_user', 'addt_update_time', 'addt_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'addt_id' => $this->addt_id,
        ]);

        $query->andFilterWhere(['like', 'addt_name', $this->addt_name])
            ->andFilterWhere(['like', 'addt_create_user', $this->addt_create_user])
            ->andFilterWhere(['like', 'addt_create_ip', $this->addt_create_ip])
            ->andFilterWhere(['like', 'addt_update_user', $this->addt_update_user])
            ->andFilterWhere(['like', 'addt_create_time', $this->addt_create_time])
            ->andFilterWhere(['like', 'addt_update_time', $this->addt_update_time])
            ->andFilterWhere(['like', 'addt_update_ip', $this->addt_update_ip]);

        return $dataProvider;
    }
}
