<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\City;

/**
 * CitySearch represents the model behind the search form about `app\models\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'city_cou_id', 'city_reg_id'], 'integer'],
            [['city_name', 'city_create_user', 'city_create_time', 'city_create_ip', 'city_update_user', 'city_update_time', 'city_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->city_cou_id === '') {
            $this->city_reg_id = '';
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'city_id' => $this->city_id,
            'city_cou_id' => $this->city_cou_id,
            'city_reg_id' => $this->city_reg_id,
        ]);

        $query->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'city_create_user', $this->city_create_user])
            ->andFilterWhere(['like', 'city_create_ip', $this->city_create_ip])
            ->andFilterWhere(['like', 'city_update_user', $this->city_update_user])
            ->andFilterWhere(['like', 'city_create_time', $this->city_create_time])
            ->andFilterWhere(['like', 'city_update_time', $this->city_update_time])
            ->andFilterWhere(['like', 'city_update_ip', $this->city_update_ip]);

        return $dataProvider;
    }
}
