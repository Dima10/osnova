<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%entity}}".
 *
 * @property integer $ent_id
 * @property integer $ent_entt_id
 * @property string $ent_name
 * @property string $ent_name_short
 * @property string $ent_orgn
 * @property string $ent_inn
 * @property string $ent_kpp
 * @property string $ent_okpo
 * @property string $ent_comment
 * @property integer $ent_agent_id
 * @property integer $ent_manager_id
 * @property string $ent_create_user
 * @property string $ent_create_time
 * @property string $ent_create_ip
 * @property string $ent_update_user
 * @property string $ent_update_time
 * @property string $ent_update_ip
 *
 * @property Company[] $companies
 * @property EntityType $entEntt
 * @property Ab $ent
 * @property Ab $entAgent
 * @property Ab $entManager
 * @property EntityClassLnk[] $entityClassLnks
 * @property EntityClass[] $entityClasses
 * @property Staff[] $staff
 * @property Agreement[] $agreements
 * @property AgreementAct[] $agreementActs
 * @property AgreementAnnex[] $agreementAnnexes
 * @property Person[] $stfPrs
 */
class Entity extends \yii\db\ActiveRecord
{
    public $entc_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ent_entt_id', 'ent_name', 'ent_name_short'], 'required'],
            [['ent_entt_id', 'ent_agent_id', 'ent_manager_id', 'entc_id'], 'integer'],
            [['ent_create_time', 'ent_update_time'], 'safe'],
            [['ent_name'], 'string', 'max' => 192],
            [['ent_comment'], 'string', 'max' => 65535],
            [['ent_name_short'], 'string', 'max' => 128],
            [['ent_orgn', 'ent_inn', 'ent_kpp', 'ent_okpo'], 'string', 'max' => 16],
            [['ent_create_user', 'ent_create_ip', 'ent_update_user', 'ent_update_ip'], 'string', 'max' => 64],
            //[['ent_name_short'], 'unique'],
            //[['ent_name'], 'unique'],
            [['ent_entt_id'], 'exist', 'skipOnError' => true, 'targetClass' => EntityType::class, 'targetAttribute' => ['ent_entt_id' => 'entt_id']],
            [['ent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['ent_id' => 'ab_id']],
            //[['ent_agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['ent_agent_id' => 'ab_id']],
            //[['ent_manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ab::class, 'targetAttribute' => ['ent_manager_id' => 'ab_id']],
        ];
    }


    /**
     * Возвращает массив составных атрибутов для тэгов
     *
     * @return array 
    public function subAttribute()
    {
        $val = [];

        $class = EntityType::class;
        foreach ((new $class)->attributeLabels() as $key => $value) {
            $val['entEntt->'.$key] = 'entEntt->'.$key . " ($value)";
        }

        return $val;
    }
     */

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ent_id' => Yii::t('app', 'Ent ID'),
            'ent_entt_id' => Yii::t('app', 'Ent Entt ID'),
            'ent_name' => Yii::t('app', 'Ent Name'),
            'ent_name_short' => Yii::t('app', 'Ent Name Short'),
            'ent_orgn' => Yii::t('app', 'Ent Orgn'),
            'ent_inn' => Yii::t('app', 'Ent Inn'),
            'ent_kpp' => Yii::t('app', 'Ent Kpp'),
            'ent_okpo' => Yii::t('app', 'Ent Okpo'),
            'ent_comment' => Yii::t('app', 'Ent Comment'),
            'ent_agent_id' => Yii::t('app', 'Ent Agent ID'),
            'ent_manager_id' => Yii::t('app', 'Ent Manager ID'),
            'ent_create_user' => Yii::t('app', 'Create User'),
            'ent_create_time' => Yii::t('app', 'Create Time'),
            'ent_create_ip' => Yii::t('app', 'Create Ip'),
            'ent_update_user' => Yii::t('app', 'Update User'),
            'ent_update_time' => Yii::t('app', 'Update Time'),
            'ent_update_ip' => Yii::t('app', 'Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStfPrs()
    {
        return $this->hasMany(Person::class, ['prs_id' => 'stf_prs_id'])->viaTable(Staff::tableName(), ['stf_ent_id' => 'ent_id']);
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::class, ['comp_ent_id' => 'ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityClassLnks() 
    { 
        return $this->hasMany(EntityClassLnk::class, ['entcl_ent_id' => 'ent_id']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityClasses()
    { 
        return $this->hasMany(EntityClass::class, ['entc_id' => 'entcl_entc_id'])->viaTable(EntityClassLnk::tableName(), ['entcl_ent_id' => 'ent_id']);
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntEntt()
    {
        return $this->hasOne(EntityType::class, ['entt_id' => 'ent_entt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnt()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntAgent()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'ent_agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntManager()
    {
        return $this->hasOne(Ab::class, ['ab_id' => 'ent_manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::class, ['stf_ent_id' => 'ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::class, ['agr_ab_id' => 'ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementAnnexes()
    {
        return $this->hasMany(AgreementAnnex::class, ['agra_ab_id' => 'ent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementActs()
    {
        return $this->hasMany(AgreementAct::class, ['act_ab_id' => 'ent_id']);
    }

    /**
     * @inheritdoc
     * @return EntityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntityQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {

            $ab = new Ab();
            $ab->ab_type = 1;
            $ab->ab_name = $this->ent_name_short;
            $ab->save();

            $this->ent_id = $ab->ab_id;
            $this->ent_agent_id = $this->ent_agent_id == 0 ? null : $this->ent_agent_id;
            $this->ent_manager_id = $this->ent_manager_id == 0 ? null : $this->ent_manager_id;

            $this->ent_create_user = Yii::$app->user->identity->username;
            $this->ent_create_ip = Yii::$app->request->userIP;
            $this->ent_create_time = date('Y-m-d H:i:s');
            return true;
        } else {

            $ab = Ab::findOne($this->ent_id);
            $ab->ab_name = $this->ent_name_short;
            $ab->save();

            $this->ent_agent_id = $this->ent_agent_id == 0 ? null : $this->ent_agent_id;
            $this->ent_manager_id = $this->ent_manager_id == 0 ? null : $this->ent_manager_id;

            $this->ent_update_user = Yii::$app->user->identity->username;
            $this->ent_update_ip = Yii::$app->request->userIP;
            $this->ent_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $model = Ab::findOne($this->ent_id);
        $model->delete();
    } 


}
