<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonVisitEvent;

/**
 * PersonVisitEventSearch represents the model behind the search form about `app\models\PersonVisitEvent`.
 */
class PersonVisitEventSearch extends PersonVisitEvent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pve_id'], 'integer'],
            [['pve_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonVisitEvent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pve_id' => $this->pve_id,
        ]);

        $query->andFilterWhere(['like', 'pve_name', $this->pve_name]);

        return $dataProvider;
    }
}
