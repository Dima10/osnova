<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplFinal;

/**
 * ApplFinalSearch represents the model behind the search form about `app\models\ApplFinal`.
 */
class ApplFinalSearch extends ApplFinal
{

    public $trt_id;
    public $applfSvdt;
    public $applfTrt;
    public $applfTrp;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applf_reestr', 'applf_id', 'applf_svdt_id', 'applf_trt_id', 'applf_trp_id', 'applf_trp_hour'], 'integer'],
            [['applf_name_first', 'applf_name_last', 'applf_name_middle', 'applf_name_full', 'applf_cmd_date', 'applf_end_date', 'applf_number', 'applf_file_name',  'applf_file0_name', 'applf_create_user', 'applf_create_time', 'applf_create_ip', 'applf_update_user', 'applf_update_time', 'applf_update_ip'], 'safe'],
            [['applfSvdt', 'applfTrt', 'applfTrp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer $id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        if (!is_null($id)) {
            $query = ApplFinal::find()
                ->leftJoin(ApplMain::tableName(), 'applf_applm_id = applm_id')
                ->leftJoin(ApplRequest::tableName(), 'applf_applr_id = applr_id')
                ->innerJoin(TrainingProg::tableName(), 'applf_trp_id = trp_id')
                ->innerJoin(TrainingType::tableName(), 'applf_trt_id = trt_id')
                ->where(['applm_ab_id' => $id]);
        } else {
            $query = ApplFinal::find();
        }
        $query->joinWith(['applfSvdt']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['applf_end_date' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['applf_trt_id'] = [
            'asc' => ['applf_trt_id' => SORT_ASC],
            'desc' => ['applf_trt_id' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applfSvdt'] = [
            'asc' => ['svc_doc_type.svdt_name' => SORT_ASC],
            'desc' => ['svc_doc_type.svdt_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applfTrt'] = [
            'asc' => ['training_type.trt_name' => SORT_ASC],
            'desc' => ['training_type.trt_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['applfTrp'] = [
            'asc' => ['training_prog.trp_name' => SORT_ASC],
            'desc' => ['training_prog.trp_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->applf_reestr;
        if (User::isSpecAdmin()) {
            $reestr = 0;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'applf_id' => $this->applf_id,
            'applf_reestr' => $reestr,
            'applf_svdt_id' => $this->applf_svdt_id,
            'applf_trt_id' => $this->applf_trt_id,
            'applf_trp_id' => $this->applf_trp_id,
            'svc_doc_type.svdt_id' => $this->applfSvdt,
            'training_type.trt_id' => $this->applfTrt,
        ]);

        $query
            ->andFilterWhere(['like', 'applf_trp_hour', $this->applf_trp_hour])
            ->andFilterWhere(['like', 'applf_cmd_date', $this->applf_cmd_date])
            ->andFilterWhere(['like', 'applf_end_date', $this->applf_end_date])
            ->andFilterWhere(['like', 'applf_create_time', $this->applf_create_time])
            ->andFilterWhere(['like', 'applf_update_time', $this->applf_update_time])

            ->andFilterWhere(['like', 'applf_name_first', $this->applf_name_first])
            ->andFilterWhere(['like', 'applf_name_last', $this->applf_name_last])
            ->andFilterWhere(['like', 'applf_name_middle', $this->applf_name_middle])
            ->andFilterWhere(['like', 'applf_name_full', $this->applf_name_full])
            ->andFilterWhere(['like', 'applf_number', $this->applf_number])
            ->andFilterWhere(['like', 'training_prog.trp_name', $this->applfTrp])
            ->andFilterWhere(['like', 'applf_file_name', $this->applf_file_name])
            ->andFilterWhere(['like', 'applf_file0_name', $this->applf_file0_name])
            ->andFilterWhere(['like', 'applf_create_user', $this->applf_create_user])
            ->andFilterWhere(['like', 'applf_create_ip', $this->applf_create_ip])
            ->andFilterWhere(['like', 'applf_update_user', $this->applf_update_user])
            ->andFilterWhere(['like', 'applf_update_ip', $this->applf_update_ip]);

        return $dataProvider;
    }
}
