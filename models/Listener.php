<?php
/**
 * @author Ruslan Bondarenko r.i.bondarenko@gmail.com (Dnipro)
 * @copyright Copyright (c) 2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 *
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 */
class Listener extends Model
{
    public $prs_last_name;
    public $prs_first_name;
    public $prs_middle_name;
    public $applr_number;
    public $applr_date;
    public $applr_ast_name;
    public $applr_reestr;
    public $svc_id;
    public $svc_name;
    public $trp_id;
    public $trp_name;
    public $applr_flag;
    public $applr_ast_id;
    public $applrc_applf_id;
    public $applrc_id;
    public $pv_time;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prs_last_name', 'prs_first_name', 'prs_middle_name', 'applr_number', 'pv_time',
                'applr_date', 'applr_ast_name','svc_name', 'trp_name'], 'string'],
            [['applrc_id', 'applr_flag',  'applr_reestr', 'applr_ast_id', 'applrc_applf_id', 'svc_id', 'trp_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'applrc_id' => Yii::t('app', 'Applrc ID'),
            'prs_last_name' => Yii::t('app', 'Prs Last Name'),
            'prs_first_name' => Yii::t('app', 'Prs First Name'),
            'prs_middle_name' => Yii::t('app', 'Prs Middle Name'),
            'applr_number' => Yii::t('app', 'Applr Number'),
            'pv_time' => Yii::t('app', 'Education Time'),
            'applr_date' => Yii::t('app', 'Applr Date'),
            'applr_ast_name' => Yii::t('app', 'Applr Ast ID'),
            'applr_reestr' => Yii::t('app', 'Applr Reestr'),
            'svc_name' => Yii::t('app', 'Applrc Svc ID'),
            'svc_id' => Yii::t('app', 'Applrc Svc ID'),
            'trp_name' => Yii::t('app', 'Applrc Trp ID'),
            'trp_id' => Yii::t('app', 'Applrc Trp ID'),
            'applr_flag' => Yii::t('app', 'Applr Flag'),
            'applr_ast_id' => Yii::t('app', 'Applr Ast ID'),
            'applrc_applf_id' => Yii::t('app', 'Applrc Applf ID'),
        ];
    }
}
