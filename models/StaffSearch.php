<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Staff;

/**
 * StaffSearch represents the model behind the search form about `app\models\Staff`.
 */
class StaffSearch extends Staff
{
    public $yurLico;
    public $fizLico;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stf_id', 'stf_ent_id', 'stf_prs_id'], 'integer'],
            [['stf_position', 'stf_create_user', 'stf_create_time', 'stf_create_ip', 'stf_update_user', 'stf_update_time', 'stf_update_ip'], 'safe'],
            [['yurLico'], 'safe'],
            [['fizLico'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Staff::find()->joinWith(['stfEnt' => function($query) { $query->from(['stfEnt' => 'entity']);}])
                                ->joinWith(['stfPrs' => function($query) { $query->from(['stfPrs' => 'person']);}]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['yurLico'] = [
            'asc' => ['stfEnt.ent_name' => SORT_ASC],
            'desc' => ['stfEnt.ent_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['fizLico'] = [
            'asc' => ['stfPrs.prs_full_name' => SORT_ASC],
            'desc' => ['stfPrs.prs_full_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'stf_id' => $this->stf_id,
            'stf_ent_id' => $this->stf_ent_id,
            'stf_prs_id' => $this->stf_prs_id,
        ]);

        $query->andFilterWhere(['like', 'stf_position', $this->stf_position])
            ->andFilterWhere(['like', 'stf_create_user', $this->stf_create_user])
            ->andFilterWhere(['like', 'stfEnt.ent_name', $this->yurLico])
            ->andFilterWhere(['like', 'stfPrs.prs_full_name', $this->fizLico])
            ->andFilterWhere(['like', 'stf_create_ip', $this->stf_create_ip])
            ->andFilterWhere(['like', 'stf_update_user', $this->stf_update_user])
            ->andFilterWhere(['like', 'stf_create_time', $this->stf_create_time])
            ->andFilterWhere(['like', 'stf_update_time', $this->stf_update_time])
            ->andFilterWhere(['like', 'stf_update_ip', $this->stf_update_ip]);

        return $dataProvider;
    }
}
