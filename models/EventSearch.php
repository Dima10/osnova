<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;

/**
 * EventSearch represents the model behind the search form about `app\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'event_ab_id', 'event_evt_id', 'event_evrt_id', 'event_user_id'], 'integer'],
            [['event_date', 'event_note', 'event_create_user', 'event_create_time', 'event_create_ip', 'event_update_user', 'event_update_time', 'event_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event_id' => $this->event_id,
            'event_ab_id' => $this->event_ab_id,
            'event_evt_id' => $this->event_evt_id,
            'event_evrt_id' => $this->event_evrt_id,
            'event_date' => $this->event_date,
            'event_user_id' => $this->event_user_id,
            'event_create_time' => $this->event_create_time,
            'event_update_time' => $this->event_update_time,
        ]);

        $query->andFilterWhere(['like', 'event_note', $this->event_note])
            ->andFilterWhere(['like', 'event_create_user', $this->event_create_user])
            ->andFilterWhere(['like', 'event_create_ip', $this->event_create_ip])
            ->andFilterWhere(['like', 'event_update_user', $this->event_update_user])
            ->andFilterWhere(['like', 'event_update_ip', $this->event_update_ip]);

        return $dataProvider;
    }
}
