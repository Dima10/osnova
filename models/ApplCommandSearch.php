<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplCommand;

/**
 * ApplCommandSearch represents the model behind the search form about `app\models\ApplCommand`.
 */
class ApplCommandSearch extends ApplCommand
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['applcmd_id', 'applcmd_reestr', 'applcmd_ab_id', 'applcmd_comp_id', 'applcmd_trt_id', 'applcmd_svdt_id'], 'integer'],
            [['applcmd_number', 'applcmd_date', 'applcmd_create_user', 'applcmd_create_time', 'applcmd_create_ip', 'applcmd_update_user', 'applcmd_update_time', 'applcmd_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplCommand::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['applcmd_date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->applcmd_reestr;
        if (User::isSpecAdmin()) {
            $reestr = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applcmd_id' => $this->applcmd_id,
            'applcmd_reestr' => $reestr,
            'applcmd_ab_id' => $this->applcmd_ab_id,
            'applcmd_comp_id' => $this->applcmd_comp_id,
            'applcmd_trt_id' => $this->applcmd_trt_id,
            'applcmd_svdt_id' => $this->applcmd_svdt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'applcmd_date', $this->applcmd_date])
            ->andFilterWhere(['like', 'applcmd_create_time', $this->applcmd_create_time])
            ->andFilterWhere(['like', 'applcmd_update_time', $this->applcmd_update_time])
            ->andFilterWhere(['like', 'applcmd_number', $this->applcmd_number])
            ->andFilterWhere(['like', 'applcmd_create_user', $this->applcmd_create_user])
            ->andFilterWhere(['like', 'applcmd_create_ip', $this->applcmd_create_ip])
            ->andFilterWhere(['like', 'applcmd_update_user', $this->applcmd_update_user])
            ->andFilterWhere(['like', 'applcmd_update_ip', $this->applcmd_update_ip])
        ;

        return $dataProvider;
    }
}
