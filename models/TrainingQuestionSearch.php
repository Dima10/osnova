<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainingQuestion;

/**
 * TrainingQuestionSearch represents the model behind the search form about `app\models\TrainingQuestion`.
 */
class TrainingQuestionSearch extends TrainingQuestion
{

    public $trm_name;
    public $trm_code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trq_id', 'trq_trm_id'], 'integer'],
            [['trm_name', 'trm_code'], 'string'],
            [['trq_question', 'trq_create_user', 'trq_create_time', 'trq_create_ip', 'trq_update_user', 'trq_update_time', 'trq_update_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainingQuestion::find()->innerJoin(TrainingModule::tableName(), 'trq_trm_id = trm_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trq_id' => $this->trq_id,
            //'trq_trm_id' => $this->trq_trm_id,
            //'trq_create_time' => $this->trq_create_time,
            //'trq_update_time' => $this->trq_update_time,
        ]);

        $query
            ->andFilterWhere(['like', 'trq_question', $this->trq_question])
            ->andFilterWhere(['like', 'trm_name', $this->trm_name])
            ->andFilterWhere(['like', 'trm_code', $this->trm_code])
            ->andFilterWhere(['like', 'trq_fname', $this->trq_fname])
            ->andFilterWhere(['like', 'trq_create_user', $this->trq_create_user])
            ->andFilterWhere(['like', 'trq_create_time', $this->trq_create_time])
            ->andFilterWhere(['like', 'trq_create_ip', $this->trq_create_ip])
            ->andFilterWhere(['like', 'trq_update_user', $this->trq_update_user])
            ->andFilterWhere(['like', 'trq_update_time', $this->trq_update_time])
            ->andFilterWhere(['like', 'trq_update_ip', $this->trq_update_ip])
        ;

        return $dataProvider;
    }
}
