<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%training_question}}".
 *
 * @property integer $trq_id
 * @property string $trq_question
 * @property string $trq_fname
 * @property resource $trq_fdata
 * @property integer $trq_trm_id
 * @property string $trq_create_user
 * @property string $trq_create_time
 * @property string $trq_create_ip
 * @property string $trq_update_user
 * @property string $trq_update_time
 * @property string $trq_update_ip
 *
 * @property TrainingAnswer[] $trainingAnswers
 * @property TrainingModule $trqTrm
 */
class TrainingQuestion extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training_question}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trq_question', 'trq_trm_id'], 'required'],
            [['trq_question', 'trq_fdata'], 'string'],
            [['trq_fname'], 'string', 'max' => 256],
            [['trq_trm_id'], 'integer'],
            [['trq_create_time', 'trq_update_time'], 'safe'],
            [['trq_create_user', 'trq_create_ip', 'trq_update_user', 'trq_update_ip'], 'string', 'max' => 64],
            [['trq_trm_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingModule::class, 'targetAttribute' => ['trq_trm_id' => 'trm_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trq_id' => Yii::t('app', 'Trq ID'),
            'trq_question' => Yii::t('app', 'Trq Question'),
            'trq_fdata' => Yii::t('app', 'Trq Fdata'),
            'trq_fname' => Yii::t('app', 'Trq Fname'),
            'trq_trm_id' => Yii::t('app', 'Trq Trm ID'),
            'trq_create_user' => Yii::t('app', 'Trq Create User'),
            'trq_create_time' => Yii::t('app', 'Trq Create Time'),
            'trq_create_ip' => Yii::t('app', 'Trq Create Ip'),
            'trq_update_user' => Yii::t('app', 'Trq Update User'),
            'trq_update_time' => Yii::t('app', 'Trq Update Time'),
            'trq_update_ip' => Yii::t('app', 'Trq Update Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingAnswers()
    {
        return $this->hasMany(TrainingAnswer::class, ['tra_trq_id' => 'trq_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrqTrm()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'trq_trm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrm_name()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'trq_trm_id'])->one()->trm_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrm_code()
    {
        return $this->hasOne(TrainingModule::class, ['trm_id' => 'trq_trm_id'])->one()->trm_code;
    }

    /**
     * @inheritdoc
     * @return TrainingQuestionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrainingQuestionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->trq_create_user = Yii::$app->user->identity->username;
            $this->trq_create_ip = Yii::$app->request->userIP;
            $this->trq_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->trq_update_user = Yii::$app->user->identity->username;
            $this->trq_update_ip = Yii::$app->request->userIP;
            $this->trq_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }
}
