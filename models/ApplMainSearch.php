<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplMain;

/**
 * ApplMainSearch represents the model behind the search form about `app\models\ApplMain`.
 */
class ApplMainSearch extends ApplMain
{

    public $prs_full_name;
    public $svc_name;
    public $trp_name;
    public $ab_name;
    public $comp_name;
    public $trt_name;
    public $svdt_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'applm_reestr', 'applm_applrc_id', 'applm_prs_id', 'applm_svc_id', 'applm_trp_id', 'applm_ab_id',
                'applm_comp_id', 'applm_trt_id', 'applm_svdt_id', 'applm_applr_id', 'applm_applcmd_id',
                'applm_apple_id'
                ], 'integer'],
            [['applm_applr_date', 'applm_applcmd_date', 'applm_date_upk', 'applm_date_start', 'applm_apple_date', 'applm_appls_date', 'applm_applsx_date', 'applm_appls0_date', 'applm_appls0x_date', 'applm_agr_date', 'applm_applxxx_date', 'applm_create_time', 'applm_update_time', 'applm_id'], 'safe'],
            [['applm_position', 'applm_number_upk', 'applm_agr_number', 'applm_agr_number', 'applm_create_user', 'applm_create_ip', 'applm_update_user', 'applm_update_ip'], 'string', 'max' => 64],
            [['prs_full_name', 'svc_name', 'trp_name', 'ab_name', 'comp_name', 'trt_name', 'svdt_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query =
            ApplMain::find()
                ->leftJoin(Person::tableName(), 'applm_prs_id = prs_id')
                ->leftJoin(Svc::tableName(), 'applm_svc_id = svc_id')
                ->leftJoin(Ab::tableName(), 'applm_ab_id = ab_id')
                ->leftJoin(Company::tableName(), 'applm_comp_id = comp_id')
                ->leftJoin(TrainingType::tableName(), 'applm_trt_id = trt_id')
                ->leftJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                ->leftJoin(SvcDocType::tableName(), 'applm_svdt_id = svdt_id')
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['prs_full_name'] = [
            'asc' => ['prs_full_name' => SORT_ASC],
            'desc' => ['prs_full_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svc_name'] = [
            'asc' => ['svc_name' => SORT_ASC],
            'desc' => ['svc_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['trp_name'] = [
            'asc' => ['trp_name' => SORT_ASC],
            'desc' => ['trp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ab_name'] = [
            'asc' => ['ab_name' => SORT_ASC],
            'desc' => ['ab_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['comp_name'] = [
            'asc' => ['comp_name' => SORT_ASC],
            'desc' => ['comp_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['trt_name'] = [
            'asc' => ['trt_name' => SORT_ASC],
            'desc' => ['trt_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['svdt_name'] = [
            'asc' => ['svdt_name' => SORT_ASC],
            'desc' => ['svdt_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $reestr = $this->applm_reestr;
        if(User::isSpecAdmin()) {
            $reestr = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'applm_id' => $this->applm_id,
            'applm_reestr' => $reestr,
            'applm_prs_id' => $this->applm_prs_id,
            //'applm_svc_id' => $this->applm_svc_id,
            //'applm_ab_id' => $this->applm_ab_id,
            //'applm_comp_id' => $this->applm_comp_id,
            //'applm_trt_id' => $this->applm_trt_id,
            'svdt_id' => $this->svdt_name,
            'trt_id' => $this->trt_name
        ]);

        $query
            ->andFilterWhere(['like', 'prs_full_name', $this->prs_full_name])
            ->andFilterWhere(['like', 'svc_name', $this->svc_name])
            ->andFilterWhere(['like', 'trp_name', $this->trp_name])
            ->andFilterWhere(['like', 'ab_name', $this->ab_name])
            ->andFilterWhere(['like', 'comp_name', $this->comp_name])

            ->andFilterWhere(['like', 'applm_position', $this->applm_position])
            ->andFilterWhere(['like', 'applm_number_upk', $this->applm_number_upk])
            ->andFilterWhere(['like', 'applm_agr_number', $this->applm_agr_number])
            ->andFilterWhere(['like', 'applm_create_user', $this->applm_create_user])
            ->andFilterWhere(['like', 'applm_create_time', $this->applm_create_time])
            ->andFilterWhere(['like', 'applm_create_ip', $this->applm_create_ip])
            ->andFilterWhere(['like', 'applm_update_user', $this->applm_update_user])
            ->andFilterWhere(['like', 'applm_update_time', $this->applm_update_time])
            ->andFilterWhere(['like', 'applm_update_ip', $this->applm_update_ip])

            ->andFilterWhere(['like', 'applm_date_upk', $this->applm_date_upk])
            ->andFilterWhere(['like', 'applm_apple_date', $this->applm_apple_date])
            ->andFilterWhere(['like', 'applm_applsx_date', $this->applm_applsx_date])
            ->andFilterWhere(['like', 'applm_appls0x_date', $this->applm_appls0x_date])
            ->andFilterWhere(['like', 'applm_appls_date', $this->applm_appls_date])
            ->andFilterWhere(['like', 'applm_appls0_date', $this->applm_appls0_date])
            ->andFilterWhere(['like', 'applm_applcmd_date', $this->applm_applcmd_date])
            ->andFilterWhere(['like', 'applm_applr_date', $this->applm_applr_date])
            ->andFilterWhere(['like', 'applm_date_start', $this->applm_date_start])
            ->andFilterWhere(['like', 'applm_agr_date', $this->applm_agr_date])
            ->andFilterWhere(['like', 'applm_applxxx_date', $this->applm_applxxx_date])
        ;

        return $dataProvider;
    }
}
