<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%appl_sh}}".
 *
 * @property integer $appls_id
 * @property string $appls_number
 * @property string $appls_date
 * @property int $appls_applr_id
 * @property int $appls_applm_id
 * @property int $appls_prs_id
 * @property string $appls_magic
 * @property int $appls_trp_id
 * @property int $appls_svdt_id
 * @property int $appls_score_max
 * @property int $appls_score
 * @property int $appls_passed
 * @property int $appls_reestr
 * @property int $appls_try
 * @property string $appls_begin
 * @property string $appls_end

 * @property int $appls_pat_id
 * @property string $appls_file_name
 * @property resource $appls_file_data
 * @property integer $appls_trt_id
 
 * @property string $appls_create_user
 * @property string $appls_create_time
 * @property string $appls_create_ip
 * @property string $appls_update_user
 * @property string $appls_update_time
 * @property string $appls_update_ip
 *
 * @property ApplRequest $applsApplr
 * @property Pattern $applsPat
 * @property Person $applsPrs
 * @property TrainingProg $applsTrp
 * @property TrainingType $applsTrt
 * @property SvcDocType $applsSvdt
 * @property ApplShContent[] $applShContents
 */
class ApplSh extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%appl_sh}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appls_number', 'appls_date', 'appls_prs_id', 'appls_trp_id', 'appls_score_max'], 'required'],
            [['appls_date', 'appls_create_time', 'appls_update_time'], 'safe'],
            [['appls_applr_id', 'appls_applm_id', 'appls_prs_id', 'appls_trp_id', 'appls_score_max', 'appls_score', 'appls_passed', 'appls_reestr'], 'integer'],

            [['appls_try', 'appls_svdt_id', 'appls_pat_id', 'appls_trt_id'], 'integer'],

            [['appls_begin', 'appls_end'], 'string'],
            [['appls_magic'], 'string'],
            [['appls_number'], 'unique'],

            [['appls_number', 'appls_create_user', 'appls_create_ip', 'appls_update_user', 'appls_update_ip'], 'string', 'max' => 64],
            [['appls_file_name'], 'string'],
            [['appls_file_data'], 'file', 'skipOnEmpty' => true, 'extensions' => 'docx, pdf'],

            [['appls_prs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['appls_prs_id' => 'prs_id']],
            [['appls_trp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingProg::class, 'targetAttribute' => ['appls_trp_id' => 'trp_id']],
            [['appls_pat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::class, 'targetAttribute' => ['appls_pat_id' => 'pat_id']],
            [['appls_trt_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingType::class, 'targetAttribute' => ['appls_trt_id' => 'trt_id']],
            [['appls_svdt_id'], 'exist', 'skipOnError' => true, 'targetClass' => SvcDocType::class, 'targetAttribute' => ['appls_svdt_id' => 'svdt_id']],
            [['appls_applr_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplRequest::class, 'targetAttribute' => ['appls_applr_id' => 'applr_id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'appls_id' => Yii::t('app', 'Appls ID'),
            'appls_number' => Yii::t('app', 'Appls Number'),
            'appls_date' => Yii::t('app', 'Appls Date'),
            'appls_applr_id' => Yii::t('app', 'Appls Applr ID'),
            'appls_prs_id' => Yii::t('app', 'Appls Prs ID'),
            'appls_magic' => Yii::t('app', 'Appls Magic'),
            'appls_trp_id' => Yii::t('app', 'Appls Trp ID'),
            'appls_score_max' => Yii::t('app', 'Appls Score Max'),
            'appls_score' => Yii::t('app', 'Appls Score'),
            'appls_passed' => Yii::t('app', 'Appls Passed'),
            'appls_reestr' => Yii::t('app', 'Appls Reestr'),
            'appls_file_name' => Yii::t('app', 'Appls File Name'),
            'appls_pat_id' => Yii::t('app', 'Appls Pat ID'),
            'appls_trt_id' => Yii::t('app', 'Appls Trt ID'),
            'appls_svdt_id' => Yii::t('app', 'Appls Svdt ID'),
            'appls_try' => Yii::t('app', 'Appls Try'),
            'appls_begin' => Yii::t('app', 'Appls Begin'),
            'appls_end' => Yii::t('app', 'Appls End'),
            'appls_file_data' => Yii::t('app', 'Appls File Data'),
            'appls_create_user' => Yii::t('app', 'Appls Create User'),
            'appls_create_time' => Yii::t('app', 'Appls Create Time'),
            'appls_create_ip' => Yii::t('app', 'Appls Create Ip'),
            'appls_update_user' => Yii::t('app', 'Appls Update User'),
            'appls_update_time' => Yii::t('app', 'Appls Update Time'),
            'appls_update_ip' => Yii::t('app', 'Appls Update Ip'),
            'appls_applm_id' => Yii::t('app', 'Appls Applm ID'),
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsApplr()
    {
        return $this->hasOne(ApplRequest::class, ['applr_id' => 'appls_applr_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsSvdt()
    {
        return $this->hasOne(SvcDocType::class, ['svdt_id' => 'appls_svdt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsTrt()
    {
        return $this->hasOne(TrainingType::class, ['trt_id' => 'appls_trt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsPrs()
    {
        return $this->hasOne(Person::class, ['prs_id' => 'appls_prs_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplsTrp()
    {
        return $this->hasOne(TrainingProg::class, ['trp_id' => 'appls_trp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplShContents()
    {
        return $this->hasMany(ApplShContent::class, ['applsc_appls_id' => 'appls_id']);
    }

    /**
     * @inheritdoc
     * @return ApplShQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApplShQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;
        if ($insert) {
            $this->appls_create_user = Yii::$app->user->identity->username;
            $this->appls_create_ip = Yii::$app->request->userIP;
            $this->appls_create_time = date('Y-m-d H:i:s');
            return true;
        } else {
            $this->appls_update_user = Yii::$app->user->identity->username;
            $this->appls_update_ip = Yii::$app->request->userIP;
            $this->appls_update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * Calculate test result percent
     * @return float
     */
    public function answerRightPersent()
    {
        return $this->appls_score_max > 0.0 ? round(($this->appls_score / $this->appls_score_max) * 100) : 0;
    }

    /**
     * Test result
     * @return string
     */
    public function testResult()
    {
        return $this->appls_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test');
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            ApplShContent::deleteAll(['applsc_appls_id' => $this->appls_id]);
            return true;
        }
        return false;
    }

}
