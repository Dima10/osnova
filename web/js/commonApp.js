var CommonApp = Marionette.Application.extend({'helper': {}});

var app = new CommonApp();

/**
 * Модель которая будет отвечать за глобальную локацию пользователя
 * если ваш js меняется при смене локации следите за данной моделью
 * Хочешь узнать как следить за моделями, спроси меня "как"
 */

    // true/false
    app.helper.selectOnDropDownLocationWidget = false;

    app.helper.setEventClickOnDropDownLocationWidget = function () {
        app.helper.selectOnDropDownLocationWidget = true;
    };

    app.helper.unsetEventClickOnDropDownLocationWidget = function () {
        app.helper.selectOnDropDownLocationWidget = false;
    };

    app.helper.validateEmail = function (email, errorTargetContainer) {
    errorTargetContainer = errorTargetContainer || null;
    var match = email.match(/^[0-9a-zA-Z-\.\_]+\@[0-9a-z-]{1,}\.[a-z]{2,}(\.?)([a-z]{0,4})$/i);
    if (!match) {
        return false;
    } else {
        return true;
    }
};

app.helper.validatePassword = function (password, errorTargetContainer) {
    errorTargetContainer = errorTargetContainer || null;
    var match = password.match(/^.{5,15}$/);    // Любые символы. Длина от 5 до 15
    if (!match) {
        return false;
    } else {
        return true;
    }
};

//app.helper.t(5, ['объявление', 'объявления', 'объявлений'])  5 объявлений
app.helper.t = function (n, titles) {
    var nInt = n;
    if(typeof n == 'string') {
        nInt = parseInt(n.replace(' ',''));    // удаление пробелов и преобразование строки в число
    }
    cases = [2, 0, 1, 1, 1, 2];
    return n + ' ' + titles[ (nInt % 100 > 4 && nInt % 100 < 20) ? 2 : cases[(nInt % 10 < 5) ? nInt % 10 : 5] ];
};

app.helper.checkXSSTags = function (string) {
    if (string.toLowerCase().indexOf('<script>') != -1 || string.toLowerCase().indexOf('</script>') != -1) {
        return true;
    } else {
        return false;
    }
};

app.helper.setCookie = function (cname, cvalue, exTimeInSeconds) {
    var d = new Date();
    d.setTime(d.getTime() + (exTimeInSeconds * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

app.helper.getCookie = function (cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

app.helper.deleteCookie = function (cname) {
    document.cookie = cname + '=; path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

/**
 * Проверяет на доступность локальных хранилищ
 *
 * @param {String} type   localStorage | sessionStorage
 * @returns {Boolean}
 */
app.helper.storageAvailable = function (type) {
    try {
        var storage = window[type],
                x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch (e) {
        return false;
    }
};

app.helper.validateFloat = function(e, maxIntegerDigit, maxFractionalDigit, dot){

    maxIntegerDigit = maxIntegerDigit ? maxIntegerDigit : 9;
    maxFractionalDigit = maxFractionalDigit ? maxFractionalDigit : 2;
    dot = dot ? dot : '.';

    var regExpDot = new RegExp('\\d*(\\' + dot + ').*');
    var regExpFract = new RegExp('\\d*' + dot + '(.*)');
    var regExpReplace = new RegExp('[^\\d\\' + dot + ']+', 'g');

    var val = $(e.currentTarget).val().replace(/[\,]+/g, dot).replace(regExpReplace, '');
    var intVal = val.match(/(\d+).*/)!= null ? val.match(/(\d+).*/)[1].slice(0, maxIntegerDigit) : '';
    var dot = val.match(regExpDot) != null ? val.match(regExpDot)[1] : '';
    var fractVal = val.match(regExpFract)!=null ? val.match(regExpFract)[1].replace(/\.+/g, '').slice(0, maxFractionalDigit) : '';

    if(fractVal != '') {
        val = intVal + dot + fractVal;
    } else if(intVal == '' && dot != '') {
        val = '0' + dot;
    } else {
        val = intVal + dot;
    }
    $(e.currentTarget).val(val);
    return false;
},


app.helper.digitalOnlyInputValidate = function(e){
    return (inputValidate(e, /^[0-9]+$/ ));
};

app.helper.digitalAndSeparatorInputValidate = function(e){
    return (inputValidate(e, /^[0-9\.\,]+$/ ));
};

app.helper.pasteDigitalOnly = function (e) {
    var textData = e.originalEvent.clipboardData.getData('Text');

    setTimeout(function() {
        e.target.value = textData.replace(/[^0-9]/ig, '');
    }, 100);
},

app.helper.letterOnlyInputKeyUp = function(e){
    var text = $(e.target).val();
    if(text != text.replace(/[^а-яёіїґє\w\s]/ig, '').replace(/[0-9]/g,'')){
       $(e.target).val($(e.target).val().replace(/[^а-яёіїґє\w\s]/ig, '').replace(/[0-9]/g,''));
    }
};

app.helper.letterDigitalInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'a-zA-Z0-9\s ]+$/ ));
};

app.helper.letterHyphenInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'a-zA-Z\-\s ]+$/ ));
};

app.helper.letterHyphenCyrInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'\-\s ]+$/ ));
};

app.helper.letterHyphenCyrDigitalInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'0-9\-\s ]+$/ ));
};

app.helper.letterCyrDigitalInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'0-9\s ]+$/ ));
};

app.helper.letterOnlyInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐ\'a-zA-Z\s ]+$/ ));
};

app.helper.letterCyrOnlyInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'\s ]+$/ ));
};

app.helper.emailInputValidate = function(e){
    return (inputValidate(e, /^[a-zA-Z0-9\.\_\@\-]+$/ ));
};

app.helper.streetInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'a-zA-Z0-9 \-\.]+$/ ));
};

app.helper.houseNumberInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'a-zA-Z0-9№\/\-\.\, ]+$/ ));
};

app.helper.textInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\'a-zA-Z0-9;:№,.\-_ ?!\/\r\n]+$/ ));
};

app.helper.textExtendInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\+%\'a-zA-Z0-9;:№,.\-_ ?!\/\"\(\)\*=\r\n]+$/ ));
};

app.helper.descriptionInputValidate = function(e){
    return (inputValidate(e, /^[а-яА-ЯёЁіІїЇґҐєЄ\+%\'a-zA-Z0-9;:№,.\-_ ?!\/\r\n]+$/ ));
};

/**
 * Очистка елемента посимвольно согласно валидатора
 *
 * @param type  Тип валидатора (начало функций InputValidate)
 * @param str    jQuery обьект елемента
 *
 */
app.helper.InputSanitize = function(type, str){
    var sanitize = '';
    for(i=0; i<str.length; i++ ) {
        var chr = eval("this." + type + "InputValidate")(str[i]);
        sanitize += chr ? str[i] : '';
    }
    return sanitize.trim();
};

app.helper.inputPaste = function(e, type, strLen) {
    strLen = strLen || 255;
    e.target.value = app.helper.InputSanitize(type, getTextValue(e)).substr(0, strLen);
};

app.helper.checkNested = function (obj /*, level1, level2, ... levelN*/) {
    var args = Array.prototype.slice.call(arguments, 1);

    for (var i = 0; i < args.length; i++) {
        if (!obj || !obj.hasOwnProperty(args[i])) {
            return false;
        }
        obj = obj[args[i]];
    }
    return true;
};

app.helper.inputChange = function(e, type, strLen) {
    strLen = strLen || 255;
    e.target.value = app.helper.InputSanitize(type, e.target.value).substr(0, strLen);
};

app.helper.discardSubmitByEnter = function (e) {
    if(e.keyCode == 13) {
        e.preventDefault();
        e.stopPropagation();
    }
};

app.helper.plural = function (num, data) {
    var text = '';
    if (data.length === 3) {
        if (parseInt(num) === 1) {
            text = num + ' ' + data[0];
        } else if (parseInt(num) >= 2 && parseInt(num) <= 4) {
            text = num + ' ' + data[1];
        } else if (parseInt(num) > 4) {
            text = num + ' ' + data[2];
        }
    }
    return text;
};

/**
 * Просто перенаправление на другую страницу
 * @param sPage
 */
app.helper.goPage = function (sPage) {
    window.location.href = sPage;
};

// Удаляем параметр(get) из location.search в урле
app.helper.removeURLParameter = function (url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        return url;
    } else {
        return url;
    }
};

app.helper.getTextValue = function(e) {
    var text;
    e.preventDefault();
    text = (e.originalEvent || e).clipboardData.getData('text/plain');

    return text;
};

app.helper.inputValidate = function(e, template) {
    e = e || event;
    var chr = e instanceof Object ? getChar(e) : e;
    if(chr == null) {
        return true;
    } else {
        return (template.test(chr));
    }
};

app.helper.getChar = function(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
    }

    return null; // специальная клавиша
};

app.helper.replaceAll = function(str, find, replace) {
    return str.replace(new RegExp(escapeRegExpr(find), 'g'), replace);
};

// Для экранирования в replaceAll
app.helper.escapeRegExpr = function (str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
};

app.helper.getUrlToRoute = function (controller, action) {
    return '/index.php?r='+controller+'/'+action
};

app.helper.toPage = function (page) {
    var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    var urlParams = new URLSearchParams(window.location.search);
    urlParams.set('page', page);
    var newLocation = domain+'/?'+urlParams.toString();
    window.location.href = newLocation;
};

app.helper.getPage = function () {
    var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    var urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('page');
};