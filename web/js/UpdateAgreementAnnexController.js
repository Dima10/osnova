var UpdateAgreementAnnexController = Marionette.View.extend({

    el: "#forUpdateAgreementAnnexController",

    events: {
        "click .processUpdate": "getUpdatingAnnexLine",
        "click .procesSave": "saveUpdatingAnnexLine",
        "keyup .processUpdateIput": "recalculateSum",
        "click .updateApp": "updateApp",
        "click .deleteService": "deleteService",
        "click #showHideServiceId": "showHideServiceId",
        "click #add_svc": "addService",
    },

    initialize: function() {

    },

    getUpdatingAnnexLine: function(e) {

        var anaId = e.currentTarget.dataset.ana_id;

        if($('.inputQty:visible').length > 0){

            this.showModalMessage('Нельзя редактировать несколько услуг одновременно. <br /> Сохраните редактируемую прежде услугу.')
            return false;
        }

        var self = this;

        $.post(app.helper.getUrlToRoute('ajax','service-relation-documents'), { anaId:anaId })
            .done(function (data) {
                data = JSON.parse(data);

                if(data.result === true){
                    self.showModalMessage(data.message);

                } else {
                    $('.'+anaId+'_processUpdate').toggleClass('hidden');
                }

            });

        e.preventDefault();
        e.stopPropagation();
    },

    showHideServiceId: function(e) {

        $('#serviceForm').toggleClass('hidden');

        e.preventDefault();
        e.stopPropagation();
    },

    deleteService: function(e) {

        if(confirm('Вы действительно хотите удалить запись?')){
            
            var anaId = e.currentTarget.dataset.ana_id;
            $('#tr_'+anaId).remove();
            this.recalculateSum();
            var appPrice = $('#agreementannex-agra_sum').val();

            var self = this;

            $.post(app.helper.getUrlToRoute('ajax','service-delete'), {anaId:anaId, appPrice: appPrice})
                .done(function (data) {
                    data = JSON.parse(data);

                    self.showModalMessage('Услуга удалена из приложения');
                });
        }

        e.preventDefault();
        e.stopPropagation();
    },

    saveUpdatingAnnexLine: function(e) {

        var anaId = e.currentTarget.dataset.ana_id;

        var qty = $('#'+anaId+'_ana_qty').val();
        var servicePrice = $('#'+anaId+'_ana_price').val();
        var serviceTax = $('#'+anaId+'_ana_tax').val();
        var appPrice = $('#agreementannex-agra_sum').val();
        var appTax = $('#agreementannex-agra_tax').val();

        var self = this;

        $.post(app.helper.getUrlToRoute('ajax','save-service-row'), {anaId:anaId, qty:qty, servicePrice: servicePrice, serviceTax: serviceTax, appPrice: appPrice, appTax: appTax})
            .done(function (data) {
                data = JSON.parse(data);

                $('#'+anaId+'_priceQty').text(data.qty);
                $('#'+anaId+'_priceLabel').text(data.servicePrice);

                self.showModalMessage('Приложение обновлено');

                $('.'+anaId+'_processUpdate').toggleClass('hidden');
            });

        e.preventDefault();
        e.stopPropagation();
    },

    addService: function(e) {

            var _svc = $("#svc").val();
            var _prog = $("#prog").val();
            var _price = $("#price").val();
            var _qty = $("#qty").val();
            var _date = $("#agreementannex-agra_date").val();
            var _app_id = $('#appIdValue').val();

            var self = this;

            $.get(app.helper.getUrlToRoute('entity-frm', 'ajax-agreement-a-row-create'),
                {
                    svc_id : _svc,
                    trp_id : _prog,
                    price : _price,
                    qty : _qty,
                    date : _date,
                    app_id : _app_id

                },
                function (data) {
                    var data = JSON.parse(data);
                    $('#servicesRows').append(data.row);

                    self.recalculateSum();
                    $('#showHideServiceId').click();
                    self.saveUpdatingAnnexLine({preventDefault: function(){}, stopPropagation: function(){}, currentTarget:{dataset:{ana_id:data.serviceId}}})

                }
            );

        e.preventDefault();
        e.stopPropagation();
    },

    updateApp: function(e) {

        if($('.inputQty:visible').length <= 0){
            return;
        }

        this.showModalMessage('<b>До обновления приложения нужно сохранить данные по услугам!</b>');

        e.preventDefault();
        e.stopPropagation();
    },

    recalculateSum: function () {
        var sum = this.calculateSumOfServices();
        $('#agreementannex-agra_sum').val(sum);

        var sumTax = this.calculateSumOfTaxes();
        $('#agreementannex-agra_tax').val(sumTax);
    },

    calculateSumOfServices: function () {

        var sum = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-anaid');

            var qty = $(value).val();
            var servicePrice = $('#'+rowId+'_ana_price').val();

            sum = sum + ((qty * (servicePrice*10000))/10000);
        });

        return sum;
    },

    calculateSumOfTaxes: function () {

        var sumTax = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-anaid');

            if($('#'+rowId+'_ana_tax').length > 0){
                var qty = $(value).val();
                var servicePrice = $('#'+rowId+'_ana_price').val();

                var price = ((qty * (servicePrice*10000))/10000);
                var tax =  (price / 1.2 ) * 0.2;
                tax = parseFloat(tax.toFixed(3));

                $('#'+rowId+'_ana_tax_span').html(tax);
                $('#'+rowId+'_ana_tax').val(tax);

                sumTax = sumTax + tax;
            }

        });

        return sumTax;
    },

    showModalMessage: function (message) {
        $('#exampleModalLabel').html(message);
        $('#exampleModalBtn').click();
    }

});

var updateAgreementAnnexController = new UpdateAgreementAnnexController();