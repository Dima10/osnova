var ApplSheetController = Marionette.View.extend({

    el: "#forApplSheetController",

    events: {
        "click #toPageBtn": "clickToPageBtn",
    },

    initialize: function() {
        if(parseInt(app.helper.getPage()) <= 0 || app.helper.getPage()==='' || app.helper.getPage()===null){
            $('#toPageInput').val(1);
        } else {
            $('#toPageInput').val(app.helper.getPage());
        }
    },

    clickToPageBtn: function(e) {
        var page = $('#toPageInput').val();
        app.helper.toPage(page);

    },

});

var applSheetController = new ApplSheetController();

