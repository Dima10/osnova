var ApplRequestController = Marionette.View.extend({

    el: "#forApplRequestController",

    events: {
        "click #createUpdateBtn": "clickCreateUpdateBtn",
        "click #toPageBtn": "clickToPageBtn",
    },

    initialize: function() {
        if(parseInt(app.helper.getPage()) <= 0 || app.helper.getPage()==='' || app.helper.getPage()===null){
            $('#toPageInput').val(1);
        } else {
            $('#toPageInput').val(app.helper.getPage());
        }
    },

    clickToPageBtn: function(e) {
        var page = $('#toPageInput').val();
        app.helper.toPage(page);

    },

    clickCreateUpdateBtn: function(e) {

        var curatorEmails = $('#applr_curator_addr').val();
        curatorEmails = curatorEmails.replace(/\s/g, '');
        $('#applr_curator_addr').val(curatorEmails);

        return true;

        // e.preventDefault();
        // e.stopPropagation();
    },

});

var applRequestController = new ApplRequestController();