var UserController = Marionette.View.extend({

    el: "#forUserController",

    events: {
        "change #userRole": "changeUserRole",
    },

    initialize: function() {

    },

    changeUserRole: function(e) {
        var self = this;

        var userId = $('#userId').val();
        var roleId = $('#userRole').val();
        if(confirm('Вы действительно хотите изменить роль?')) {

            $.ajax({
                url: '/index.php?r=ac-user-role/update-role'+'&userId='+userId+'&roleId='+roleId,
                type: 'POST',
                success: function(res) {
                    console.log(res);
                    $('#userRoleModal').modal('show');
                }
            });

        }

        e.preventDefault();
        e.stopPropagation();
    },

});

var userController = new UserController();