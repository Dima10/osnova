var UpdateAgreementActController = Marionette.View.extend({

    el: "#forUpdateAgreementActController",

    events: {
        "keyup .processUpdateIput": "recalculateSum",
        "click .deleteService": "deleteService",
        "click #showHideServiceId": "showHideServiceId",
        "click #add_svc": "addService",
    },

    initialize: function() {

    },

    deleteService: function(e) {

        if(confirm('Вы действительно хотите удалить запись?')){
            
            var actaId = e.currentTarget.dataset.acta_id;
            $('#tr_'+actaId).remove();
            this.recalculateSum();
            var actPrice = $('#agreementact-act_sum').val();
            var actTax = $('#agreementact-act_tax').val();

            var self = this;

            $.post(app.helper.getUrlToRoute('ajax-act','service-delete'), {actaId:actaId, actPrice: actPrice, actTax: actTax})
                .done(function (data) {
                    data = JSON.parse(data);

                    $('#showHideServiceId').click();

                    $('#newFileLink').html('<a style="color: red" href="'+data.documentLink+'" target="_blank">Новый файл сформирован</a>');
                    self.showModalMessage('Услуга удалена из акта');
                });
        }

        e.preventDefault();
        e.stopPropagation();
    },

    addService: function(e) {

        var _id = $("#annex_a").val();
        var _price = $("#price").val();
        var _qty = $("#qty").val();
        var _act_id = $("#actIdValue").val();

            var self = this;

            $.get(app.helper.getUrlToRoute('ajax-act', 'ajax-agreement-act-a-create'),
               {
                 ana_id : _id,
                 price : _price,
                 qty : _qty,
                 act_id : _act_id,
               },
               function (data) {
                   var data = JSON.parse(data);
                   if(data.row == ''){
                       return;
                   }

                   $('#servicesRows').append(data.row);

                   $('#newFileLink').html('<a style="color: red" href="'+data.documentLink+'" target="_blank">Новый файл сформирован</a>');

                   self.recalculateSum();
                   $('#showHideServiceId').click();
               }
            );

        e.preventDefault();
        e.stopPropagation();
    },

    //===================================
    showHideServiceId: function(e) {

        $('#serviceForm').toggleClass('hidden');

        if($('#serviceForm').is(':visible')){
            $("#agreementact-act_agr_id").trigger("change");
        }

        e.preventDefault();
        e.stopPropagation();
    },

    recalculateSum: function () {
        var sum = this.calculateSumOfServices();
        $('#agreementact-act_sum').val(sum);

        var sumTax = this.calculateSumOfTaxes();
        $('#agreementact-act_tax').val(sumTax);
    },

    calculateSumOfServices: function () {

        var sum = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-actaid');

            var qty = $(value).val();
            var servicePrice = $('#'+rowId+'_acta_price').val();

            sum = sum + ((qty * (servicePrice*10000))/10000);
        });

        return sum;
    },

    calculateSumOfTaxes: function () {

        var sumTax = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-actaid');

            if($('#'+rowId+'_acta_tax').length > 0 && $('#'+rowId+'_acta_tax').val() > 0){
                var qty = $(value).val();
                var servicePrice = $('#'+rowId+'_acta_price').val();

                var price = ((qty * (servicePrice*10000))/10000);
                var tax =  (price / 1.2 ) * 0.2;
                tax = parseFloat(tax.toFixed(3));

                $('#'+rowId+'_acta_tax_span').html(tax);
                $('#'+rowId+'_acta_tax').val(tax);

                sumTax = sumTax + tax;
            }

        });

        return sumTax;
    },

    showModalMessage: function (message) {
        $('#exampleModalLabel').html(message);
        $('#exampleModalBtn').click();
    }

});

var updateAgreementActController = new UpdateAgreementActController();