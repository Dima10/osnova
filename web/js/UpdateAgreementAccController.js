var UpdateAgreementAccController = Marionette.View.extend({

    el: "#forUpdateAgreementAccController",

    events: {
        "keyup .processUpdateIput": "recalculateSum",
        "click .deleteService": "deleteService",
        "click #showHideServiceId": "showHideServiceId",
        "click #add_svc": "addService",
    },

    initialize: function() {

    },

    deleteService: function(e) {

        if(confirm('Вы действительно хотите удалить запись?')){
            
            var acaId = e.currentTarget.dataset.aca_id;
            $('#tr_'+acaId).remove();
            this.recalculateSum();
            var agaPrice = $('#agreementacc-aga_sum').val();
            var agaTax = $('#agreementacc-aga_tax').val();

            var self = this;

            $.post(app.helper.getUrlToRoute('ajax-acc','service-delete'), {acaId:acaId, agaPrice: agaPrice, agaTax: agaTax})
                .done(function (data) {
                    data = JSON.parse(data);

                    $('#showHideServiceId').click();

                    $('#newFileLink').html('<a style="color: red" href="'+data.documentLink+'" target="_blank">Новый файл сформирован</a>');
                    self.showModalMessage('Услуга удалена из счета');
                });
        }

        e.preventDefault();
        e.stopPropagation();
    },

    addService: function(e) {

        var _id = $("#annex_a").val();
        var _price = $("#price").val();
        var _qty = $("#qty").val();
        var _aga_id = $("#accIdValue").val();

            var self = this;

            $.get(app.helper.getUrlToRoute('ajax-acc', 'ajax-agreement-acc-a-create'),
               {
                 ana_id : _id,
                 price : _price,
                 qty : _qty,
                 aga_id : _aga_id,
               },
               function (data) {
                   var data = JSON.parse(data);
                   if(data.row == ''){
                       return;
                   }

                   $('#servicesRows').append(data.row);

                   $('#newFileLink').html('<a style="color: red" href="'+data.documentLink+'" target="_blank">Новый файл сформирован</a>');

                   self.recalculateSum();
                   $('#showHideServiceId').click();
               }
            );

        e.preventDefault();
        e.stopPropagation();
    },

    //===================================
    showHideServiceId: function(e) {

        $('#serviceForm').toggleClass('hidden');

        if($('#serviceForm').is(':visible')){
            $("#agreementacc-aga_agr_id").trigger("change");
        }

        e.preventDefault();
        e.stopPropagation();
    },

    recalculateSum: function () {
        var sum = this.calculateSumOfServices();
        $('#agreementacc-aga_sum').val(sum);

        var sumTax = this.calculateSumOfTaxes();
        $('#agreementacc-aga_tax').val(sumTax);
    },

    calculateSumOfServices: function () {

        var sum = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-acaid');

            var qty = $(value).val();
            var servicePrice = $('#'+rowId+'_aca_price').val();

            sum = sum + ((qty * (servicePrice*10000))/10000);
        });

        return sum;
    },

    calculateSumOfTaxes: function () {

        var sumTax = 0;

        $('.inputQty').each(function (index, value) {
            var rowId = $(value).data('relate-acaid');

            if($('#'+rowId+'_aca_tax').length > 0 && $('#'+rowId+'_aca_tax').val() > 0){
                var qty = $(value).val();
                var servicePrice = $('#'+rowId+'_aca_price').val();

                var price = ((qty * (servicePrice*10000))/10000);
                var tax =  (price / 1.2 ) * 0.2;
                tax = parseFloat(tax.toFixed(3));

                $('#'+rowId+'_aca_tax_span').html(tax);
                $('#'+rowId+'_aca_tax').val(tax);

                sumTax = sumTax + tax;
            }

        });

        return sumTax;
    },

    showModalMessage: function (message) {
        $('#exampleModalLabel').html(message);
        $('#exampleModalBtn').click();
    }

});

var updateAgreementAccController = new UpdateAgreementAccController();