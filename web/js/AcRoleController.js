var AcRoleController = Marionette.View.extend({

    el: "#forAcRoleController",
    events: {
        "click .accessCheckBox": "clickAccessCheckBox",
        "click #selectAllAccess": "clickSelectAllAccess",
        "click #unselectAllAccess": "clickUnselectAllAccess",
        "click #updateAccess": "clickUpdateAccess",
    },

    clickAccessCheckBox: function(e) {

    },

    clickSelectAllAccess: function(e) {
        $('.accessCheckBox').prop('checked', true);
        e.stopPropagation();
        e.preventDefault();
    },

    clickUnselectAllAccess: function(e) {
        $('.accessCheckBox').prop('checked', false);
        e.stopPropagation();
        e.preventDefault();
    },

    clickUpdateAccess: function() {

        var self = this;
        var roleId = $('#roleId').val();
        var idsForAccess = [];

        $('.accessCheckBox').each(function (i, v) {
            if($(v).is(':checked')){
                idsForAccess.push(parseInt($(v).val()));
            }
        });

        $.ajax({
            url: '/index.php?r=ac-role/update-access'+'&roleId='+roleId+'&idsForAccess='+idsForAccess.toString(),
            type: 'POST',
            success: function(res) {
                alert('Список доступов для роли обновлен');
            }
        });

    },

});

var acRoleController = new AcRoleController();

