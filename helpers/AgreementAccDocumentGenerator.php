<?php

namespace app\helpers;

use app\models\Account;
use app\models\Address;
use app\models\AgreementAcc;
use app\models\AgreementAccA;
use app\models\AgreementAct;
use app\models\AgreementActA;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use app\models\AgreementStatus;
use app\models\Bank;
use app\models\Constant;
use app\models\Entity;
use app\models\Pattern;
use app\models\Person;
use app\models\Staff;
use app\models\Tools;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class AgreementAccDocumentGenerator extends DocumentGenerator
{
    protected $agaId;

    public function __construct($agaId)
    {
        $this->agaId = $agaId;
    }

    public function MSWordDocument()
    {

        $model = AgreementAcc::findOne($this->agaId);

        if (isset($model->agaAgr)) {
            $date_id = AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id])->count('aga_date') + 1;
            $number = $model->agaAgr->agr_number.'/'.$date_id;

            while (AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id, 'aga_number' => $number])->exists()) {
                $date_id ++;
                $number = $model->agaAgr->agr_number.'/'.$date_id;
            }

            $model->aga_number = $model->agaAgr->agr_number.'/'.$date_id;
        }

        $pattern = Pattern::findOne($model->aga_pat_id);
        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
        }

        $servicesRow = AgreementAccA::find()
            ->select('aca_ana_id as ana_id, aca_price as price, aca_tax as tax, aca_qty as qty')
            ->where(['aca_aga_id' => $this->agaId])->createCommand()
            ->queryAll();

        $grid = [];
        foreach($servicesRow as $k => $serviceRow){
            $ana = AgreementAnnexA::findOne($serviceRow['ana_id']);
            $serviceRow['tax'] = floatval($serviceRow['tax']);
            $serviceRow['ana_name'] =
//                            ($ana->agreementAccAs[0]->acaAga->aga_number ?? '').' *** '.
//                            (isset($ana->anaAgra) ? $ana->anaAgra->agra_number : '') . ' / ' .
                (isset($ana->anaSvc) ? $ana->anaSvc->svc_name : '') . ' / ' .
                (isset($ana->anaTrp) ? $ana->anaTrp->trp_name : '');
            $grid[$k] = $serviceRow;
        }

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
        $file_name = Yii::getAlias('@app') . '/storage/' . str_replace('/', '-', $model->aga_number).' '.$pattern->pat_fname;

        if(isset($file_name) && file_exists($file_name)){
            chmod($tmpl_name, 0777);
            chmod($file_name, 0777);
        }

        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $vars = $document->getVariables();

        if (array_search('num', $vars) !== false) {
            $document->cloneRow('num', count($grid));
        }

        $model->aga_fdata = str_replace('/', '-', $model->aga_number).' '.$pattern->pat_fname;

        $tag_val = $model->aga_number;
        $document->setValue('DOC_NUMBER', $tag_val);

        $tag_val = \DateTime::createFromFormat('Y-m-d', $model->aga_date)->format('d-m-Y');
        $document->setValue('DOC_DATE', $tag_val);

        if (!is_null($model->agaAgr)) {
            $tag_val = $model->agaAgr->agr_number;
            $document->setValue('AGR_NUMBER', $tag_val);
        }

        if (!is_null($model->agaAgr)) {
            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agaAgr->agr_date)->format('d-m-Y');
            $document->setValue('AGR_DATE', $tag_val);
        }
        if ($obj = Entity::findOne($model->aga_ab_id)) {
            $tag_val = $obj->entEntt->entt_name;
            $document->setValue('ORGANISATION_TYPE_FULL', $tag_val);
        }
        if ($obj = Entity::findOne($model->aga_ab_id)) {
            $tag_val = $obj->ent_name;
            $document->setValue('ORGANISATION_NAME_FULL', $tag_val);
        }
        if ($obj = Address::find()->where(['add_id'=>$model->aga_add_id])->one()) {
            $tag_val =
                (!empty($obj->add_index) ? $obj->add_index.', ' : '') .
                (!empty($obj->addCou) ? $obj->addCou->cou_name.', ' : '') .
                (!empty($obj->addReg)? $obj->addReg->reg_name.', ' : '') .
                (!empty($obj->addCity) ? $obj->addCity->city_name.', ' : '') .
                (!empty($obj->add_data) ? $obj->add_data : '');
            $document->setValue('ORGANISATION_U_ADDRESS', $tag_val);
        }

        if ($obj = Entity::findOne($model->aga_ab_id)) {
            $tag_val = $obj->ent_inn;
            $document->setValue('ORGANISATION_INN', $tag_val);
        }

        if ($obj = Entity::findOne($model->aga_ab_id)) {
            $tag_val = $obj->ent_kpp;
            $document->setValue('ORGANISATION_KPP', $tag_val);
        }
        if ($obj = Account::findOne(['acc_id' => $model->aga_acc_id])) {
            $tag_val = $obj->acc_number;
            $document->setValue('ORGANISATION_R_SCHET', $tag_val);
        }
        if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
            if ($obj = Bank::findOne($acc->acc_bank_id)) {
                $tag_val = $obj->bank_name;
                $document->setValue('ORGANISATION_BANK_NAME', $tag_val);
            }
        }
        if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
            if ($obj = Bank::findOne($acc->acc_bank_id)) {
                $tag_val = $obj->bank_account;
            }
        } else {
            $tag_val = '';
        }
        $document->setValue('ORGANISATION_BANK_K_SCHET', $tag_val);

        if ($acc = Account::findOne(['acc_id' => $model->aga_acc_id])) {
            if ($obj = Bank::findOne($acc->acc_bank_id)) {
                $tag_val = $obj->bank_bic;
            }
        } else {
            $tag_val = '';
        }
        $document->setValue('ORGANISATION_BANK_BIK', $tag_val);

        if ($obj = Person::findOne($model->aga_prs_id)) {
            $tag_val = $obj->prs_full_name;
            $document->setValue('ORGANISATION_FIO', $tag_val);
        }

        if ($obj = Staff::find()->where(['stf_ent_id' => $model->aga_ab_id, 'stf_prs_id' => $model->aga_prs_id])->one()) {
            $tag_val = $obj->stf_position;
            $document->setValue('ORGANISATION_DOLGNOST', $tag_val);
        }

        $tag_val = number_format($model->aga_sum, 2);
        $document->setValue('DOC_SUM', $tag_val);

        $tag_val = Tools::num2str($model->aga_sum);
        $document->setValue('DOC_SUM_TEXT', $tag_val);

        $tag_val = number_format($model->aga_tax, 2);
        $document->setValue('DOC_TAX', $tag_val);

        $tag_val = Tools::num2str($model->aga_tax);
        $document->setValue('DOC_TAX_TEXT', $tag_val);

        $qty = 0;
        for ($i=1; $i <= count($grid); $i++) {
            $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
            $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];

            //$qty += $grid[$i-1]['qty'];
            $qty = $i;

            $document->setValue('num#'.$i, $i);
            $document->setValue('SVC#'.$i, $grid[$i-1]['ana_name']);

            $document->setValue('PRICE#'.$i, number_format($grid[$i-1]['price'], 2));
            $document->setValue('QTY#'.$i,  $grid[$i-1]['qty']);
            $document->setValue('SUM_T#'.$i, $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2));
            $document->setValue('SUM#'.$i, number_format($sum, 2));
        }

        $document->setValue('DOC_QTY', $qty);

        $document->saveAs($file_name);

        $model->aga_data = file_get_contents($file_name);

        chmod($tmpl_name, 0777);
        chmod($file_name, 0777);
        unlink($file_name);
        unlink($tmpl_name);

        $model->save();

        return '/index.php?r=entity-frm/download-agreement-acc&id='.$model->aga_id;

    }
}
