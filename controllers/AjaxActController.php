<?php

namespace app\controllers;

use app\helpers\AgreementActDocumentGenerator;
use app\helpers\DocumentGenerator;
use app\models\AgreementAcc;
use app\models\AgreementAccA;
use app\models\AgreementAct;
use app\models\AgreementActA;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\UploadedFile;

class AjaxActController extends Controller
{

    public function actionAjaxAgreementActACreate($ana_id, $price, $qty, $act_id, $date='2019-01-01') {
        $ana = AgreementAnnexA::findOne($ana_id);
        $act = AgreementAct::findOne($act_id);

        $acta = AgreementActA::find()->select(['acta_qty', 'acta_price'])->where('acta_ana_id = '.$ana_id)->all();

        $sum = 0;
        foreach ($acta as $actaItem) {
            $sum = $sum + $actaItem->acta_qty*$actaItem->acta_price;
        }

        if(($sum + ($price*$qty)) > ($ana->ana_price*$ana->ana_qty)){
            return json_encode(['row' => '', 'serviceId' => '', 'documentLink' => '' ]);
        }

        $serviceRow = new AgreementActA();

        $serviceRow->setAttribute('acta_act_id', $act_id);
        $serviceRow->setAttribute('acta_ana_id', $ana->ana_id);
        $serviceRow->setAttribute('acta_qty', $qty);
        $serviceRow->setAttribute('acta_price', $price);
        $serviceRow->setAttribute('acta_tax', $ana->anaSvc->svc_tax_flag==1 ? ( (strtotime($date) >= strtotime(date('2019-01-01'))) ? ($price / 1.2 ) * 0.2 : ($price / 1.18 ) * 0.18) : null);

        $serviceRow->setAttribute('acta_create_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('acta_create_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('acta_create_ip', Yii::$app->request->userIP);
        $serviceRow->setAttribute('acta_update_user', Yii::$app->user->identity->username);
        $serviceRow->setAttribute('acta_update_time', date('Y-m-d H:i:s'));
        $serviceRow->setAttribute('acta_update_ip', Yii::$app->request->userIP);

        $serviceRow->save();

        $act->act_sum += floatval($price) * intval($qty);
        $act->act_tax += floatval($serviceRow->acta_tax);
        $act->save();

        $spanTax = null;
        $inputTax = null;
        if($serviceRow->acta_tax != null){
            $spanTax = '<span id="'.$serviceRow->acta_id.'_acta_tax_span" class="'.$serviceRow->acta_id.'_processTax">'.$serviceRow->acta_tax.'</span>';
            $inputTax = Html::input('text','acta_tax', $serviceRow->acta_tax, ['class'=>'hidden '.$serviceRow->acta_id.'_processTax', 'readonly'=>'readonly', 'id'=>$serviceRow->acta_id.'_acta_tax', 'size'=>8, 'style'=>'text-align:center']);
        }

        $row = '<tr id="tr_'.$serviceRow->acta_id.'" data-key="'.$serviceRow->acta_id.'">
                    <td> - </td>
                    <td>
                        <a href="/index.php?r=svc%2Fview&id='.$ana->anaSvc->svc_id.'" title="Просмотр" target="_blank"><span class="glyphicon glyphicon-eye-open"></span></a>  
                        <a class="deleteService" href="/index.php?r=entity-frm/delete&id='.$serviceRow->acta_id.'" title="Удалить" data-acta_id="'.$serviceRow->acta_id.'"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td> '.$serviceRow->acta_id.' </td>

                    <td>'.($ana->agreementAccAs[0]->acaAga->aga_number ?? '').' *** '.
                            (isset($ana->anaAgra) ? $ana->anaAgra->agra_number : '') . ' / ' .
                            (isset($ana->anaSvc) ? $ana->anaSvc->svc_name : '') . ' / ' .
                            (isset($ana->anaTrp) ? $ana->anaTrp->trp_name : '').
                    '</td>

                    <td><span id="'.$serviceRow->acta_id.'_priceQty" class="'.$serviceRow->acta_id.'_processUpdate">'.$qty.'</span><input type="hidden" id="'.$serviceRow->acta_id.'_acta_qty" class="'.$serviceRow->acta_id.'_processUpdate processUpdateIput inputQty" name="acta_qty" value="'.$qty.'" size="5" data-relate-actaid="'.$serviceRow->acta_id.'" style="text-align:center"></td>
                    <td><span id="'.$serviceRow->acta_id.'_priceLabel" class="'.$serviceRow->acta_id.'_processUpdate">'.$price.'</span><input type="hidden" id="'.$serviceRow->acta_id.'_acta_price" class="'.$serviceRow->acta_id.'_processUpdate processUpdateIput" name="acta_price" value="'.$price.'" size="8" style="text-align:center"></td>
                    
                    <td>'.($serviceRow->acta_tax != null && $ana->anaSvc->svc_tax_flag==1 ? $spanTax.$inputTax : "Без НДС").'</td>
                    <td>'.Yii::$app->user->identity->username.'</td>
                    <td>'.date('Y-m-d H:i:s').'</td>
                    <td>'.Yii::$app->request->userIP.'</td>
                </tr>';


        $documentGenerator = new AgreementActDocumentGenerator($act_id);
        $documentLink = $documentGenerator->rtfDocument();

        return json_encode(['row' => $row, 'serviceId' =>$serviceRow->acta_id, 'documentLink' => $documentLink ]);
    }

    public function actionServiceDelete() {

        $actaId = $_POST['actaId'];
        $actPrice = $_POST['actPrice'];
        $actTax = $_POST['actTax'];

        $serviceRow = AgreementActA::findOne($actaId);
        $act = AgreementAct::findOne($serviceRow->acta_act_id);

        $act->setAttribute('act_sum', $actPrice);
        $act->setAttribute('act_tax', $actTax);
        $act->save();

        $serviceRow->delete();

        $documentGenerator = new AgreementActDocumentGenerator($act->act_id);
        $documentLink = $documentGenerator->rtfDocument();

        return json_encode(
            [
                'result' => true,
                'documentLink' => $documentLink,
            ]
        );

    }

}
