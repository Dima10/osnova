<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{Agreement, AgreementSearch, AgreementStatus, Entity, Company, Excel_XML};

use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgreementController implements the CRUD actions for Agreement model.
 */
class AgreementController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Agreement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/AgreementController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new AgreementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['AgreementSearchParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entity' => ArrayHelper::map(Entity::find()->select('ent_id, ent_name')->all(), 'ent_id', 'ent_name'),
            'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
        ]);
    }

    /**
     * Displays a single Agreement model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agreement();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->agr_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agr_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agr_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            ]);
        }
        */
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->agr_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata = $model->oldAttributes['agr_fdata'];
                $model->agr_data = $model->oldAttributes['agr_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata_sign = $model->oldAttributes['agr_fdata_sign'];
                $model->agr_data_sign = $model->oldAttributes['agr_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agr_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agr_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'addrbook' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            ]);
        }
        */
    }

    /**
     * Deletes an existing Agreement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = Agreement::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agr_data, $model->agr_fdata);

    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSign($id)
    {
        return $this->redirect(['download-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadSignF($id)
    {
        $model = Agreement::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agr_data_sign, $model->agr_fdata_sign);

    }



    /**
     * Finds the Agreement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agreement the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agreement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Export to Excell
     * @see AgreementSearch->search()
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml()
    {
        $query = Agreement::find();

        $searchModel = new AgreementSearch();
        $searchModel->load(Yii::$app->session['AgreementSearchParams']);

        $query->andFilterWhere([
            'agr_ab_id' => $searchModel->agr_ab_id,
            'agr_comp_id' => $searchModel->agr_comp_id,
            'agr_ast_id' => $searchModel->agr_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'agr_id', $searchModel->agr_id])
            ->andFilterWhere(['like', 'agr_number', $searchModel->agr_number])
            ->andFilterWhere(['like', 'agr_date', $searchModel->agr_date])
            ->andFilterWhere(['like', 'agr_sum', $searchModel->agr_sum])
            ->andFilterWhere(['like', 'agr_tax', $searchModel->agr_tax])
            //->andFilterWhere(['like', 'agr_fdata', $this->agr_fdata])
            //->andFilterWhere(['like', 'agr_fdata_sign', $this->agr_fdata_sign])
            ->andFilterWhere(['like', 'agr_create_time', $searchModel->agr_create_time])
            ->andFilterWhere(['like', 'agr_create_ip', $searchModel->agr_create_ip])
            ->andFilterWhere(['like', 'agr_update_time', $searchModel->agr_update_time])
            ->andFilterWhere(['like', 'agr_update_user', $searchModel->agr_update_user])
            ->andFilterWhere(['like', 'agr_update_ip', $searchModel->agr_update_ip])
            ->andFilterWhere(['like', 'agr_create_user', $searchModel->agr_create_user])
        ;

        $data[] = [
            Yii::t('app', 'Agr ID'),
            Yii::t('app', 'Agr Number'),
            Yii::t('app', 'Agr Date'),
            Yii::t('app', 'Agr Ast ID'),
            Yii::t('app', 'Agr Ab ID'),
            Yii::t('app', 'Agr Comp ID'),
            Yii::t('app', 'Agr Sum'),
            Yii::t('app', 'Agr Tax'),
            Yii::t('app', 'Agr Fdata'),
            Yii::t('app', 'Agr Fdata Sign'),
        ];

        foreach ($query->all() as &$model) {
            $data[] = [
                $model->agr_id ?? '',
                $model->agr_number ?? '',
                $model->agr_date ?? '',
                $model->agrAst->ast_name ?? '',
                $model->agrAb->ab_name ?? '',
                $model->agrComp->comp_name ?? '',
                $model->agr_sum ?? '',
                $model->agr_tax ?? '',
                $model->agr_fdata ?? '',
                $model->agr_fdata_sign ?? '',
            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'Agreement';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'Agreement.xls');
    }
}
