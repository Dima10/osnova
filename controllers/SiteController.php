<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\SentEmailsFromRequest;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Yii::$app->on(\app\models\PersonVisit::EVENT_PERSON_VISIT, ['\app\models\PersonVisit', 'updateState']);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->level < 0)) {
            return $this->redirect(['person/training-program']);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $nowDate = new \DateTime('now');
            SentEmailsFromRequest::updateAll(['portal' => $nowDate->format('Y-m-d H:i:s')], ['portal'=>'0000-00-00 00:00:00', 'login'=>$model->username]);

            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('http://pdo-osnova.ru');
        //return $this->goHome();

    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays core page.
     *
     * @return string
     */
    public function actionCore()
    {
        return $this->render('core');
    }

    /**
     * Displays setting page.
     *
     * @return string
     */
    public function actionSetting()
    {
        return $this->render('setting');
    }

    /**
     * Displays finance page.
     *
     * @return string
     */
    public function actionFinance()
    {
        return $this->render('finance');
    }

    /**
     * Displays frm page.
     *
     * @return string
     */
    public function actionFrm()
    {
        return $this->render('frm');
    }

    /**
     * Displays acl page.
     *
     * @return string
     */
    public function actionAcl()
    {
        return $this->render('acl');
    }

    /**
     * Displays Svc page.
     *
     * @return string
     */
    public function actionSvc()
    {
        return $this->render('svc');
    }

    /**
     * Save user time on site
     */
    public function actionTimeOn()
    {
        Yii::$app->trigger(\app\models\PersonVisit::EVENT_PERSON_VISIT, new \yii\base\Event(['sender' => $this]));
    }
}
