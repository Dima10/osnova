<?php

namespace app\controllers;

use app\models\Ab;
use app\models\Agreement;
use app\models\AgreementAcc;
use app\models\AgreementAnnex;
use app\models\AgreementStatus;
use app\models\ApplFinal;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplRequestContentReportSearch;
use app\models\ApplSh;
use app\models\ApplSheet;
use app\models\Entity;
use app\models\EntityClass;
use app\models\EntityClassLnk;
use app\models\Excel_XML;
use app\models\Person;
use app\models\SentEmailsFromRequest;
use app\models\SentEmailsFromRequestSearch;
use app\models\Staff;
use app\models\Svc;
use app\models\SvcDocType;
use app\models\TrainingProg;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ReportRequestController implements the R actions.
 */
class ReportRequestMailingController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SentEmailsFromRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['SentEmailsFromRequestSearch'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplRequestContentReportSearch the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApplRequestContentReportSearch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Export to Excell
     * @see ApplRequestContentReportSearch->search()
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionExportXml()
    {
        $query = SentEmailsFromRequest::find()
            ->leftJoin(ApplSh::tableName(). ' as sh', '(sh.appls_prs_id = applrc_prs_id AND sh.appls_trp_id = applrc_trp_id)' )
            ->leftJoin(ApplSheet::tableName(). ' as sheet', '(sheet.appls_prs_id = applrc_prs_id AND sheet.appls_trp_id = applrc_trp_id)' )
            ->leftJoin(TrainingProg::tableName(), 'applrc_trp_id = trp_id')
            ->leftJoin(Ab::tableName().' as ab1', 'applr_ab_id = ab1.ab_id')
            ->leftJoin(Ab::tableName().' as ab2', 'applr_manager_id = ab2.ab_id')
            ->leftJoin(ApplFinal::tableName(), '(applf_prs_id = applrc_prs_id AND applf_trp_id = applrc_trp_id)')
        ;

        $searchModel = new SentEmailsFromRequestSearch();
        $searchModel->load(Yii::$app->session['SentEmailsFromRequestSearch']);

        $query
            ->andFilterWhere(['=', 'applr_id', $searchModel->applr_id])
            ->andFilterWhere(['=', 'applrc_prs_id', $searchModel->applrc_prs_id])
            ->andFilterWhere(['like', 'login', $searchModel->login])
            ->andFilterWhere(['like', 'fio', $searchModel->fio])
            ->andFilterWhere(['like', 'trp_name', $searchModel->trp_name])
            ->andFilterWhere(['=', 'ab1.ab_id', $searchModel->ab_id])
            ->andFilterWhere(['like', 'ab1.ab_name', $searchModel->ab_name])
            ->andFilterWhere(['like', 'ab2.ab_name', $searchModel->manager_ab_name])
            ->andFilterWhere(['=', 'sh.appls_passed', $searchModel->appls_sh_passed])
            ->andFilterWhere(['like', 'sh.appls_date', $searchModel->appl_sh_date_end])
            ->andFilterWhere(['=', 'sheet.appls_passed', $searchModel->appls_sheet_passed])
            ->andFilterWhere(['like', 'sheet.appls_date', $searchModel->appl_sheet_date_end])
            ->andFilterWhere(['like', 'applf_end_date', $searchModel->applf_end_date])
            ->andFilterWhere(['like', 'recipient_email', $searchModel->recipient_email])
            ->andFilterWhere(['like', 'sender_email', $searchModel->sender_email])
            ->andFilterWhere(['like', 'send_date', $searchModel->send_date])
            ->andFilterWhere(['like', 'portal', $searchModel->portal])
            ->andFilterWhere(['=', 'status', $searchModel->status])
            ->andFilterWhere(['=', 'applr_flag', $searchModel->applr_flag])
            ->andFilterWhere(['=', 'applr_reestr', $searchModel->applr_reestr]);

        if(isset($searchModel->applf_file_name) && $searchModel->applf_file_name === ''){
        }
        elseif (isset($searchModel->applf_file_name) && $searchModel->applf_file_name == 1){
            $query->andFilterWhere(['is not', 'applf_file_name', new \yii\db\Expression('null')]);
        } else if (isset($searchModel->applf_file_name) && $searchModel->applf_file_name == 0) {
            $query->andFilterWhere(['is', 'applf_file_name', new \yii\db\Expression('null')]);
        }

        $data[] = [
            Yii::t('app', 'Appl Request'),
            Yii::t('app', 'Prs ID'),
            Yii::t('app', 'Prs Connect User'),
            Yii::t('app', 'Prs Full Name'),
            Yii::t('app', 'Sprog Trp ID'),
            Yii::t('app', 'Ab ID Code'),
            Yii::t('app', 'Ab Name UR'),
            Yii::t('app', 'Applr Manager ID'),
            Yii::t('app', 'Recipient Email'),
            Yii::t('app', 'Sender Email'),
            Yii::t('app', 'Status'),
            Yii::t('app', 'Send Date'),
            Yii::t('app', 'Applr Reestr'),
            Yii::t('app', 'Applr Flag'),
            Yii::t('app', 'Время первого входа на портал после отправки'),
            Yii::t('app', 'Appl Sh Test'),
            Yii::t('app', 'Appl Sh Test Date End'),
            Yii::t('app', 'Appl Sheet Test'),
            Yii::t('app', 'Appl Sheet Test Date End'),
            Yii::t('app', 'Finish education'),
            Yii::t('app', 'Applf End Date'),
        ];

        foreach ($query->all() as &$model) {
            $data[] = [
                $model->applr_id ?? '',
                $model->applrc_prs_id ?? '',
                $model->login ?? '',
                $model->fio ?? '',
                $model->applrcTrp->trp_name ?? '',
                $model->ab->ab_id ?? '',
                $model->ab->ab_name ?? '',
                $model->manager->ab_name ?? '',
                $model->recipient_email ?? '',
                $model->sender_email ?? '',
                ($model->status == 1) ? 'Отправлено' : 'Не отправлено',
                $model->send_date ?? '',
                \app\models\Constant::reestr_val()[$model->applr_reestr ?? ''],
                \app\models\Constant::YES_NO[$model->applr_flag ?? ''],
                $model->portal ?? '',
                (isset($model->applSh) && $model->applSh->appls_passed == 1) ? 'Сдано' : 'Не сдано',
                $model->applSh->appls_date ?? '',
                (isset($model->applSheet) && $model->applSheet->appls_passed == 1) ? 'Сдано' : 'Не сдано',
                $model->applSheet->appls_date ?? '',
                (!empty($model->applFinal->applf_file_name)) ? 'Завершено' : 'Не завершено',
                $model->applFinal->applf_end_date ?? '',
            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'ReportRequestMailing';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'ReportRequestMailing.xls');

    }
}
