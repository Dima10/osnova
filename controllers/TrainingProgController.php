<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{Constant,
    TrainingProg,
    TrainingProgSearch,
    TrainingModule,
    TrainingProgModule,
    TrainingProgModuleSearch};

use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * TrainingProgController implements the CRUD actions for TrainingProg model.
 */
class TrainingProgController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['download-a1', 'download-a1f', 'download-a2', 'download-a2f', 'download-b1', 'download-b1f', 'download-b2', 'download-b2f'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all TrainingProg models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingProgSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reestr' => Constant::reestr_val(true),
        ]);
    }

    /**
     * Displays a single TrainingProg model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModelProgModule = new TrainingProgModuleSearch();
        $dataProviderProgModule = $searchModelProgModule->search(['TrainingProgModuleSearch' => ['trpl_trp_id' => $id]]);

        return $this->render('view', [
            'model' => $model,
            'prog' => ArrayHelper::map(TrainingProg::find()->where(['trp_id'=>$model->trp_id])->all(), 'trp_id', 'trp_name'),
            'module' => ArrayHelper::map(TrainingModule::find()->all(), 'trm_id', 'trm_name'),
            'dataProviderProgModule' => $dataProviderProgModule,
            'searchModelProgModule' => $searchModelProgModule,
            'reestr' => Constant::reestr_val(true),
        ]);
    }

    /**
     * Creates a new TrainingProg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingProg();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trp_dataA1')) != null) {
                $model->trp_dataA1_name = $file->name;
                $model->trp_dataA1 = file_get_contents($file->tempName);
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataA2')) != null) {
                $model->trp_dataA2_name = $file->name;
                $model->trp_dataA2 = file_get_contents($file->tempName);
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB1')) != null) {
                $model->trp_dataB1_name = $file->name;
                $model->trp_dataB1 = file_get_contents($file->tempName);
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB2')) != null) {
                $model->trp_dataB2_name = $file->name;
                $model->trp_dataB2 = file_get_contents($file->tempName);
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trp_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'reestr' => Constant::reestr_val(true),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'reestr' => Constant::reestr_val(true),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trp_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        */
    }

    /**
     * Updates an existing TrainingProg model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'trp_dataA1')) != null) {
                $model->trp_dataA1_name = $file->name;
                $model->trp_dataA1 = file_get_contents($file->tempName);
            } else {
                $model->trp_dataA1_name = $model->oldAttributes['trp_dataA1_name'];
                $model->trp_dataA1 = $model->oldAttributes['trp_dataA1'];
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataA2')) != null) {
                $model->trp_dataA2_name = $file->name;
                $model->trp_dataA2 = file_get_contents($file->tempName);
            } else {
                $model->trp_dataA2_name = $model->oldAttributes['trp_dataA2_name'];
                $model->trp_dataA2 = $model->oldAttributes['trp_dataA2'];
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB1')) != null) {
                $model->trp_dataB1_name = $file->name;
                $model->trp_dataB1 = file_get_contents($file->tempName);
            } else {
                $model->trp_dataB1_name = $model->oldAttributes['trp_dataB1_name'];
                $model->trp_dataB1 = $model->oldAttributes['trp_dataB1'];
            }
            if (($file = UploadedFile::getInstance($model, 'trp_dataB2')) != null) {
                $model->trp_dataB2_name = $file->name;
                $model->trp_dataB2 = file_get_contents($file->tempName);
            } else {
                $model->trp_dataB2_name = $model->oldAttributes['trp_dataB2_name'];
                $model->trp_dataB2 = $model->oldAttributes['trp_dataB2'];
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->trp_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'reestr' => Constant::reestr_val(true),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'reestr' => Constant::reestr_val(true),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trp_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        */
    }


    /**
     * Deletes an existing TrainingProg model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainingProg model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingProg the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = TrainingProg::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadA1($id) { return $this->redirect(['download-a1f', 'id' => $id]); }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadA1f($id) { $model = TrainingProg::findOne($id); return Yii::$app->response->sendContentAsFile($model->trp_dataA1, $model->trp_dataA1_name); }
    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadA2($id) { return $this->redirect(['download-a2f', 'id' => $id]); }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadA2f($id) { $model = TrainingProg::findOne($id); return Yii::$app->response->sendContentAsFile($model->trp_dataA2, $model->trp_dataA2_name); }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadB1($id) { return $this->redirect(['download-b1f', 'id' => $id]); }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadB1f($id) { $model = TrainingProg::findOne($id); return Yii::$app->response->sendContentAsFile($model->trp_dataB1, $model->trp_dataB1_name); }
    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadB2($id) { return $this->redirect(['download-b2f', 'id' => $id]); }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadB2f($id) { $model = TrainingProg::findOne($id); return Yii::$app->response->sendContentAsFile($model->trp_dataB2, $model->trp_dataB2_name); }



    /**
     * Creates a new TrainingProgModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $trp_id
     * @return mixed
     */
    public function actionCreateProgModule($trp_id)
    {
        $model = new TrainingProgModule();

        $model->trpl_trp_id = $trp_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trpl_trp_id]);
        } else {

            $module = ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'CONCAT(trm_code, \' \', trm_name) as trm_name'])->leftJoin(TrainingProgModule::tableName(), 'trm_id = trpl_trm_id AND trpl_trp_id ='. $trp_id)->where(['trpl_id' => null])->all(), 'trm_id', 'trm_name');
            $module_ex = ArrayHelper::map(TrainingModule::find()->select(['trm_trm_id', 'trm_trm_id'])->where(['not', ['trm_trm_id' => null]])->groupBy(['trm_trm_id', 'trm_trm_id'])->all(), 'trm_trm_id', 'trm_trm_id');
            $module = array_diff_key($module, $module_ex);

            return $this->render('create_prog_module', [
                'model' => $model,
                'prog' => ArrayHelper::map(TrainingProg::find()->where(['trp_id' => $trp_id])->all(), 'trp_id', 'trp_name'),
                'module' => [0=>'<>'] + $module,
            ]);
        }
    }

    /**
     * List filtered TrainingMudule models.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $trp_id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxModuleSearch($trp_id, $text)
    {
        $module = ArrayHelper::map(
            TrainingModule::find()
                ->select(['trm_id', 'CONCAT(trm_code, \' \', trm_name) as trm_name'])
                ->leftJoin(TrainingProgModule::tableName(), 'trm_id = trpl_trm_id AND trpl_trp_id ='. $trp_id)
                ->where(['trpl_id' => null])
                ->andWhere(['like', 'trm_name', $text])->all(), 'trm_id', 'trm_name');
        $module_ex = ArrayHelper::map(TrainingModule::find()->select(['trm_trm_id', 'trm_trm_id'])->where(['not', ['trm_trm_id' => null]])->groupBy(['trm_trm_id', 'trm_trm_id'])->all(), 'trm_trm_id', 'trm_trm_id');
        $module = array_diff_key($module, $module_ex);

        return $this->renderAjax('_ajax_module', [
            'module' => $module,
        ]);
    }


    /**
     * Deletes an existing TrainingProgModule model.
     * If deletion is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteProgModule($id)
    {
        $model = TrainingProgModule::findOne($id);
        $trp_id = $model->trpl_trp_id;
        $model->delete();

        return $this->redirect(['view', 'id' => $trp_id]);
    }

}
