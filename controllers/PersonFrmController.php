<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{ApplRequest,
    ApplRequestContent,
    ContactType,
    Person,
    PersonSearch,
    Contact,
    AgreementAccA,
    AgreementAccASearch,
    Account,
    Bank,
    Agreement,
    AgreementAnnex,
    AgreementAnnexA,
    AgreementAnnexASearch,
    AgreementAct,
    AgreementActA,
    AgreementActASearch,
    AgreementAcc,
    AgreementStatus,
    Staff,
    Svc,
    SvcProg,
    Pattern,
    Company,
    Tag,
    Tools,
    TrainingProg,
    FinanceBook};

use yii\data\ActiveDataProvider;
use yii\base\DynamicModel;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * PersonFrmController implements the CRUD actions for Person model.
 */
class PersonFrmController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Person models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Person model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        // Contact
        $dataProviderContact = new ActiveDataProvider([
            'query' => Contact::find()->where(['con_ab_id' => $model->prs_id]),
        ]);


        // Agreement
        $dataProviderAgreement = new ActiveDataProvider([
            'query' => Agreement::find()->where(['agr_ab_id' => $model->prs_id]),
        ]);

        // AgreementAnnex
        $dataProviderAgreementAnnex = new ActiveDataProvider([
            'query' => AgreementAnnex::find()->where(['agra_ab_id' => $model->prs_id]),
        ]);

        // AgreementAcc
        $dataProviderAgreementAcc = new ActiveDataProvider([
            'query' => AgreementAcc::find()->where(['aga_ab_id' => $model->prs_id]),
        ]);

        // AgreementAct
        $dataProviderAgreementAct = new ActiveDataProvider([
            'query' => AgreementAct::find()->where(['act_ab_id' => $model->prs_id]),
        ]);


        $dataProviderRequest = new ActiveDataProvider([
            'query' => ApplRequestContent::find()->where(['applrc_prs_id' => $model->prs_id]),
        ]);

        return $this->render('view', [
            // Person Tab
            'model' => $model,

            // Contact Tab
            'searchModelContact' => null,
            'dataProviderContact' => $dataProviderContact,

            // Company Tab
            'searchModelRequest' => null,
            'dataProviderRequest' => $dataProviderRequest,

            // Agreement Tab
            'searchModelAgreement' => null,
            'dataProviderAgreement' => $dataProviderAgreement,

            // AgreementAnnex Tab
            'searchModelAgreementAnnex' => null,
            'dataProviderAgreementAnnex' => $dataProviderAgreementAnnex,

            // AgreementAcc Tab
            'searchModelAgreementAcc' => null,
            'dataProviderAgreementAcc' => $dataProviderAgreementAcc,

            // AgreementAct Tab
            'searchModelAgreementAct' => null,
            'dataProviderAgreementAct' => $dataProviderAgreementAct,


        ]);
    }

    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Person();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prs_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Person model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prs_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Person model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws HttpException
     */
    public function actionCreateContact($prs_id)
    {
        $person = $this->findModel($prs_id);

        $model = new Contact();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $prs_id]);
        } else {
            return $this->render('create_contact', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $person->prs_id])->all(), 'prs_id', 'prs_full_name'),
                'addrbook' => ArrayHelper::map(Person::find()->where(['prs_id' => $person->prs_id])->all(), 'prs_id', 'prs_full_name'),
                'contactType' => ArrayHelper::map(ContactType::find()->all(), 'cont_id', 'cont_name'),
            ]);
        }
    }


    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $prs_id
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdateContact($prs_id, $id)
    {
        $person = $this->findModel($prs_id);

        $model = Contact::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $prs_id]);
        } else {
            return $this->render('update_contact', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $person->prs_id])->all(), 'prs_id', 'prs_full_name'),
                'contactType' => ArrayHelper::map(ContactType::find()->all(), 'cont_id', 'cont_name'),
            ]);
        }
    }



    /***********************************************************************************
     *
     *                                  Agreement
     *
     ***********************************************************************************/

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $prs_id
     * @return mixed
     * @throws HttpException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreateAgreement($prs_id)
    {
        $person = $this->findModel($prs_id);

        $dynModel = DynamicModel::validateData(['date', 'sum', 'tax', 'comp_id', 'pat_id', 'prs_id'],
            [
                [['comp_id', 'pat_id', 'prs_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );
        $dynModel->prs_id = $prs_id;

        $model = new Agreement();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->agr_ab_id = $dynModel->prs_id;
            $model->agr_comp_id = $dynModel->comp_id;
            $model->agr_pat_id = $dynModel->pat_id;
            $model->agr_prs_id = $dynModel->prs_id;
            $model->agr_sum = $dynModel->sum;
            $model->agr_tax = $dynModel->tax;

            $model->agr_ast_id = AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;
            $model->agr_date = $dynModel->date;
            $pattern = Pattern::findOne($model->agr_pat_id);

            // ------------- NEW -------------
            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->agr_date . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);

            $date_id = Agreement::find()->where(['agr_date' => $model->agr_date, 'agr_comp_id' => $model->agr_comp_id])->count('agr_date') + 1;
            $number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd') . '-' . $date_id . ($pattern->pat_code ?? '');

            while (Agreement::find()->where(['agr_date' => $model->agr_date, 'agr_comp_id' => $model->agr_comp_id, 'agr_number' => $number])->exists()) {
                $date_id++;
                $number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd') . '-' . $date_id . ($pattern->pat_code ?? '');
            }

            $model->agr_number = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('ymd') . '-' . $date_id . ($pattern->pat_code ?? '');
            $model->agr_fdata = str_replace('/', '-', $model->agr_number) . ' ' . $pattern->pat_fname;


            $tag_val = $model->agr_number;
            $document->setValue('DOC_NUMBER', $tag_val);
            $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agr_date)->format('d-m-Y');
            $document->setValue('DOC_DATE', $tag_val);
            if ($obj = Person::findOne($dynModel->prs_id)) {
                $tag_val = $obj->prs_full_name;
                $document->setValue('PERSONA_FIO', $tag_val);
                $tag_val = $obj->prs_pass_serial.' '.$obj->prs_pass_number;
                $document->setValue('PERSONA_PASSPORT_NUMBER', $tag_val);
                if (isset($obj->prs_pass_date)) {
                    $tag_val = \DateTime::createFromFormat('Y-m-d', $obj->prs_pass_date)->format('d-m-Y');
                    $document->setValue('PERSONA_PASSPORT_DATE', $tag_val);
                }
                $tag_val = $obj->prs_pass_issued_by;
                $document->setValue('PERSONA_PASSPORT_ISSUED', $tag_val);

                $tag_val = $obj->prs_inn;
                $document->setValue('PERSONA_INN', $tag_val);
                $tag_val = '';
                foreach ($obj->prs->addresses as $val) {
                    if ($val->addAddt->addt_id == 4) {
                        $tag_val = $val->add_index .' '.$val->addCou->cou_name.' '.$val->addReg->reg_name.' '.$val->add_data;
                    }
                }
                $document->setValue('PERSONA_R_ADDRESS', $tag_val);
            }
            if (is_float($model->agr_sum)) {
                $tag_val = number_format($model->agr_sum, 2);
                $document->setValue('DOC_SUM', $tag_val);
                $tag_val = Tools::num2str($model->agr_sum);
                $document->setValue('DOC_SUM_TEXT', $tag_val);
            }
            if (is_float($model->agr_tax)) {
                $tag_val = number_format($model->agr_tax, 2);
                $document->setValue('DOC_TAX', $tag_val);
                $tag_val = Tools::num2str($model->agr_tax);
                $document->setValue('DOC_TAX_TEXT', $tag_val);
            }

            $document->saveAs($file_name);

            $model->agr_fdata = str_replace('/', '-', $model->agr_number) . ' ' . $pattern->pat_fname;
            $model->agr_data = file_get_contents($file_name);

            unlink($file_name);
            unlink($tmpl_name);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $person->prs_id]);
            } else {
                return $this->render('create_agreement', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $person->prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id' => 1])->all(), 'pat_id', 'pat_name'),
                ]);
            }
        } else {
            $dynModel->date = date('Y-m-d');
            return $this->render('create_agreement', [
                'model' => $dynModel,
                'modelA' => $model,
                'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $person->prs_id])->all(), 'prs_id', 'prs_full_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>1])->all(), 'pat_id', 'pat_name'),
            ]);
        }
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $prs_id
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdateAgreement($prs_id, $id)
    {
        $entity = $this->findModel($prs_id);

        $model = Agreement::findOne($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agr_data')) != null) {
                $model->agr_fdata = $file->name;
                $model->agr_data = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata = $model->oldAttributes['agr_fdata'];
                $model->agr_data = $model->oldAttributes['agr_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agr_data_sign')) != null) {
                $model->agr_fdata_sign = $file->name;
                $model->agr_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agr_fdata_sign = $model->oldAttributes['agr_fdata_sign'];
                $model->agr_data_sign = $model->oldAttributes['agr_data_sign'];
            }


            if ($model->save()) {
                return $this->redirect(['view', 'id' => $prs_id]);
            } else {
                return $this->render('update_agreement', [
                    'model' => $model,
                    'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                    'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update_agreement', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->where(['ent_id' => $entity->ent_id])->all(), 'prs_id', 'prs_full_name'),
                'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }
    }

    /**
     * Delete an existing Agreement model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAgreement($id)
    {
        $model = Agreement::findOne($id);
        $person = $this->findModel($model->agr_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $person->prs_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreement($id)
    {
        return $this->redirect(['download-agreement-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementF($id)
    {
        $model = Agreement::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agr_data, $model->agr_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementSign($id)
    {
        return $this->redirect(['download-agreement-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementSignF($id)
    {
        $model = Agreement::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agr_data_sign, $model->agr_fdata_sign);
    }


    /***********************************************************************************
     *
     *                                  AgreementAnnex
     *
     ***********************************************************************************/


    /**
     * Creates a new AgreementAnnex model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreateAgreementAnnex($ent_id)
    {

        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(['ent_id', 'date', 'agr_id', 'pat_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'pat_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        $model = new AgreementAnnex();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->agra_ab_id  = $dynModel->ent_id;
            $model->agra_date   = $dynModel->date;
            $model->agra_agr_id = $dynModel->agr_id;
            $model->agra_pat_id = $dynModel->pat_id;
            $model->agra_sum    = $dynModel->sum;
            $model->agra_tax    = $dynModel->tax;

            $model->agra_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

            $pattern = Pattern::findOne($model->agra_pat_id);
            $str = $pattern->pat_fdata;

            $date_id = AgreementAnnex::find()->where(['agra_agr_id' => $model->agra_agr_id])->count('agra_date') + 1;
            $number = $model->agraAgr->agr_number.'-'.$date_id;
            while (AgreementAnnex::find()->where(['agra_agr_id' => $model->agra_agr_id, 'agra_number' => $number])->exists()) {
                $date_id ++;
                $number = $model->agraAgr->agr_number.'-'.$date_id;
            }
            $model->agra_number = $model->agraAgr->agr_number.'-'.$date_id;
            $model->agra_fdata = str_replace('/', '-', $model->agra_number).' '.$pattern->pat_fname;

            $tags = Tag::find()->all();

            $tag_val = '';
            foreach ($tags as $key => $tag) {
                $tag_name = $tag->tag_name;
                if ($tag_name == '<DOC_NUMBER>') {
                    $tag_val = $model->agra_number;
                }
                if ($tag_name == '<DOC_DATE>') {
                    $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agra_date)->format('d-m-Y');
                }
                if ($tag_name == '<AGR_NUMBER>') {
                    $tag_val = isset($model->agraAgr) ? $model->agraAgr->agr_number : '';
                }
                if ($tag_name == '<AGR_DATE>') {
                    $tag_val = isset($model->agraAgr) ? \DateTime::createFromFormat('Y-m-d', $model->agraAgr->agr_date)->format('d-m-Y') : '';
                }
                if ($tag_name == '<ORGANISATION_TYPE_FULL>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->entEntt->entt_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_NAME_FULL>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_U_ADDRESS>') {
                    if ($obj = Address::find()->where(['add_ab_id'=>$model->agra_ab_id, 'add_addt_id'=>1])->orderBy('add_id')->one()) {
                        $tag_val = ($obj->add_index.', '??'') . ($obj->addCou->cou_name.', '??'') . ($obj->addReg->reg_name.', '??'') . ($obj->addCity->city_name.', '??'') . ($obj->add_data??'');
                    }
                }
                if ($tag_name == '<ORGANISATION_INN>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_inn;
                    }
                }
                if ($tag_name == '<ORGANISATION_KPP>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_kpp;
                    }
                }

                if ($tag_name == '<ORGANISATION_R_SCHET>') {
                    if ($obj = Account::findOne(['acc_id' => $model->agraAgr->agr_acc_id])) {
                        $tag_val = $obj->acc_number;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_NAME>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agraAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_K_SCHET>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agraAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_account;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_BIK>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agraAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_bic;
                    }
                }

                if ($tag_name == '<ORGANISATION_FIO>') {
                    if ($obj = Person::findOne($model->agraAgr->agr_prs_id)) {
                        $tag_val = $obj->prs_full_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_DOLGNOST>') {
                    if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $model->agraAgr->agr_prs_id])->one()) {
                        $tag_val = $obj->stf_position;
                    }
                }
                if ($tag_name == '<DOC_SUM>') {
                    $tag_val = number_format($model->agra_sum, 2);
                }
                if ($tag_name == '<DOC_SUM_TEXT>') {
                    $tag_val = Tools::num2str($model->agra_sum);
                }
                if ($tag_name == '<DOC_TAX>') {
                    $tag_val = number_format($model->agra_tax, 2);
                }
                if ($tag_name == '<DOC_TAX_TEXT>') {
                    $tag_val = Tools::num2str($model->agra_tax);
                }

                $str = str_replace($tag_name, iconv('UTF-8', 'Windows-1251', $tag_val), $str);

                $tag_val = '';

            }


            $grid = Yii::$app->session['grid_agreement_annex_a'];
            $i = 1;

            while ($i <= 100) {
                if (count($grid) >= $i) {
                    $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
                    $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];
                    $str = str_replace('<'.$i.':SVC>',      iconv('UTF-8', 'Windows-1251', $grid[$i-1]['svc_name']), $str);
                    $str = str_replace('<'.$i.':PROG>',     iconv('UTF-8', 'Windows-1251', $grid[$i-1]['trp_name']), $str);
                    $str = str_replace('<'.$i.':DOC>',      iconv('UTF-8', 'Windows-1251', $grid[$i-1]['svc_doc']), $str);
                    $str = str_replace('<'.$i.':HOUR>',     iconv('UTF-8', 'Windows-1251', $grid[$i-1]['svc_hour']), $str);
                    $str = str_replace('<'.$i.':PRICE>',    number_format($grid[$i-1]['price'], 2), $str);
                    $str = str_replace('<'.$i.':QTY>',      $grid[$i-1]['qty'], $str);
                    $str = str_replace('<'.$i.':SUM_T>',    iconv('UTF-8', 'Windows-1251', $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2) ), $str);
                    $str = str_replace('<'.$i.':SUM>',      number_format($sum, 2), $str);
                } else {
                    $str = str_replace('<'.$i.':SVC>',      '', $str);
                    $str = str_replace('<'.$i.':PROG>',     '', $str);
                    $str = str_replace('<'.$i.':DOC>',      '', $str);
                    $str = str_replace('<'.$i.':HOUR>',     '', $str);
                    $str = str_replace('<'.$i.':PRICE>',    '', $str);
                    $str = str_replace('<'.$i.':QTY>',      '', $str);
                    $str = str_replace('<'.$i.':SUM_T>',    '', $str);
                    $str = str_replace('<'.$i.':SUM>',      '', $str);
                }
                $i++;

                // 'svc_id', 'svc_name', 'svc_doc', 'trp_id', 'trp_name', 'cost_a', 'price_a', 'price', 'tax', 'qty',
                //<1:SVC>	<1:PROG>	<1:DOC>	<1:HOUR>	<1:PRICE>	<1:QTY>	<1:SUM_T>	<1:SUM>

            }

            $model->agra_data = $str;

            if ($model->save()) {

                $grid = Yii::$app->session['grid_agreement_annex_a'];
                foreach ($grid as $key => $value) {
                    $ana = new AgreementAnnexA();
                    $ana->ana_agra_id = $model->agra_id;
                    $ana->ana_svc_id = $value['svc_id'];
                    $ana->ana_trp_id = $value['trp_id'];
                    $ana->ana_cost_a = $value['cost_a'];
                    $ana->ana_price_a = $value['price_a'];
                    $ana->ana_price = $value['price'];
                    $ana->ana_qty = $value['qty'];
                    $ana->ana_tax = $value['tax'];
                    $ana->save();
                }

                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                return $this->render('create_agreement_annex', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>3])->all(), 'pat_id', 'pat_name'),
                    'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_annex_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'person' => $entity,
                    'account' => null,
                ]);

            }

        } else {
            $dynModel->date = date('Y-m-d');

            Yii::$app->session['grid_agreement_annex_a'] = [];

            return $this->render('create_agreement_annex', [
                'model' => $dynModel,
                'modelA' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>3])->all(), 'pat_id', 'pat_name'),
                'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => [],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'person' => $entity,
                'account' => null,
            ]);
        }
    }

    /**
     * Add row and return grid.
     * @param integer $svc_id
     * @param integer $trp_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementACreate($svc_id, $trp_id, $price, $qty) {
        $svc = Svc::findOne($svc_id);
        $trp = TrainingProg::findOne($trp_id);
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $grid[] = [
            'svc_id' => $svc_id,
            'svc_name' => $svc->svc_name,
            'svc_doc' => $svc->svcSvdt->svdt_name,
            'svc_hour' => $svc->svc_hour,
            'trp_id' => $trp_id,
            'trp_name' => isset($trp) ?  $trp->trp_name : '',
            'cost_a' => $svc->svc_cost,
            'price_a' => $svc->svc_price,
            'price' => $price,
            'tax' => $svc->svc_tax_flag==1 ? ($price / 1.18 ) * 0.18 : null,
            'qty' => $qty,
        ];
        Yii::$app->session['grid_agreement_annex_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementADelete($id) {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['svc_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_annex_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementASum() {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementATax() {
        $grid = Yii::$app->session['grid_agreement_annex_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * Return cost from Svc model.
     * @param integer $svc_id
     * @return mixed
     */
    public function actionAjaxAgreementACost($svc_id) {
        $svc = Svc::findOne($svc_id);
        return $this->renderAjax('_ajax_agreement_a_cost', [
            'svc' => $svc,
            ]
        );

    }

    /**
     * Return price from Svc model.
     * @param integer $svc_id
     * @return mixed
     */
    public function actionAjaxAgreementAPrice($svc_id) {
        $svc = Svc::findOne($svc_id);
        return $this->renderAjax('_ajax_agreement_a_price', [
                'svc' => $svc,
            ]
        );

    }

    /**
     * Return list Svc model.
     * @param string $svc_name
     * @return mixed
     */
    public function actionAjaxAgreementASvc($svc_name) {
        $svc = ArrayHelper::map(
            Svc::find()
                ->where(['like', 'svc_name', $svc_name])
                ->all()
            , 'svc_id', 'svc_name');
        return $this->renderAjax('_ajax_agreement_a_svc', [
                'svc' => [0 => '<>'] + $svc,
            ]
        );

    }

    /**
     * Return list TraningProg Svc model.
     * @param integer $svc_id
     * @param string $trp_name default ''
     * @return mixed
     */
    public function actionAjaxAgreementATrp($svc_id, $trp_name = '') {
        $prog = ArrayHelper::map(
            TrainingProg::find()
                ->innerJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id')
                ->where(['sprog_svc_id' => $svc_id])
                ->andWhere(['like', 'trp_name', $trp_name])
                ->all()
            , 'trp_id', 'trp_name');
        return $this->renderAjax('_ajax_agreement_a_trp', [
                'prog' => $prog,
            ]
        );

    }

    /**
     * Updates an existing AgreementAnnex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdateAgreementAnnex($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = AgreementAnnex::findOne($id);

        $dataProvider = (new AgreementAnnexASearch())->search(['AgreementAnnexASearch' => ['ana_agra_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agra_data')) != null) {
                $model->agra_fdata = $file->name;
                $model->agra_data = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata = $model->oldAttributes['agra_fdata'];
                $model->agra_data = $model->oldAttributes['agra_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agra_data_sign')) != null) {
                $model->agra_fdata_sign = $file->name;
                $model->agra_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata_sign = $model->oldAttributes['agra_fdata_sign'];
                $model->agra_data_sign = $model->oldAttributes['agra_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_annex', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                    'person' => $entity,
                    'account' => null,
                ]);
            }
        } else {
            return $this->render('update_agreement_annex', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
                'person' => $entity,
                'account' => null,

            ]);
        }

    }


    /**
     * Delete an existing AgreementAnnex model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAgreementAnnex($id)
    {
        $model = AgreementAnnex::findOne($id);
        $entity = $this->findModel($model->agra_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAnnex($id)
    {
        return $this->redirect(['download-agreement-annex-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAnnex model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementAnnexF($id)
    {
        $model = AgreementAnnex::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agra_data, $model->agra_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAnnexSign($id)
    {
        return $this->redirect(['download-agreement-annex-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAnnex model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementAnnexSignF($id)
    {
        $model = AgreementAnnex::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->agra_data_sign, $model->agra_fdata_sign);
    }


    /***********************************************************************************
     *
     *                                  AgreementAcc
     *
     ***********************************************************************************/


    /**
     * Creates a new AgreementAcc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreateAgreementAcc($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $dynModel = DynamicModel::validateData(['ent_id', 'date', 'agr_id', 'agra_id', 'pat_id', 'comp_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'agra_id',  'pat_id',  'comp_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        $model = new AgreementAcc();

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->aga_ab_id   = $dynModel->ent_id;
            $model->aga_date    = $dynModel->date;
            $model->aga_agr_id  = $dynModel->agr_id;
            $model->aga_agra_id = $dynModel->agra_id;
            $model->aga_pat_id  = $dynModel->pat_id;
            $model->aga_comp_id = $dynModel->comp_id;
            $model->aga_sum     = $dynModel->sum;
            $model->aga_tax     = $dynModel->tax;

            $model->aga_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

            $pattern = Pattern::findOne($model->aga_pat_id);
            $str = $pattern->pat_fdata;

            if (isset($model->agaAgr)) {
                $date_id = AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id])->count('aga_date') + 1;
                $number = $model->agaAgr->agr_number.'/'.$date_id;

                while (AgreementAcc::find()->where(['aga_agr_id' => $model->aga_agr_id, 'aga_number' => $number])->exists()) {
                    $date_id ++;
                    $number = $model->agaAgr->agr_number.'/'.$date_id;
                }

                $model->aga_number = $model->agaAgr->agr_number.'/'.$date_id;
            }

            /*
            elseif (isset($model->agaAgra)) {
                $date_id = AgreementAcc::find()->where(['aga_agra_id' => $model->aga_agra_id])->count('aga_date') + 1;
                $model->aga_number = $model->agaAgra->agra_number.'/'.$date_id;
            } else {
                $date_id = AgreementAcc::find()->where(['aga_date' => $model->aga_date])->count('aga_date') + 1;
                $model->aga_number = date('Ymd').'/'.$date_id;
            }
            */

            $model->aga_fdata = str_replace('/', '-', $model->aga_number).' '.$pattern->pat_fname;
            $model->aga_data = $str;

            $tags = Tag::find()->all();

            $tag_val = '';
            foreach ($tags as $key => $tag) {
                $tag_name = $tag->tag_name;
                if ($tag_name == '<DOC_NUMBER>') {
                    $tag_val = $model->aga_number;
                }
                if ($tag_name == '<DOC_DATE>') {
                    $tag_val = \DateTime::createFromFormat('Y-m-d', $model->aga_date)->format('d-m-Y');
                }
                if ($tag_name == '<AGR_NUMBER>') {
                    if (isset($model->agaAgr)) {
                        $tag_val = $model->agaAgr->agr_number;
                    }
                }
                if ($tag_name == '<AGR_DATE>') {
                    if (isset($model->agaAgr)) {
                        $tag_val = \DateTime::createFromFormat('Y-m-d', $model->agaAgr->agr_date)->format('d-m-Y');
                    }
                }
                if ($tag_name == '<ORGANISATION_TYPE_FULL>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->entEntt->entt_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_NAME_FULL>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_U_ADDRESS>') {
                    if ($obj = Address::find()->where(['add_ab_id'=>$model->aga_ab_id, 'add_addt_id'=>1])->orderBy('add_id')->one()) {
                        $tag_val = ($obj->add_index.', '??'') . ($obj->addCou->cou_name.', '??'') . ($obj->addReg->reg_name.', '??'') . ($obj->addCity->city_name.', '??'') . ($obj->add_data??'');
                    }
                }
                if ($tag_name == '<ORGANISATION_INN>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_inn;
                    }
                }
                if ($tag_name == '<ORGANISATION_KPP>') {
                    if ($obj = Entity::findOne($dynModel->ent_id)) {
                        $tag_val = $obj->ent_kpp;
                    }
                }

                if ($tag_name == '<ORGANISATION_R_SCHET>') {
                    if ($obj = Account::findOne(['acc_id' => $model->agaAgr->agr_acc_id])) {
                        $tag_val = $obj->acc_number;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_NAME>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agaAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_K_SCHET>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agaAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_account;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_BIK>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->agaAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_bic;
                    }
                }

                if ($tag_name == '<ORGANISATION_FIO>') {
                    if ($obj = Person::findOne($model->agaAgr->agr_prs_id)) {
                        $tag_val = $obj->prs_full_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_DOLGNOST>') {
                    if ($obj = Staff::find()->where(['stf_ent_id' => $dynModel->ent_id, 'stf_prs_id' => $model->agaAgr->agr_prs_id])->one()) {
                        $tag_val = $obj->stf_position;
                    }
                }
                if ($tag_name == '<DOC_SUM>') {
                    $tag_val = number_format($model->aga_sum, 2);
                }
                if ($tag_name == '<DOC_SUM_TEXT>') {
                    $tag_val = Tools::num2str($model->aga_sum);
                }
                if ($tag_name == '<DOC_TAX>') {
                    $tag_val = number_format($model->aga_tax, 2);
                }
                if ($tag_name == '<DOC_TAX_TEXT>') {
                    $tag_val = Tools::num2str($model->aga_tax);
                }

                $str = str_replace($tag_name, iconv('UTF-8', 'Windows-1251', $tag_val), $str);

                $tag_val = '';

            }

            $grid = Yii::$app->session['grid_agreement_acc_a'];
            $i = 1;

            while ($i <= 100) {
                if (count($grid) >= $i) {
                    $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
                    $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];
                    $str = str_replace('<'.$i.':SVC>',      iconv('UTF-8', 'Windows-1251', $grid[$i-1]['ana_name']), $str);
                    $str = str_replace('<'.$i.':PRICE>',    number_format($grid[$i-1]['price'], 2), $str);
                    $str = str_replace('<'.$i.':QTY>',      $grid[$i-1]['qty'], $str);
                    $str = str_replace('<'.$i.':SUM_T>',    iconv('UTF-8', 'Windows-1251', $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2) ), $str);
                    $str = str_replace('<'.$i.':SUM>',      number_format($sum, 2), $str);
                } else {
                    $str = str_replace('<'.$i.':SVC>',      '', $str);
                    $str = str_replace('<'.$i.':PRICE>',    '', $str);
                    $str = str_replace('<'.$i.':QTY>',      '', $str);
                    $str = str_replace('<'.$i.':SUM_T>',    '', $str);
                    $str = str_replace('<'.$i.':SUM>',      '', $str);
                }
                $i++;

                // 'ana_id', 'ana_name', 'price', 'tax', 'qty',
                //<1:SVC>	<1:PRICE>	<1:QTY>	<1:SUM_T>	<1:SUM>

            }


            $model->aga_data = $str;

            if ($model->save()) {

                $grid = Yii::$app->session['grid_agreement_acc_a'];
                foreach ($grid as $key => $value) {
                    $ana = new AgreementAccA();
                    $ana->aca_aga_id = $model->aga_id;
                    $ana->aca_ana_id = $value['ana_id'];
                    $ana->aca_price = $value['price'];
                    $ana->aca_qty = $value['qty'];
                    $ana->aca_tax = $value['tax'];
                    $ana->save();
                }

                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                return $this->render('create_agreement_acc', [
                    'model' => $dynModel,
                    'modelA' => $model,
                    'company' => ArrayHelper::map(Company::find()->where([])->all(), 'comp_id', 'comp_name'),
                    'agreement' => ['0' => '<>'] + ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>4])->all(), 'pat_id', 'pat_name'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_acc_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'person' => $entity,
                    'annex_a' => null,
                ]);
            }
        } else {
            Yii::$app->session['grid_agreement_acc_a'] = [];

            $dynModel->date = date('Y-m-d');

            return $this->render('create_agreement_acc', [
                'model' => $dynModel,
                'modelA' => $model,
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ['0' => '<>'] + ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>4])->all(), 'pat_id', 'pat_name'),
                'annex_a' => [0 => '<>'],
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => Yii::$app->session['grid_agreement_acc_a'],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'person' => $entity,
            ]);
        }
    }

    /**
     * Add row and return grid.
     * @param integer $ana_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementAccACreate($ana_id, $price, $qty) {
        $ana = AgreementAnnexA::findOne($ana_id);

        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = floatval($price) * intval($qty);
        foreach ($grid as $key => $value) {
            if ($value['ana_id'] == $ana_id) {
                $sum += $value['price'] * $value['qty'];
            }
        }

        if (($ana->ana_price*$ana->ana_qty) >= $sum)
        $grid[] = [
            'ana_id' => $ana_id,
            'ana_name' => $ana->anaSvc->svc_name.' / '.$ana->anaTrp->trp_name,
            'price' => $price,
            'tax' =>  ($price / $ana->ana_price ) * $ana->ana_tax,
            'qty' => $qty,
        ];
        Yii::$app->session['grid_agreement_acc_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_acc_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementAccADelete($id) {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['ana_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_acc_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_acc_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementAccA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementAccASum() {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementAccA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementAccATax() {
        $grid = Yii::$app->session['grid_agreement_acc_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }


    /**
     * Return list AgreementAnnexA array.
     * @param integer $agr_id
     * @return mixed
     */
    public function actionAjaxAgreementAccA($agr_id) {
        $data = [];

        foreach (AgreementAnnexA::find()
                     ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                     ->leftJoin(AgreementAccA::tableName(), 'ana_id = aca_ana_id')
                     ->where(['agra_agr_id' => $agr_id])
                     ->andWhere(['or', 'aca_ana_id IS NULL', 'ana_price * ana_qty > aca_price * aca_qty'])
                     ->all() as $key => $value) {
            $data[$value->ana_id] = $value->anaAgra->agra_number . ' / ' .$value->anaSvc->svc_name .' / '.$value->anaTrp->trp_name;
        }

        return $this->renderAjax('_ajax_agreement_acc_a', [
                'annex_a' => [0 => '<>']+$data,
            ]
        );

    }

    /**
     * Return PRICE value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementAccAPrice($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_price,
            ]
        );
    }
    /**
     * Return QTY value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementAccAQty($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_qty,
            ]
        );

    }


    /**
     * Updates an existing AgreementAcc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdateAgreementAcc($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = AgreementAcc::findOne($id);

        $dataProvider = (new AgreementAccASearch())->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'aga_data')) != null) {
                $model->aga_fdata = $file->name;
                $model->aga_data = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata = $model->oldAttributes['aga_fdata'];
                $model->aga_data = $model->oldAttributes['aga_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'aga_data_sign')) != null) {
                $model->aga_fdata_sign = $file->name;
                $model->aga_data_sign = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata_sign = $model->oldAttributes['aga_fdata_sign'];
                $model->aga_data_sign = $model->oldAttributes['aga_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_acc', [
                    'model' => $model,
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                    'person' => $entity,
                ]);
            }
        } else {
            return $this->render('update_agreement_acc', [
                'model' => $model,
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
                'person' => $entity,
            ]);
        }

    }


    /**
     * Delete an existing AgreementAcc model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAgreementAcc($id)
    {
        $model = AgreementAcc::findOne($id);
        $entity = $this->findModel($model->aga_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAcc($id)
    {
        return $this->redirect(['download-agreement-acc-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAcc model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementAccF($id)
    {
        $model = AgreementAcc::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->aga_data, $model->aga_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAccSign($id)
    {
        return $this->redirect(['download-agreement-acc-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAcc model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementAccSignF($id)
    {
        $model = AgreementAcc::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->aga_data_sign, $model->aga_fdata_sign);
    }

    /***********************************************************************************
     *
     *                                  AgreementAct
     *
     ***********************************************************************************/


    /**
     * Creates a new AgreementAct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreateAgreementAct($ent_id)
    {
        $entity = $this->findModel($ent_id);

        $model = new AgreementAct();

        $dynModel = DynamicModel::validateData(['ent_id', 'date', 'acc_id', 'comp_id', 'agr_id', 'agra_id', 'pat_id', 'sum', 'tax'],
            [
                [['ent_id', 'agr_id', 'acc_id', 'agra_id', 'comp_id', 'pat_id'], 'integer'],
                [['date'], 'string'],
                [['sum', 'tax'], 'number']
            ]
        );

        $dynModel->ent_id = $ent_id;

        if ($dynModel->load(Yii::$app->request->post())) {

            $model->act_ab_id   = $dynModel->ent_id;
            $model->act_date    = $dynModel->date;
            $model->act_agr_id  = $dynModel->agr_id;
            $model->act_agra_id = $dynModel->agra_id;
            $model->act_pat_id  = $dynModel->pat_id;
            $model->act_comp_id = $dynModel->comp_id;
            $model->act_sum     = $dynModel->sum;
            $model->act_tax     = $dynModel->tax;

            $model->act_ast_id  =  AgreementStatus::find()->orderBy('ast_id')->one()->ast_id;

            $pattern = Pattern::findOne($model->act_pat_id);

            if (isset($model->actAgr)) {
                $date_id = AgreementAct::find()->where(['act_agr_id' => $model->act_agr_id])->count('act_date') + 1;
                $number = substr($model->actAgr->agr_number, 2) . str_pad((string) $date_id, 2, '0', STR_PAD_LEFT);

                while (AgreementAct::find()->where(['act_agr_id' => $model->act_agr_id, 'act_number' => $number])->exists()) {
                    $date_id ++;
                    $number = substr($model->actAgr->agr_number, 2) . str_pad((string) $date_id, 2, '0', STR_PAD_LEFT);
                }

                $model->act_number = substr($model->actAgr->agr_number, 2) . str_pad((string) $date_id, 2, '0', STR_PAD_LEFT);

            }


            $model->act_fdata = str_replace('/', '-', $model->act_number).' '.$pattern->pat_fname;

            $str = $pattern->pat_fdata;

            $tags = Tag::find()->all();

            $grid = Yii::$app->session['grid_agreement_act_a'];

            $tag_name = '';
            $tag_val = '';
            foreach ($tags as $key => $tag) {
                $tag_name = $tag->tag_name;
                if ($tag_name == '<DOC_NUMBER>') {
                    $tag_val = $model->act_number;
                }
                if ($tag_name == '<DOC_DATE>') {
                    $tag_val = \DateTime::createFromFormat('Y-m-d', $model->act_date)->format('d-m-Y');
                }
                if ($tag_name == '<AGR_NUMBER>') {
                    if (isset($model->actAgr)) {
                        $tag_val = $model->actAgr->agr_number;
                    }
                }
                if ($tag_name == '<AGR_DATE>') {
                    if (isset($model->actAgr)) {
                        $tag_val = \DateTime::createFromFormat('Y-m-d', $model->actAgr->agr_date)->format('d-m-Y');
                    }
                }
                if ($tag_name == '<ORGANISATION_TYPE_FULL>') {
                    if ($obj = Entity::findOne($model->act_ab_id)) {
                        $tag_val = $obj->entEntt->entt_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_NAME_FULL>') {
                    if ($obj = Entity::findOne($model->act_ab_id)) {
                        $tag_val = $obj->ent_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_U_ADDRESS>') {
                    if ($obj = Address::find()->where(['add_ab_id'=>$model->act_ab_id, 'add_addt_id'=>1])->orderBy('add_id')->one()) {
                        $tag_val = ($obj->add_index.', '??'') . ($obj->addCou->cou_name.', '??'') . ($obj->addReg->reg_name.', '??'') . ($obj->addCity->city_name.', '??'') . ($obj->add_data??'');
                    }
                }
                if ($tag_name == '<ORGANISATION_INN>') {
                    if ($obj = Entity::findOne($model->act_ab_id)) {
                        $tag_val = $obj->ent_inn;
                    }
                }
                if ($tag_name == '<ORGANISATION_KPP>') {
                    if ($obj = Entity::findOne($model->act_ab_id)) {
                        $tag_val = $obj->ent_kpp;
                    }
                }

                if ($tag_name == '<ORGANISATION_R_SCHET>') {
                    if ($obj = Account::findOne(['acc_id' => $model->actAgr->agr_acc_id])) {
                        $tag_val = $obj->acc_number;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_NAME>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->actAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_K_SCHET>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->actAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_account;
                    }
                }
                if ($tag_name == '<ORGANISATION_BANK_BIK>') {
                    if ($obj = Bank::findOne(Account::findOne(['acc_id' => $model->actAgr->agr_acc_id])->acc_bank_id)) {
                        $tag_val = $obj->bank_bic;
                    }
                }

                if ($tag_name == '<ORGANISATION_FIO>') {
                    if ($obj = Person::findOne($model->actAgr->agr_prs_id)) {
                        $tag_val = $obj->prs_full_name;
                    }
                }
                if ($tag_name == '<ORGANISATION_DOLGNOST>') {
                    if ($obj = Staff::find()->where(['stf_ent_id' => $model->act_ab_id, 'stf_prs_id' => $model->actAgr->agr_prs_id])->one()) {
                        $tag_val = $obj->stf_position;
                    }
                }
                if ($tag_name == '<DOC_QTY>') {
                    $qty = 0;
                    foreach ($grid as $k => $value) {
                        $qty += $value['qty'];
                    }
                    $tag_val = number_format($qty);
                }
                if ($tag_name == '<DOC_SUM>') {
                    $tag_val = number_format($model->act_sum, 2);
                }
                if ($tag_name == '<DOC_SUM_TEXT>') {
                    $tag_val = Tools::num2str($model->act_sum);
                }
                if ($tag_name == '<DOC_TAX>') {
                    $tag_val = number_format($model->act_tax, 2);
                }
                if ($tag_name == '<DOC_TAX_TEXT>') {
                    $tag_val = Tools::num2str($model->act_tax);
                }

                $str = str_replace($tag_name, iconv('UTF-8', 'Windows-1251', $tag_val), $str);

                $tag_val = '';

            }

            $i = 1;

            while ($i <= 100) {
                if (count($grid) >= $i) {
                    $tax = $grid[$i-1]['tax']*$grid[$i-1]['qty'];
                    $sum = $grid[$i-1]['price']*$grid[$i-1]['qty'];
                    $str = str_replace('<'.$i.':SVC>',      iconv('UTF-8', 'Windows-1251', $grid[$i-1]['ana_name']), $str);
                    $str = str_replace('<'.$i.':PRICE>',    number_format($grid[$i-1]['price'], 2), $str);
                    $str = str_replace('<'.$i.':QTY>',      $grid[$i-1]['qty'], $str);
                    $str = str_replace('<'.$i.':SUM_T>',    iconv('UTF-8', 'Windows-1251', $tax==0 ? Yii::t('app', 'Without Tax') : number_format($tax, 2) ), $str);
                    $str = str_replace('<'.$i.':SUM>',      number_format($sum, 2), $str);
                } else {
                    $str = str_replace('<'.$i.':SVC>',      '', $str);
                    $str = str_replace('<'.$i.':PRICE>',    '', $str);
                    $str = str_replace('<'.$i.':QTY>',      '', $str);
                    $str = str_replace('<'.$i.':SUM_T>',    '', $str);
                    $str = str_replace('<'.$i.':SUM>',      '', $str);
                }
                $i++;

                // 'ana_id', 'ana_name', 'price', 'tax', 'qty',
                //<1:SVC>	<1:PRICE>	<1:QTY>	<1:SUM_T>	<1:SUM>

            }

            $model->act_data = $str;

            if ($model->save()) {
                $grid = Yii::$app->session['grid_agreement_act_a'];
                foreach ($grid as $key => $value) {
                    $ana = new AgreementActA();
                    $ana->acta_act_id = $model->act_id;
                    $ana->acta_ana_id = $value['ana_id'];
                    $ana->acta_price = $value['price'];
                    $ana->acta_qty = $value['qty'];
                    $ana->acta_tax = $value['tax'];
                    $ana->save();
                }

                return $this->redirect(['view', 'id' => $ent_id]);
            } else {
                return $this->render('create_agreement_act', [
                    'model' => $dynModel,
                    'company' => ArrayHelper::map(Company::find()->where([])->all(), 'comp_id', 'comp_name'),
                    'agreement' => [0 => '<>'] + ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                    'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>5])->all(), 'pat_id', 'pat_name'),
                    'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => Yii::$app->session['grid_agreement_act_a'],
                        'pagination' => [
                            'pageSize' => 0,
                        ],

                    ]),
                    'person' => $entity,
                ]);

            }
        } else {
            Yii::$app->session['grid_agreement_act_a'] = [];

            $dynModel->date = date('Y-m-d');

            return $this->render('create_agreement_act', [
                'model' => $dynModel,
                'company' => ArrayHelper::map(Company::find()->where([])->all(), 'comp_id', 'comp_name'),
                'agreement' => [0 => '<>'] + ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $entity->ent_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ['0' => '<>'] + ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $entity->ent_id])->all(), 'agra_id', 'agra_number'),
                'pattern' => ArrayHelper::map(Pattern::find()->where(['pat_patt_id'=>5])->all(), 'pat_id', 'pat_name'),
                'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => Yii::$app->session['grid_agreement_act_a'],
                    'pagination' => [
                        'pageSize' => 0,
                    ],

                ]),
                'person' => $entity,
            ]);
        }
    }


    /**
     * Add row and return grid.
     * @param integer $ana_id
     * @param string $price
     * @param integer $qty
     * @return mixed
     */
    public function actionAjaxAgreementActACreate($ana_id, $price, $qty) {
        $ana = AgreementAnnexA::findOne($ana_id);

        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = floatval($price) * intval($qty);
        foreach ($grid as $key => $value) {
            if ($value['ana_id'] == $ana_id) {
                $sum += $value['price'] * $value['qty'];
            }
        }

        if (($ana->ana_price*$ana->ana_qty) >= $sum)
            $grid[] = [
                'ana_id' => $ana_id,
                'ana_name' => $ana->anaSvc->svc_name.' / '.$ana->anaTrp->trp_name,
                'price' => $price,
                'tax' =>  ($price / $ana->ana_price ) * $ana->ana_tax,
                'qty' => $qty,
            ];
        Yii::$app->session['grid_agreement_act_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);
        return $this->renderAjax('_ajax_agreement_act_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }

    /**
     * Delete row and return grid.
     * @param integer $id
     * @return mixed
     */
    public function actionAjaxAgreementActADelete($id) {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $key = -1;
        foreach ($grid as $k => $value) {
            if ($value['ana_id'] == $id) {
                $key = $k;
                break;
            }
        }
        if ($key >= 0)
            unset($grid[$key]);

        Yii::$app->session['grid_agreement_act_a'] = $grid;

        $provider = new ArrayDataProvider([
            'allModels' => $grid,
            'pagination' => [
                'pageSize' => 0,
            ],

        ]);

        return $this->renderAjax('_ajax_agreement_act_a_grid', [
                'dataProvider' => $provider,
            ]
        );
    }


    /**
     * return AgreementAccA Sum.
     * @return mixed
     */
    public function actionAjaxAgreementActASum() {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['price']) * floatval($value['qty']);
        }

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }

    /**
     * return AgreementAccA Tax.
     * @return mixed
     */
    public function actionAjaxAgreementActATax() {
        $grid = Yii::$app->session['grid_agreement_act_a'];
        $sum = 0.0;
        foreach ($grid as $k => $value) {
            $sum  += floatval($value['tax']) * floatval($value['qty']);
        }
        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $sum,
            ]
        );
    }


    /**
     * Return list AgreementAnnexA array.
     * @param integer $agr_id
     * @return mixed
     */
    public function actionAjaxAgreementActA($agr_id) {
        $data = [];

        foreach (AgreementAnnexA::find()
                     ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                     ->leftJoin(AgreementActA::tableName(), 'ana_id = acta_ana_id')
                     ->where(['agra_agr_id' => $agr_id])
                     ->andWhere(['or', 'acta_ana_id IS NULL', 'ana_price * ana_qty > acta_price * acta_qty'])
                     ->all() as $key => $value) {
            $data[$value->ana_id] = $value->anaAgra->agra_number . ' / ' .$value->anaSvc->svc_name .' / '.$value->anaTrp->trp_name;
        }

        return $this->renderAjax('_ajax_agreement_act_a', [
                'annex_a' => [0 => '<>']+$data,
            ]
        );

    }

    /**
     * Return PRICE value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementActAPrice($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_price,
            ]
        );
    }
    /**
     * Return QTY value.
     * @param integer $ana_id
     * @return mixed
     */
    public function actionAjaxAgreementActAQty($ana_id) {
        $data = AgreementAnnexA::find()->where(['ana_id' => $ana_id])->one();

        return $this->renderAjax('_ajax_agreement_a_float', [
                'value' => $data->ana_qty,
            ]
        );

    }


    /**
     * Updates an existing AgreementAct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdateAgreementAct($ent_id, $id)
    {
        $entity = $this->findModel($ent_id);

        $model = AgreementAct::findOne($id);

        $dataProvider = (new AgreementActASearch())->search(['AgreementActASearch' => ['acta_act_id' => $id]]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'act_data')) != null) {
                $model->act_fdata = $file->name;
                $model->act_data = file_get_contents($file->tempName);
            } else {
                $model->act_fdata = $model->oldAttributes['act_fdata'];
                $model->act_data = $model->oldAttributes['act_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'act_data_sign')) != null) {
                $model->act_fdata_sign = $file->name;
                $model->act_data_sign = file_get_contents($file->tempName);
            } else {
                $model->act_fdata_sign = $model->oldAttributes['act_fdata_sign'];
                $model->act_data_sign = $model->oldAttributes['act_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $entity->ent_id]);
            } else {
                return $this->render('update_agreement_act', [
                    'model' => $model,
                    'company' => ArrayHelper::map(Company::find()->where([])->all(), 'comp_id', 'comp_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                    'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                    'dataProvider' => $dataProvider,
                    'person' => $entity,
                ]);
            }
        } else {
            return $this->render('update_agreement_act', [
                'model' => $model,
                'company' => ArrayHelper::map(Company::find()->where([])->all(), 'comp_id', 'comp_name'),
                'entity' => ArrayHelper::map(Entity::find()->where(['ent_id' => $entity->ent_id])->all(), 'ent_id', 'ent_name_short'),
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $entity->ent_id])->all(), 'acc_id', 'acc_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                'dataProvider' => $dataProvider,
                'person' => $entity,
            ]);
        }

    }

    /**
     * Delete an existing AgreementAct model.
     * If delete is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteAgreementAct($id)
    {
        $model = AgreementAct::findOne($id);
        $entity = $this->findModel($model->act_ab_id);
        $model->delete();
        return $this->redirect(['view', 'id' => $entity->ent_id]);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementAct($id)
    {
        return $this->redirect(['download-agreement-act-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAct model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementActF($id)
    {
        $model = AgreementAct::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->act_data, $model->act_fdata);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadAgreementActSign($id)
    {
        return $this->redirect(['download-agreement-act-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing AgreementAct model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadAgreementActSignF($id)
    {
        $model = AgreementAct::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->act_data_sign, $model->act_fdata_sign);
    }



    /**
     * Lists FinanceBook models.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePay($id)
    {
        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        $account = AgreementAcc::findOne($id);
        if (!$account) {
            return $this->redirect(['entity-frm/index']);
        }

        return $this->render('update_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'account' => $account,
            'person' => null,
            'model' => null,
            'payArray' => null,
        ]);
    }


    /**
     * Check sum pay.
     * @param string $value
     * @return mixed
     */
    public function actionPayCheckSum($value)
    {
        $value = floatval($value);
        return $this->renderAjax('_ajax_pay_check_sum', [
            //'searchModel' => $searchModel,
            'value' => $value,
        ]);
    }

    /**
     * Calculate sum pay.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionPayCalculate($id, $value)
    {
        $payArray = [];

        $line = AgreementAccA::find()->innerJoin(FinanceBook::tableName(), 'fb_aca_id = aca_id')->where(['aca_aga_id' => $id])->andWhere(['fb_pay' => null])->all();
        $pay = floatval($value);

        foreach ($line as $key => $obj) {
            // Сумма оплаты больше необходимого
            if (($obj->getSum()-$obj->paySum) <= $pay) {
                $payArray[$obj->aca_id] = $obj->getSum()-$obj->paySum;
            }

            // Сумма оплаты меньше необходимого
            if (($obj->getSum()-$obj->paySum) > $pay) {
                $payArray[$obj->aca_id] = $pay;
            }

            $pay -= floatval($payArray[$obj->aca_id]);

            if ($pay <= 0.001)
                break;
        }

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        Yii::$app->session['PersonFrmPay'] = $payArray;

        return $this->renderAjax('_grid_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['PersonFrmPay'],
        ]);


    }

    /**
     * Update pay Sum.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdatePaySum($id, $value)
    {
        $payArray = Yii::$app->session['PersonFrmPay'];
        $payArray[$id] = floatval($value);
        Yii::$app->session['PersonFrmPay'] = $payArray;
        $value = Yii::$app->session['PersonFrmPay'][$id];

        return $this->renderAjax('_ajax_pay_check_sum', [
            //'searchModel' => $searchModel,
            'value' => $value,
        ]);
    }


    /**
     * Save sum pay.
     * @param integer $id
     * @param string $basis
     * @param string $date
     * @return mixed
     */
    public function actionSaveCalculate($id, $basis, $date)
    {

        $payArray = Yii::$app->session['PersonFrmPay'];

        foreach ($payArray as $k => $value) {
            $line = FinanceBook::find()->where(['fb_aca_id' => $k])->all();
            $pay = floatval($value);

            foreach ($line as $key => $obj) {
                if (!isset($obj->fb_pay)) {
                    // Сумма оплаты больше необходимого
                    if (floatval($obj->fb_sum) <= $pay) {
                        $obj->fb_pay = $obj->fb_sum;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    // Сумма оплаты меньше необходимого
                    if (floatval($obj->fb_sum) > $pay) {
                        $obj->fb_pay = $pay;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    if ((floatval($obj->fb_sum) > floatval($obj->fb_pay))) {

                        $p = $pay;
                        if ($obj->fb_tax > 0.001) {
                            if (strtotime($obj->fb_date) >= strtotime(date('2019-01-01'))) {
                                $p_tax = round($pay/1.2 * 0.2, 2);
                            } else {
                                $p_tax = round($pay/1.18 * 0.18, 2);
                            }
                        } else {
                            $p_tax = 0;
                        }

                        $fb = new FinanceBook();
                        $fb->fb_fb_id = $obj->fb_id;
                        $fb->fb_aca_id = $obj->fb_aca_id;
                        $fb->fb_svc_id = $obj->fb_svc_id;
                        $fb->fb_fbt_id = $obj->fb_fbt_id;
                        $fb->fb_ab_id = $obj->fb_ab_id;
                        $fb->fb_comp_id = $obj->fb_comp_id;
                        $fb->fb_sum = $obj->fb_sum - $p;
                        $fb->fb_tax = $obj->fb_tax - $p_tax;
                        $fb->fb_aga_id = $obj->fb_aga_id;
                        $fb->fb_acc_id = $obj->fb_acc_id;
                        $fb->fb_fbs_id = $obj->fb_fbs_id;

                        $obj->fb_sum = $p;
                        $obj->fb_tax = $p_tax;

                        if ($fb->save()) {
                            $obj->save(false);
                        }
                    }

                    $pay -= floatval($obj->fb_pay);
                }
                $obj->save(false);
                if ($pay <= 0.001)
                    break;
            }
        }

        AgreementAcc::findOne($id)->updateStatus();

        $searchModel = new AgreementAccASearch();
        $dataProvider = $searchModel->search(['AgreementAccASearch' => ['aca_aga_id' => $id]]);

        Yii::$app->session['PersonFrmPay'] = [];

        return $this->renderAjax('_grid_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['PersonFrmPay'],
        ]);


    }


}
