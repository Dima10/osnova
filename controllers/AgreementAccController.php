<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
    AgreementAcc, AgreementAccSearch, Entity, Company, Agreement, AgreementStatus, AgreementAnnex, Excel_XML
};
use yii\web\{
    Controller, HttpException, NotFoundHttpException, UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgreementAccController implements the CRUD actions for AgreementAcc model.
 */
class AgreementAccController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AgreementAcc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgreementAccSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['AgrementAccParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
            'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
            'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
        ]);
    }

    /**
     * Displays a single AgreementAcc model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new AgreementAcc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AgreementAcc();


        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'aga_data')) != null) {
                $model->aga_fdata = $file->name;
                $model->aga_data = file_get_contents($file->tempName);
            }

            if (($file = UploadedFile::getInstance($model, 'aga_data_sign')) != null) {
                $model->aga_fdata_sign = $file->name;
                $model->aga_data_sign = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->aga_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }


        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->aga_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->all(), 'agra_id', 'agra_number'),
            ]);
        }
        */
    }

    /**
     * Updates an existing AgreementAcc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'aga_data')) != null) {
                $model->aga_fdata = $file->name;
                $model->aga_data = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata = $model->oldAttributes['aga_fdata'];
                $model->aga_data = $model->oldAttributes['aga_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'aga_data_sign')) != null) {
                $model->aga_fdata_sign = $file->name;
                $model->aga_data_sign = file_get_contents($file->tempName);
            } else {
                $model->aga_fdata_sign = $model->oldAttributes['aga_fdata_sign'];
                $model->aga_data_sign = $model->oldAttributes['aga_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->aga_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                    'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->aga_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
                'company' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'agreement' => ArrayHelper::map(Agreement::find()->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->all(), 'agra_id', 'agra_number'),
            ]);
        }
        */
    }

    /**
     * Deletes an existing AgreementAcc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = AgreementAcc::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->aga_data, $model->aga_fdata);

    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSign($id)
    {
        return $this->redirect(['download-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadSignF($id)
    {
        $model = AgreementAcc::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->aga_data_sign, $model->aga_fdata_sign);

    }


    /**
     * Finds the AgreementAcc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgreementAcc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgreementAcc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * List filtered AgreementAcc models.
     * @param integer $id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSearch($id, $text)
    {
        $data = ArrayHelper::map(
            AgreementAcc::find()
                ->where(['aga_ab_id' =>  $text])->all(), 'aga_id', 'aga_number');

        return $this->renderAjax('_ajax_search', [
            'id' => $id,
            'data' => [0 => '<>'] + $data,
        ]);
    }

    /**
     * @return
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml ()
    {
        $query = AgreementAcc::find()
            ->leftJoin(Entity::tableName(), 'aga_ab_id = ent_id')
            ->leftJoin(Agreement::tableName(), 'aga_agr_id = agr_id')
            ->leftJoin(AgreementAnnex::tableName(), 'aga_agra_id = agra_id');

        $acc = new AgreementAccSearch();
        $acc->load(Yii::$app->session['AgrementAccParams']);
        // grid filtering conditions
        $query->andFilterWhere([
            'aga_id' => $acc->aga_id,
            'aga_ab_id' => $acc->aga_ab_id,
            'aga_comp_id' => $acc->aga_comp_id,
            'aga_agr_id' => $acc->aga_agr_id,
            'aga_agra_id' => $acc->aga_agra_id,
            'aga_ast_id' => $acc->aga_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'agra_number', $acc->agra_number])
            ->andFilterWhere(['like', 'ent_name', $acc->ent_name])

            ->andFilterWhere(['like', 'aga_comment', $acc->aga_comment])
            ->andFilterWhere(['like', 'aga_number', $acc->aga_number])
            ->andFilterWhere(['like', 'aga_date', $acc->aga_date])
            ->andFilterWhere(['like', 'aga_sum', $acc->aga_sum])
            ->andFilterWhere(['like', 'aga_tax', $acc->aga_tax])
            ->andFilterWhere(['like', 'aga_create_user', $acc->aga_create_user])
            ->andFilterWhere(['like', 'aga_create_time', $acc->aga_create_time])
            ->andFilterWhere(['like', 'aga_create_ip', $acc->aga_create_ip])
            ->andFilterWhere(['like', 'aga_update_user', $acc->aga_update_user])
            ->andFilterWhere(['like', 'aga_update_time', $acc->aga_update_time])
            ->andFilterWhere(['like', 'aga_update_ip', $acc->aga_update_ip])
        ;

        $data = [];
        foreach ($query->all() as &$model) {
            $data[] = [
                $model->aga_id,
                $model->aga_number,
                $model->aga_date,
                $model->agaAb->ab_name ?? '',
                $model->agaAb->entity->entAgent->ab_name ?? '',
                $model->agaAst->ast_name ?? '',
                $model->agaAgr->agr_number ?? '',
                $model->agaAgra->agra_number ?? '',
                $model->agaComp->comp_name ?? '',
                $model->aga_sum,
                $model->aga_tax,
                $model->aga_comment,
            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'AgreementAcc';
        $xls->addArray($data);
        //$xls->generateXML('AgreementAcc');

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'AgreementAcc.xls');
    }
}
