<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Excel_XML;
use app\models\Person;
use app\models\TrainingProg;
use Yii;
use app\models\TrainingProgSession;
use app\models\TrainingProgSessionSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * TrainingProgSessionController implements the CRUD actions for TrainingProgSession model.
 */
class TrainingProgSessionController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all TrainingProgSession models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/TrainingProgSessionController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new TrainingProgSessionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['TrainingProgSessionSearchParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrainingProgSession model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new TrainingProgSession model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingProgSession();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tps_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TrainingProgSession model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tps_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TrainingProgSession model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainingProgSession model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingProgSession the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainingProgSession::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Export to Excell
     * @see TrainingProgSessionSearch->search()
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml()
    {
        $query = (new \yii\db\Query())->select([
            'training_prog_session.tps_id',
            'training_prog_session.tps_session_id',
            'training_prog.trp_name',
            'person.prs_full_name',
            'training_prog_session.tps_create_user',
            'training_prog_session.tps_create_time',
            'training_prog_session.tps_create_ip',
        ])
            ->from('training_prog_session')
            ->leftJoin(TrainingProg::tableName(),'tps_trp_id = trp_id')
            ->leftJoin(Person::tableName(),'tps_prs_id = prs_id');

        $searchModel = new TrainingProgSessionSearch();
        $searchModel->load(Yii::$app->session['TrainingProgSessionSearchParams']);

        $query->andFilterWhere([
            'tps_id' => $searchModel->tps_id,
        ]);

        $query->andFilterWhere(['like', 'tps_session_id', $searchModel->tps_session_id])
            ->andFilterWhere(['like', 'tps_create_user', $searchModel->tps_create_user])
            ->andFilterWhere(['like', 'tps_create_time', $searchModel->tps_create_time])
            ->andFilterWhere(['like', 'tps_create_ip', $searchModel->tps_create_ip])
            ->andFilterWhere(['like', 'training_prog.trp_name', $searchModel->trp_name])
            ->andFilterWhere(['like', 'person.prs_full_name', $searchModel->prs_full_name]);

        $data[] = [
            Yii::t('app', 'Tps ID'),
            Yii::t('app', 'Tps Session ID'),
            Yii::t('app', 'Tps Trp ID'),
            Yii::t('app', 'Tps Prs ID'),
            Yii::t('app', 'Prs Connect User'),
            Yii::t('app', 'Tps Create Time'),
            Yii::t('app', 'Tps Create Ip'),
        ];

        $data = array_merge($data, $query->all());

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'TrainingProgSession';
        $xls->addArray($data);
        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'TrainingProgSession.xls');
    }

}
