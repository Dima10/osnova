<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\Ab;
use app\models\EntityClass;
use app\models\EntityClassLnk;
use app\models\Svc;
use app\models\SvcSearch;
use app\models\SvcProg;
use app\models\SvcProgSearch;
use app\models\TrainingProg;
use app\models\SvcDocType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SvcController implements the CRUD actions for Svc model.
 */
class SvcController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Svc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'doc_type' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
            'ab' => ArrayHelper::map(
                Ab::find()
                    ->leftJoin(EntityClassLnk::tableName(),'entcl_ent_id = ab_id')
                    ->leftJoin(EntityClass::tableName(), 'entc_id =entcl_entc_id')
                    ->where('entc_sy like "%A%"')
                    ->all(), 'ab_id', 'ab_name'),
        ]);
    }

    /**
     * Displays a single Svc model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $searchModelSvcProg = new SvcProgSearch();
        $dataProviderSvcProg = $searchModelSvcProg->search(['SvcProgSearch' => ['sprog_svc_id' => $id]]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'prog' => null,//ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
            'svc' => null,//ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
            'searchModelSvcProg' => null,//$searchModelSvcProg,
            'dataProviderSvcProg' => $dataProviderSvcProg,
            'doc_type' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
            'ab' => ArrayHelper::map(
                Ab::find()
                    ->leftJoin(EntityClassLnk::tableName(),'entcl_ent_id = ab_id')
                    ->leftJoin(EntityClass::tableName(), 'entc_id =entcl_entc_id')
                    ->where('entc_sy like "%A%"')
                    ->all(), 'ab_id', 'ab_name'),
        ]);
    }

    /**
     * Creates a new Svc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Svc();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->svc_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'doc_type' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
                'ab' => ArrayHelper::map(
                    Ab::find()
                        ->leftJoin(EntityClassLnk::tableName(),'entcl_ent_id = ab_id')
                        ->leftJoin(EntityClass::tableName(), 'entc_id =entcl_entc_id')
                        ->where('entc_sy like "%A%"')
                        ->all(), 'ab_id', 'ab_name'),
                'prog' => [],
            ]);
        }
    }

    /**
     * Updates an existing Svc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->svc_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'doc_type' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
                'ab' => ArrayHelper::map(
                    Ab::find()
                        ->leftJoin(EntityClassLnk::tableName(),'entcl_ent_id = ab_id')
                        ->leftJoin(EntityClass::tableName(), 'entc_id =entcl_entc_id')
                        ->where('entc_sy like "%A%"')
                        ->all(), 'ab_id', 'ab_name'),
            ]);
        }
    }

    /**
     * Deletes an existing Svc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Svc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Svc the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Svc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Creates a new SvcProg model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $svc_id
     * @return mixed
     */
    public function actionCreateSvcProg($svc_id)
    {
        $model = new SvcProg();

        $model->sprog_svc_id = $svc_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sprog_svc_id]);
        } else {
            //$prog = ArrayHelper::map(TrainingProg::find()->leftJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id AND sprog_svc_id ='. $svc_id)->where(['sprog_id' => null])->all(), 'trp_id', 'trp_name');
            $prog = [];

            return $this->render('create_svc_prog', [
                'model' => $model,
                'svc' => ArrayHelper::map(Svc::find()->where(['svc_id' => $svc_id])->all(), 'svc_id', 'svc_name'),
                'prog' => $prog,
            ]);
        }
    }


    /**
     * Search TraningProg model.
     * @param integer $svc_id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSvcProgSearch($svc_id, $text)
    {
        $prog = ArrayHelper::map(TrainingProg::find()
            ->leftJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id AND sprog_svc_id ='. $svc_id)
                ->where(['sprog_id' => null])
                ->andWhere(['like', 'trp_name', $text])
            ->all(), 'trp_id', 'trp_name');

        return $this->renderAjax('_ajax_svc_prog', [
            'prog' => $prog,
        ]);
    }

    /**
     * Search TraningProg model.
     * @param integer $svc_id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSvcProgSearchA($svc_id, $text)
    {
        $prog = ArrayHelper::map(TrainingProg::find()
            ->leftJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id AND sprog_svc_id ='. $svc_id)
            ->where('sprog_id is not null')
            ->andWhere(['like', 'trp_name', $text])
            ->all(), 'trp_id', 'trp_name');

        return $this->renderAjax('_ajax_svc_prog', [
            'prog' => $prog,
        ]);
    }

    /**
     * Deletes an existing SvcProg model.
     * If deletion is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteSvcProg($id)
    {
        $model = SvcProg::findOne($id);
        $svc_id = $model->sprog_svc_id;
        $model->delete();

        return $this->redirect(['view', 'id' => $svc_id]);
    }

    /**
     * Search Svc models.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSearch($id, $text)
    {
        $svc = ArrayHelper::map(Svc::find()
            ->where(['like', 'svc_name', $text])
            ->all(), 'svc_id', 'svc_name');

        return $this->renderAjax('_ajax_search', [
            'id' => $id,
            'svc' => [0 => '<>'] + $svc,
        ]);
    }

    /**
     * Search TraningProg model.
     * @param integer $svc_id
     * @param string $id
     * @return mixed
     */
    public function actionAjaxProgSearch($svc_id, $id)
    {
        $prog = ArrayHelper::map(
                TrainingProg::find()
                    ->select(['trp_id', 'CONCAT(trp_name, trp_hour) as trp_name'])
                    ->innerJoin(SvcProg::tableName(), 'trp_id = sprog_trp_id')
                    ->where(['sprog_svc_id' => $svc_id])
                    ->all(),
                'trp_id', 'trp_name');

        return $this->renderAjax('_ajax_prog', [
            'id' => $id,
            'prog' => $prog,
        ]);
    }

}
