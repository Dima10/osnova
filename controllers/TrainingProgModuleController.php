<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
    TrainingProgModule,
    TrainingProgModuleSearch,
    TrainingProg,
    TrainingModule
};
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * TrainingProgModuleController implements the CRUD actions for TrainingProgModule model.
 */
class TrainingProgModuleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all TrainingProgModule models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingProgModuleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'CONCAT(trm_code, \' \', trm_name) as trm_name'])->all(), 'trm_id', 'trm_name'),
            'prog' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'CONCAT(trp_code, \' \', trp_name) as trp_name'])->all(), 'trp_id', 'trp_name'),
        ]);
    }

    /**
     * Displays a single TrainingProgModule model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingProgModule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingProgModule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trpl_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'CONCAT(trm_code, \' \', trm_name) as trm_name'])->all(), 'trm_id', 'trm_name'),
                'prog' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'CONCAT(trp_code, \' \', trp_name) as trp_name'])->all(), 'trp_id', 'trp_name'),
            ]);
        }
    }

    /**
     * Updates an existing TrainingProgModule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->trpl_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'module' => ArrayHelper::map(TrainingModule::find()->select(['trm_id', 'CONCAT(trm_code, \' \', trm_name) as trm_name'])->all(), 'trm_id', 'trm_name'),
                'prog' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'CONCAT(trp_code, \' \', trp_name) as trp_name'])->all(), 'trp_id', 'trp_name'),
            ]);
        }
    }

    /**
     * Deletes an existing TrainingProgModule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainingProgModule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingProgModule the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = TrainingProgModule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
