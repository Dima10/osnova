<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
                TrainingAnswer,
                TrainingAnswerSearch,
                TrainingQuestion
};;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * TrainingAnswerController implements the CRUD actions for TrainingAnswer model.
 */
class TrainingAnswerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all TrainingAnswer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingAnswerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'question' => ArrayHelper::map(TrainingQuestion::find()->select(['trq_id', 'trq_question']) ->all(), 'trq_id', 'trq_question'),
        ]);
    }

    /**
     * Displays a single TrainingAnswer model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrainingAnswer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainingAnswer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tra_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'question' => ArrayHelper::map(TrainingQuestion::find()->select(['trq_id', 'trq_question']) ->all(), 'trq_id', 'trq_question'),
            ]);
        }
    }

    /**
     * Updates an existing TrainingAnswer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tra_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'question' => ArrayHelper::map(TrainingQuestion::find()->select(['trq_id', 'trq_question']) ->all(), 'trq_id', 'trq_question'),
            ]);
        }
    }

    /**
     * Deletes an existing TrainingAnswer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Send image from existing model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionImage($id)
    {
        $model = $this->findModel($id);
        return Yii::$app->response->sendContentAsFile($model->tra_file_data, $model->tra_file_name);
    }


    /**
     * Finds the TrainingAnswer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrainingAnswer the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = TrainingAnswer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
