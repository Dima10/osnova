<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\SvcDocType;
use app\models\TrainingType;
use Yii;
use app\models\Pattern;
use app\models\PatternSearch;
use app\models\PatternType;

use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PatternController implements the CRUD actions for Pattern model.
 */
class PatternController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['download', 'download-f'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Pattern models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatternSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
            'pattern_svdt' => ArrayHelper::map(SvcDocType::find()->where('svdt_id > 0')->all(), 'svdt_id', 'svdt_name'),
            'pattern_trt' => [0 => '<>'] + ArrayHelper::map(TrainingType::find()->where('trt_id > 0')->all(), 'trt_id', 'trt_name'),
        ]);
    }

    /**
     * Displays a single Pattern model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pattern model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pattern();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $file = UploadedFile::getInstance($model, 'pat_fdata');
            $model->pat_fname = $file->name;
            $model->pat_fdata = file_get_contents($file->tempName);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->pat_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
                    'pattern_svdt' => [0 => '<>'] + ArrayHelper::map(SvcDocType::find()->where('svdt_id > 0')->all(), 'svdt_id', 'svdt_name'),
                    'pattern_trt' => [0 => '<>'] + ArrayHelper::map(TrainingType::find()->where('trt_id > 0')->all(), 'trt_id', 'trt_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
                'pattern_svdt' => [0 => '<>'] + ArrayHelper::map(SvcDocType::find()->where('svdt_id > 0')->all(), 'svdt_id', 'svdt_name'),
                'pattern_trt' => [0 => '<>'] + ArrayHelper::map(TrainingType::find()->where('trt_id > 0')->all(), 'trt_id', 'trt_name'),
            ]);
        }


        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pat_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
            ]);
        }
        */
    }

    /**
     * Updates an existing Pattern model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if (($file = UploadedFile::getInstance($model, 'pat_fdata')) != null)
            {
                $model->pat_fname = $file->name;
                $model->pat_fdata = file_get_contents($file->tempName);
            } else {
                $model->pat_fname = $model->oldAttributes['pat_fname'];
                $model->pat_fdata = $model->oldAttributes['pat_fdata'];
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->pat_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
                    'pattern_svdt' => [0 => '<>'] + ArrayHelper::map(SvcDocType::find()->where('svdt_id > 0')->all(), 'svdt_id', 'svdt_name'),
                    'pattern_trt' => [0 => '<>'] + ArrayHelper::map(TrainingType::find()->where('trt_id > 0')->all(), 'trt_id', 'trt_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
                'pattern_svdt' => [0 => '<>'] + ArrayHelper::map(SvcDocType::find()->where('svdt_id > 0')->all(), 'svdt_id', 'svdt_name'),
                'pattern_trt' => [0 => '<>'] + ArrayHelper::map(TrainingType::find()->where('trt_id > 0')->all(), 'trt_id', 'trt_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pat_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'pattern_type' => ArrayHelper::map(PatternType::find()->where('patt_id > 0')->all(), 'patt_id', 'patt_name'),
            ]);
        }
        */
    }

    /**
     * Deletes an existing Pattern model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Pattern model.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadF($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->user->identity->level < 0) {
            if ($id == 80) { // Инструкция для личного кабинета Слушателя
                return Yii::$app->response->sendContentAsFile($model->pat_fdata, $model->pat_fname);
            } else {
                return "";
            }
        }
        return Yii::$app->response->sendContentAsFile($model->pat_fdata, $model->pat_fname);
    }

    /**
     * Finds the Pattern model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pattern the loaded model
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    protected function findModel($id)
    {
        if (($model = Pattern::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
