<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplEnd;
use app\models\ApplMain;
use app\models\ApplOutContent;
use app\models\ApplOutContentSearch;
use app\models\Pattern;
use app\models\PatternType;
use app\models\SvcDocType;
use app\models\TrainingType;
use Yii;
use app\models\ApplOut;
use app\models\ApplOutSearch;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\base\Exception;
use yii\web\NotFoundHttpException;


/**
 * ApplOutController implements the CRUD actions for ApplOut model.
 */
class ApplOutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplOut models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplOutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            'svdt' => ArrayHelper::map(SvcDocType::find()->all(), 'svdt_id', 'svdt_name'),
        ]);
    }

    /**
     * Displays a single ApplOut model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplOut model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {

        $dynModel = DynamicModel::validateData(['date1', 'date2'],
            [
                [['date1', 'date2'], 'string'],
            ]
        );

        if (Yii::$app->request->isPost) {

            $dynModel->load(Yii::$app->request->post());
            foreach (ApplOut::find()->where(['between', 'applout_date', $dynModel->date1, $dynModel->date2])->all() as $key => $obj) {
                ApplOutContent::deleteAll(['apploutc_applout_id' => $obj->applout_id]);
            }

            $end = ApplEnd::find()->where(['between', 'apple_date', $dynModel->date1, $dynModel->date2])->all();
            foreach ($end as $key => $obj) {
                if (
                    !$model =
                        ApplOut::find()
                            ->where([
                                'applout_date' => $obj->apple_date,
                                'applout_trt_id' => $obj->apple_trt_id,
                                'applout_svdt_id' => $obj->apple_svdt_id
                            ])
                            ->one()
                )
                {
                    $model = new ApplOut();
                    $model->applout_date = $obj->apple_date;
                    $model->applout_trt_id = $obj->apple_trt_id;
                    $model->applout_svdt_id = $obj->apple_svdt_id;

                    $pattern = Pattern::find()
                        ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                        ->where(['pat_svdt_id' => $model->applout_svdt_id])
                        ->andWhere(['patt_id' => 13])
                        ->andWhere(['pat_trt_id' => $model->applout_trt_id])
                        ->one();
                    if ($pattern == null) {
                        throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
                    }

                    $model->applout_number = \DateTime::createFromFormat('Y-m-d', $obj->apple_date)->format('Ymd') . '-'.$pattern->pat_code;

                }

                if (!$model->save()) {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                foreach ($obj->applEndContents as $k1 => $value) {
                    $modelA = new ApplOutContent();
                    $modelA->apploutc_applout_id = $model->applout_id;


                    $modelA->apploutc_prs_id = $value->applec_prs_id;
                    $modelA->apploutc_trp_id = $value->applec_trp_id;
                    $modelA->apploutc_applsx_id = $value->applecApple->apple_applsx_id;
                    $modelA->apploutc_apple_id = $value->applec_apple_id;
                    $modelA->apploutc_number_upk = $value->applec_number;

                    if ($main = ApplMain::find()->where(['applm_reestr' => 0, 'applm_prs_id' => $modelA->apploutc_prs_id, 'applm_trp_id' => $modelA->apploutc_trp_id, 'applm_apple_date' => $value->applecApple->apple_date])->one()) {
                        $modelA->apploutc_position = $main->applm_position;
                        $modelA->apploutc_ab_id = $main->applm_ab_id;
                        $modelA->apploutc_applcmd_id = $main->applm_applcmd_id;
                    }

                    if (!$modelA->save()) {
                        return $this->render('create_content', [
                            'model' => $modelA,
                        ]);
                    }

                }

                $pattern = Pattern::find()
                    ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                    ->where(['pat_svdt_id' => $model->applout_svdt_id])
                    ->andWhere(['patt_id' => 13])
                    ->andWhere(['pat_trt_id' => $model->applout_trt_id])
                    ->one();
                if ($pattern == null) {
                    throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
                }

                $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
                $file_name = Yii::getAlias('@app') . '/storage/' . $model->applout_date . '_' . $pattern->pat_fname;
                file_put_contents($tmpl_name, $pattern->pat_fdata);

                $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
                $document->cloneRow('num', count($model->applOutContents));

                $i = 1;
                foreach ($model->applOutContents as $k => $val) {
                    $document->setValue('num#' . $i, $i);
                    $document->setValue('PERSONA_FIO#' . $i, $val->apploutcPrs->prs_full_name);
                    $document->setValue('PROGRAMMA#' . $i, $val->apploutcTrp->trp_name);
                    $document->setValue('OUT_NUMBER#' . $i, $val->apploutcApplend->apple_number);
                    $document->setValue('OUT_DOC_NUMBER#' . $i, $val->apploutc_number_upk);
                    $document->setValue('VED_ITOG_NUMBER#' . $i, $val->apploutcApplsx->applsx_number);
                    $document->setValue('PERSONA_WORK#' . $i, $val->apploutcAb->ab_name ?? '');
                    $document->setValue('IN_NUMBER#' . $i, isset($val->apploutcApplcmd) ? $val->apploutcApplcmd->applcmd_number : '');
                    $i++;
                }
                $document->saveAs($file_name);

                $model->applout_file_name = $model->applout_date . '_' . $pattern->pat_fname;
                $model->applout_file_data = file_get_contents($file_name);
                $model->applout_pat_id = $pattern->pat_id;

                unlink($file_name);
                unlink($tmpl_name);

                if (!$model->save()) {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }


            }

            return $this->redirect(['index']);
        }
        return $this->render('create_new', ['model' => $dynModel]);

    }

    /**
     * Updates an existing ApplOut model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applout_id]);
        } else {

            $searchModel = new ApplOutContentSearch();
            $dataProvider = $searchModel->search(['ApplOutContentSearch' => ['apploutc_applout_id' => $id]]);

            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing ApplOut model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplOut model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplOut the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplOut::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = ApplOut::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->applout_file_data, $model->applout_file_name);
    }

}
