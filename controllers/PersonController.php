<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplCommandContent;
use app\models\ApplMain;
use app\models\ApplOutContent;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplSh;
use app\models\ApplSheet;
use app\models\ApplSheetSearch;
use app\models\ApplShSearch;
use app\models\SentEmailsFromRequest;
use app\models\TrainingModulePerson;
use app\models\TrainingModulePersonMail;
use app\models\TrainingProg;
use app\models\TrainingProgModule;
use app\models\TrainingProgModuleSearch;
use app\models\TrainingProgSession;
use app\models\UserI;
use app\models\UserMail;
use Yii;
use app\models\Person;
use app\models\PersonSearch;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * PersonController implements the CRUD actions for Person model.
 */
class PersonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['connect'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => [
                            'training-program',
                            'training-index',
                            'appl-sh',
                            'appl-sheet',
                            'ajax-read-module',
                            'ajax-ready-module'
                        ],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all Person models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PersonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Person model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Person();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prs_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Person model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->prs_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Person model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Generate unique ConnectLink string an existing Person model.
     * If generate is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionGenConnectLink($id)
    {
        $model = $this->findModel($id);
        $model->genConnectLink();

        if ($model->save()) {
            return $this->render('gen_connect_link', ['model' => $model]);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Generate unique ConnectLink string an existing Person model.
     * If generate is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionViewConnectLink($id)
    {
        $model = $this->findModel($id);

        return $this->render('gen_connect_link', ['model' => $model]);
    }

    /**
     * Generate unique ConnectLink string an existing Person model.
     * If generate is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionConnectLink($id)
    {
        if ($model = Person::find()->where(['prs_connect_link' => $id])->one()) {
            return $this->redirect(['view', 'id' => $model->prs_id]);
        } else {
            return $this->redirect(['site/index']);
        }
    }

    /**
     * List filtered Person models.
     * @param integer $id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSearch($id, $text)
    {
        //$data = ArrayHelper::map(
        //    Person::find()
        //        ->where(['like', 'prs_full_name', $text])->all(), 'prs_id', 'prs_full_name');

        $prs = Person::find()
            ->where(['like', 'prs_full_name', $text])->all();
        $data = [];
        foreach ($prs as $person) {
            $data[$person->prs_id] = $person->prs_full_name.' / '.$person->prs_connect_user;
        }

        return $this->renderAjax('_ajax_search', [
            'id' => $id,
            'data' => [0 => '<>'] + $data,
        ]);
    }

    /**
     * List filtered Person models.
     * @param integer $id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSearchNumber($id, $text)
    {
        //$data = ArrayHelper::map(
        //    Person::find()->select('prs_id, prs_full_name')
        //        ->where(['like', 'prs_pass_number', $text])->all(), 'prs_id', 'prs_full_name');

        $prs = Person::find()
            ->where(['like', 'prs_pass_number', $text])->all();
        $data = [];
        foreach ($prs as $person) {
            $data[$person->prs_id] = $person->prs_full_name.' / '.$person->prs_connect_user;
        }

        return $this->renderAjax('_ajax_search', [
            'id' => $id,
            'data' => [0 => '<>'] + $data,
        ]);
    }

    /**
     * Update prs_connect_xxx in Person models.
     * @return mixed
     */
    public function actionUpdateConnect()
    {
        $prs = Person::find()->where(['prs_connect_pwd' => null])->all();

        foreach ($prs as $key => $person) {
            $person->prs_connect_user = 'os-'.$person->prs_id;
            srand();
            $person->prs_connect_pwd = (string) rand(10000, 99999);
            $person->save();
        }
        return $this->redirect(['index']);
    }

    /**
     * Connection to system use Person model.
     * @return mixed
     * @throws
     */
    public function actionConnect()
    {
        $dynModel = DynamicModel::validateData(['user', 'password'],
            [
                //[['user', 'password'], 'required'],
                [['user', 'password'], 'string'],
            ]
        );

        if (Yii::$app->request->isPost && $dynModel->load(Yii::$app->request->post())) {
            $user = UserI::findByUsername($dynModel->user);
            if (!$user || !$user->validatePassword($dynModel->password)) {
                $dynModel->addError('password', Yii::t('app', 'Incorrect username or password.'));
            } else {

                $nowDate = new \DateTime('now');
                SentEmailsFromRequest::updateAll(['portal' => $nowDate->format('Y-m-d H:i:s')], ['portal'=>'0000-00-00 00:00:00', 'login'=>$dynModel->user]);

                Yii::$app->user->login(UserI::findByUsername($dynModel->user), 600);
                return $this->redirect(['training-program']);
            }
        }

        return $this->render('connect', [
            'model' => $dynModel,
        ]);

    }


    /**
     * View TrainingProgram.
     * @return mixed
     * @throws \Exception
     */
    public function actionTrainingProgram()
    {
        if ($prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username])) {

            $date = new \DateTime();
            $date->sub(new \DateInterval('P62D'));

            $commandApplmIds = Yii::$app->db->createCommand('
                select am.applm_id
                from appl_out_content aoc
                inner join appl_main am on aoc.apploutc_trp_id = am.applm_trp_id and aoc.apploutc_applcmd_id=am.applm_applcmd_id and aoc.apploutc_prs_id = am.applm_prs_id
                where aoc.apploutc_prs_id=:prs_id'
            );

            $prsId = $prs->prs_id;
            $commandApplmIds->bindParam(':prs_id', $prsId);
            $applmIds = $commandApplmIds->queryColumn();

            $query = TrainingProg::find()
                ->select('appl_request_content.applrc_applr_id as applrcApplrId, `training_prog`.*')
                ->innerJoin(ApplRequestContent::tableName(), 'trp_id = applrc_trp_id')
                ->innerJoin(ApplRequest::tableName(), "applrc_applr_id = applr_id") // and applr_date < '{$date->format('Y-m-d')}'
                ->leftJoin(ApplOutContent::tableName(), 'apploutc_trp_id = applrc_trp_id and apploutc_prs_id = applrc_prs_id')
                ->leftJoin(ApplMain::tableName(), 'applrc_id = applm_applrc_id')
                ->where(['applrc_prs_id' => $prs->prs_id, 'applr_ast_id' => 10])
                ->andWhere(['not in', 'applm_id', $applmIds])
                ->andWhere(['>', 'applm_date_upk', $date->format('Y-m-d')])
            ;

//            $query = TrainingProg::find()
//                ->select('appl_request_content.applrc_applr_id as applrcApplrId, `training_prog`.*')
//                ->leftJoin(ApplMain::tableName(), 'trp_id = applm_trp_id')
//                ->innerJoin(ApplRequestContent::tableName(), 'applrc_id = applm_applrc_id')
//                ->where(['applm_prs_id' => $prs->prs_id])
//                ->andWhere(['>', 'applm_date_upk', $date->format('Y-m-d')])
//            ;

            /*
            $query = TrainingProg::find()
                ->leftJoin(ApplCommandContent::tableName(), 'trp_id = applcmdc_trp_id')
                ->leftJoin(ApplOutContent::tableName(), 'apploutc_trp_id = applcmdc_trp_id and apploutc_prs_id = applcmdc_prs_id')
                ->where(['applcmdc_prs_id' => $prs->prs_id])
                ->andWhere(['apploutc_prs_id' => null])
            ;
            */

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);


            return $this->render('training_program', [
                'searchModel' => null,
                'dataProvider' => $dataProvider,
                'prs' => $prs,
            ]);
        }
        return $this->redirect(['/site/index']);
    }

    /**
     * View TrainingProgram.
     * @param integer $id
     * @return mixed
     */
    public function actionTrainingIndex($id)
    {
        TrainingProgSession::saveData($id);

        $searchModelMod = new TrainingProgModuleSearch();
        $dataProviderMod = $searchModelMod->search(['TrainingProgModuleSearch' => ['trpl_trp_id' => $id]]);

        if (!$prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username])) {
            return $this->redirect(['/site/index']);
        }

        if (!$m = TrainingModulePersonMail::find()->where(['tmpm_prs_id' => $prs->prs_id, 'tmpm_trm_id' => $id])->one()) {
            $m = new TrainingModulePersonMail();
            $m->tmpm_prs_id = $prs->prs_id;
            $m->tmpm_trm_id = $id;

            if($m->save()){
                foreach ($prs->contacts as $contact) {
                    if ($contact->conCont->cont_type==1) {
                        UserMail::sendMail(2, $contact->con_text,
                            [
                                'link' => $prs->prs_connect_link,
                                'login' => $prs->prs_connect_user,
                                'password' => $prs->prs_connect_pwd,
                                'fio' => $prs->prs_full_name,
                                'programma' => TrainingProg::findOne($id)->trp_name,
                            ]
                        );
                    }
                }
            }
        }

        $searchModelSh = new ApplShSearch();
        $dataProviderSh = $searchModelSh->search(['ApplShSearch' => ['appls_prs_id' => $prs->prs_id, 'appls_trp_id' => $id]]);
        $sh = ApplSh::find()->where(['appls_prs_id' => $prs->prs_id, 'appls_trp_id' => $id])->one();

        $searchModelSheet = new ApplSheetSearch();
        $dataProviderSheet = $searchModelSheet->search(['ApplSheetSearch' => ['appls_prs_id' => $prs->prs_id]]);
        if ($sheet = ApplSheet::find()->where(['appls_prs_id' => $prs->prs_id, 'appls_trp_id' => $id])->one()) {
            /*
                if ($sheet->appls_passed == 1) {
                    return $this->redirect(['training-program']);
                }
            */
        }

        /**
         * Если он нажал на кнопку ознакомлен более или равно по 50% модулей этой программы,
         * появляется сообщение: "Просьба пройти промежуточное тестирование" - текст позже уточню,
         * и открывается доступ ко второй вкладке с промежуточным тестированием.
         */

        $mod_count_ready = TrainingModulePerson::find()
            ->innerJoin(TrainingProgModule::tableName(), 'trpl_trm_id = tmp_trm_id')
            ->where(['tmp_prs_id' => $prs->prs_id, 'trpl_trp_id' => $id])->count();

        /*
        $mod_count = TrainingProgModule::find()
            ->innerJoin(ApplCommandContent::tableName(), 'applcmdc_trp_id = trpl_trp_id')
            ->where(['trpl_trp_id' => $id, 'applcmdc_prs_id' => $prs->prs_id])
            ->count();
        */

        $mod_count = TrainingProgModule::find()
            ->innerJoin(ApplRequestContent::tableName(), 'applrc_trp_id = trpl_trp_id')
            ->innerJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
            ->where(['trpl_trp_id' => $id, 'applrc_prs_id' => $prs->prs_id, 'applr_ast_id' => 10])
            ->count();

        $ready = ($mod_count_ready) / ($mod_count > 0 ? $mod_count : 1)  >= 0.5;

        return $this->render('training_index', [
            'searchModelMod' => $searchModelMod,
            'dataProviderMod' => $dataProviderMod,
            'searchModelSh' => $searchModelSh,
            'dataProviderSh' => $dataProviderSh,
            'searchModelSheet' => $searchModelSheet,
            'dataProviderSheet' => $dataProviderSheet,
            'prs' => $prs,
            'sh' => $sh,
            'sheet' => $sheet,
            'ready' => $ready,
            'trp_id' => $id,
        ]);
    }

    /**
     * Go to Full Test
     * @param integer $trp_id
     * @return mixed
     */
    public function actionApplSheet($trp_id)
    {
        $prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]);

        $cmd = ApplRequest::find()
            ->leftJoin(ApplRequestContent::tableName(), 'applr_id = applrc_applr_id')
            ->leftJoin(ApplOutContent::tableName(), 'apploutc_trp_id = applrc_trp_id and apploutc_prs_id = applrc_prs_id')
            ->where(['applrc_prs_id' => $prs->prs_id, 'applrc_trp_id' => $trp_id, 'apploutc_prs_id' => null])
            ->one()
        ;

        if (!is_null($cmd)) {
            return $this->redirect(Url::toRoute(['/appl-sheet/generate', 'applr_id' => $cmd->applr_id, 'prs_id' => $prs->prs_id, 'applrc_trp_id' => $trp_id, 'date' => '', 'test' => true]));
        } else {
            return $this->redirect(['training-index', 'id' => $trp_id]);
        }
    }

    /**
     * Go to Short Test
     * @param integer $trp_id
     * @return mixed
     */
    public function actionApplSh($trp_id)
    {
        $prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]);

        $cmd = ApplRequest::find()
            ->leftJoin(ApplRequestContent::tableName(), 'applr_id = applrc_applr_id')
            ->leftJoin(ApplOutContent::tableName(), 'apploutc_trp_id = applrc_trp_id and apploutc_prs_id = applrc_prs_id')
            ->where(['applrc_prs_id' => $prs->prs_id, 'applrc_trp_id' => $trp_id, 'apploutc_prs_id' => null])
            ->one()
        ;

        if (!is_null($cmd)) {
            return $this->redirect(Url::toRoute(['/appl-sh/generate', 'applr_id' => $cmd->applr_id, 'prs_id' => $prs->prs_id, 'applrc_trp_id' => $trp_id, 'date' => '', 'test' => true]));
        } else {
            return $this->redirect(['training-index', 'id' => $trp_id]);
        }
    }

    /**
     * Create TrainingModulePerson object
     * @param integer $trm_id
     * @return mixed
     */
    public function actionAjaxReadModule($trm_id)
    {
        $prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]);

        $ready = true;
        if (!$trm_prs = TrainingModulePerson::find()->where(['tmp_prs_id' => $prs->prs_id, 'tmp_trm_id' => $trm_id])->one()) {
            $trm_prs = new TrainingModulePerson();
            $trm_prs->tmp_prs_id = $prs->prs_id;
            $trm_prs->tmp_trm_id = $trm_id;
            $ready = $trm_prs->save();
        }

        return $this->renderAjax('_ajax_read_module', [
            'id' => $trm_id,
            'ready' => $ready,
        ]);
    }

    /**
     * Check count TrainingModulePerson
     * @param int $id
     * @return mixed
     */
    public function actionAjaxReadyModuleNext($id)
    {
        $prs = Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username]);

        $mod_count_ready = TrainingModulePerson::find()
            ->innerJoin(TrainingProgModule::tableName(), 'trpl_trm_id = tmp_trm_id')
            ->where(['tmp_prs_id' => $prs->prs_id, 'trpl_trp_id' => $id])->count();

        $mod_count = TrainingProgModule::find()
            ->innerJoin(ApplRequestContent::tableName(), 'applrc_trp_id = trpl_trp_id')
            ->innerJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
            ->where(['trpl_trp_id' => $id, 'applrc_prs_id' => $prs->prs_id, 'applr_ast_id' => 10])
            ->count();

        $ready = ($mod_count_ready) / ($mod_count > 0 ? $mod_count : 1)  >= 0.5;
        if (!isset(Yii::$app->session['Person.training.ready'.$id])) {
            Yii::$app->session['Person.training.ready'.$id] = $ready;
        } else {
            if (Yii::$app->session['Person.training.ready'.$id]) {
                //$ready = false;
            } else {
                Yii::$app->session['Person.training.ready'.$id] = $ready;
            }
        }

        return $this->renderAjax('_ajax_ready_module', [
            'ready' => $ready,
        ]);
    }
}
