<?php
namespace app\controllers;

use app\models\Ab;
use app\models\ApplCommand;
use app\models\ApplCommandContent;
use app\models\ApplEnd;
use app\models\ApplEndContent;
use app\models\ApplFinal;
use app\models\ApplFinalFile;
use app\models\ApplMain;
use app\models\ApplMainSearch;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplSh;
use app\models\ApplShContent;
use app\models\ApplSheetX;
use app\models\ApplSheetXContent;
use app\models\ApplShX;
use app\models\ApplShXContent;
use app\models\ApplXxx;
use app\models\ApplXxxContent;
use app\models\Company;
use app\models\Constant;
use app\models\Pattern;
use app\models\PatternType;
use DateTime;
use Yii;
use app\models\ApplSheet;
use app\models\ApplSheetContent;

use app\models\Person;
use app\models\TrainingProg;

use yii\base\DynamicModel;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ApplGenController extends Controller
{

    const DATE_TYPE_CMD = 1;
    const DATE_TYPE_APPL_SH_X = 2;
    const DATE_TYPE_APPLSX = 3;
    const DATE_TYPE_APPL_E = 4;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all ApplSheet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplMainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplSheet model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ApplSheet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->appls_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'program' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
                'ab' => ArrayHelper::map(Ab::find()->all(), 'ab_id', 'ab_name'),
                'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            ]);
        }
    }

    /**
     * Deletes an existing ApplSheet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplSheet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplSheet the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplSheet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     ************************************************************
     *  ApplCommand - Приказ о зачислении
     ************************************************************
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplCommand($date)
    {
        foreach (ApplMain::find()->select(['applm_trt_id', 'applm_svdt_id', 'applm_reestr'])->where(['applm_applcmd_date' => $date])->andWhere(['applm_reestr'=>0])->groupBy(['applm_trt_id', 'applm_svdt_id', 'applm_reestr'])->all() as $k1 => $m1)
        {
            $cmd = null;

            foreach (ApplMain::find()->where(['applm_applcmd_date' => $date, 'applm_trt_id' => $m1->applm_trt_id, 'applm_svdt_id' => $m1->applm_svdt_id, 'applm_reestr' => $m1->applm_reestr])->all() as $key => $main) {
                if (!$cmd = ApplCommand::find()
                    ->where([
                        'applcmd_date' => $date,
                        'applcmd_trt_id' => $main->applm_trt_id,
                        'applcmd_svdt_id' => $main->applm_svdt_id,
                        'applcmd_reestr' => $main->applm_reestr,
                        //'applcmd_ab_id' => $main->applm_ab_id,
                        //'applcmd_comp_id' => $main->applm_comp_id
                    ])
                    ->one()
                ) {
                    $cmd = new ApplCommand();
                }

                $cmd->applcmd_date = $date;
                $cmd->applcmd_trt_id = $main->applm_trt_id;
                $cmd->applcmd_svdt_id = $main->applm_svdt_id;
                $cmd->applcmd_reestr = $main->applm_reestr;
                //$cmd->applcmd_ab_id = $main->applm_ab_id;
                //$cmd->applcmd_comp_id = $main->applm_comp_id;

                $pattern = Pattern::find()
                    ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                    ->where(['pat_svdt_id' => $cmd->applcmd_svdt_id])
                    ->andWhere(['patt_id' => 6])
                    ->andWhere(['pat_trt_id' => $cmd->applcmd_trt_id])
                    ->one();
                if ($pattern == null) {
                    throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplCommand) not found'));
                }

                $cmd->applcmd_number = \DateTime::createFromFormat('Y-m-d', $cmd->applcmd_date)->format('Ymd') . '-' . $pattern->pat_code;

                if (!$cmd->save()) {
                    return $this->render('create_appl_command', [
                        'model' => $cmd,
                        'ab' => ArrayHelper::map(Ab::find()->where(['ab_id' => $cmd->applcmd_ab_id])->all(), 'ab_id', 'ab_name'),
                        'comp' => ArrayHelper::map(Company::find()->where(['comp_id' => $cmd->applcmd_comp_id])->all(), 'comp_id', 'comp_name'),
                    ]);
                }

                $main->applm_applcmd_id = $cmd->applcmd_id;
                if (!$main->save()) {
                    throw new HttpException(500, yii::t('app', 'Шеф! Все пропало! Клиенту завтра снимают гипс!'));
                }

                ApplCommandContent::deleteAll(['applcmdc_applcmd_id' => $cmd->applcmd_id, 'applcmdc_prs_id' => $main->applm_prs_id, 'applcmdc_trp_id' => $main->applm_trp_id]);

                $cmdC = new ApplCommandContent();
                $cmdC->applcmdc_applcmd_id = $cmd->applcmd_id;
                $cmdC->applcmdc_trp_id = $main->applm_trp_id;
                $cmdC->applcmdc_prs_id = $main->applm_prs_id;
                $cmdC->applcmdc_agr_number = $main->applm_agr_number;

                if (!$cmdC->save()) {
                    return $this->render('create_appl_command_cnt', [
                        'model' => $cmdC,
                        'person' => ArrayHelper::map(Person::find()->where(['prs_id' => $cmdC->applcmdc_prs_id])->all(), 'prs_id', 'prs_full_name'),
                        'program' => ArrayHelper::map(TrainingProg::find()->where(['trp_id' => $cmdC->applcmdc_trp_id])->all(), 'trp_id', 'trp_name'),
                    ]);
                }
            }

            foreach (ApplCommand::find()->where(['applcmd_date' => $date])->all() as $k => $cmd)
            {

                $pattern = Pattern::find()
                    ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                    ->where(['pat_svdt_id' => $cmd->applcmd_svdt_id])
                    ->andWhere(['patt_id' => 6])
                    ->andWhere(['pat_trt_id' => $cmd->applcmd_trt_id])
                    ->one();

                if ($pattern == null) {
                    throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplCommand0) not found'));
                }

                $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
                $file_name = Yii::getAlias('@app') . '/storage/' . $cmd->applcmd_date . '_' . $cmd->applcmd_trt_id . '_' . $pattern->pat_fname;
                file_put_contents($tmpl_name, $pattern->pat_fdata);

                $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
                $document->cloneRow('num', count($cmd->applCommandContents));

                $document->setValue('DOC_NUMBER', $cmd->applcmd_number);

                $dateVal = \DateTime::createFromFormat('Y-m-d', $cmd->applcmd_date)->getTimestamp();
                $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
                $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
                $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

                $i = 1;
                foreach ($cmd->applCommandContents as $k1 => $obj) {
                    $document->setValue('num#' . $i, $i);
                    $document->setValue('PERSONA_FIO#' . $i, $obj->applcmdcPrs->prs_full_name);
                    $document->setValue('PROGRAMMA#' . $i, $obj->applcmdcTrp->trp_name);
                    $document->setValue('DOGOVOR_NUMBER#' . $i, $obj->applcmdc_agr_number);

                    $i++;
                }
                $document->saveAs($file_name);

                if (count($cmd->applCommandContents) > 0) {
                    $cmd->applcmd_file_name = $cmd->applcmd_date . '_' . $cmd->applcmd_trt_id . '_' . $pattern->pat_fname;
                    $cmd->applcmd_file_data = file_get_contents($file_name);
                    $cmd->applcmd_pat_id = $pattern->pat_id;
                } else {
                    $cmd->applcmd_file_name = null;
                    $cmd->applcmd_file_data = null;
                    $cmd->applcmd_pat_id = null;
                }
                if (!$cmd->save()) {
                    return $this->render('create_appl_command', [
                        'model' => $cmd,
                        'ab' => ArrayHelper::map(Ab::find()->where(['ab_id' => $cmd->applcmd_ab_id])->all(), 'ab_id', 'ab_name'),
                        'comp' => ArrayHelper::map(Company::find()->where(['comp_id' => $cmd->applcmd_comp_id])->all(), 'comp_id', 'comp_name'),
                    ]);
                }
                unlink($file_name);
                unlink($tmpl_name);
            }
        }
        return 0;
    }


    /**
     ************************************************************
     * ApplSh - Протокол промежуточного тестирования
     ************************************************************
     * @param integer $prs_id
     * @param string $date
     * @param integer $trp_id
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplShId($prs_id, $date, $trp_id)
    {
        srand();
        $main = ApplMain::find()->where(['applm_prs_id' => $prs_id, 'applm_appls0_date' => $date, 'applm_trp_id' => $trp_id])->all();

        $sh = null;
        foreach ($main as $k => $obj) {
            if (!$sh = ApplSh::find()->where(['appls_prs_id' => $prs_id, 'appls_date' => $date, 'appls_trp_id' => $trp_id])->one()) {
                $sh = new ApplSh();
            }

            $sh->appls_date = $date;
            $sh->appls_prs_id = $obj->applm_prs_id;
            $sh->appls_trp_id = $obj->applm_trp_id;
            $sh->appls_trt_id = $obj->applm_trt_id;
            $sh->appls_svdt_id = $obj->applm_svdt_id;
            $sh->appls_reestr = $obj->applm_reestr;
            $sh->appls_passed = 1;
            $sh->appls_score = 0;
            $sh->appls_score_max = 0;
            $sh->appls_applm_id = $obj->applm_id;

            $appl_r_c = ApplRequestContent::findOne($obj->applm_applrc_id);
            if ($appl_r_c) {
                $sh->appls_applr_id = $appl_r_c->applrc_applr_id;
            }

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $sh->appls_svdt_id])
                ->andWhere(['patt_id' => 10])
                ->andWhere(['pat_trt_id' => $sh->appls_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', "createApplShId(prs_id=$prs_id, date=$date, trp_id=$trp_id: Pattern::find(svdt_id={$sh->appls_svdt_id}, patt_id=10, trt_id={$sh->appls_trt_id}}) not found"));
            }

            $sh->appls_number = \DateTime::createFromFormat('Y-m-d', $sh->appls_date)->format('Ymd') . '-' . $pattern->pat_code.'-'.$sh->appls_prs_id. '-N' .rand(1,999) .''.rand(1,999);

            if (!$sh->save()) {
                return $this->render('create_appl_sheet', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }

            ApplShContent::deleteAll(['applsc_appls_id' => $sh->appls_id]);

            // Кол-во вопросов
            $question_count = 0;
            $prog = TrainingProg::findOne($sh->appls_trp_id);
            // Кол-во модулей
            $mcount = 5;
            foreach ($prog->trainingProgModules as $km => $module) {
                $mcount--;
                if ($mcount < 0) {
                    break;
                }
                // Сколько вопросов нужно задать с модуля
                $question_count += ($module->trplTrm->trm_test_question > count($module->trplTrm->trainingQuestions)) ? count($module->trplTrm->trainingQuestions) : $module->trplTrm->trm_test_question;
            }

            // Допустимое кол-во неправильных ответов
            $question_neg = $question_count - round($question_count/100.0 * 80.0 + 1.0);
            $question_pos = 0;

            $mcount = 5;
            foreach ($prog->trainingProgModules as $km => $mod) {
                $mcount--;
                if ($mcount < 0) {
                    break;
                }
                $module = $mod->trplTrm;
                $training_question_count = ($module->trm_test_question > count($module->trainingQuestions)) ? count($module->trainingQuestions) : $module->trm_test_question;
                $train_ques_a = [];
                // Случайный выбор вопроса
                for ($i = 0; $i < $training_question_count; $i++) {
                    if (count($module->trainingQuestions) > 0)
                    {
                        $r = rand(0, count($module->trainingQuestions) - 1);
                        while (in_array($r, $train_ques_a)) {
                            $r = rand(0, count($module->trainingQuestions) - 1);
                        }
                        $train_ques_a[] = $r;
                    }
                }

                foreach ($train_ques_a as $item => $value) {

                    $question = $module->trainingQuestions[$value];

                    $answer = null;
                    // Выбор варианта (Верно/неверно)
                    $question_var = rand(1, 100) <= 80 ? 1 : 0;

                    if ($question_neg > 0) {
                        if ($question_var == 0) {
                            foreach ($question->trainingAnswers as $ka => $answer_) {
                                if ($answer_->tra_variant == 0) {
                                    $answer = $answer_;
                                    $question_neg--;
                                    break;
                                }
                            }
                        } else {
                            foreach ($question->trainingAnswers as $ka => $answer_) {
                                if ($answer_->tra_variant == 1) {
                                    $answer = $answer_;
                                    $question_pos++;
                                    break;
                                }
                            }
                        }
                    } else {
                        foreach ($question->trainingAnswers as $ka => $answer_) {
                            if ($answer_->tra_variant == 1) {
                                $answer = $answer_;
                                $question_pos++;
                                break;
                            }
                        }
                    }

                    if (is_null($answer))
                        continue;

                    $shC = new ApplShContent();
                    $shC->applsc_appls_id = $sh->appls_id;

                    $shC->applsc_trq_id = $question->trq_id;
                    $shC->applsc_trq_question = $question->trq_question;

                    $shC->applsc_tra_id = $answer->tra_id;
                    $shC->applsc_tra_answer = $answer->tra_answer;
                    $shC->applsc_tra_variant = $answer->tra_variant;

                    if (!$shC->save()) {
                        return $this->render('create_appl_sheet_cnt', [
                            'model' => $shC,
                        ]);
                    }
                }
            }
            $sh->appls_score_max = $question_count;
            $sh->appls_score = $question_pos;
            if ($question_count > 0) {
                $sh->appls_passed = (($question_count - $question_pos)/$question_count) <= 0.4 ? 1 : 0;
            } else {
                $sh->appls_passed = 0;
            }

            if (!$sh->save()) {
                return $this->render('create_appl_sheet', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }
        }


        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $sh->appls_svdt_id])
            ->andWhere(['patt_id' => 10])
            ->andWhere(['pat_trt_id' => $sh->appls_trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', "createApplShId0(prs_id=$prs_id, date=$date, trp_id=$trp_id: Pattern::find(svdt_id={$sh->appls_svdt_id}, patt_id=10, trt_id={$sh->appls_trt_id}}) not found"));
        }

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;

        $file_name = Yii::getAlias('@app') . '/storage/' . $sh->appls_date . '_' . $sh->appls_prs_id . '_' . $pattern->pat_fname;
        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $document->cloneRow('num', count($sh->applShContents));

        $document->setValue('DOC_NUMBER', $sh->appls_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $sh->appls_date)->getTimestamp();
        $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

        // Ф.И.О. Слушателя:
        $document->setValue('PERSONA_FIO', $sh->applsPrs->prs_full_name);
        // Программа обучения:
        $document->setValue('PROGRAMMA', $sh->applsTrp->trp_name);

        // Итого вопросов:
        $document->setValue('QUESTION_QTY', $sh->appls_score_max);
        //Итого верных ответов:
        $document->setValue('ANSWER_RIGHT_QTY', $sh->appls_score);
        //Результат тестирования:
        $document->setValue('ANSWER_RIGHT_PERCENT', $sh->appls_score_max > 0.0 ? round(($sh->appls_score / $sh->appls_score_max) * 100) : 0);

        $document->setValue('TEST_RESULT', $sh->appls_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));

        $i = 1;
        $shC = $sh->applShContents;
        foreach ($shC as $k => $obj) {
            $document->setValue('num#' . $i, $i);
            $document->setValue('QUЕSTION_NUM#' . $i, $obj->applsc_trq_id);
            $document->setValue('QUESTION#' . $i, $obj->applsc_trq_question);
            $document->setValue('ANSWER#' . $i, $obj->applsc_tra_answer);
            $document->setValue('ANSWER_CORRECT#' . $i, $obj->applsc_tra_variant == 1 ? Yii::t('app', 'Correct Answer') : Yii::t('app', 'Incorrect Answer'));
            $i++;
        }
        $document->saveAs($file_name);

        $sh->appls_file_name = $sh->appls_date . '_' . $sh->appls_prs_id . '_' . $pattern->pat_fname;
        $sh->appls_file_data = file_get_contents($file_name);
        $sh->appls_pat_id = $pattern->pat_id;
        $sh->save();

        unlink($file_name);
        unlink($tmpl_name);

        return 0;

    }


    /**
     ************************************************************
     *  ApplShX - Ведомость промежуточного тестирования
     ************************************************************
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplShX($date)
    {
        foreach (ApplMain::find()
                     ->select(['applm_reestr', 'applm_trt_id', 'applm_svdt_id', 'applm_appls0_date'])
                     ->where(['applm_appls0x_date' => $date, 'applm_reestr'=>0])
                     ->groupBy(['applm_trt_id', 'applm_svdt_id', 'applm_appls0_date'])
                     ->all()
                 as $k1 => $m1)
        {

            $sheetX = null;
            if (
                !$sheetX = ApplShX::find()
                    ->where([
                        'applsx_date' => $date,
                        'applsx_trt_id' => $m1->applm_trt_id,
                        'applsx_svdt_id' => $m1->applm_svdt_id
                    ])
                    ->one()
                )
            {
                $sheetX = new ApplShX();
            }

            $sheetX->applsx_date = $date;
            $sheetX->applsx_trt_id = $m1->applm_trt_id;
            $sheetX->applsx_svdt_id = $m1->applm_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $m1->applm_svdt_id])
                ->andWhere(['patt_id' => 11])
                ->andWhere(['pat_trt_id' => $m1->applm_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplShX) not found'));
            }

            $sheetX->applsx_number = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->format('Ymd') .'-'. $pattern->pat_code; // .'-'. $sheetX->applsx_trt_id;
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            ApplShXContent::deleteAll(['applsxc_applsx_id' => $sheetX->applsx_id]);

            $shC = null;
            foreach (ApplSh::find()
                         ->where([
                             'appls_trt_id' => $sheetX->applsx_trt_id,
                             'appls_svdt_id' => $sheetX->applsx_svdt_id,
                             'appls_date' => $m1->applm_appls0_date
                         ])->all() as $key => $sheet) {


                if ($sheet->appls_reestr != 0){
                    continue;
                }

                $shC = new ApplShXContent();
                $shC->applsxc_applsx_id = $sheetX->applsx_id;

                $shC->applsxc_trp_id = $sheet->appls_trp_id;
                $shC->applsxc_score = $sheet->appls_score;
                $shC->applsxc_prs_id = $sheet->appls_prs_id;
                $shC->applsxc_passed = $sheet->appls_passed;


                $m2 = ApplMain::find()
                    ->select(['applm_applcmd_id'])
                    ->where(
                        [
                            'applm_reestr'=>0,
                            'applm_appls0x_date' => $date,
                            'applm_trt_id' => $m1->applm_trt_id,
                            'applm_svdt_id' => $m1->applm_svdt_id,
                            'applm_prs_id' => $sheet->appls_prs_id
                        ]
                    )->one();

                if (!$m2) {
                    throw new HttpException(500, Yii::t('app', "Не найдена запись в главной таблице date=$date, trt_id={$m1->applm_trt_id}, svdt_id={$m1->applm_svdt_id}, prs_id={$sheet->appls_prs_id}"));
                }
                $shC->applsxc_applcmd_id = $m2->applm_applcmd_id;

                if (!$shC->save()) {
                    return $this->render('create_appl_sheetx_cnt', [
                        'model' => $shC,
                    ]);
                }
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($sheetX->applShXContents));

            $document->setValue('DOC_NUMBER', $sheetX->applsx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($sheetX->applShXContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applsxcPrs->prs_full_name);
                $document->setValue('PROGRAMMA#' . $i, $obj->applsxcTrp->trp_name);
                $document->setValue('OUT_DATE#' . $i, $obj->applsxcApplsx->applsx_date);
                $document->setValue('TEST_CORRECT#' . $i, $obj->applsxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $document->setValue('PROGRAMMA_HOURS#' . $i, $obj->applsxcTrp->trp_hour);
                $document->setValue('IN_NUMBER#' . $i, $obj->applsxcApplcmd->applcmd_number ?? '');
                $i++;
            }
            $document->saveAs($file_name);

            if (count($sheetX->applShXContents) > 0) {
                $sheetX->applsx_file_name = $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $sheetX->applsx_svdt_id . '_' . $pattern->pat_fname;
                $sheetX->applsx_file_data = file_get_contents($file_name);
                $sheetX->applsx_pat_id = $pattern->pat_id;
            } else {
                $sheetX->applsx_file_name = null;
                $sheetX->applsx_file_data = null;
                $sheetX->applsx_pat_id = null;
            }
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

        }
        return 0;
    }


    /**
     ************************************************************
     *  ApplSheet - Протокол
     ************************************************************
     * @param integer $applm_id
     * @throws Exception
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */

    public function actionApplSheet($applm_id)
    {
        $main = ApplMain::find()->where(['applm_id' => $applm_id])->one();
        $this->createApplSheetId($main->applm_prs_id, $main->applm_appls_date, $main->applm_trp_id);
    }

    /**
     * Creates|Updates a new|old ApplSheet model.
     * @param integer $prs_id
     * @param string $date
     * @param integer $trp_id
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplSheetId($prs_id, $date, $trp_id)
    {
        srand();
        $main = ApplMain::find()->where(['applm_prs_id' => $prs_id, 'applm_appls_date' => $date, 'applm_trp_id' => $trp_id])->all();

        $sh = null;
        foreach ($main as $k => $obj) {
            if (!$sh = ApplSheet::find()->where(['appls_prs_id' => $prs_id, 'appls_date' => $date, 'appls_trp_id' => $trp_id])->one()) {
                $sh = new ApplSheet();
            }

            $sh->appls_date = $date;
            $sh->appls_prs_id = $obj->applm_prs_id;
            $sh->appls_trp_id = $obj->applm_trp_id;
            $sh->appls_trt_id = $obj->applm_trt_id;
            $sh->appls_svdt_id = $obj->applm_svdt_id;
            $sh->appls_reestr = $obj->applm_reestr;
            $sh->appls_passed = 1;
            $sh->appls_score = 0;
            $sh->appls_score_max = 0;
            $sh->appls_applm_id = $obj->applm_id;

            $appl_r_c = ApplRequestContent::findOne($obj->applm_applrc_id);
            if ($appl_r_c) {
                $sh->appls_applr_id = $appl_r_c->applrc_applr_id;
            }

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $sh->appls_svdt_id])
                ->andWhere(['patt_id' => 7])
                ->andWhere(['pat_trt_id' => $sh->appls_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', "createApplSheetId(prs_id=$prs_id, date=$date, trp_id=$trp_id: Pattern::find(svdt_id={$sh->appls_svdt_id}, patt_id=10, trt_id={$sh->appls_trt_id}}) not found"));
            }

            $sh->appls_number = \DateTime::createFromFormat('Y-m-d', $sh->appls_date)->format('Ymd') . '-' . $pattern->pat_code.'-'.$sh->appls_prs_id. '-N' .rand(1,999) .'' .rand(1,999);

            if (!$sh->save()) {
                return $this->render('create_appl_sheet', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }

            ApplSheetContent::deleteAll(['applsc_appls_id' => $sh->appls_id]);

            // Кол-во вопросов
            $question_count = 0;
            $prog = TrainingProg::findOne($sh->appls_trp_id);
            foreach ($prog->trainingProgModules as $km => $module) {
                // Сколько вопросов нужно задать с модуля
                $question_count += ($module->trplTrm->trm_test_question > count($module->trplTrm->trainingQuestions)) ? count($module->trplTrm->trainingQuestions) : $module->trplTrm->trm_test_question;
            }

            // Допустимое кол-во неправильных ответов
            $question_neg = $question_count - round($question_count/100.0 * 60.0 + 1.0);
            // Кол-во правильных ответов
            $question_pos = 0;

            foreach ($prog->trainingProgModules as $km => $mod) {
                $module = $mod->trplTrm;
                // Кол-во вопросов из модуля
                $training_question_count = ($module->trm_test_question > count($module->trainingQuestions)) ? count($module->trainingQuestions) : $module->trm_test_question;
                $train_ques_a = [];
                for ($i = 0; $i < $training_question_count; $i++) {
                    if (count($module->trainingQuestions) > 0) {
                        $r = rand(0, count($module->trainingQuestions) - 1);
                        while (in_array($r, $train_ques_a)) {
                            // Случайный выбор вопроса
                            $r = rand(0, count($module->trainingQuestions) - 1);
                        }
                        $train_ques_a[] = $r;
                    }
                }

                foreach ($train_ques_a as $item => $value) {

                    $question = $module->trainingQuestions[$value];

                    $answer = null;
                    // Правильный | Неправильный ответ - вероятность
                    $question_var = rand(1, 100) <= 80 ? 1 : 0;

                    if ($question_neg > 0) {
                        if ($question_var == 0) {
                            foreach ($question->trainingAnswers as $ka => $answer_) {
                                if ($answer_->tra_variant == 0) {
                                    $answer = $answer_;
                                    $question_neg--;
                                    break;
                                }
                            }
                        } else {
                            foreach ($question->trainingAnswers as $ka => $answer_) {
                                if ($answer_->tra_variant == 1) {
                                    $answer = $answer_;
                                    $question_pos++;
                                    break;
                                }
                            }
                        }
                    } else {
                        foreach ($question->trainingAnswers as $ka => $answer_) {
                            if ($answer_->tra_variant == 1) {
                                $answer = $answer_;
                                $question_pos++;
                                break;
                            }
                        }
                    }

                    if (is_null($answer))
                        continue;

                    $shC = new ApplSheetContent();
                    $shC->applsc_appls_id = $sh->appls_id;

                    $shC->applsc_trq_id = $question->trq_id;
                    $shC->applsc_trq_question = $question->trq_question;

                    $shC->applsc_tra_id = $answer->tra_id;
                    $shC->applsc_tra_answer = $answer->tra_answer;
                    $shC->applsc_tra_variant = $answer->tra_variant;

                    if (!$shC->save()) {
                        return $this->render('create_appl_sheet_cnt', [
                            'model' => $shC,
                        ]);
                    }
                }
            }
            $sh->appls_score_max = $question_count;
            $sh->appls_score = $question_pos;
            if ($question_count > 0) {
                $sh->appls_passed = ($question_count - $question_pos)/$question_count <= 0.4 ? 1 : 0;
            } else {
                $sh->appls_passed = 0;
            }

            if (!$sh->save()) {
                return $this->render('create_appl_sheet', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }
        }

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $sh->appls_svdt_id])
            ->andWhere(['patt_id' => 7])
            ->andWhere(['pat_trt_id' => $sh->appls_trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', "createApplSheetId0(prs_id=$prs_id, date=$date, trp_id=$trp_id: Pattern::find(svdt_id={$sh->appls_svdt_id}, patt_id=10, trt_id={$sh->appls_trt_id}}) not found"));
        }

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;

        $file_name = Yii::getAlias('@app') . '/storage/' . $sh->appls_date . '_' . $sh->appls_prs_id . '_' . $pattern->pat_fname;
        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $document->cloneRow('num', count($sh->applSheetContents));

        $document->setValue('DOC_NUMBER', $sh->appls_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $sh->appls_date)->getTimestamp();
        $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

        // Ф.И.О. Слушателя:
        $document->setValue('PERSONA_FIO', $sh->applsPrs->prs_full_name);
        // Программа обучения:
        $document->setValue('PROGRAMMA', $sh->applsTrp->trp_name);

        // Итого вопросов:
        $document->setValue('QUESTION_QTY', $sh->appls_score_max);
        //Итого верных ответов:
        $document->setValue('ANSWER_RIGHT_QTY', $sh->appls_score);
        //Результат тестирования:
        $document->setValue('ANSWER_RIGHT_PERCENT', $sh->appls_score_max > 0.0 ? round(($sh->appls_score / $sh->appls_score_max) * 100) : 0);

        $document->setValue('TEST_RESULT', $sh->appls_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));

        $i = 1;
        $shC = $sh->applSheetContents;
        foreach ($shC as $k => $obj) {
            $document->setValue('num#' . $i, $i);
            $document->setValue('QUЕSTION_NUM#' . $i, $obj->applsc_trq_id);
            $document->setValue('QUESTION#' . $i, $obj->applsc_trq_question);
            $document->setValue('ANSWER#' . $i, $obj->applsc_tra_answer);
            $document->setValue('ANSWER_CORRECT#' . $i, $obj->applsc_tra_variant == 1 ? Yii::t('app', 'Correct Answer') : Yii::t('app', 'Incorrect Answer'));
            $i++;
        }
        $document->saveAs($file_name);

        $sh->appls_file_name = $sh->appls_date . '_' . $sh->appls_prs_id . '_' . $pattern->pat_fname;
        $sh->appls_file_data = file_get_contents($file_name);
        $sh->appls_pat_id = $pattern->pat_id;
        $sh->save();

        unlink($file_name);
        unlink($tmpl_name);

        return 0;
    }

    /**
     ************************************************************
     *  ApplSheetX - Ведомость
     ************************************************************
     */

    /**
     * Creates|Updates a new|old ApplSheetX model.
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplSheetX($date)
    {
        foreach (ApplMain::find()->select(['applm_trt_id', 'applm_svdt_id', 'applm_appls_date', 'applm_reestr'])->where(['applm_applsx_date' => $date, 'applm_reestr'=>0])->groupBy(['applm_trt_id', 'applm_svdt_id', 'applm_appls_date'])->all() as $k1 => $m1)
        {

            if (!$sheetX = ApplSheetX::find()->where(['applsx_date' => $date, 'applsx_trt_id' => $m1->applm_trt_id, 'applsx_svdt_id' => $m1->applm_svdt_id])->one()) {
                $sheetX = new ApplSheetX();
            }

            $sheetX->applsx_date = $date;
            $sheetX->applsx_trt_id = $m1->applm_trt_id;
            $sheetX->applsx_svdt_id = $m1->applm_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $m1->applm_svdt_id])
                ->andWhere(['patt_id' => 8])
                ->andWhere(['pat_trt_id' => $m1->applm_trt_id])
                ->one();
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplSheetX) not found'));
            }

            $sheetX->applsx_number = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->format('Ymd') . '-' . $pattern->pat_code;

            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            ApplSheetXContent::deleteAll(['applsxc_applsx_id' => $sheetX->applsx_id]);

            foreach (ApplSheet::find()->where(['appls_trt_id' => $sheetX->applsx_trt_id, 'appls_svdt_id' => $sheetX->applsx_svdt_id, 'appls_date' => $sheetX->applsx_date])->all() as $key => $sheet) {

                if ($sheet->appls_reestr != 0){
                    continue;
                }

                $shC = new ApplSheetXContent();
                $shC->applsxc_applsx_id = $sheetX->applsx_id;

                $shC->applsxc_trp_id = $sheet->appls_trp_id;
                $shC->applsxc_score = $sheet->appls_score;
                $shC->applsxc_prs_id = $sheet->appls_prs_id;
                $shC->applsxc_passed = $sheet->appls_passed;

                $m2 = ApplMain::find()->where(['applm_reestr'=>0, 'applm_applsx_date' => $date, 'applm_trt_id' => $m1->applm_trt_id, 'applm_svdt_id' => $m1->applm_svdt_id, 'applm_prs_id' => $sheet->appls_prs_id])->one();

                if (!$m2) {
                    throw new HttpException(500, Yii::t('app', "Не найдена запись в главной таблице date=$date, trt_id={$m1->applm_trt_id}, svdt_id={$m1->applm_svdt_id}, prs_id={$sheet->appls_prs_id}"));
                }
// TODO Что-то где-то как-то
                $shC->applsxc_applcmd_id = $m2->applm_applcmd_id;
                $shC->applsxc_ab_id= $m2->applm_ab_id;
                $shC->applsxc_position = $m2->applm_position;

                if (!$shC->save()) {
                    return $this->render('create_appl_sheetx_cnt', [
                        'model' => $shC,
                    ]);
                }
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($sheetX->applSheetXContents));

            $document->setValue('DOC_NUMBER', $sheetX->applsx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($sheetX->applSheetXContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applsxcPrs->prs_full_name);
                $document->setValue('PROGRAMMA#' . $i, $obj->applsxcTrp->trp_name);
                $document->setValue('IN_NUMBER#' . $i, isset($obj->applsxcApplcmd) ? $obj->applsxcApplcmd->applcmd_number : '');
                $document->setValue('OUT_DATE#' . $i, $obj->applsxcApplsx->applsx_date);
                $document->setValue('TEST_CORRECT#' . $i, $obj->applsxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $i++;
            }
            $document->saveAs($file_name);

            if (count($sheetX->applSheetXContents) > 0) {
                $sheetX->applsx_file_name = $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
                $sheetX->applsx_file_data = file_get_contents($file_name);
                $sheetX->applsx_pat_id = $pattern->pat_id;
            } else {
                $sheetX->applsx_file_name = null;
                $sheetX->applsx_file_data = null;
                $sheetX->applsx_pat_id = null;
            }
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

            $r = $this->createApplXxx($sheetX->applsx_id);
            if ($r != 0) {
                return $r;
            }
            $r = $this->createApplEnd($sheetX->applsx_svdt_id, $sheetX->applsx_trt_id, $sheetX->applsx_date);
            if ($r != 0) {
                return $r;
            }
        }
        return 0;
    }

    /**
     * Creates|Updates a new|old ApplXxx model.
     * @param $applsx_id
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplXxx($applsx_id)
    {
        if ($sheet = ApplSheetX::findOne($applsx_id))
        {
            if (!$xxx = ApplXxx::find()->where(['applxxx_date' => $sheet->applsx_date, 'applxxx_trt_id' => $sheet->applsx_trt_id, 'applxxx_svdt_id' => $sheet->applsx_svdt_id])->one()) {
                $xxx = new ApplXxx();
            }
            $xxx->applxxx_date = $sheet->applsx_date;

            $xxx->applxxx_trt_id = $sheet->applsx_trt_id;
            $xxx->applxxx_svdt_id = $sheet->applsx_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $xxx->applxxx_svdt_id])
                ->andWhere(['patt_id' => 12])
                ->andWhere(['pat_trt_id' => $xxx->applxxx_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplXxx) not found'));
            }

            $xxx->applxxx_number = \DateTime::createFromFormat('Y-m-d', $xxx->applxxx_date)->format('Ymd') . '-' . $pattern->pat_code;

            if (!$xxx->save()) {
                return $this->render('create_appl_xxx', [
                    'model' => $xxx,
                ]);
            }

            ApplXxxContent::deleteAll(['applxxxc_applxxx_id' => $xxx->applxxx_id]);

            foreach (ApplSheetXContent::find()->where(['applsxc_applsx_id' => $sheet->applsx_id])->all() as $key => $sheetXContent) {

                $xxxC = new ApplXxxContent();
                $xxxC->applxxxc_applxxx_id = $xxx->applxxx_id;

                $xxxC->applxxxc_prs_id = $sheetXContent->applsxc_prs_id;
                $xxxC->applxxxc_trp_id = $sheetXContent->applsxc_trp_id;

                // TODO Что-то где-то как-то
                $xxxC->applxxxc_ab_id = $sheetXContent->applsxc_ab_id;
                $xxxC->applxxxc_position = $sheetXContent->applsxc_position;

                $xxxC->applxxxc_passed = $sheetXContent->applsxc_passed;

                if (!$xxxC->save()) {
                    return $this->render('create_appl_xxx_cnt', [
                        'model' => $xxxC,
                    ]);
                }
            }

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $xxx->applxxx_svdt_id])
                ->andWhere(['patt_id' => 12])
                ->andWhere(['pat_trt_id' => $xxx->applxxx_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplXxx0) not found'));
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $xxx->applxxx_date . '_' . $xxx->applxxx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($xxx->applXxxContents));

            $document->setValue('DOC_NUMBER', $xxx->applxxx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $xxx->applxxx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));


            $document->setValue('VED_ITOG_NUMBER', $sheet->applsx_number);
            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheet->applsx_date)->getTimestamp();
            $document->setValue('VED_ITOG_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('VED_ITOG_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('VED_ITOG_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($xxx->applXxxContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applxxxcPrs->prs_full_name);
                $document->setValue('PERSONA_DOLGNOST#' . $i, $obj->applxxxc_position);
                $document->setValue('PROGRAMMA#' . $i, $obj->applxxxcTrp->trp_name);
                $document->setValue('PERSONA_WORK#' . $i, isset($obj->applxxxcAb) ? $obj->applxxxcAb->entity->entEntt->entt_name_short .' '. $obj->applxxxcAb->entity->ent_name : '');
                $document->setValue('TEST_CORRECT#' . $i, $obj->applxxxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $i++;
            }
            $document->saveAs($file_name);

            if (count($xxx->applXxxContents) > 0) {
                $xxx->applxxx_file_name = $xxx->applxxx_date . '_' . $xxx->applxxx_trt_id . '_' . $pattern->pat_fname;
                $xxx->applxxx_file_data = file_get_contents($file_name);
                $xxx->applxxx_pat_id = $pattern->pat_id;
            } else {
                $xxx->applsx_file_name = null;
                $xxx->applsx_file_data = null;
                $xxx->applsx_pat_id = null;
            }
            if (!$xxx->save()) {
                return $this->render('create_appl_xxx', [
                    'model' => $xxx,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

        }
        return 0;
    }

    /**
     ************************************************************
     *  ApplEnd - Приказ о окончании                                                       *
     ************************************************************
     */

    /**
     * Creates|Updates a new|old ApplEnd model.
     * @param string $date
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \yii\db\Exception
     */
    public function createApplEndMain($date)
    {
        foreach (ApplMain::find()->select(['applm_trt_id', 'applm_svdt_id'])->where(['applsx_date' => $date])->groupBy(['applm_trt_id', 'applm_svdt_id'])->all() as $k1 => $m1)
        {

        }

        $main = ApplMain::find()->where(['applm_apple_date' => $date])->all();

        if (!$end = ApplEnd::find()->where(['apple_date' => $date])->one()) {
            $end = new ApplEnd();
        }
        $end->apple_date = $date;
        $end->apple_applsx_id = null;
        $end->apple_number = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->format('Ymd') . '-П';

        if (!$end->save()) {
            return $this->render('create_appl_end', [
                'model' => $end,
            ]);
        }

        foreach ($main as $k => $obj) {
            if (!$endC = ApplEndContent::find()
                ->where(['applec_apple_id' => $end->apple_id,
                    'applec_prs_id' => $obj->applm_prs_id,
                    'applec_trp_id' => $obj->applm_trp_id,
                ])->one()
            ) {
                $endC = new ApplEndContent();
            }
            $endC->applec_apple_id = $end->apple_id;
            $endC->applec_prs_id = $obj->applm_prs_id;
            $endC->applec_trp_id = $obj->applm_trp_id;
            if (!$endC->save()) {
                return $this->render('create_appl_end_cnt', [
                    'model' => $endC,
                ]);
            }
        }

        $pattern = Pattern::findOne(14);

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
        $file_name = Yii::getAlias('@app') . '/storage/' . $end->apple_date . '_' . $pattern->pat_fname;
        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $document->cloneRow('num', count($main));
        $i = 1;

        $document->setValue('DOC_NUMBER', $end->apple_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->getTimestamp();
        $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

        $endC = ApplEndContent::find()->where(['applec_apple_id' => $end->apple_id])->all();
        foreach ($endC as $k => $obj) {
            $document->setValue('num#' . $i, $i);
            $document->setValue('PERSONA_FIO#' . $i, $obj->applecPrs->prs_full_name);
            $document->setValue('PROGRAMMA#' . $i, $obj->applecTrp->trp_name);
            $document->setValue('OUT_DATE#' . $i, $end->apple_date);
            $document->setValue('OUT_DOC_NUMBER#' . $i, $obj->applec_number);
        }
        $document->saveAs($file_name);

        $end->apple_file_name = $end->apple_date . '_' . $pattern->pat_fname;
        $end->apple_file_data = file_get_contents($file_name);
        $end->apple_pat_id = $pattern->pat_id;
        $end->save();

        unlink($file_name);
        unlink($tmpl_name);

        return 0;
    }

    /**
     ************************************************************
     *  ApplEnd - Приказ о окончании
     ************************************************************
     */

    /**
     * Creates|Updates a new|old ApplEnd model.
     * @param integer $svdt_id
     * @param integer $trt_id
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function createApplEnd($svdt_id, $trt_id, $date)
    {
        if (!$sheetX = ApplSheetX::find()->where(['applsx_date' => $date, 'applsx_trt_id' => $trt_id, 'applsx_svdt_id' => $svdt_id])->one()) {
            return null;
        }

        if (!$end = ApplEnd::find()->where(['apple_date' => $date, 'apple_trt_id' =>  $trt_id, 'apple_svdt_id' =>  $svdt_id])->one()) {
            $end = new ApplEnd();
        }
        $end->apple_date = $date;
        $end->apple_trt_id = $trt_id;
        $end->apple_svdt_id = $svdt_id;
        $end->apple_applsx_id = $sheetX->applsx_id;

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $svdt_id])
            ->andWhere(['patt_id' => 9])
            ->andWhere(['pat_trt_id' => $trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplEnd) not found'));
        }

        $end->apple_number = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->format('Ymd') . '-'.$pattern->pat_code;

        if (!$end->save()) {
            return $this->render('create_appl_end', [
                'model' => $end,
            ]);
        }

        ApplEndContent::deleteAll(['applec_apple_id' => $end->apple_id]);

        foreach ($sheetX->applSheetXContents as $k => $obj) {
            $endC = new ApplEndContent();
            $endC->applec_apple_id = $end->apple_id;
            $endC->applec_prs_id = $obj->applsxc_prs_id;
            $endC->applec_trp_id = $obj->applsxc_trp_id;

            if ($main = ApplMain::find()->where(['applm_prs_id' => $endC->applec_prs_id, 'applm_trp_id' => $endC->applec_trp_id, 'applm_trt_id' => $trt_id, 'applm_apple_date' => $date, 'applm_reestr'=>0])->one()) {
                $endC->applec_number = $main->applm_number_upk;
            }

            if (!$endC->save()) {
                return $this->render('create_appl_end_cnt', [
                    'model' => $endC,
                ]);
            }
        }

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $svdt_id])
            ->andWhere(['patt_id' => 9])
            ->andWhere(['pat_trt_id' => $trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(createApplEnd0) not found'));
        }

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
        $file_name = Yii::getAlias('@app') . '/storage/' . $end->apple_date . '_' . $pattern->pat_fname;
        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $document->cloneRow('num', count($end->applEndContents));
        $document->setValue('DOC_NUMBER', $end->apple_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->getTimestamp();
        $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

        $document->setValue('VED_ITOG_NUMBER', $end->appleApplsx->applsx_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $end->appleApplsx->applsx_date)->getTimestamp();
        $document->setValue('VED_ITOG_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('VED_ITOG_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('VED_ITOG_DATE_YYYY', strftime('%Y', $dateVal));
        $i = 1;
        foreach ($end->applEndContents as $k => $obj) {
            $document->setValue('num#' . $i, $i);
            $document->setValue('PERSONA_FIO#' . $i, $obj->applecPrs->prs_full_name);
            $document->setValue('PROGRAMMA#' . $i, $obj->applecTrp->trp_name);
            $document->setValue('PROGRAMMA_HOURS#' . $i, $obj->applecTrp->trp_hour);
            $document->setValue('OUT_DATE#' . $i, $end->apple_date);
            $document->setValue('OUT_DOC_NUMBER#' . $i, $obj->applec_number);
            $i++;
        }
        $document->saveAs($file_name);

        $end->apple_file_name = $end->apple_date . '_' . $pattern->pat_fname;
        $end->apple_file_data = file_get_contents($file_name);
        $end->apple_pat_id = $pattern->pat_id;
        $end->save();

        unlink($file_name);
        unlink($tmpl_name);

        return 0;
    }


     /**
     ************************************************************
     *  ApplMain - All
     ************************************************************
     */

    /**
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreateApplAll()
    {
        $endDate = '2020-01-01';
        $dateIsInvalidForThisGenerationMessage = null;

        $applmArray = [];
        $model = DynamicModel::validateData(['date1', 'date2', 'obj1', 'obj2', 'obj3', 'obj4'],
            [
                [['date1', 'date2'], 'string'],
                [['obj1', 'obj2', 'obj3', 'obj4'], 'integer'],
            ]
        );

        $model->date1 = '2017-01-01';
        $model->date2 = '2019-12-31';
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $date1 = $model->date1;
            $date2 = $model->date2;

            $dateMoreOrEqualThen2020 = (new DateTime($date2)) >= (new DateTime($endDate));

            Yii::$app->session['ApplGenAll/date1'] = $date1;
            Yii::$app->session['ApplGenAll/date2'] = $date2;

            if (Yii::$app->request->post('delete') == 'delete') {

                $data = ApplEnd::find()->where(['between', 'apple_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSh::find()->where(['between', 'appls_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplShX::find()->where(['between', 'applsx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSheet::find()->where(['between', 'appls_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSheetX::find()->where(['between', 'applsx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
//                $data = ApplRequest::find()->where(['between', 'applr_date', $date1, $date2])->all();
//                foreach ($data as $obj) {
//                    $obj->delete();
//                }
                $data = ApplXxx::find()->where(['between', 'applxxx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplCommand::find()->where(['between', 'applcmd_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplFinal::find()->where(['between', 'applf_end_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
            }


            if (Yii::$app->request->post('update') == 'update' && !$dateMoreOrEqualThen2020) {
                //echo "Приказ о зачислении";
                $main = ApplMain::find()->select(['applm_applcmd_date', 'applm_id', 'applm_reestr'])->where(['between', 'applm_applcmd_date', $date1, $date2])->andWhere(['applm_reestr'=>0])->groupBy('applm_applcmd_date')->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    //$r =
                    try{
                        $this->createApplCommand($obj->applm_applcmd_date);
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Приказ о зачислении';
                    }
                    //if ($r !== 0) {
                    //    return $r;
                    //}
                }

                //echo "Протокол промежуточного тестирования";
                $main = ApplMain::find()
                    ->select(['applm_appls0_date', 'applm_prs_id', 'applm_trp_id', 'applm_id'])
                    ->where(['between', 'applm_appls0_date', $date1, $date2])
                    ->groupBy(['applm_appls0_date', 'applm_prs_id', 'applm_trp_id'])
                    ->all();
                foreach ($main as $k => $obj) {
                    try{
                        $r = $this->createApplShId($obj->applm_prs_id, $obj->applm_appls0_date, $obj->applm_trp_id);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Протокол промежуточного тестирования';
                    }
                }

                //echo "Ведомость промежуточного тестирования";
                $main = ApplMain::find()
                    ->select(['applm_appls0x_date', 'applm_id', 'applm_reestr'])
                    ->where(['between', 'applm_appls0x_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->groupBy(['applm_appls0x_date'])
                    ->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    try{
                        $r = $this->createApplShX($obj->applm_appls0x_date);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch(\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Ведомость промежуточного тестирования';
                    }
                }

                //echo "Протокол";
                $main = ApplMain::find()
                    ->select(['applm_appls_date', 'applm_prs_id', 'applm_trp_id', 'applm_id'])
                    ->where(['between', 'applm_appls_date', $date1, $date2])
                    ->groupBy(['applm_appls_date', 'applm_prs_id', 'applm_trp_id'])->all();
                foreach ($main as $k => $obj) {
                    try{
                        $r = $this->createApplSheetId($obj->applm_prs_id, $obj->applm_appls_date, $obj->applm_trp_id);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Протокол';
                    }
                }

                //echo "Ведомость";
                $main = ApplMain::find()
                    ->select(['applm_applsx_date', 'applm_id', 'applm_reestr'])
                    ->where(['between', 'applm_applsx_date', $date1, $date2])
//                    ->andWhere(['applm_reestr'=>0])
                    ->groupBy(['applm_applsx_date'])
                    ->all();
                foreach ($main as $k => $obj) {
//                    if ($obj->applm_reestr != 0){
//                        continue;
//                    }
                    try{
                        $r = $this->createApplSheetX($obj->applm_applsx_date);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Ведомость';
                    }
                }

                //echo "Приказ о окончании";
                $main = ApplMain::find()
                    ->select(['applm_apple_date', 'applm_svdt_id', 'applm_trt_id', 'applm_id', 'applm_reestr'])
                    ->where(['between', 'applm_apple_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->groupBy(['applm_apple_date', 'applm_svdt_id', 'applm_trt_id'])
                    ->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    //$this->actionCreateApplEndMain($obj->applm_apple_date);
                    try{
                        $r = $this->createApplEnd($obj->applm_svdt_id, $obj->applm_trt_id, $obj->applm_apple_date);
                        if ($r !== 0 && $r !== null) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id. 'Приказ об окончании';
                    }
                }

                //echo "итоговый документ";
                $main = ApplMain::find()
                    ->select(['applm_id'])
                    ->where(['between', 'applm_date_upk', $date1, $date2])
//                    ->groupBy(['applm_date_upk', 'applm_svdt_id', 'applm_trt_id'])
                    ->all();

                foreach ($main as $k => $obj) {
                    try{
                        $applfId = $this->createApplFinal($obj->applm_id);
                        if(!empty($applfId)){
                            $applFinalFile = new ApplFinalFile();
                            $applFinalFile->createFinalDocumentFile($applfId);
                        }
                    } catch(\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Итоговый документ';
                    }
                }
            }
        }

        if (!empty($dateMoreOrEqualThen2020)) {
            $dateIsInvalidForThisGenerationMessage = 'Вы не можете выбрать дату больше 2019-12-31';
        }

        if (isset(Yii::$app->session['ApplGenAll/date1'])) {
            $model->date1 = Yii::$app->session['ApplGenAll/date1'];
        }
        if (isset(Yii::$app->session['ApplGenAll/date2'])) {
            $model->date2 = Yii::$app->session['ApplGenAll/date2'];
        }


        $model->obj1 = ApplCommand::find()->where(['between', 'applcmd_date', $model->date1, $model->date2])->count();
        $model->obj2 = ApplSheet::find()->where(['between', 'appls_date', $model->date1, $model->date2])->count();
        $model->obj3 = ApplSheetX::find()->where(['between', 'applsx_date', $model->date1, $model->date2])->count();
        $model->obj4 = ApplEnd::find()->where(['between', 'apple_date', $model->date1, $model->date2])->count();
        return $this->render('create_appl_all', [
            'model' => $model,
            'applmArray' => $applmArray,
            'headTextGen20' => 'Генерация документов для 2019гг. и раньше',
            'dateIsInvalidForThisGenerationMessage' => $dateIsInvalidForThisGenerationMessage,
        ]);

    }

    /**
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreateApplAll20()
    {
        $startDate = '2020-01-01';
        $dateIsInvalidForThisGenerationMessage = null;
        $dateMoreOrEqualThen2020 = true;

        $applmArray = [];
        $model = DynamicModel::validateData(['date1', 'date2', 'obj1', 'obj2', 'obj3', 'obj4'],
            [
                [['date1', 'date2'], 'string'],
                [['obj1', 'obj2', 'obj3', 'obj4'], 'integer'],
            ]
        );

        $model->date1 = $startDate;
        $model->date2 = '2020-12-31';
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $date1 = $model->date1;
            $date2 = $model->date2;

            $dateMoreOrEqualThen2020 = (new DateTime($date1)) >= (new DateTime($startDate));

            Yii::$app->session['ApplGenAll20/date1'] = $date1;
            Yii::$app->session['ApplGenAll20/date2'] = $date2;

            if (Yii::$app->request->post('delete') == 'delete') {

                $data = ApplEnd::find()->where(['between', 'apple_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSh::find()->where(['between', 'appls_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplShX::find()->where(['between', 'applsx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSheet::find()->where(['between', 'appls_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplSheetX::find()->where(['between', 'applsx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }

                $data = ApplXxx::find()->where(['between', 'applxxx_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplCommand::find()->where(['between', 'applcmd_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
                $data = ApplFinal::find()->where(['between', 'applf_end_date', $date1, $date2])->all();
                foreach ($data as $obj) {
                    $obj->delete();
                }
            }


            if (Yii::$app->request->post('update') == 'update' && $dateMoreOrEqualThen2020) {
                //echo "Приказ о зачислении";
                $cmdRequests = $this->requestsContainsBothReestrsForDatesByType($date1, $date2, 'applm_applcmd_date');
                $cmdIntersect = isset($cmdRequests['intersect']) && !empty($cmdRequests['intersect']);

                $command = ApplMain::find()
                    ->select(['applm_applcmd_date', 'applm_id', 'applm_reestr'])
                    ->innerJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                    ->where(['between', 'applm_applcmd_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->andWhere(['trp_reestr'=>0])
                    ->groupBy('applm_applcmd_date');
                    if ($cmdIntersect) {
                        $command->innerJoin(ApplRequestContent::tableName(), 'applm_applrc_id = applrc_id');
                        $command->andWhere(['not in', 'applrc_applr_id', $cmdRequests['intersect']]);

                        foreach ($cmdRequests['intersect'] as $intersect) {
                            $applmArray[] = $intersect.' заявка не вошла в генерацию ПРИКАЗА О ЗАЧИСЛЕНИИ по причине содержания и основного и дополнительного реестра';
                        }
                    }
                    $main = $command->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    try{
                        $this->createApplCommand($obj->applm_applcmd_date);
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Приказ о зачислении';
                    }
                }

                //echo "Протокол промежуточного тестирования";
                $main = ApplMain::find()
                    ->select(['applm_appls0_date', 'applm_prs_id', 'applm_trp_id', 'applm_id'])
                    ->where(['between', 'applm_appls0_date', $date1, $date2])
                    ->groupBy(['applm_appls0_date', 'applm_prs_id', 'applm_trp_id'])
                    ->all();
                foreach ($main as $k => $obj) {
                    try{
                        $r = $this->createApplShId($obj->applm_prs_id, $obj->applm_appls0_date, $obj->applm_trp_id);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Протокол промежуточного тестирования';
                    }
                }

                //echo "Ведомость промежуточного тестирования";
                $applshxRequests = $this->requestsContainsBothReestrsForDatesByType($date1, $date2, 'applm_appls0x_date');
                $applshxIntersect = isset($applshxRequests['intersect']) && !empty($applshxRequests['intersect']);

                $command = ApplMain::find()
                    ->select(['applm_appls0x_date', 'applm_id', 'applm_reestr'])
                    ->innerJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                    ->where(['between', 'applm_appls0x_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->andWhere(['trp_reestr'=>0])
                    ->groupBy(['applm_appls0x_date']);
                    if ($applshxIntersect) {
                        $command->innerJoin(ApplRequestContent::tableName(), 'applm_applrc_id = applrc_id');
                        $command->andWhere(['not in', 'applrc_applr_id', $applshxRequests['intersect']]);

                        foreach ($applshxRequests['intersect'] as $intersect) {
                            $applmArray[] = $intersect.' заявка не вошла в генерацию ВЕДОМОСТЕЙ ПРОМЕЖУТОЧНОГО ТЕСТИРОВАНИЯ по причине содержания и основного и дополнительного реестра';
                        }
                    }
                $main = $command->all();

                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    try{
                        $r = $this->createApplShX($obj->applm_appls0x_date);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch(\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Ведомость промежуточного тестирования';
                    }
                }

                //echo "Протокол";
                $main = ApplMain::find()
                    ->select(['applm_appls_date', 'applm_prs_id', 'applm_trp_id', 'applm_id'])
                    ->where(['between', 'applm_appls_date', $date1, $date2])
                    ->groupBy(['applm_appls_date', 'applm_prs_id', 'applm_trp_id'])->all();
                foreach ($main as $k => $obj) {
                    try{
                        $r = $this->createApplSheetId($obj->applm_prs_id, $obj->applm_appls_date, $obj->applm_trp_id);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Протокол';
                    }
                }

                //echo "Ведомость";
                $applsxRequests = $this->requestsContainsBothReestrsForDatesByType($date1, $date2, 'applm_applsx_date');
                $applsxIntersect = isset($applsxRequests['intersect']) && !empty($applsxRequests['intersect']);

                $command = ApplMain::find()
                    ->select(['applm_applsx_date', 'applm_id', 'applm_reestr'])
                    ->innerJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                    ->where(['between', 'applm_applsx_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->andWhere(['trp_reestr'=>0])
                    ->groupBy(['applm_applsx_date']);
                if ($applsxIntersect) {
                    $command->innerJoin(ApplRequestContent::tableName(), 'applm_applrc_id = applrc_id');
                    $command->andWhere(['not in', 'applrc_applr_id', $applsxRequests['intersect']]);

                    foreach ($applsxRequests['intersect'] as $intersect) {
                        $applmArray[] = $intersect.' заявка не вошла в генерацию ВЕДОМОСТЕЙ ИТОГОВОГО ТЕСТИРОВАНИЯ по причине содержания и основного и дополнительного реестра';
                    }
                }
                $main = $command->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }
                    try{
                        $r = $this->createApplSheetX($obj->applm_applsx_date);
                        if ($r !== 0) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Ведомость';
                    }
                }

                //echo "Приказ об окончании";
                $applERequests = $this->requestsContainsBothReestrsForDatesByType($date1, $date2, 'applm_apple_date');
                $applEIntersect = isset($applERequests['intersect']) && !empty($applERequests['intersect']);

                $command = ApplMain::find()
                    ->select(['applm_apple_date', 'applm_svdt_id', 'applm_trt_id', 'applm_id', 'applm_reestr'])
                    ->innerJoin(TrainingProg::tableName(), 'applm_trp_id = trp_id')
                    ->where(['between', 'applm_apple_date', $date1, $date2])
                    ->andWhere(['applm_reestr'=>0])
                    ->andWhere(['trp_reestr'=>0])
                    ->groupBy(['applm_apple_date', 'applm_svdt_id', 'applm_trt_id']);
                if ($applEIntersect) {
                    $command->innerJoin(ApplRequestContent::tableName(), 'applm_applrc_id = applrc_id');
                    $command->andWhere(['not in', 'applrc_applr_id', $applERequests['intersect']]);

                    foreach ($applERequests['intersect'] as $intersect) {
                        $applmArray[] = $intersect.' заявка не вошла в генерацию ПРИКАЗОВ ОБ ОКОНЧАНИИ по причине содержания и основного и дополнительного реестра';
                    }
                }
                $main = $command->all();
                foreach ($main as $k => $obj) {
                    if ($obj->applm_reestr != 0){
                        continue;
                    }

                     try{
                        $r = $this->createApplEnd($obj->applm_svdt_id, $obj->applm_trt_id, $obj->applm_apple_date);
                        if ($r !== 0 && $r !== null) {
                            return $r;
                        }
                    } catch (\Exception $exception){
                        $applmArray[] = $obj->applm_id. 'Приказ об окончании';
                    }
                }

                //echo "итоговый документ";
                $main = ApplMain::find()
                    ->select(['applm_id'])
                    ->where(['between', 'applm_date_upk', $date1, $date2])
//                    ->groupBy(['applm_date_upk', 'applm_svdt_id', 'applm_trt_id'])
                    ->all();

                foreach ($main as $k => $obj) {
                    try{
                        $applfId = $this->createApplFinal20($obj->applm_id);
                        if(!empty($applfId)){
                            $applFinalFile = new ApplFinalFile();
                            $applFinalFile->createFinalDocumentFile($applfId);
                        }
                    } catch(\Exception $exception){
                        $applmArray[] = $obj->applm_id.' Итоговый документ';
                    }
                }
            }
        }

        if (empty($dateMoreOrEqualThen2020)) {
            $dateIsInvalidForThisGenerationMessage = 'Вы не можете выбрать дату меньше 2020-01-01';
        }

        if (isset(Yii::$app->session['ApplGenAll20/date1'])) {
            $model->date1 = Yii::$app->session['ApplGenAll20/date1'];
        }
        if (isset(Yii::$app->session['ApplGenAll20/date2'])) {
            $model->date2 = Yii::$app->session['ApplGenAll20/date2'];
        }


        $model->obj1 = ApplCommand::find()->where(['between', 'applcmd_date', $model->date1, $model->date2])->count();
        $model->obj2 = ApplSheet::find()->where(['between', 'appls_date', $model->date1, $model->date2])->count();
        $model->obj3 = ApplSheetX::find()->where(['between', 'applsx_date', $model->date1, $model->date2])->count();
        $model->obj4 = ApplEnd::find()->where(['between', 'apple_date', $model->date1, $model->date2])->count();
        return $this->render('create_appl_all', [
            'model' => $model,
            'applmArray' => $applmArray,
            'headTextGen20' => 'Генерация документов для 2020гг. и позже',
            'dateIsInvalidForThisGenerationMessage' => $dateIsInvalidForThisGenerationMessage,
        ]);

    }

    private function requestsContainsBothReestrsForDatesByType($dateStart, $dateEnd, $dateColumn) {

        //get requests with not main reestr from date
        $command = Yii::$app->db->createCommand('
        select ar.applr_id
            from appl_main a
            left join appl_request_content arc on (a.applm_applrc_id = arc.applrc_id)
            left join training_prog tp on (arc.applrc_trp_id = tp.trp_id)
            left join appl_request ar on (arc.applrc_applr_id = ar.applr_id)
            where tp.trp_reestr=1
              and
                  ar.applr_reestr = 0 and
                  a.'.$dateColumn.'>=\''.$dateStart.'\' and a.'.$dateColumn.'<=\''.$dateEnd.'\'
            group by ar.applr_id;
        ');
        $requestsNotMainReesters = $command->queryColumn();

        //get requests with main reestr from date
        $command = Yii::$app->db->createCommand('
        select ar.applr_id
            from appl_main a
            left join appl_request_content arc on (a.applm_applrc_id = arc.applrc_id)
            left join training_prog tp on (arc.applrc_trp_id = tp.trp_id)
            left join appl_request ar on (arc.applrc_applr_id = ar.applr_id)
            where tp.trp_reestr=0
              and
                  ar.applr_reestr = 0 and
                  a.'.$dateColumn.'>=\''.$dateStart.'\' and a.'.$dateColumn.'<=\''.$dateEnd.'\'
            group by ar.applr_id;
        ');
        $requestsMainReesters = $command->queryColumn();


        $requests['main'] = $requestsMainReesters;
        $requests['notMain'] = $requestsNotMainReesters;
        $requests['intersect'] = array_intersect($requestsNotMainReesters, $requestsMainReesters);;
        $requests['intersect'] = array_unique($requests['intersect']);

        $requests['main'] = array_diff($requests['main'], $requests['intersect']);
        $requests['notMain'] = array_diff($requests['notMain'], $requests['intersect']);

        //get requests with main reestr but Maybe have notMain as well
        if (count($requests['main']) > 0) {
            $command = Yii::$app->db->createCommand('
                    select ar.applr_id
                    from appl_request_content arc
                    left join appl_request ar on (arc.applrc_applr_id = ar.applr_id)
                    left join training_prog tp on (arc.applrc_trp_id = tp.trp_id)
                    where 
                          tp.trp_reestr=1 and
                          ar.applr_reestr = 0 and
                          ar.applr_id IN ('.implode(',', $requests['main']).') 
                    group by ar.applr_id;
            ');
            $excludedMainRequests = $command->queryColumn();

            foreach($excludedMainRequests as $excludedMainRequest) {
                $requests['intersect'][] = $excludedMainRequest;
            }
        }

        //get requests with notMain reestr but Maybe have main as well
        if (count($requests['notMain']) > 0) {
            $command = Yii::$app->db->createCommand('
                    select ar.applr_id
                    from appl_request_content arc
                    left join appl_request ar on (arc.applrc_applr_id = ar.applr_id)
                    left join training_prog tp on (arc.applrc_trp_id = tp.trp_id)
                    where 
                          tp.trp_reestr=0 and 
                          ar.applr_reestr = 0 and
                          ar.applr_id IN ('.implode(',', $requests['notMain']).') 
                    group by ar.applr_id;
            ');
            $excludedNotMainRequests = $command->queryColumn();

            foreach($excludedNotMainRequests as $excludedNotMainRequest) {
                $requests['intersect'][] = $excludedNotMainRequest;
            }
        }

        return $requests;
    }

    /**
     ************************************************************
     *  Generate Doc
     ************************************************************
     */

    private function createApplFinal ($applmId) {

//        if ($appl = ApplRequestContent::findOne($applrc_id)) {
        if ($main = ApplMain::find()->where(['applm_id' => $applmId])->one()) {
            $applf = ApplFinal::find()->where(['applf_prs_id' => $main->applm_prs_id, 'applf_number' => $main->applm_number_upk])->one();
            $person = Person::find()->where(['prs_id' => $main->applm_prs_id])->one();
            $training = TrainingProg::find()->where(['trp_id' => $main->applm_trp_id])->one();

            if ($applf == NULL) {
                $model = new ApplFinal();
                $model->applf_prs_id = $person->prs_id;
                $model->applf_name_first = $person->prs_first_name;
                $model->applf_name_last = $person->prs_last_name;
                $model->applf_name_middle = $person->prs_middle_name;
                $model->applf_name_full = $person->prs_full_name;
                $model->applf_end_date = $main->applm_date_upk;
                $model->applf_svdt_id = $main->applm_svdt_id;
                $model->applf_trt_id = $main->applm_trt_id;
                $model->applf_trp_id = $main->applm_trp_id;
                $model->applf_reestr = $main->applm_reestr;
                $model->applf_trp_hour = $training->trp_hour; //$main->applrcTrp->trp_hour;
                $model->applf_cmd_date = $main->applm_applcmd_date ?? null;
                $model->applf_number = $main->applm_number_upk ?? null;
                $model->applf_applm_id = $main->applm_id;
//                    $model->applf_applr_id = $appl->applrc_applr_id;

                if ($model->save()) {
//                        $appl->applrc_applf_id = $model->applf_id;
//                        $appl->save(false);
                    return $model->applf_id;
                }
            } else {
//                return null;
                return $applf->applf_id;
            }
        }
//        }
    }

    private function createApplFinal20 ($applmId) {

        if ($main = ApplMain::find()->where(['applm_id' => $applmId])->one()) {
            if ($appl = ApplRequestContent::findOne($main->applm_applrc_id)) {
                $applf = ApplFinal::find()->where(['applf_prs_id' => $main->applm_prs_id, 'applf_number' => $main->applm_number_upk])->one();
//                $person = Person::find()->where(['prs_id' => $main->applm_prs_id])->one();
//                $training = TrainingProg::find()->where(['trp_id' => $main->applm_trp_id])->one();

                if ($applf == NULL) {
                    $model = new ApplFinal();
                    $model->applf_prs_id = $appl->applrcPrs->prs_id;
                    $model->applf_name_first = $appl->applrcPrs->prs_first_name;
                    $model->applf_name_last = $appl->applrcPrs->prs_last_name;
                    $model->applf_name_middle = $appl->applrcPrs->prs_middle_name;
                    $model->applf_name_full = $appl->applrcPrs->prs_full_name;
                    $model->applf_end_date = $appl->applrc_date_upk;
                    $model->applf_svdt_id = $appl->applrcSvc->svc_svdt_id;
                    $model->applf_trt_id = $appl->applrcApplr->applr_trt_id;
                    $model->applf_trp_id = $appl->applrc_trp_id;
                    $model->applf_trp_hour = $appl->applrcTrp->trp_hour;
                    $model->applf_reestr = $main->applm_reestr;
                    $model->applf_cmd_date = $main->applm_applcmd_date ?? null;
                    $model->applf_number = $main->applm_number_upk ?? null;
                    $model->applf_applr_id = $appl->applrc_applr_id;
                    $model->applf_applm_id = $main->applm_id;

                    if ($model->save()) {
                        $appl->applrc_applf_id = $model->applf_id;
                        $appl->save(false);
                        return $model->applf_id;
                    }
                } else {
                    return $applf->applf_id;
                }
            }
        }
    }


    /**
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreateAppl()
    {
        $model = DynamicModel::validateData(['date1', 'date2'],
            [
                [['date1', 'date2'], 'string'],
            ]
        );

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $date1 = $model->date1;
            $date2 = $model->date2;
            Yii::$app->session['ApplGen/date1'] = $date1;
            Yii::$app->session['ApplGen/date2'] = $date2;

            //echo "Ведомость промежуточного тестирования";
            $main = ApplSh::find()
                ->select(['appls_date'])
                ->where(['between', 'appls_date', $date1, $date2])
                ->groupBy(['appls_date'])
                ->all();
            foreach ($main as $k => $obj) {
                $r = $this->generateApplShX($obj->appls_date);
                if ($r !== 0) {
                    return $r;
                }
            }

            //echo "Ведомость";
            $main = ApplSheet::find()
                ->select(['appls_date'])
                ->where(['between', 'appls_date', $date1, $date2])
                ->groupBy(['appls_date'])
                ->all();
            foreach ($main as $k => $obj) {
                $r = $this->generateApplSheetX($obj->appls_date);
                if ($r !== 0) {
                    return $r;
                }
            }

            //echo "Приказ о окончании";
            $main = ApplSheetX::find()
                ->select(['applsx_date', 'applsx_svdt_id','applsx_trt_id'])
                ->where(['between', 'applsx_date', $date1, $date2])
                ->groupBy(['applsx_date', 'applsx_svdt_id','applsx_trt_id'])
                ->all();
            foreach ($main as $k => $obj) {
                $r = $this->generateApplEnd($obj->applsx_svdt_id, $obj->applsx_trt_id, $obj->applsx_date);
                if ($r !== 0) {
                    return $r;
                }
                $r = $this->generateApplXxx($obj->applsx_date);
                if ($r !== 0) {
                    return $r;
                }
            }
        }

        if (isset(Yii::$app->session['ApplGen/date1'])) {
            $model->date1 = Yii::$app->session['ApplGen/date1'];
        }
        if (isset(Yii::$app->session['ApplGen/date2'])) {
            $model->date2 = Yii::$app->session['ApplGen/date2'];
        }

        return $this->render('create_appl', [
            'model' => $model,
        ]);

    }


    /**
     ************************************************************
     *  ApplShX - Ведомость промежуточного тестирования
     ************************************************************
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateApplShX($date)
    {
        foreach (ApplSh::find()
                     ->select(['appls_trt_id', 'appls_svdt_id', 'appls_date'])
                     ->where(['appls_date' => $date])
                     ->groupBy(['appls_trt_id', 'appls_svdt_id', 'appls_date'])
                     ->all()
                 as $k1 => $sh1)
        {
            $sheetX = null;
            if (
            !$sheetX = ApplShX::find()
                ->where([
                    'applsx_date' => $date,
                    'applsx_trt_id' => $sh1->appls_trt_id,
                    'applsx_svdt_id' => $sh1->appls_svdt_id
                ])
                ->one()
            )
            {
                $sheetX = new ApplShX();
            }

            $sheetX->applsx_date = $date;
            $sheetX->applsx_trt_id = $sh1->appls_trt_id;
            $sheetX->applsx_svdt_id = $sh1->appls_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $sh1->appls_svdt_id])
                ->andWhere(['patt_id' => 11])
                ->andWhere(['pat_trt_id' => $sh1->appls_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplShX) not found'));
            }

            $sheetX->applsx_number = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->format('Ymd') .'-'. $pattern->pat_code;
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            ApplShXContent::deleteAll(['applsxc_applsx_id' => $sheetX->applsx_id]);

            $shC = null;
            foreach (ApplSh::find()
                         ->where([
                             'appls_trt_id' => $sheetX->applsx_trt_id,
                             'appls_svdt_id' => $sheetX->applsx_svdt_id,
                             'appls_date' => $sh1->appls_date
                         ])->all() as $key => $sheet) {

                $shC = new ApplShXContent();
                $shC->applsxc_applsx_id = $sheetX->applsx_id;

                $shC->applsxc_trp_id = $sheet->appls_trp_id;
                $shC->applsxc_score = $sheet->appls_score;
                $shC->applsxc_prs_id = $sheet->appls_prs_id;
                $shC->applsxc_passed = $sheet->appls_passed;


                $m2 = ApplCommand::find()
                    ->select(['applcmd_id'])
                    ->innerJoin(ApplCommandContent::tableName(), 'applcmdc_applcmd_id = applcmd_id')
                    ->where(
                        [
                            'applcmd_trt_id' => $sh1->appls_trt_id,
                            'applcmd_svdt_id' => $sh1->appls_svdt_id,
                            'applcmdc_prs_id' => $sheet->appls_prs_id
                        ]
                    )
                    ->orderBy('applcmd_date desc')
                    ->one();

                if (!$m2) {
                    throw new HttpException(500, Yii::t('app', "Не найдена запись в приказах date=$date, trt_id={$sh1->appls_trt_id}, svdt_id={$sh1->appls_svdt_id}, prs_id={$sheet->appls_prs_id}"));
                }
                $shC->applsxc_applcmd_id = $m2->applcmd_id;

                if (!$shC->save()) {
                    return $this->render('create_appl_sheetx_cnt', [
                        'model' => $shC,
                    ]);
                }
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($sheetX->applShXContents));

            $document->setValue('DOC_NUMBER', $sheetX->applsx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($sheetX->applShXContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applsxcPrs->prs_full_name);
                $document->setValue('PROGRAMMA#' . $i, $obj->applsxcTrp->trp_name);
                $document->setValue('OUT_DATE#' . $i, $obj->applsxcApplsx->applsx_date);
                $document->setValue('TEST_CORRECT#' . $i, $obj->applsxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $document->setValue('PROGRAMMA_HOURS#' . $i, $obj->applsxcTrp->trp_hour);
                $document->setValue('IN_NUMBER#' . $i, $obj->applsxcApplcmd->applcmd_number);
                $i++;
            }
            $document->saveAs($file_name);

            if (count($sheetX->applShXContents) > 0) {
                $sheetX->applsx_file_name = $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $sheetX->applsx_svdt_id . '_' . $pattern->pat_fname;
                $sheetX->applsx_file_data = file_get_contents($file_name);
                $sheetX->applsx_pat_id = $pattern->pat_id;
            } else {
                $sheetX->applsx_file_name = null;
                $sheetX->applsx_file_data = null;
                $sheetX->applsx_pat_id = null;
            }
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

        }
        return 0;
    }


    /**
     ************************************************************
     *  ApplSheetX - Ведомость
     ************************************************************
     */

    /**
     * Creates|Updates a new|old ApplSheetX model.
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateApplSheetX($date)
    {
        foreach (ApplSheet::find()->select(['appls_trt_id', 'appls_svdt_id', 'appls_date'])->where(['appls_date' => $date])->groupBy(['appls_trt_id', 'appls_svdt_id', 'appls_date'])->all() as $k1 => $sh1)
        {
            if (!$sheetX = ApplSheetX::find()->where(['applsx_date' => $date, 'applsx_trt_id' => $sh1->appls_trt_id, 'applsx_svdt_id' => $sh1->appls_svdt_id])->one()) {
                $sheetX = new ApplSheetX();
            }

            $sheetX->applsx_date = $date;
            $sheetX->applsx_trt_id = $sh1->appls_trt_id;
            $sheetX->applsx_svdt_id = $sh1->appls_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $sh1->appls_svdt_id])
                ->andWhere(['patt_id' => 8])
                ->andWhere(['pat_trt_id' => $sh1->appls_trt_id])
                ->one();
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplSheetX) not found'));
            }

            $sheetX->applsx_number = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->format('Ymd') . '-' . $pattern->pat_code;

            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            ApplSheetXContent::deleteAll(['applsxc_applsx_id' => $sheetX->applsx_id]);

            foreach (ApplSheet::find()->where(['appls_trt_id' => $sheetX->applsx_trt_id, 'appls_svdt_id' => $sheetX->applsx_svdt_id, 'appls_date' => $sheetX->applsx_date])->all() as $key => $sheet) {

                $shC = new ApplSheetXContent();
                $shC->applsxc_applsx_id = $sheetX->applsx_id;

                $shC->applsxc_trp_id = $sheet->appls_trp_id;
                $shC->applsxc_score = $sheet->appls_score;
                $shC->applsxc_prs_id = $sheet->appls_prs_id;
                $shC->applsxc_passed = $sheet->appls_passed;

                $m2 = ApplCommand::find()->leftJoin(ApplCommandContent::tableName(), 'applcmd_id = applcmdc_applcmd_id')->where(['applcmd_trt_id' => $sh1->appls_trt_id, 'applcmd_svdt_id' => $sh1->appls_svdt_id, 'applcmdc_prs_id' => $sheet->appls_prs_id])->one();

                if (!$m2) {
                    throw new HttpException(500, Yii::t('app', "Не найдена запись в приказе date=$date, trt_id={$sh1->appls_trt_id}, svdt_id={$sh1->appls_svdt_id}, prs_id={$sheet->appls_prs_id}"));
                }
                $shC->applsxc_applcmd_id = $m2->applcmd_id;
                $shC->applsxc_ab_id= $m2->applcmd_ab_id;
                //$shC->applsxc_position = $m2->applcmd_position;

                if (!$shC->save()) {
                    return $this->render('create_appl_sheetx_cnt', [
                        'model' => $shC,
                    ]);
                }
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($sheetX->applSheetXContents));

            $document->setValue('DOC_NUMBER', $sheetX->applsx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheetX->applsx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($sheetX->applSheetXContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applsxcPrs->prs_full_name);
                $document->setValue('PROGRAMMA#' . $i, $obj->applsxcTrp->trp_name);
                $document->setValue('IN_NUMBER#' . $i, isset($obj->applsxcApplcmd) ? $obj->applsxcApplcmd->applcmd_number : '');
                $document->setValue('OUT_DATE#' . $i, $obj->applsxcApplsx->applsx_date);
                $document->setValue('TEST_CORRECT#' . $i, $obj->applsxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $i++;
            }
            $document->saveAs($file_name);

            if (count($sheetX->applSheetXContents) > 0) {
                $sheetX->applsx_file_name = $sheetX->applsx_date . '_' . $sheetX->applsx_trt_id . '_' . $pattern->pat_fname;
                $sheetX->applsx_file_data = file_get_contents($file_name);
                $sheetX->applsx_pat_id = $pattern->pat_id;
            } else {
                $sheetX->applsx_file_name = null;
                $sheetX->applsx_file_data = null;
                $sheetX->applsx_pat_id = null;
            }
            if (!$sheetX->save()) {
                return $this->render('create_appl_sheetx', [
                    'model' => $sheetX,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

        }
        return 0;
    }


    /**
     ************************************************************
     *  ApplEnd - Приказ о окончании
     ************************************************************
     */

    /**
     * Creates|Updates a new|old ApplEnd model.
     * @param integer $svdt_id
     * @param integer $trt_id
     * @param string $date
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateApplEnd($svdt_id, $trt_id, $date)
    {
        if (!$sheetX = ApplSheetX::find()->where(['applsx_date' => $date, 'applsx_trt_id' => $trt_id, 'applsx_svdt_id' => $svdt_id])->one()) {
            return null;
        }

        if (!$end = ApplEnd::find()->where(['apple_date' => $date, 'apple_trt_id' =>  $trt_id, 'apple_svdt_id' =>  $svdt_id])->one()) {
            $end = new ApplEnd();
        }
        $end->apple_date = $date;
        $end->apple_trt_id = $trt_id;
        $end->apple_svdt_id = $svdt_id;
        $end->apple_applsx_id = $sheetX->applsx_id;

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $svdt_id])
            ->andWhere(['patt_id' => 9])
            ->andWhere(['pat_trt_id' => $trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplEnd) not found'));
        }

        $end->apple_number = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->format('Ymd') . '-'.$pattern->pat_code;

        if (!$end->save()) {
            return $this->render('create_appl_end', [
                'model' => $end,
            ]);
        }

        ApplEndContent::deleteAll(['applec_apple_id' => $end->apple_id]);

        foreach ($sheetX->applSheetXContents as $k => $obj) {
            $endC = new ApplEndContent();
            $endC->applec_apple_id = $end->apple_id;
            $endC->applec_prs_id = $obj->applsxc_prs_id;
            $endC->applec_trp_id = $obj->applsxc_trp_id;

            if ($main = ApplMain::find()->where(['applm_prs_id' => $endC->applec_prs_id, 'applm_trp_id' => $endC->applec_trp_id, 'applm_trt_id' => $trt_id, 'applm_apple_date' => $date])->one()) {
                $endC->applec_number = $main->applm_number_upk;
            }

            if (!$endC->save()) {
                return $this->render('create_appl_end_cnt', [
                    'model' => $endC,
                ]);
            }
        }

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $svdt_id])
            ->andWhere(['patt_id' => 9])
            ->andWhere(['pat_trt_id' => $trt_id])
            ->one();

        if ($pattern == null) {
            throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplEnd0) not found'));
        }

        $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
        $file_name = Yii::getAlias('@app') . '/storage/' . $end->apple_date . '_' . $pattern->pat_fname;
        file_put_contents($tmpl_name, $pattern->pat_fdata);

        $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
        $document->cloneRow('num', count($end->applEndContents));
        $document->setValue('DOC_NUMBER', $end->apple_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->getTimestamp();
        $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

        $document->setValue('VED_ITOG_NUMBER', $end->appleApplsx->applsx_number);

        $dateVal = \DateTime::createFromFormat('Y-m-d', $end->appleApplsx->applsx_date)->getTimestamp();
        $document->setValue('VED_ITOG_DATE_DD', strftime('%d', $dateVal));
        $document->setValue('VED_ITOG_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
        $document->setValue('VED_ITOG_DATE_YYYY', strftime('%Y', $dateVal));
        $i = 1;
        foreach ($end->applEndContents as $k => $obj) {
            $document->setValue('num#' . $i, $i);
            $document->setValue('PERSONA_FIO#' . $i, $obj->applecPrs->prs_full_name);
            $document->setValue('PROGRAMMA#' . $i, $obj->applecTrp->trp_name);
            $document->setValue('PROGRAMMA_HOURS#' . $i, $obj->applecTrp->trp_hour);
            $document->setValue('OUT_DATE#' . $i, $end->apple_date);
            $document->setValue('OUT_DOC_NUMBER#' . $i, $obj->applec_number);
            $i++;
        }
        $document->saveAs($file_name);

        $end->apple_file_name = $end->apple_date . '_' . $pattern->pat_fname;
        $end->apple_file_data = file_get_contents($file_name);
        $end->apple_pat_id = $pattern->pat_id;
        $end->save();

        unlink($file_name);
        unlink($tmpl_name);

        return 0;
    }


    /**
     * Creates|Updates a new|old ApplXxx model.
     * @param $applsx_id
     * @return mixed
     * @throws Exception if not found Pattern
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateApplXxx($applsx_id)
    {
        if ($sheet = ApplSheetX::findOne($applsx_id))
        {
            if (!$xxx = ApplXxx::find()->where(['applxxx_date' => $sheet->applsx_date, 'applxxx_trt_id' => $sheet->applsx_trt_id, 'applxxx_svdt_id' => $sheet->applsx_svdt_id])->one()) {
                $xxx = new ApplXxx();
            }
            $xxx->applxxx_date = $sheet->applsx_date;

            $xxx->applxxx_trt_id = $sheet->applsx_trt_id;
            $xxx->applxxx_svdt_id = $sheet->applsx_svdt_id;

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $xxx->applxxx_svdt_id])
                ->andWhere(['patt_id' => 12])
                ->andWhere(['pat_trt_id' => $xxx->applxxx_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplXxx) not found'));
            }

            $xxx->applxxx_number = \DateTime::createFromFormat('Y-m-d', $xxx->applxxx_date)->format('Ymd') . '-' . $pattern->pat_code;

            if (!$xxx->save()) {
                return $this->render('create_appl_xxx', [
                    'model' => $xxx,
                ]);
            }

            ApplXxxContent::deleteAll(['applxxxc_applxxx_id' => $xxx->applxxx_id]);

            foreach (ApplSheetXContent::find()->where(['applsxc_applsx_id' => $sheet->applsx_id])->all() as $key => $sheetXContent) {

                $xxxC = new ApplXxxContent();
                $xxxC->applxxxc_applxxx_id = $xxx->applxxx_id;

                $xxxC->applxxxc_prs_id = $sheetXContent->applsxc_prs_id;
                $xxxC->applxxxc_trp_id = $sheetXContent->applsxc_trp_id;

                // TODO Что-то где-то как-то
                $xxxC->applxxxc_ab_id = $sheetXContent->applsxc_ab_id;
                $xxxC->applxxxc_position = $sheetXContent->applsxc_position;

                $xxxC->applxxxc_passed = $sheetXContent->applsxc_passed;

                if (!$xxxC->save()) {
                    return $this->render('create_appl_xxx_cnt', [
                        'model' => $xxxC,
                    ]);
                }
            }

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $xxx->applxxx_svdt_id])
                ->andWhere(['patt_id' => 12])
                ->andWhere(['pat_trt_id' => $xxx->applxxx_trt_id])
                ->one();

            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find(generateApplXxx0) not found'));
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $xxx->applxxx_date . '_' . $xxx->applxxx_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($xxx->applXxxContents));

            $document->setValue('DOC_NUMBER', $xxx->applxxx_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $xxx->applxxx_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));


            $document->setValue('VED_ITOG_NUMBER', $sheet->applsx_number);
            $dateVal = \DateTime::createFromFormat('Y-m-d', $sheet->applsx_date)->getTimestamp();
            $document->setValue('VED_ITOG_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('VED_ITOG_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('VED_ITOG_DATE_YYYY', strftime('%Y', $dateVal));

            $i = 1;
            foreach ($xxx->applXxxContents as $k2 => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('PERSONA_FIO#' . $i, $obj->applxxxcPrs->prs_full_name);
                $document->setValue('PERSONA_DOLGNOST#' . $i, $obj->applxxxc_position);
                $document->setValue('PROGRAMMA#' . $i, $obj->applxxxcTrp->trp_name);
                $document->setValue('PERSONA_WORK#' . $i, isset($obj->applxxxcAb) ? $obj->applxxxcAb->entity->entEntt->entt_name_short .' '. $obj->applxxxcAb->entity->ent_name : '');
                $document->setValue('TEST_CORRECT#' . $i, $obj->applxxxc_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));
                $i++;
            }
            $document->saveAs($file_name);

            if (count($xxx->applXxxContents) > 0) {
                $xxx->applxxx_file_name = $xxx->applxxx_date . '_' . $xxx->applxxx_trt_id . '_' . $pattern->pat_fname;
                $xxx->applxxx_file_data = file_get_contents($file_name);
                $xxx->applxxx_pat_id = $pattern->pat_id;
            } else {
                $xxx->applsx_file_name = null;
                $xxx->applsx_file_data = null;
                $xxx->applsx_pat_id = null;
            }
            if (!$xxx->save()) {
                return $this->render('create_appl_xxx', [
                    'model' => $xxx,
                ]);
            }

            unlink($file_name);
            unlink($tmpl_name);

        }
        return 0;
    }

}

