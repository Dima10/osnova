<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Excel_XML;
use Yii;
use app\models\{
    AgreementAnnex,
    AgreementAnnexSearch,
    Agreement,
    AgreementStatus,
    Entity,
    Company
};

use yii\web\{
    Controller, NotFoundHttpException, UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgreementAnnexController implements the CRUD actions for AgreementAnnex model.
 */
class AgreementAnnexController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all AgreementAnnex models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->getView()->registerJsFile('/js/AgreementAnnexController.js',  ['position' => yii\web\View::POS_END]);
        $searchModel = new AgreementAnnexSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['AgreementAnnexParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
            'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
        ]);
    }

    /**
     * Displays a single AgreementAnnex model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgreementAnnex model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AgreementAnnex();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agra_data')) != null) {
                $model->agra_fdata = $file->name;
                $model->agra_data = file_get_contents($file->tempName);
            }

            if (($file = UploadedFile::getInstance($model, 'agra_data_sign')) != null) {
                $model->agra_fdata_sign = $file->name;
                $model->agra_data_sign = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agra_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agra_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->all(), 'agr_id', 'agr_number'),
            ]);
        }
        */
    }

    /**
     * Updates an existing AgreementAnnex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agra_data')) != null) {
                $model->agra_fdata = $file->name;
                $model->agra_data = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata = $model->oldAttributes['agra_fdata'];
                $model->agra_data = $model->oldAttributes['agra_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'agra_data_sign')) != null) {
                $model->agra_fdata_sign = $file->name;
                $model->agra_data_sign = file_get_contents($file->tempName);
            } else {
                $model->agra_fdata_sign = $model->oldAttributes['agra_fdata_sign'];
                $model->agra_data_sign = $model->oldAttributes['agra_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agra_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->agra_ab_id])->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agra_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        */
    }

    /**
     * Deletes an existing AgreementAnnex model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * List an existing AgreementAnnex models.
     * @param integer $ab_id
     * @param string $id
     * @return mixed
     */
    public function actionAjaxList($ab_id, $id)
    {
        $models = ArrayHelper::map(
            AgreementAnnex::find()->where(['agra_ab_id' => $ab_id])->all(), 'agra_id', 'agra_number'
        );

        return $this->renderAjax('_ajax_list', [
            'ab_id' => $ab_id,
            'id' => $id,
            'models' => [0 => '<>'] + $models,
        ]);
        
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = AgreementAnnex::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agra_data, $model->agra_fdata);

    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSign($id)
    {
        return $this->redirect(['download-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadSignF($id)
    {
        $model = AgreementAnnex::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->agra_data_sign, $model->agra_fdata_sign);

    }


    /**
     * Finds the AgreementAnnex model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgreementAnnex the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = AgreementAnnex::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml() {

        $query = AgreementAnnex::find()
            ->joinWith(['agraEnt' => function($query) { $query->from(['agraEnt' => 'entity']);}])
            ->joinWith(['agraAgr' => function($query) { $query->from(['agraAgr' => 'agreement']);}]);


        $annex = new AgreementAnnexSearch();
        $annex->load(Yii::$app->session['AgreementAnnexParams']);

        // grid filtering conditions
        $query->andFilterWhere([
            'agra_id' => $annex->agra_id,
            'agra_comp_id' => $annex->agra_comp_id,
            'agra_agr_id' => $annex->agra_agr_id,
            'agra_ast_id' => $annex->agra_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'agra_id', $annex->agra_id])
            ->andFilterWhere(['like', 'agra_number', $annex->agra_number])
            ->andFilterWhere(['like', 'agra_date', $annex->agra_date])
            ->andFilterWhere(['like', 'agra_sum', $annex->agra_sum])
            ->andFilterWhere(['like', 'agra_tax', $annex->agra_tax])
            ->andFilterWhere(['like', 'agra_date', $annex->agra_date])
            ->andFilterWhere(['like', 'agra_comment', $annex->agra_comment])
            ->andFilterWhere(['like', 'agraEnt.ent_name', $annex->yurLico])
            ->andFilterWhere(['like', 'agraAgr.agr_number', $annex->dogovor])
            ->andFilterWhere(['like', 'agra_create_user', $annex->agra_create_user])
            ->andFilterWhere(['like', 'agra_create_time', $annex->agra_create_time])
            ->andFilterWhere(['like', 'agra_create_ip', $annex->agra_create_ip])
            ->andFilterWhere(['like', 'agra_update_user', $annex->agra_update_user])
            ->andFilterWhere(['like', 'agra_update_time', $annex->agra_update_time])
            ->andFilterWhere(['like', 'agra_update_ip', $annex->agra_update_ip])
        ;

        $data = [];
        foreach ($query->all() as &$model) {
            $data[] = [
//                $model->actAb->entity->entAgent->ab_name ?? '',
                $model->agra_number,
                $model->agra_date,
                $model->agraAst->ast_name,
                $model->agraEnt->ent_name,
                $model->agraAb->ab_name,
                $model->agraAgr->agr_number,
//                $model->actAb->ab_name ?? '',
//                $model->actAst->ast_name ?? '',
//                $model->act_number,
//                $model->act_date,
//                $model->actAgr->agr_number ?? '',
//                $model->actAgra->agra_number ?? '',
                $model->agra_sum,
                $model->agra_tax,

            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'AgreementAnnex';
        $xls->addArray($data);

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'AgreementAnnex.xls');

    }
}
