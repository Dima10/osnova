<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
                SvcValue,
                SvcValueSearch,
                Svc,
                SvcProp
};

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * SvcValueController implements the CRUD actions for SvcValue model.
 */
class SvcValueController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all SvcValue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SvcValueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
            'svc_prop' => ArrayHelper::map(SvcProp::find()->all(), 'svcp_id', 'svcp_name'),
        ]);
    }

    /**
     * Displays a single SvcValue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SvcValue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SvcValue();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->svcv_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'svc_prop' => ArrayHelper::map(SvcProp::find()->all(), 'svcp_id', 'svcp_name'),
            ]);
        }
    }

    /**
     * Updates an existing SvcValue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->svcv_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'svc_prop' => ArrayHelper::map(SvcProp::find()->all(), 'svcp_id', 'svcp_name'),
            ]);
        }
    }

    /**
     * Deletes an existing SvcValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SvcValue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SvcValue the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = SvcValue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
