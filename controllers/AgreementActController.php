<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use app\models\{
    AgreementAct, AgreementActSearch, Entity, Agreement, AgreementAnnex, AgreementStatus, Excel_XML
};

use yii\web\{
    Controller,
    NotFoundHttpException,
    UploadedFile
};

use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgreementActController implements the CRUD actions for AgreementAct model.
 */
class AgreementActController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all AgreementAct models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->getView()->registerJsFile('/js/AgreementActController.js',  ['position' => yii\web\View::POS_END]);
        $searchModel = new AgreementActSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['AgrementActParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'entity' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
            'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
            'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),

        ]);
    }

    /**
     * Displays a single AgreementAct model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgreementAct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AgreementAct();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'act_data')) != null) {
                $model->act_fdata = $file->name;
                $model->act_data = file_get_contents($file->tempName);
            }

            if (($file = UploadedFile::getInstance($model, 'act_data_sign')) != null) {
                $model->act_fdata_sign = $file->name;
                $model->act_data_sign = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->act_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->select('agra_id, agra_number')->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->act_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        */
    }

    /**
     * Updates an existing AgreementAct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'act_data')) != null) {
                $model->act_fdata = $file->name;
                $model->act_data = file_get_contents($file->tempName);
            } else {
                $model->act_fdata = $model->oldAttributes['act_fdata'];
                $model->act_data = $model->oldAttributes['act_data'];
            }

            if (($file = UploadedFile::getInstance($model, 'act_data_sign')) != null) {
                $model->act_fdata_sign = $file->name;
                $model->act_data_sign = file_get_contents($file->tempName);
            } else {
                $model->act_fdata_sign = $model->oldAttributes['act_fdata_sign'];
                $model->act_data_sign = $model->oldAttributes['act_data_sign'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->act_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                    'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->where(['agr_ab_id' => $model->act_ab_id])->all(), 'agr_id', 'agr_number'),
                'agreement_annex' => ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $model->act_ab_id])->all(), 'agra_id', 'agra_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->act_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        */
    }

    /**
     * Deletes an existing AgreementAct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = AgreementAct::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->act_data, $model->act_fdata);

    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownloadSign($id)
    {
        return $this->redirect(['download-sign-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadSignF($id)
    {
        $model = AgreementAct::findOne($id);

        return Yii::$app->response->sendContentAsFile($model->act_data_sign, $model->act_fdata_sign);

    }


    /**
     * Finds the AgreementAct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgreementAct the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = AgreementAct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml() {

        $query = AgreementAct::find();
        $act = new AgreementActSearch();
        $act->load(Yii::$app->session['AgrementActParams']);
        // grid filtering conditions
        $query->andFilterWhere([
            'act_id' => $act->act_id,
            'act_ab_id' => $act->act_ab_id,
            'act_comp_id' => $act->act_comp_id,
            'act_agr_id' => $act->act_agr_id,
            'act_agra_id' => $act->act_agra_id,
            'act_ast_id' => $act->act_ast_id,
        ]);

        $query
            ->andFilterWhere(['like', 'act_number', $act->act_number])
            ->andFilterWhere(['like', 'act_date', $act->act_date])
            ->andFilterWhere(['like', 'act_sum', $act->act_sum])
            ->andFilterWhere(['like', 'act_tax', $act->act_tax])
            ->andFilterWhere(['like', 'act_fdata', $act->act_data])
            ->andFilterWhere(['like', 'act_fdata_sign', $act->act_data_sign])
            ->andFilterWhere(['like', 'act_create_user', $act->act_create_user])
            ->andFilterWhere(['like', 'act_create_time', $act->act_create_time])
            ->andFilterWhere(['like', 'act_create_ip', $act->act_create_ip])
            ->andFilterWhere(['like', 'act_update_user', $act->act_update_user])
            ->andFilterWhere(['like', 'act_update_time', $act->act_update_time])
            ->andFilterWhere(['like', 'act_update_ip', $act->act_update_ip])
        ;

        $data = [];
        foreach ($query->all() as &$model) {
            $data[] = [
                $model->actAb->entity->entAgent->ab_name ?? '',
                $model->actAb->ab_name ?? '',
                $model->actAst->ast_name ?? '',
                $model->act_number,
                $model->act_date,
                $model->actAgr->agr_number ?? '',
                $model->actAgra->agra_number ?? '',
                $model->act_sum,
                $model->act_tax,
            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'AgreementAct';
        $xls->addArray($data);

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'AgreementAct.xls');

    }

}
