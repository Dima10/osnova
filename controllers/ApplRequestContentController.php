<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\SvcDocType;
use Yii;
use app\models\ApplRequestContent;
use app\models\ApplRequestContentSearch;
use app\models\Person;
use app\models\Svc;

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ApplRequestContentController implements the CRUD actions for ApplRequestContent model.
 */
class ApplRequestContentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplRequestContent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplRequestContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplRequestContent model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplRequestContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $applr_id
     * @return mixed
     */
    public function actionCreate($applr_id)
    {
        $model = new ApplRequestContent();
        $model->applrc_applr_id = $applr_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applrc_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
            ]);
        }
    }

    /**
     * Updates an existing ApplRequestContent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applrc_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'svc' => ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
            ]);
        }
    }

    /**
     * Deletes an existing ApplRequestContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplRequestContent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplRequestContent the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplRequestContent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
