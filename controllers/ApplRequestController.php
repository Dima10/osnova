<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Ab;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexA;
use app\models\AgreementStatus;
use app\models\ApplRequestContent;
use app\models\Calendar;
use app\models\Constant;
use app\models\Entity;
use app\models\Pattern;
use app\models\PatternType;
use app\models\SentEmailsFromRequest;
use app\models\TrainingProg;
use app\models\TrainingType;
use app\models\User;
use app\models\UserMail;
use Faker\Provider\Base;
use Yii;
use app\models\ApplRequest;
use app\models\ApplRequestSearch;
use app\models\ApplRequestContentSearch;
use app\models\Company;
use app\models\Person;
use app\models\Svc;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ApplRequestController implements the CRUD actions for ApplRequest model.
 */
class ApplRequestController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/ApplRequestController.js',  ['position' => yii\web\View::POS_END]);
        $searchModel = new ApplRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ab' => ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name'),
            'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agra' => [],
            'reestr' => Constant::reestr_val(),
        ]);
    }

    /**
     * Displays a single ApplRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $ent_id
     * @param string $return return route
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate($ent_id = null, $return = null)
    {
        $model = new ApplRequest();
        $ab = [];

        $this->getView()->registerJsFile('/js/ApplRequestController.js',  ['position' => yii\web\View::POS_END]);

        if ($model->load(Yii::$app->request->post())) {

            $pattern = Pattern::findOne($model->applr_pat_id);

            $model->applr_number = \DateTime::createFromFormat('Y-m-d', $model->applr_date)->format('Ymd') . '-' . $pattern->pat_code;
            $id_day = ApplRequest::find()->where(['applr_date' => $model->applr_date])->max('applr_id_day');
            $id = (is_null($id_day) ? 1 : $id_day+1);
            $model->applr_id_day = $id;
            $model->applr_number .= '-'.$id;

            if( $model->save()) {
                return $this->redirect(['update', 'id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return]);
            } else {

                if (is_null($ent_id)) {
                    $ab = [0 => '<>'] + ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name');
                    $agra = [];
                } else {
                    $ab = ArrayHelper::map(Entity::find()->where(['ent_id' => $ent_id])->all(), 'ent_id', 'ent_name');
                    $agra = ArrayHelper::map(
                        AgreementAnnex::find()
                            ->leftJoin(ApplRequest::tableName(), 'agra_id = applr_agra_id')
                            ->where(['agra_ab_id' => $ent_id/*, 'applr_id' => null*/])
                            ->all()
                        , 'agra_id', 'agra_number');
                }

                return $this->render('create', [
                    'model' => $model,
                    'ab' => $ab,
                    'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'astatus' => ArrayHelper::map(AgreementStatus::find()->all(), 'ast_id', 'ast_name'),
                    'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
                    'agra' => $agra,
                    'pattern' => ArrayHelper::map(
                        Pattern::find()
                            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                            ->where(['patt_id' => 2])
                            ->all(), 'pat_id', 'pat_name'),
                    'reestr' => Constant::reestr_val(true),
                    'manager' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->applr_manager_id])->all(), 'ab_id', 'ab_name'),
                    'ent_id' => $ent_id,
                    'return' => $return,
                ]);

            }
        }

        $model->applr_date = date('Y-m-d');
        $model->applr_flag = 1;
        $model->applr_ab_id = $ent_id;
        $model->applr_manager_id = $model->applrAb->entity->ent_agent_id ?? null;

        if (User::isSpecAdmin()) {
            $model->applr_comp_id = 1; // osnova
            $model->applr_pat_id = 2; // обучение юрлица
            $model->applr_flag = 0;
            $model->applr_flag_send = 0;
        }

        if (is_null($ent_id)) {
            $ab = [0 => '<>'] + ArrayHelper::map(Entity::find()->all(), 'ent_id', 'ent_name');
            $agra = [];
        } else {
            $ab = ArrayHelper::map(Entity::find()->where(['ent_id' => $ent_id])->all(), 'ent_id', 'ent_name');
            $agra = ArrayHelper::map(
                AgreementAnnex::find()
                    ->leftJoin(ApplRequest::tableName(), 'agra_id = applr_agra_id')
                    ->where(['agra_ab_id' => $ent_id, /*'applr_id' => null*/])
                    ->all()
                , 'agra_id', 'agra_number');
        }

        return $this->render('create', [
            'model' => $model,
            'ab' => $ab,
            'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'astatus' => ArrayHelper::map(AgreementStatus::find()->all(), 'ast_id', 'ast_name'),
            'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            'agra' => $agra,
            'pattern' => ArrayHelper::map(
                        Pattern::find()
                            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                            ->where(['patt_id' => 2])
                            ->all(), 'pat_id', 'pat_name'),
            'reestr' => Constant::reestr_val(true),
            'manager' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->applr_manager_id])->all(), 'ab_id', 'ab_name'),
            'ent_id' => $ent_id,
            'return' => $return,
        ]);

    }

    /**
     * Updates an existing ApplRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $ent_id
     * @param integer $return
     * @return mixed
     * @throws \yii\db\Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $ent_id = null, $return = null)
    {
        $model = $this->findModel($id);

        $this->getView()->registerJsFile('/js/ApplRequestController.js',  ['position' => yii\web\View::POS_END]);

        $searchModelC = new ApplRequestContentSearch();
        $dataProviderC = $searchModelC->search(['applr_id' => $id] /*Yii::$app->request->queryParams*/);

        if ($model->load(Yii::$app->request->post())) {

            $pattern = Pattern::findOne($model->applr_pat_id);
            $model->applr_number = \DateTime::createFromFormat('Y-m-d', $model->applr_date)->format('Ymd') . '-' . $pattern->pat_code;
            $model->applr_number .= '-'.$model->applr_id_day;

            if ($model->save()) {
                if (isset($ent_id)) {
                    if (isset($return)) {
                        return $this->redirect([$return, 'id' => $ent_id]);
                    }
                    return $this->redirect(['entity-frm/view', 'id' => $ent_id]);
                }
                return $this->redirect(['update', 'id' => $model->applr_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'ab' => ArrayHelper::map(Entity::find()->where(['ent_id' => $model->applr_ab_id])->all(), 'ent_id', 'ent_name'),
                    'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                    'agra' => ArrayHelper::map(
                        AgreementAnnex::find()
                            ->leftJoin(ApplRequest::tableName(), 'agra_id = applr_agra_id')
                            ->where(['agra_ab_id' => $model->applr_ab_id, 'applr_id' => null])
                            ->all()
                        , 'agra_id', 'agra_number'),
                    'astatus' => ArrayHelper::map(AgreementStatus::find()->all(), 'ast_id', 'ast_name'),
                    'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
                    'searchModelC' => null, //$searchModelC,
                    'dataProviderC' => $dataProviderC,
                    'pattern' => ArrayHelper::map(
                        Pattern::find()
                            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                            ->where(['patt_id' => 2])
                            ->all(), 'pat_id', 'pat_name'),
                    'reestr' => Constant::reestr_val(true),
                    'manager' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->applr_manager_id])->all(), 'ab_id', 'ab_name'),
                    'ent_id' => $ent_id,
                    'return' => $return,
                ]);

            }
        }

        return $this->render('update', [
            'model' => $model,
            'ab' => ArrayHelper::map(Entity::find()->where(['ent_id' => $model->applr_ab_id])->all(), 'ent_id', 'ent_name'),
            'comp' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
            'agra' => ArrayHelper::map(AgreementAnnex::find()->where(['agra_ab_id' => $model->applr_ab_id])->all(), 'agra_id', 'agra_number'),
            'astatus' => ArrayHelper::map(AgreementStatus::find()->all(), 'ast_id', 'ast_name'),
               'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            'searchModelC' => null, //$searchModelC,
            'dataProviderC' => $dataProviderC,
            'pattern' => ArrayHelper::map(
                    Pattern::find()
                        ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                        ->where(['patt_id' => 2])
                        ->all(), 'pat_id', 'pat_name'),
            'reestr' => Constant::reestr_val(true),
            'manager' => [0 => '<>'] + ArrayHelper::map(Ab::find()->select(['ab_id', 'ab_name'])->where(['ab_id' => $model->applr_manager_id])->all(), 'ab_id', 'ab_name'),
            'ent_id' => $ent_id,
            'return' => $return,
        ]);
    }

    /**
     * Deletes an existing ApplRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $ent_id
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id, $ent_id = null)
    {
        $this->findModel($id)->delete();

        if (isset($ent_id)) {
            return $this->redirect(['entity-frm/view', 'id' => $ent_id]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplRequest the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApplRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Creates a new Person model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @param integer $applr_id
     * @param integer $ent_id
     * @param string $return
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreatePerson($applr_id, $ent_id = null, $return = null)
    {
        $applReq = $this->findModel($applr_id);

        $model = new Person();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->objectSave();
            return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return,]);
        } else {
            return $this->render('create_person', [
                'model' => $model,
                'applRequest' => $applReq,
                'ent_id' => $ent_id,
                'return' => $return,
            ]);
        }
    }

    /**
     * Creates a new ApplRequestContent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $applr_id
     * @param integer $ent_id
     * @param string $return
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreateContent($applr_id, $ent_id = null, $return = null)
    {
        /**
         * 1. Надо поставить проверку, что в Заявки Основного реестра не должно попадать программы с признаком дополнительного реестра
         */

        $applRequest = $this->findModel($applr_id);

        $model = new ApplRequestContent();

        if ($person = Person::objectExists()) {
            $model->applrc_prs_id = $person->prs_id;
            Person::objectClear();
        }

        $model->applrc_applr_id = $applr_id;

        if ($model->load(Yii::$app->request->post())) {

            if ($model->applrc_flag == 1 && !Calendar::workDateCheck($model->applrc_date_upk)) {
                $model->addError('applrc_date_upk', Yii::t('app', 'Дата попадает на выходной'));
                return $this->render('create_content', [
                    'model' => $model,
                    'applRequests' => ArrayHelper::map(ApplRequest::find()->where(['applr_id' => $applr_id])->all(), 'applr_id', 'applr_number'),
                    'applRequest' => $applRequest,
                    'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                    //'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                    'svc' => [0 => '<>'] +
                        ArrayHelper::map(
                            Svc::find()->select('svc_id, svc_name')
                                ->innerJoin(AgreementAnnexA::tableName(), 'ana_svc_id = svc_id')
                                ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                                ->where(['agra_id' => $applRequest->applr_agra_id])
                                ->all()
                            , 'svc_id', 'svc_name'),
                    'prog' => [0 => '<>'],
                    'ent_id' => $ent_id,
                    'return' => $return,
                ]);
            }
            if (($applRequest->applr_reestr == 0) && ($model->applrcTrp->trp_reestr ?? 0) == 1)
            {
                // 1. Надо поставить проверку, что в Заявки Основного реестра не должно попадать программы с признаком дополнительного реестра

                $model->addError('applrc_trp_id', Yii::t('app', 'Программа с признаком доп. реестр'));
                return $this->render('create_content', [
                    'model' => $model,
                    'applRequests' => ArrayHelper::map(ApplRequest::find()->where(['applr_id' => $applr_id])->all(), 'applr_id', 'applr_number'),
                    'applRequest' => $applRequest,
                    'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                    //'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                    'svc' => [0 => '<>'] +
                        ArrayHelper::map(
                            Svc::find()->select('svc_id, svc_name')
                                ->innerJoin(AgreementAnnexA::tableName(), 'ana_svc_id = svc_id')
                                ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                                ->where(['agra_id' => $applRequest->applr_agra_id])
                                ->all()
                            , 'svc_id', 'svc_name'),
                    'prog' => [0 => '<>'],
                    'ent_id' => $ent_id,
                    'return' => $return,
                ]);
            }

            $qtyServiceAmount = AgreementAnnexA::find()
                ->select('sum(ana_qty)')
                ->where(['ana_agra_id'=>$applRequest->applr_agra_id, 'ana_svc_id'=>$model->applrc_svc_id, 'ana_trp_id'=>$model->applrc_trp_id])
                ->scalar();

            $qtyServiceCount = ApplRequestContent::findBySql(
                " SELECT count(*)
                        FROM appl_request_content 
                        left join appl_request ON appl_request_content.applrc_applr_id = appl_request.applr_id
                        where 
                        appl_request_content.applrc_svc_id = $model->applrc_svc_id 
                        and appl_request_content.applrc_trp_id=$model->applrc_trp_id 
                        and appl_request.applr_agra_id = $applRequest->applr_agra_id")
                ->scalar();

            if($qtyServiceCount < $qtyServiceAmount) {
                if ($model->save()) {
                    return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return,]);
                }
            } else {
                $requestRowIsDeclined = '<span style="color:red">Строка заявки не создана тк кол-во услуг превышено.</span>';
            }
        }

        $model->applrc_flag = 1;
        $model->applrc_notify = 1;
        return $this->render('create_content', [
                'model' => $model,
                'applRequests' => ArrayHelper::map(ApplRequest::find()->where(['applr_id' => $applr_id])->all(), 'applr_id', 'applr_number'),
                'applRequest' => $applRequest,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                //'svc' => [0 => '<>'] + ArrayHelper::map(Svc::find()->all(), 'svc_id', 'svc_name'),
                'svc' => [0 => '<>'] +
                    ArrayHelper::map(
                        Svc::find()->select('svc_id, svc_name')
                            ->innerJoin(AgreementAnnexA::tableName(), 'ana_svc_id = svc_id')
                            ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id')
                            ->where(['agra_id' => $applRequest->applr_agra_id])
                            ->all()
                        , 'svc_id', 'svc_name'),
                'prog' => [0 => '<>'],
                'ent_id' => $ent_id,
                'request_row_is_declined' => (isset($requestRowIsDeclined) && !empty($requestRowIsDeclined) ? $requestRowIsDeclined : ''),
                'return' => $return,
        ]);
    }

    /**
     * Search TraningProg model.
     * @param integer $svc_id
     * @param string $id
     * @param integer $applr_id
     * @return mixed
     */
    public function actionAjaxProgSearch($id, $svc_id, $applr_id)
    {
        $trp =
            TrainingProg::find()
                ->select(['trp_id', 'CONCAT(trp_name, trp_hour) as trp_name'])
                ->innerJoin(AgreementAnnexA::tableName(), 'ana_trp_id = trp_id') // aga
                ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id') // agra
                ->innerJoin(ApplRequest::tableName(), 'applr_agra_id = agra_id') // applr
                ->leftJoin(ApplRequestContent::tableName(), 'applrc_trp_id = trp_id and applrc_applr_id = applr_id') //applrc
                ->where(['ana_svc_id' => $svc_id, 'applr_id' => $applr_id])
                ->all();
        foreach ($trp as $key => &$trp_prog) {
            // Заявлено
            $countA = AgreementAnnexA::find()  // aga
                ->innerJoin(AgreementAnnex::tableName(), 'ana_agra_id = agra_id') // agra
                ->innerJoin(ApplRequest::tableName(), 'agra_id = applr_agra_id') // applr
                ->where(['applr_id'=> $applr_id, 'ana_trp_id' => $trp_prog->trp_id])->sum('ana_qty');
            // Внесено
            $countB =
                ApplRequestContent::find()
                    ->innerJoin(ApplRequest::tableName(), 'applrc_applr_id = applr_id')
                    ->where(['applr_id' => $applr_id, 'applrc_trp_id' =>  $trp_prog->trp_id])->count();
            if ($countA <= $countB) {
                unset($trp[$key]);
            }
        }

        $prog = ArrayHelper::map( $trp, 'trp_id', 'trp_name');

        return $this->renderAjax('_ajax_prog', [
            'id' => $id,
            'prog' => $prog,
        ]);
    }

    /**
     * Delete a ApplRequestContent model.
     * @param integer $applrc_id
     * @param integer $ent_id
     * @param string $return
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteContent($applrc_id, $ent_id = null, $return = null)
    {
        $model = ApplRequestContent::findOne($applrc_id);

        $applr_id = $model->applrc_applr_id;

        $model->delete();

        return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return]);
    }

    /**
     * Return AgreementAnnex model list.
     * @param integer $ab_id
     * @param string $id
     * @return mixed
     */
    public function actionAjaxAnnexList($ab_id, $id)
    {
        $models = ArrayHelper::map(
        AgreementAnnex::find()
            ->leftJoin(ApplRequest::tableName(), 'agra_id = applr_agra_id')
            ->where(['agra_ab_id' => $ab_id, 'applr_id' => null])
            ->all()
        , 'agra_id', 'agra_number');

        return $this->renderAjax('_ajax_annex_list', [
                'ab_id' => $ab_id,
                'id' => $id,
                'models' => [0 => '<>'] + $models,
            ]
        );
    }

    /**
     * Return ApplRequest model list.
     * @param string $id
     * @param string $text
     * @return mixed
     */
    public function actionAjaxSearch($id, $text)
    {
        $all = [0 => '<>']+ArrayHelper::map(ApplRequest::find()->select(['applr_id', 'applr_number'])->where(['like', 'applr_number', $text])->all(), 'applr_id', 'applr_number');
        return $this->renderAjax('_ajax_search', ['id' => $id, 'all' => $all]);
    }

    /**
     * Return ApplRequestContent model list.
     * @param string $id
     * @param string $applr
     * @return mixed
     */
    public function actionAjaxPerson($id, $applr)
    {
        $all = ArrayHelper::map(
            Person::find()
                ->select(['prs_id', 'prs_full_name'])
                ->leftJoin(ApplRequestContent::tableName(), 'applrc_prs_id = prs_id')
                ->where(['applrc_applr_id' => $applr])->all(), 'prs_id', 'prs_full_name');
        return $this->renderAjax('_ajax_search', ['id' => $id, 'all' => $all]);
    }

    /**
     * Send notify
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionNotify($id) {
        if ($model = ApplRequestContent::findOne($id)) {
                foreach ($model->applrcPrs->contacts as $contact) {
                    if ($contact->conCont->cont_type == 1) {
                        if(
                            UserMail::sendMail(1, $contact->con_text,
                                [
                                    'link' => $model->applrcPrs->prs_connect_link,
                                    'login' => $model->applrcPrs->prs_connect_user,
                                    'password' => $model->applrcPrs->prs_connect_pwd,
                                    'programma' => $model->applrcTrp->trp_name,
                                    'fio' => $model->applrcPrs->prs_full_name,
                                    ':personal' => $model->applrcApplr->applr_flag,
                                    ':reestr' => $model->applrcApplr->applr_reestr,
                                ])
                        ){

                            $userMail = UserMail::find()->where(['um_umt_id' => 1, 'um_reestr' => $model->applrcApplr->applr_reestr, 'um_flag' => $model->applrcApplr->applr_flag])->one();

                            if (trim($userMail->umSm->sm_imap_server) != '') {
                                $fromMail = $userMail->umSm->sm_imap_user;
                            } else {
                                $fromMail = $userMail->umSm->sm_user;
                            }

                            $dateTime = new \DateTime('now');
                            $sentEmailsFromRequest = new SentEmailsFromRequest();
                            $sentEmailsFromRequest->applr_id = $model->applrc_applr_id;
                            $sentEmailsFromRequest->applrc_prs_id = $model->applrc_prs_id;
                            $sentEmailsFromRequest->fio = $model->applrcPrs->prs_full_name;
                            $sentEmailsFromRequest->login = $model->applrcPrs->prs_connect_user;
                            $sentEmailsFromRequest->applrc_trp_id = $model->applrcTrp->trp_id;
                            $sentEmailsFromRequest->applr_ab_id = $model->applrcApplr->applr_ab_id;
                            $sentEmailsFromRequest->applr_manager_id = $model->applrcApplr->applr_manager_id;
                            $sentEmailsFromRequest->recipient_email = $contact->con_text;
                            $sentEmailsFromRequest->sender_email = $fromMail;
                            $sentEmailsFromRequest->send_date = $dateTime->format('Y-m-d H:i:s');
                            $sentEmailsFromRequest->applr_reestr = $model->applrcApplr->applr_reestr;
                            $sentEmailsFromRequest->applr_flag = $model->applrcApplr->applr_flag;
                            $sentEmailsFromRequest->status = 1;
                            $sentEmailsFromRequest->save(false);

                        }
                    }
                }
        }
        return 'Ok';
    }

    /**
     * Check notify
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNotifyCheck($id) {
        if ($model = ApplRequestContent::findOne($id)) {
            $model->applrc_notify = $model->applrc_notify == 1 ? 0 : 1;
            $model->check = true;
            $model->save();
        }
        return '';
    }

    /**
     * Send notify
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionNotifySend($applr_id, $ent_id = null, $return = null) {

        $model = ApplRequest::findOne($applr_id);
        foreach ($model->applRequestContents as $content) {
            $cont = '';
            if (($content->applrc_notify ?? 1) == 1)
            {
                foreach ($content->applrcPrs->contacts as $contact) {
                    if ($contact->conCont->cont_type == 1) {
                        if ($cont == '') {
                            $cont = $contact->con_text;
                        } else {
                            $cont .= ';'.$contact->con_text;
                        }
                    }
                }
            }

            if(
                UserMail::sendMail(1, $cont,
                    [
                        'link' => $content->applrcPrs->prs_connect_link,
                        'login' => $content->applrcPrs->prs_connect_user,
                        'password' => $content->applrcPrs->prs_connect_pwd,
                        'programma' => $content->applrcTrp->trp_name,
                        'fio' => $content->applrcPrs->prs_full_name,
                        ':personal' => $content->applrcApplr->applr_flag,
                        ':reestr' => $content->applrcApplr->applr_reestr,
                    ]
                )
            ){

                $userMail = UserMail::find()->where(['um_umt_id' => 1, 'um_reestr' => $content->applrcApplr->applr_flag, 'um_flag' => $content->applrcApplr->applr_flag])->one();

                if (trim($userMail->umSm->sm_imap_server) != '') {
                    $fromMail = $userMail->umSm->sm_imap_user;
                } else {
                    $fromMail = $userMail->umSm->sm_user;
                }

                $dateTime = new \DateTime('now');
                foreach (explode(';', $cont) as $emailTo){

                    if(empty($emailTo)){
                        $emailTo = 'Не указано, или оповещения отключены. Отправка на сервисные ящики';
                    }

                    $sentEmailsFromRequest = new SentEmailsFromRequest();
                    $sentEmailsFromRequest->applr_id = $applr_id;
                    $sentEmailsFromRequest->applrc_prs_id = $content->applrc_prs_id;
                    $sentEmailsFromRequest->fio = $content->applrcPrs->prs_full_name;
                    $sentEmailsFromRequest->login = $content->applrcPrs->prs_connect_user;
                    $sentEmailsFromRequest->applrc_trp_id = $content->applrcTrp->trp_id;
                    $sentEmailsFromRequest->applr_ab_id = $model->applr_ab_id;
                    $sentEmailsFromRequest->applr_manager_id = $model->applr_manager_id;
                    $sentEmailsFromRequest->recipient_email = $emailTo;
                    $sentEmailsFromRequest->sender_email = $fromMail;
                    $sentEmailsFromRequest->send_date = $dateTime->format('Y-m-d H:i:s');
                    $sentEmailsFromRequest->applr_reestr = $model->applr_reestr;
                    $sentEmailsFromRequest->applr_flag = $model->applr_flag;
                    $sentEmailsFromRequest->status = 1;
                    $sentEmailsFromRequest->save(false);

                }
            }
        }
        return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return]);
    }

    /**
     * Set notify 0
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNotifySend0($applr_id, $ent_id = null, $return = null) {

        $model = ApplRequest::findOne($applr_id);
        foreach ($model->applRequestContents as $content) {
            $content->applrc_notify = 0;
            $content->check = true;
            $content->save();
        }
        return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return]);
    }

    /**
     * Set notify 1
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNotifySend1($applr_id, $ent_id = null, $return = null) {

        $model = ApplRequest::findOne($applr_id);
        foreach ($model->applRequestContents as $content) {
            $content->applrc_notify = 1;
            $content->check = true;
            $content->save();
        }
        return $this->redirect(['update', 'id' => $applr_id, 'ent_id' => $ent_id, 'return' => $return]);
    }
}
