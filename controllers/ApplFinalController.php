<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Account;
use app\models\ApplMain;
use app\models\ApplRequest;
use app\models\Constant;
use app\models\Pattern;
use app\models\PatternType;
use app\models\SvcDocType;
use app\models\TrainingProg;
use app\models\TrainingType;
use Yii;
use app\models\ApplFinal;
use app\models\ApplFinalSearch;
use yii\base\Arrayable;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ApplFinalController implements the CRUD actions for ApplFinal model.
 */
class ApplFinalController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplFinal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/ApplFinalController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new ApplFinalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reestr' => Constant::reestr_val(),
        ]);
    }

    /**
     * Displays a single ApplFinal model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplFinal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplFinal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->applf_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ApplFinal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $ent_id
     * @return mixed
     * @throws HttpException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws Exception
     */
    public function actionUpdate($id, $ent_id = null)
    {
        $model = $this->findModel($id);
        $upkNumber = $model->applf_number;

        if ($model->load(Yii::$app->request->post())) {

            //===========
            $post = Yii::$app->request->post()['ApplFinal'];
            $newUpkNumber = $post['applf_number'];
            $applfCmdDate = $post['applf_cmd_date'];
            $applfEndDate = $post['applf_end_date'];

            Yii::$app->db->createCommand(
                'UPDATE `appl_main` SET `applm_number_upk`=\''.$newUpkNumber.'\', `applm_applcmd_date`=\''.$applfCmdDate.'\', `applm_date_upk`=\''.$applfEndDate.'\' WHERE applm_number_upk = \''.$upkNumber.'\''
            )
            ->execute();
            //===========

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['pat_svdt_id' => $model->applf_svdt_id])
                ->andWhere(['patt_id' => 16])
                ->andWhere(['pat_trt_id' => $model->applf_trt_id])
                ->one();
            if ($pattern == null) {
                return $this->render('update', [
                    'model' => $model,
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_svdt_id' => $model->applf_svdt_id, 'pat_patt_id' => 16, 'pat_trt_id' => $model->applf_trt_id])->all(), 'pat_id', 'pat_name'),
                    'trt' => ArrayHelper::map(TrainingType::find()->where(['trt_id' => $model->applf_trt_id])->all(), 'trt_id', 'trt_name'),
                    'trp' => ArrayHelper::map(TrainingProg::find()->where(['trp_id' => $model->applf_trp_id])->all(), 'trp_id', 'trp_name'),
                    'svdt' => ArrayHelper::map(SvcDocType::find()->where(['svdt_id' => $model->applf_svdt_id])->all(), 'svdt_id', 'svdt_name'),
                    'applr' => ArrayHelper::map(ApplRequest::find()->where(['applr_ab_id' => $_GET['ent_id']])->all(), 'applr_id', 'applr_number'),
                    'ent_id' => $_GET['ent_id'],
                    'errorString' => 'Отсутствует шаблон документа',
                ]);
            }

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
            $file_name = Yii::getAlias('@app') . '/storage/' . $model->applf_cmd_date . '_' . $model->applf_trt_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applf_cmd_date)->getTimestamp();
            $document->setValue('PRIKAZ_IN_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('PRIKAZ_IN_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('PRIKAZ_IN_DATE_YYYY', strftime('%Y', $dateVal));

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applf_end_date)->getTimestamp();
            $document->setValue('PRIKAZ_OUT_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('PRIKAZ_OUT_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('PRIKAZ_OUT_DATE_YYYY', strftime('%Y', $dateVal));

            $data = $model->applf_name_full;
            $document->setValue('LISTENER_FIO', $data);

            $data = $model->applfTrp->trp_name;
            $document->setValue('PROG', $data);

            $data = $model->applfTrp->trp_hour;
            $document->setValue('PROG_TIME', $data);

            $data = $model->applf_number;
            $document->setValue('DOC_NUMBER', $data);

            $document->saveAs($file_name);

            $model->applf_file_name = $model->applf_cmd_date . '_' . $model->applf_trt_id . '_' . $pattern->pat_fname;
            $model->applf_file_data = file_get_contents($file_name);
            $model->applf_pat_id = $pattern->pat_id;

            unlink($file_name);
            unlink($tmpl_name);

            if($model->save()) {
                if (is_null($ent_id))
                    return $this->redirect(['view', 'id' => $model->applf_id]);
                return $this->redirect(['entity-frm/view', 'id' => $ent_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_svdt_id' => $model->applf_svdt_id, 'pat_patt_id' => 16, 'pat_trt_id' => $model->applf_trt_id])->all(), 'pat_id', 'pat_name'),
                    'trt' => ArrayHelper::map(TrainingType::find()->where(['trt_id' => $model->applf_trt_id])->all(), 'trt_id', 'trt_name'),
                    'trp' => ArrayHelper::map(TrainingProg::find()->where(['trp_id' => $model->applf_trp_id])->all(), 'trp_id', 'trp_name'),
                    'svdt' => ArrayHelper::map(SvcDocType::find()->where(['svdt_id' => $model->applf_svdt_id])->all(), 'svdt_id', 'svdt_name'),
                    'applr' => ArrayHelper::map(ApplRequest::find()->where(['applr_ab_id' => $model->applf_ent_id])->all(), 'applr_id', 'applr_number'),
                    'ent_id' => $ent_id,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'pattern' => ArrayHelper::map(Pattern::find()->select(['pat_id', 'pat_name'])->where(['pat_svdt_id' => $model->applf_svdt_id, 'pat_patt_id' => 16, 'pat_trt_id' => $model->applf_trt_id])->all(), 'pat_id', 'pat_name'),
                'trt' => ArrayHelper::map(TrainingType::find()->where(['trt_id' => $model->applf_trt_id])->all(), 'trt_id', 'trt_name'),
                'trp' => ArrayHelper::map(TrainingProg::find()->where(['trp_id' => $model->applf_trp_id])->all(), 'trp_id', 'trp_name'),
                'svdt' => ArrayHelper::map(SvcDocType::find()->where(['svdt_id' => $model->applf_svdt_id])->all(), 'svdt_id', 'svdt_name'),
                'applr' => ArrayHelper::map(ApplRequest::find()->where(['applr_id' => $model->applf_applr_id])->all(), 'applr_id', 'applr_number'),
                'ent_id' => $ent_id,
            ]);
        }
    }

    /**
     * Deletes an existing ApplFinal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplFinal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplFinal the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApplFinal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = ApplFinal::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->applf_file_data, $model->applf_file_name);
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload0($id)
    {
        return $this->redirect(['download0-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownload0F($id)
    {
        $model = ApplFinal::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->applf_file0_data, $model->applf0_file_name);
    }
}
