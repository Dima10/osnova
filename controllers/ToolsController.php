<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;
use yii\data\{
    ArrayDataProvider,
    ActiveDataProvider
};

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use app\models\{
    AgreementAccA,
    FinanceBook
};


/**
 * AbController implements the CRUD actions for Ab model.
 */
class ToolsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all tools function.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => [
                ['name' => 'FinanceBook Update', 'action' => Url::toRoute(['fb-update'])],
            ],
        ]);
        return $this->render('index',
            [
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Update FinanceBook records.
     * @return mixed
     */
    public function actionFbUpdate()
    {

        $query = AgreementAccA::find();

        $all = $query->all();

        while (count($all) > 0) {
            $value = array_shift($all);
            if (!$fb = FinanceBook::find()->where(['fb_aca_id' => $value->aca_id])->one()) {
                $fb = new FinanceBook();
            }
            $fb->fb_aca_id = $value->aca_id;
            $fb->fb_svc_id = $value->acaAna->ana_svc_id;
            $fb->fb_fbt_id = 1;
            $fb->fb_ab_id = $value->acaAga->aga_ab_id;
            $fb->fb_comp_id = $value->acaAga->aga_comp_id;
            $fb->fb_sum = $value->aca_price * $value->aca_qty;
            $fb->fb_tax = $value->aca_tax * $value->aca_qty;
            $fb->fb_aga_id = $value->aca_aga_id;
            $fb->fb_acc_id = $value->acaAga->agaAgr->agr_acc_id;
            $fb->fb_fbs_id = 1;
            $fb->fb_date = $value->acaAga->aga_date;
            $fb->fb_comment = isset($fb->fb_comment) ? null : 'tools' ;
            if (!$fb->save()) {
                return $this->render('financebook_update', [
                    'model' => $fb,
                ]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('financebook_grid', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
