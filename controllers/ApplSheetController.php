<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplMain;
use app\models\ApplRequest;
use app\models\ApplRequestContent;
use app\models\ApplSheetContent;
use app\models\ApplSheetContentSearch;
use app\models\Calendar;
use app\models\Constant;
use app\models\Pattern;
use app\models\PatternType;
use app\models\TrainingProg;
use app\models\TrainingType;
use app\models\UserMail;
use Yii;
use app\models\ApplSheet;
use app\models\ApplSheetSearch;
use app\models\Person;

use yii\base\DynamicModel;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ApplSheetController implements the CRUD actions for ApplSheet model.
 */
class ApplSheetController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'actions' => ['generate', 'test', 'test-next', 'result', 'timer'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplSheet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/ApplSheetController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new ApplSheetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'trt' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            'reestr' => Constant::reestr_val(true),
        ]);
    }

    /**
     * Displays a single ApplSheet model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplSheet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ApplSheet();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->appls_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'program' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
            ]);
        }
    }

    /**
     * Updates an existing ApplSheet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->appls_id]);
        } else {
            $searchModel = new ApplSheetContentSearch();
            $dataProvider = $searchModel->search(['ApplSheetContentSearch' => ['applsc_appls_id' => $model->appls_id]]);

            return $this->render('update', [
                'model' => $model,
                'person' => ArrayHelper::map(Person::find()->all(), 'prs_id', 'prs_full_name'),
                'program' => ArrayHelper::map(TrainingProg::find()->all(), 'trp_id', 'trp_name'),
                'searchModel' => null, //$searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing ApplSheet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplSheet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplSheet the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApplSheet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = ApplSheet::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->appls_file_data, $model->appls_file_name);
    }

    /**
     * Generate test
     * @param integer $applr_id
     * @param integer $prs_id
     * @param integer $applrc_trp_id
     * @param string $date
     * @param boolean $test
     * @return mixed
     */
    public function actionGenerate($applr_id, $prs_id=0, $applrc_trp_id=0, $date='', $test = false)
    {
        $date = $date == '' ? date('Y-m-d') : $date;

        srand();

        $req = ApplRequest::findOne($applr_id);

        $content = null;
        if ($prs_id > 0) {
            $content = ApplRequestContent::find()
                ->where(
                    [
                        'applrc_applr_id' => $applr_id,
                        'applrc_prs_id' => $prs_id,
                        'applrc_trp_id' => $applrc_trp_id,
                    ]
                )->all();
        } else {
            $content = ApplRequestContent::find()
                ->where(
                    [
                        'applrc_applr_id' => $applr_id,
                    ]
                )->all();
        }

        $sh = null;
        foreach ($content as $k => $obj) {
            $svdt_id = $obj->applrcSvc->svc_svdt_id;
            $sh = ApplSheet::find()
                ->where(
                    [
                        'appls_prs_id' => $prs_id,
                        //'appls_date' => $date,
                        'appls_trp_id' => $obj->applrc_trp_id,
                        'appls_trt_id' => $req->applr_trt_id,
                        'appls_svdt_id' => $svdt_id,
                    ]
                )
                ->one();
            if (!$sh) {
                $sh = new ApplSheet();
                $sh->appls_try = 3;
            }

            $currentApplm = ApplMain::findOne(
                [
                    'applm_applrc_id' => $obj->applrc_id,
                    'applm_prs_id' => $obj->applrc_prs_id,
                    'applm_svc_id' => $obj->applrcSvc->svc_id,
                    'applm_trp_id' => $obj->applrc_trp_id,
                    'applm_trt_id' => $req->applr_trt_id
                ]);

            $sh->appls_magic = md5(uniqid(rand(), true));
            $sh->appls_date = $date;
            $sh->appls_prs_id = $obj->applrc_prs_id;
            $sh->appls_trp_id = $obj->applrc_trp_id;
            $sh->appls_trt_id = $req->applr_trt_id;
            $sh->appls_applr_id = $req->applr_id;
            $sh->appls_applm_id = $currentApplm->applm_id;

            $sh->appls_svdt_id = $svdt_id;
            $sh->appls_score_max = 0;
            $sh->appls_number = \DateTime::createFromFormat('Y-m-d', $sh->appls_date)->format('Ymd') . '-ПР-Т-' . $prs_id.'-'.$sh->appls_trp_id;

            if (!$sh->save()) {
                return $this->render('create', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }

            ApplSheetContent::deleteAll(['applsc_appls_id' => $sh->appls_id]);

            $prog = TrainingProg::findOne($sh->appls_trp_id);

            // Кол-во вопросов
            $question_count = 0;
            // Кол-во модулей
            foreach ($prog->trainingProgModules as $mod) {
                // Сколько вопросов нужно задать с модуля
                $question_count += $mod->trplTrm->trm_test_question ?? 0;
            }

            foreach ($prog->trainingProgModules as $mod) {
                $module = $mod->trplTrm;
                $training_question_count = $module->trm_test_question ?? 0;
                $train_ques_a = [];
                // Случайный выбор вопроса
                for ($i = 0; $i < $training_question_count; $i++) {
                    if (count($module->trainingQuestions) < 1)
                        break;
                    $r = rand(0, count($module->trainingQuestions) - 1);
                    $max = 64;
                    while (in_array($r, $train_ques_a)) {
                        $r = rand(0, count($module->trainingQuestions) - 1);
                        $max--;
                        if ($max <= 0)
                            break;
                    }
                    $train_ques_a[] = $r;
                }
                foreach ($train_ques_a as $value) {

                    $question = $module->trainingQuestions[$value];

                    $shC = new ApplSheetContent();
                    $shC->applsc_appls_id = $sh->appls_id;

                    $shC->applsc_trq_id = $question->trq_id;
                    $shC->applsc_trq_question = $question->trq_question;
                    $resultQuestionsCollection[] = $shC;
//                    if (!$shC->save()) {
//                        return $this->render('create_cnt', [
//                            'model' => $shC,
//                        ]);
//                    }
                }
            }

            shuffle($resultQuestionsCollection);
            if (isset($prog->trp_test_question) && (int)$prog->trp_test_question > 0) {
                $resultQuestionsCollection = array_slice($resultQuestionsCollection, 0, (int)$prog->trp_test_question);
            }

            foreach ($resultQuestionsCollection as $question) {
                if (!$question->save()) {
                    return $this->render('create_cnt', [
                        'model' => $question,
                    ]);
                }
            }

            $sh->appls_score_max = ApplSheetContent::find()->where(['applsc_appls_id' => $sh->appls_id])->count();

            if (!$sh->save()) {
                return $this->render('create', [
                    'model' => $sh,
                    'person' => ArrayHelper::map(Person::find()->select(['prs_id', 'prs_full_name'])->where(['prs_id' => $sh->appls_prs_id])->all(), 'prs_id', 'prs_full_name'),
                    'program' => ArrayHelper::map(TrainingProg::find()->select(['trp_id', 'trp_name'])->where(['trp_id' => $sh->appls_trp_id])->all(), 'trp_id', 'trp_name'),
                ]);
            }
        }

        if ($test) {
            return $this->redirect(['test', 'uniq' => $sh->appls_magic]);
        } else {
            return $this->redirect(['update', 'id' => $sh->appls_id]);
        }
    }

    /**
     * Go to the test
     * @param string $uniq
     * @return mixed
     * @throws \Exception
     */
    public function actionTest($uniq)
    {
        if (!$sh = ApplSheet::find()->where(['appls_magic' => $uniq])->one()) {
            return "";
        }
        if ($sh->appls_try == 0) {
            /**
             * Send mail -> begin
             */
            foreach ($sh->applsPrs->contacts as $contact) {
                if ($contact->conCont->cont_type==1) {
                    UserMail::sendMail(6, $contact->con_text,
                        [
                            'link' => $sh->applsPrs->prs_connect_link,
                            'login' => $sh->applsPrs->prs_connect_user,
                            'password' => $sh->applsPrs->prs_connect_pwd,
                            'fio' => $sh->applsPrs->prs_full_name,
                            'programma' => $sh->applsTrp->trp_name,
                            ':reestr' => $sh->applsApplr->applr_reestr,
                            ':personal' => $sh->applsApplr->applr_flag,
                        ]);
                }
            }
            /**
             * Send mail -> end
             */
            return "";
        }
        $sh->appls_try = $sh->appls_try - 1;
        $sh->appls_begin = date('Y-m-d H:i:s');

        $date = new \DateTime();
        //$sh->appls_begin = $date->format('Y-m-d H:i:s');
        $date->modify('+2 hour');
        $sh->appls_end = $date->format('Y-m-d H:i:s');

        $sh->save();
        return $this->redirect(['test-next', 'uniq' => $uniq, 'id' => 0]);
    }

    /**
     * Go to the test
     * @param string $uniq
     * @param integer $id
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function actionTestNext($uniq, $id)
    {
        if (!$sh = ApplSheet::find()->where(['appls_magic' => $uniq])->one()) {
            return "";
        }

        $dynModel = DynamicModel::validateData(['answer_id'],
            [
                [['answer_id'], 'integer'],
            ]
        );

        $shCs = ApplSheetContent::find()->where(['applsc_appls_id' => $sh->appls_id])->orderBy('applsc_id')->all();

        if ($dynModel->load(Yii::$app->request->post())) {
            if (isset($shCs[$id])) {
                $shContent = $shCs[$id];
                $shContent->applsc_tra_id = $dynModel->answer_id;
                $shContent->applsc_tra_answer = isset($shContent->applscTra) ? $shContent->applscTra->tra_answer : '';
                $shContent->applsc_tra_variant = isset($shContent->applscTra) ? $shContent->applscTra->tra_variant : 0;
                $shContent->save();
                $id++;
            }
        }

        if (isset($shCs[$id])) {

            while (isset($shCs[$id]->applsc_tra_id)) {
                $id++;
            }

            return $this->render('test', [
                'model' => $dynModel,
                'sheet' => $sh,
                'question' => $shCs[$id]->applscTrq,
                'answer' => ArrayHelper::map($shCs[$id]->applscTrq->trainingAnswers, 'tra_id', 'tra_answer'),
                'id' => $id,
            ]);
        } else {
            $sh->appls_score = ApplSheetContent::find()->where(['applsc_appls_id' => $sh->appls_id, 'applsc_tra_variant' => 1])->count();
            $sh->appls_passed = ($sh->appls_score / ($sh->appls_score_max <= 0 ? 10000 : $sh->appls_score_max)) >= 0.6 ? 1 : 0;

            $date1 = new \DateTime();
            $date2 = new \DateTime($sh->appls_end);
            if ($date1 > $date2) {
                $sh->appls_passed = 0;
                /**
                 * Send mail -> begin
                 */
                foreach ($sh->applsPrs->contacts as $contact) {
                    if ($contact->conCont->cont_type==1) {
                        UserMail::sendMail(6, $contact->con_text,
                            [
                                'link' => $sh->applsPrs->prs_connect_link,
                                'login' => $sh->applsPrs->prs_connect_user,
                                'password' => $sh->applsPrs->prs_connect_pwd,
                                'fio' => $sh->applsPrs->prs_full_name,
                                'programma' => $sh->applsTrp->trp_name,
                                ':reestr' => $sh->applsApplr->applr_reestr,
                                ':personal' => $sh->applsApplr->applr_flag,
                            ]);
                    }
                }
                /**
                 * Send mail -> end
                 */
            }

            $sh->save();

            return $this->redirect(['result', 'id' => $sh->appls_id]);
        }
    }

    /**
     * Test Result
     * @param integer $id
     * @param integer $flag
     * @return mixed
     * @throws HttpException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionResult($id, $flag=0)
    {
        $model = $this->findModel($id);

        $request = ApplRequest::findOne($model->appls_applr_id);

        if ($flag==1) {
            $searchModel = new ApplSheetContentSearch();
            $dataProvider = $searchModel->search(['ApplSheetContentSearch' => ['applsc_appls_id' => $model->appls_id]]);

            return $this->render('result', [
                'model' => $model,
                'searchModel' => null, //$searchModel,
                'dataProvider' => $dataProvider,
            ]);

        }

        if ($model->applsApplr->applr_flag == 0) {
            foreach ($model->applsApplr->applRequestContents as $content) {
                if ($content->applrc_prs_id == $model->appls_prs_id
                    && $content->applrc_trp_id == $model->appls_trp_id
                ) {
                    $content->applrc_date_upk = date('Y-m-d');
                    $content->save();
                }
            }
        }

        if ($model->appls_passed == 1) {
            $applRequest = ApplRequest::findOne($model->appls_applr_id);
            $applRequest->updateMainDocumentWithoutPersonalEducation(true, $model->appls_prs_id);
        }

        $svdt_id = $model->appls_svdt_id;
        foreach ($model->applsApplr->applRequestContents as $content) {
            if ($content->applrc_prs_id == $model->appls_prs_id
                && $content->applrc_trp_id == $model->appls_trp_id
            ) {
                if (is_null($model->appls_svdt_id)) {
                    $svdt_id = $content->applrcSvc->svc_svdt_id;
                }
            }
        }
        $model->appls_svdt_id = $svdt_id;

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $model->appls_svdt_id])
            ->andWhere(['patt_id' => 7])
            ->andWhere(['pat_trt_id' => $model->appls_trt_id])
            ->one();

        if (!is_null($pattern)) {
            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;

            $file_name = Yii::getAlias('@app') . '/storage/' . $model->appls_date . '_' . $model->appls_prs_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($model->applSheetContents));

            $document->setValue('DOC_NUMBER', $model->appls_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->appls_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            // Ф.И.О. Слушателя:
            $document->setValue('PERSONA_FIO', $model->applsPrs->prs_full_name);
            // Программа обучения:
            $document->setValue('PROGRAMMA', $model->applsTrp->trp_name);

            // Итого вопросов:
            $document->setValue('QUESTION_QTY', $model->appls_score_max);
            //Итого верных ответов:
            $document->setValue('ANSWER_RIGHT_QTY', $model->appls_score);
            //Результат тестирования:
            $document->setValue('ANSWER_RIGHT_PERCENT', $model->answerRightPersent());

            $document->setValue('TEST_RESULT', $model->testResult());

            $i = 1;
            $shC = $model->applSheetContents;
            foreach ($shC as $k => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('QUЕSTION_NUM#' . $i, $obj->applsc_trq_id);
                $document->setValue('QUESTION#' . $i, $obj->applsc_trq_question);
                $document->setValue('ANSWER#' . $i, $obj->applsc_tra_answer);
                $document->setValue('ANSWER_CORRECT#' . $i, $obj->testResult());
                $i++;
            }
            $document->saveAs($file_name);

            $model->appls_file_name = $model->appls_date . '_' . $model->appls_prs_id . '_' . $pattern->pat_fname;
            $model->appls_file_data = file_get_contents($file_name);
            $model->appls_pat_id = $pattern->pat_id;
            $model->appls_reestr = $request->applr_reestr;
            $model->save();

            unlink($file_name);
            unlink($tmpl_name);
        }

        /**
         * Send mail -> begin
         */
        $table = '<table border="1">';
        $table .= '<tr>';
        $table .= '<th>№</th>';
        $table .= '<th>№ вопроса</th>';
        $table .= '<th>Вопрос</th>';
        $table .= '<th>Ответ</th>';
        $table .= '<th>Результат</th>';
        $table .= '</tr>';
        $i = 1;
        foreach ($model->applSheetContents as $cont) {

            $table .= '<tr>';
            $table .= "<td>{$i}</td>";
            $table .= "<td>{$cont->applsc_trq_id}</td>";
            $table .= "<td>{$cont->applsc_trq_question}</td>";
            $table .= "<td>{$cont->applsc_tra_answer}</td>";
            $table .= "<td>{$cont->testResult()}</td>";
            $table .= '</tr>';
            $i++;
        }

        $table .= '<tr>';
        $table .= "<td colspan=\"3\">Итого вопросов:</td></td>";
        $table .= "<td colspan=\"2\">{$model->appls_score_max}</td>";
        $table .= '</tr>';
        $table .= '<tr>';
        $table .= "<td colspan=\"3\">Итого верных ответов:</td>";
        $table .= "<td colspan=\"2\">{$model->appls_score}</td>";
        $table .= '</tr>';
        $table .= '<tr>';
        $table .= "<td colspan=\"3\">Результат тестирования:</td>";
        $table .= "<td colspan=\"2\">{$model->answerRightPersent()}%</td>";
        $table .= '</tr>';
        $table .= '<tr>';
        $table .= "<td colspan=\"3\">&nbsp;</td>";
        $table .= "<td colspan=\"2\">{$model->testResult()}</td>";
        $table .= '</tr>';

        $table .= '</table>';

        foreach ($model->applsPrs->contacts as $contact) {
            if ($contact->conCont->cont_type==1) {
                UserMail::sendMail(4, $contact->con_text,
                    [
                        'link' => $model->applsPrs->prs_connect_link,
                        'login' => $model->applsPrs->prs_connect_user,
                        'password' => $model->applsPrs->prs_connect_pwd,
                        'fio' => $model->applsPrs->prs_full_name,
                        'programma' => $model->applsTrp->trp_name,
                        'table' => $table,
                        ':reestr' => $model->applsApplr->applr_reestr,
                        ':personal' => $model->applsApplr->applr_flag,
                    ]);
            }
        }
        /**
         * Send mail -> end
         */

        /**
         * Send mail -> begin Yes/No
         */

        foreach ($model->applsPrs->contacts as $contact) {
            if ($contact->conCont->cont_type==1) {
                UserMail::sendMail($model->appls_passed > 0 ?  5 : 6, $contact->con_text,
                    [
                        'link' => $model->applsPrs->prs_connect_link,
                        'login' => $model->applsPrs->prs_connect_user,
                        'password' => $model->applsPrs->prs_connect_pwd,
                        'fio' => $model->applsPrs->prs_full_name,
                        'programma' => $model->applsTrp->trp_name,
                        ':reestr' => $model->applsApplr->applr_reestr,
                        ':personal' => $model->applsApplr->applr_flag,
                    ]);
            }
        }
        /**
         * Send mail -> end Yes/No
         */

        $searchModel = new ApplSheetContentSearch();
        $dataProvider = $searchModel->search(['ApplSheetContentSearch' => ['applsc_appls_id' => $model->appls_id]]);

        return $this->render('result', [
            'model' => $model,
            'searchModel' => null, //$searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Generate result file
     * @param int $ent_id
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws HttpException
     */
    public function actionGenerateFile($ent_id, $id)
    {
        $model = $this->findModel($id);

        $svdt_id = $model->appls_svdt_id;
        foreach ($model->applsApplr->applRequestContents as $content) {
            if ($content->applrc_prs_id == $model->appls_prs_id
                && $content->applrc_trp_id == $model->appls_trp_id
            ) {
                if (is_null($model->appls_svdt_id)) {
                    $svdt_id = $content->applrcSvc->svc_svdt_id;
                }
            }
        }

        $pattern = Pattern::find()
            ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
            ->where(['pat_svdt_id' => $svdt_id])
            ->andWhere(['patt_id' => 7])
            ->andWhere(['pat_trt_id' => $model->appls_trt_id])
            ->one();

        if (!is_null($pattern)) {

            $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;

            $file_name = Yii::getAlias('@app') . '/storage/' . $model->appls_date . '_' . $model->appls_prs_id . '_' . $pattern->pat_fname;
            file_put_contents($tmpl_name, $pattern->pat_fdata);

            $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);
            $document->cloneRow('num', count($model->applSheetContents));
            $document->setValue('DOC_NUMBER', $model->appls_number);

            $dateVal = \DateTime::createFromFormat('Y-m-d', $model->appls_date)->getTimestamp();
            $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
            $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
            $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

            // Ф.И.О. Слушателя:
            $document->setValue('PERSONA_FIO', $model->applsPrs->prs_full_name);
            // Программа обучения:
            $document->setValue('PROGRAMMA', $model->applsTrp->trp_name);

            // Итого вопросов:
            $document->setValue('QUESTION_QTY', $model->appls_score_max);
            //Итого верных ответов:
            $document->setValue('ANSWER_RIGHT_QTY', $model->appls_score);
            //Результат тестирования:
            $document->setValue('ANSWER_RIGHT_PERCENT', $model->answerRightPersent());
            //$document->setValue('ANSWER_RIGHT_PERCENT', $model->appls_score_max > 0.0 ? round(($model->appls_score / $model->appls_score_max) * 100) : 0);

            $document->setValue('TEST_RESULT', $model->testResult());
            //$document->setValue('TEST_RESULT', $model->appls_passed == 1 ? Yii::t('app', 'Correct Test') : Yii::t('app', 'Incorrect Test'));

            $i = 1;
            $shC = $model->applSheetContents;
            foreach ($shC as $k => $obj) {
                $document->setValue('num#' . $i, $i);
                $document->setValue('QUЕSTION_NUM#' . $i, $obj->applsc_trq_id);
                $document->setValue('QUESTION#' . $i, $obj->applsc_trq_question);
                $document->setValue('ANSWER#' . $i, $obj->applsc_tra_answer);
                $document->setValue('ANSWER_CORRECT#' . $i, $obj->testResult());
                $i++;
            }
            $document->saveAs($file_name);

            $model->appls_file_name = $model->appls_date . '_' . $model->appls_prs_id . '_' . $pattern->pat_fname;
            $model->appls_file_data = file_get_contents($file_name);
            $model->appls_pat_id = $pattern->pat_id;
            $model->save();

            unlink($file_name);
            unlink($tmpl_name);
        } else {
            throw new HttpException(500, yii::t('app', "Pattern::find(svdt_id={$model->appls_svdt_id}, patt_id=7, trt_id={$model->appls_trt_id}}) not found"));
        }
        return $this->redirect(['entity-frm/view', 'id' => $ent_id]);
    }

    /**
     * Get remaining time
     * @return mixed
     * @throws \Exception
     */
    public function actionTimer()
    {
        $id = Yii::$app->request->get('id', 0);
        if ($id > 0) {
            if ($sh = ApplSheet::findOne($id)) {
                $date = new \DateTime($sh->appls_end);
                $since =  $date->diff(new \DateTime());
                return $this->renderAjax('timer', [
                    'hour' => $since->h,
                    'min' => $since->i,
                    'sec' => $since->s,
                ]);

            }
        }
        return 'X';
    }

}
