<?php


namespace app\controllers;

use app\models\EntityClass;
use app\models\FinanceBookType;
use app\models\FinanceReport;
use app\models\FinanceReportDateMarker;
use app\models\PaymentType;
use app\models\ReportFinancesCreator;
use app\models\ReportFinancesSearch;
use app\models\SvcDocType;
use Yii;
use app\models\{
    AgreementAcc, AgreementAccSearch, Entity, Company, Agreement, AgreementStatus, AgreementAnnex, Excel_XML
};
use yii\web\{
    Controller, HttpException, NotFoundHttpException, UploadedFile
};
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class ReportFinancesController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {

        $reportDateMarker = new FinanceReportDateMarker();

        if (isset($_POST['refreshReport']) && !empty($_POST['refreshReport']) && $reportDateMarker->expired()) {
            $this->actionCreateReportData();
            $reportDateMarker->setCurrentDateMarker();
        }

        if ($reportDateMarker->expired()) {
            return $this->render('indexWithoutGrid');
        }

        $this->getView()->registerJsFile('/js/ReportFinancesController.js',  ['position' => yii\web\View::POS_END]);
        $searchModel = new ReportFinancesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['ReportFinancesParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payment_types' => ArrayHelper::map(PaymentType::find()->orderBy('pt_id')->all(), 'pt_id', 'pt_name'),
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            'entc_ids' => ArrayHelper::map(EntityClass::find()->orderBy('entc_id')->all(), 'entc_id', 'entc_name'),
            'fb_types' => ArrayHelper::map(FinanceBookType::find()->orderBy('fbt_id')->all(), 'fbt_id', 'fbt_name'),
            'svc_svdts' => ArrayHelper::map(SvcDocType::find()->orderBy('svdt_id')->all(), 'svdt_id', 'svdt_name'),
        ]);
    }


    public function actionCreateReportData()
    {

        Yii::$app->db->createCommand('truncate table finance_report')->execute();

        for ($i = 2017; $i <= 2025; $i++) {
            $creator = new ReportFinancesCreator();
            $creator->setDateStart(new \DateTime($i.'-01-01'));
            $creator->setDateEnd(new \DateTime($i.'-12-31'));
            $creator->addDataWithPayments();
        }

        $creator = new ReportFinancesCreator();
        $creator->addDataWithoutPayments();
    }

    /**
     * @return
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml ()
    {

        $payment_types = ArrayHelper::map(PaymentType::find()->orderBy('pt_id')->all(), 'pt_id', 'pt_name');
        $agreement_status = ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name');
        $entc_ids = ArrayHelper::map(EntityClass::find()->orderBy('entc_id')->all(), 'entc_id', 'entc_name');
        $fb_types = ArrayHelper::map(FinanceBookType::find()->orderBy('fbt_id')->all(), 'fbt_id', 'fbt_name');
        $svc_svdts = ArrayHelper::map(SvcDocType::find()->orderBy('svdt_id')->all(), 'svdt_id', 'svdt_name');
        $rfs = new ReportFinancesSearch();
        $query = $rfs->getQueryBySearch(Yii::$app->session['ReportFinancesParams']);

        $data = [
            [
                'Юр Лицо',
                'Менеджер',
                'Тип юрлица',
                'Тип',
                'Наименование услуги',
                'Наименование программы',
                'Вид выдаваемого документа',
                '№ приложения',
                'Цена',
                'Кол-во',
                'Сумма',
                'Себестоимость',
                'Сумма с/с',
                'Маржа',
                '№ счета',
                'Дата счета',
                'Статус счета',
                'Тип оплаты',
                '№ платежного поручения',
                'Дата платежного поручения/дата оплаты',
            ]
        ];
        foreach ($query->all() as &$model) {
            $data[] = [
                $model['ent_name'] ?? '',
                $model['agent_name'] ?? '',
                $entc_ids[$model['entc_id']] ?? '',
                $fb_types[$model['fb_fbt_id']] ?? '',
                $model['svc_name'] ?? '',
                $model['trp_name'] ?? '',
                $svc_svdts[$model['svc_svdt_id']] ?? '',
                $model['agra_number'] ?? '',
                $model['price'] ?? '',
                $model['qty'] ?? '',
                $model['sum'] ?? '',
                $model['ana_cost_a'] ?? '',
                $model['sum_ss'] ?? '',
                $model['marzha'] ?? '',
                $model['aga_number'] ?? '',
                $model['aga_date'] ?? '',
                $agreement_status[$model['aga_ast_id']] ?? '',
                $payment_types[$model['fb_pt_id']] ?? '',
                $model['fb_payment'] ?? '',
                $model['fb_date'] ?? '',
            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'ReportFinances';
        $xls->addArray($data);

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'ReportFinances.xls');
    }
}
