<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2019 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Ab;
use app\models\AgreementAcc;
use app\models\AgreementAccA;
use app\models\AgreementAccASearch;
use app\models\AgreementAnnex;
use app\models\AgreementAnnexSearch;
use app\models\Company;
use app\models\Excel_XML;
use app\models\FinanceBookStatus;
use app\models\FinanceBookType;
use app\models\Svc;
use Yii;
use app\models\FinanceBook;
use app\models\FinanceBookSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * FinanceBookController implements the CRUD actions for FinanceBook model.
 */
class FinanceBookController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all FinanceBook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/FinancebookController.js',  ['position' => yii\web\View::POS_END]);
        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        Yii::$app->session['FinancebookParams'] = Yii::$app->request->queryParams;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceBook model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new FinanceBook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinanceBook();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fb_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FinanceBook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fb_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FinanceBook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceBook the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = FinanceBook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Lists FinanceBook models.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatePay($id)
    {
        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        $account = AgreementAcc::findOne($id);

        return $this->render('update_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'account' => $account,
        ]);
    }

    /**
     * Updates fb_payment an existing FinanceBook model.
     * @param integer $id
     * @param string $value
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdatePayment($id, $value)
    {
        $model = $this->findModel($id);
        $model->fb_payment = $value;
        if ($model->save()) {
            return $model->fb_payment;
        } else {
            return "";
        }
    }

    /**
     * Updates fb_comment an existing FinanceBook model.
     * @param integer $id
     * @param string $value
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateComment($id, $value)
    {
        $model = $this->findModel($id);
        $model->fb_comment = $value;
        if ($model->save()) {
            return $model->fb_comment;
        } else {
            return "";
        }
    }

    /**
     * Check sum pay.
     * @param string $value
     * @return mixed
     */
    public function actionPayCheckSum($value)
    {
        $value = floatval($value);
        return $this->renderAjax('_ajax_pay_check_sum', [
            //'searchModel' => $searchModel,
            'value' => $value,
        ]);
    }

    /**
     * Check sum pay.
     * @param integer $id
     * @param string $value
     * @param string $basis
     * @return mixed
     */
    public function actionPayCalculate($id, $value, $basis)
    {
        $payArray = [];
        $line = FinanceBook::find()->where(['fb_aga_id' => $id])->all();
        $pay = floatval($value);

        foreach ($line as $key => $obj) {
            $obj->fb_payment = $basis;
            if (!isset($obj->fb_pay)) {

                // Сумма оплаты больше необходимого
                if (floatval($obj->fb_sum) <= $pay) {
                    $obj->fb_pay = $obj->fb_sum;
                    $payArray[$obj->fb_aca_id] = $obj->fb_pay;
                }

                // Сумма оплаты меньше необходимого
                if (floatval($obj->fb_sum) > $pay) {
                    $obj->fb_pay = $pay;
                    $payArray[$obj->fb_aca_id] = $pay;
                }

                if ((floatval($obj->fb_sum) > floatval($obj->fb_pay))) {

                    $p = $pay;
                    if ($obj->fb_tax > 0.001) {
                        if (strtotime($obj->fb_date) >= strtotime(date('2019-01-01'))) {
                            $p_tax = round($pay/1.2 * 0.2, 2);
                        } else {
                            $p_tax = round($pay/1.18 * 0.18, 2);
                        }
                    } else {
                        $p_tax = 0;
                    }

                    $fb = new FinanceBook();
                    $fb->fb_fb_id = $obj->fb_id;
                    $fb->fb_aca_id = $obj->fb_aca_id;
                    $fb->fb_svc_id = $obj->fb_svc_id;
                    $fb->fb_fbt_id = $obj->fb_fbt_id;
                    $fb->fb_ab_id = $obj->fb_ab_id;
                    $fb->fb_comp_id = $obj->fb_comp_id;
//                    $fb->fb_sum = $obj->fb_sum - $p;
                    $fb->fb_tax = $obj->fb_tax - $p_tax;
                    $fb->fb_aga_id = $obj->fb_aga_id;
                    $fb->fb_acc_id = $obj->fb_acc_id;
                    $fb->fb_fbs_id = $obj->fb_fbs_id;
                    $fb->fb_date = $obj->fb_date;

//                    $obj->fb_sum = $p;
                    $obj->fb_tax = $p_tax;

//                    if ($fb->save()) {
//                        $obj->save(false);
//                    }

                }

                $pay -= floatval($obj->fb_pay);
            }
//            $obj->save(false);
            if ($pay <= 0.001)
                break;
        }

        Yii::$app->session['EntityFrmPay'] = $payArray;

        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        return $this->renderAjax('_grid_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);


    }

    /**
     * Cancel pay.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionPayCancel($id)
    {
        $fb = FinanceBook::find()->where(['fb_aga_id' => $id])->all();
        foreach ($fb as $key => $obj) {
            $obj->delete();
        }

        $acc = AgreementAcc::find()->where(['aga_id' => $id])->one();
        $acc->aga_ast_id = 1;
        $acc->save(false);

        foreach (AgreementAccA::find()->where(['aca_aga_id' => $id])->all() as $key => $obj) {
            if (!$fb = FinanceBook::find()->where(['fb_aca_id' => $obj->aca_id])->one()) {
                $fb = new FinanceBook();
            }
            $fb->fb_svc_id = $obj->acaAna->ana_svc_id;
            $fb->fb_aca_id = $obj->aca_id;
            $fb->fb_fbt_id = 1;
            $fb->fb_ab_id = $obj->acaAga->aga_ab_id;
            $fb->fb_comp_id = $obj->acaAga->aga_comp_id;
            $fb->fb_sum = $obj->aca_price * $obj->aca_qty;
            $fb->fb_tax = $obj->aca_tax * $obj->aca_qty;
            $fb->fb_aga_id = $obj->aca_aga_id;
            $fb->fb_acc_id = $obj->acaAga->agaAgr->agr_acc_id;
            $fb->fb_fbs_id = 1;
            $fb->fb_date = $obj->acaAga->aga_date;
            $fb->save();
        }

        Yii::$app->session['EntityFrmPay'] = [];

        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        return $this->renderAjax('_grid_pay', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);

    }

    /**
     * Update pay Sum.
     * @param integer $id
     * @param string $value
     * @return mixed
     */
    public function actionUpdatePaySum($id, $value)
    {
        $payArray = Yii::$app->session['EntityFrmPay'];
        $payArray[$id] = floatval($value);
        Yii::$app->session['EntityFrmPay'] = $payArray;
        $value = Yii::$app->session['EntityFrmPay'][$id];

        return $this->renderAjax('_ajax_pay_check_sum', [
            'value' => $value,
        ]);
    }

    /**
     * Save sum pay.
     * @param integer $id
     * @param string $basis
     * @param string $date
     * @return mixed
     */
    public function actionSaveCalculate($id, $basis, $date)
    {

        $payArray = Yii::$app->session['EntityFrmPay'];

        foreach ($payArray as $k => $value) {
            $line = FinanceBook::find()->where(['fb_aca_id' => $k])->all();
            $pay = floatval($value);

            foreach ($line as $key => $obj) {
                if (!isset($obj->fb_pay)) {
                    // Сумма оплаты больше необходимого
                    if (floatval($obj->fb_sum) <= $pay) {
                        $obj->fb_pay = $obj->fb_sum;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    // Сумма оплаты меньше необходимого
                    if (floatval($obj->fb_sum) > $pay) {
                        $obj->fb_pay = $pay;
                        $obj->fb_payment = $basis;
                        $obj->fb_date = $date;
                    }

                    if ((floatval($obj->fb_sum) > floatval($obj->fb_pay))) {

                        $p = $pay;
                        if ($obj->fb_tax > 0.001) {
                            if (strtotime($obj->fb_date) >= strtotime(date('2019-01-01'))) {
                                $p_tax = round($pay/1.2 * 0.2, 2);
                            } else {
                                $p_tax = round($pay/1.18 * 0.18, 2);
                            }
                        } else {
                            $p_tax = 0;
                        }

                        $fb = new FinanceBook();
                        $fb->fb_fb_id = $obj->fb_id;
                        $fb->fb_aca_id = $obj->fb_aca_id;
                        $fb->fb_svc_id = $obj->fb_svc_id;
                        $fb->fb_fbt_id = $obj->fb_fbt_id;
                        $fb->fb_ab_id = $obj->fb_ab_id;
                        $fb->fb_comp_id = $obj->fb_comp_id;
                        $fb->fb_sum = $obj->fb_sum - $p;
                        $fb->fb_tax = $obj->fb_tax - $p_tax;
                        $fb->fb_aga_id = $obj->fb_aga_id;
                        $fb->fb_acc_id = $obj->fb_acc_id;
                        $fb->fb_fbs_id = $obj->fb_fbs_id;

                        $obj->fb_sum = $p;
                        $obj->fb_tax = $p_tax;

                        if ($fb->save()) {
                            $obj->save(false);
                        }
                    }

                    $pay -= floatval($obj->fb_pay);
                }
                $obj->save(false);
                if ($pay <= 0.001)
                    break;
            }
        }

        AgreementAcc::findOne($id)->updateStatus();

        Yii::$app->session['EntityFrmPay'] = [];

        $searchModel = new FinanceBookSearch();
        $dataProvider = $searchModel->search(['FinanceBookSearch' => ['fb_aga_id' => $id]]);

        return $this->renderAjax('_grid_pay', [
            'dataProvider' => $dataProvider,
            'payArray' => Yii::$app->session['EntityFrmPay'],
        ]);
    }

    /**
     *
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionExportXml() {

        $query = FinanceBook::find()
            ->leftJoin(Svc::tableName(), 'fb_svc_id = svc_id')
            ->leftJoin(Ab::tableName(), 'fb_ab_id = ab_id')
            ->leftJoin(AgreementAcc::tableName(), 'fb_aga_id = aga_id')
            ->leftJoin(FinanceBookType::tableName(), 'fbt_id = fb_fbt_id')
            ->leftJoin(Company::tableName(), 'comp_id = fb_comp_id')
            ->leftJoin(FinanceBookStatus::tableName(), 'fbs_id = fb_fbs_id')
            ->joinWith(['fbAcc' => function($query) { $query->from(['fbAcc' => 'account']);}])
            ->joinWith(['fbPt' => function($query) { $query->from(['fbPt' => 'payment_type']);}])
        ;

        $fb = new FinanceBookSearch();
        $fb->load(Yii::$app->session['FinancebookParams']);


        // grid filtering conditions
        $query->andFilterWhere([
            'fb_id' => $fb->fb_id,
            'fb_svc_id' => $fb->fb_svc_id,
            'fb_fbt_id' => $fb->fb_fbt_id,
            'fb_ab_id' => $fb->fb_ab_id,
            'fb_comp_id' => $fb->fb_comp_id,
            'fb_aca_id' => $fb->fb_aca_id,
            'fb_aga_id' => $fb->fb_aga_id,
            'fb_acc_id' => $fb->fb_acc_id,
            'fb_fbs_id' => $fb->fb_fbs_id,
            'fb_pt_id' => $fb->fb_pt_id,
        ]);

        $query
            ->andFilterWhere(['like', 'ab_name', $fb->ab_name])
            ->andFilterWhere(['like', 'svc_name', $fb->svc_name])
            ->andFilterWhere(['like', 'aga_number', $fb->aga_number])

            ->andFilterWhere(['like', 'fbAcc.acc_number', $fb->acc_number])
            ->andFilterWhere(['like', 'fbPt.pt_id', $fb->pt_id])

            ->andFilterWhere(['like', 'fb_sum', $fb->fb_sum])
            ->andFilterWhere(['like', 'fb_tax', $fb->fb_tax])
            ->andFilterWhere(['like', 'fb_pay', $fb->fb_pay])
            ->andFilterWhere(['like', 'fb_date', $fb->fb_date])
            ->andFilterWhere(['like', 'fb_payment', $fb->fb_payment])
            ->andFilterWhere(['like', 'fb_comment', $fb->fb_comment])
            ->andFilterWhere(['like', 'fb_create_user', $fb->fb_create_user])
            ->andFilterWhere(['like', 'fb_create_time', $fb->fb_create_time])
            ->andFilterWhere(['like', 'fb_create_ip', $fb->fb_create_ip])
            ->andFilterWhere(['like', 'fb_update_user', $fb->fb_update_user])
            ->andFilterWhere(['like', 'fb_update_time', $fb->fb_update_time])
            ->andFilterWhere(['like', 'fb_update_ip', $fb->fb_update_ip])
        ;

        $data = [];
        foreach ($query->all() as &$model) {
            $data[] = [
                $model->getSvc_name(),
                $model->fbFbt->fbt_name,
                $model->getAb_name(),
                $model->fbComp->comp_name,
                $model->fb_sum,
                $model->fb_tax,
                $model->aga_number,
                $model->fbAcc->acc_number,
                $model->fbFbs->fbs_name,
                $model->fbPt->pt_name,
                $model->fb_date,
                $model->fb_payment,

            ];
            unset($model);
        }

        $xls = new Excel_XML();
        $xls->WorksheetTitle = 'Financebook';
        $xls->addArray($data);

        return Yii::$app->response->sendContentAsFile($xls->generateXML0(), 'Financebook.xls');

    }

}
