<?php

namespace app\controllers;

use Yii;
use app\models\Ab;
use app\models\AbSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class BaseController extends Controller
{
    /**
     * @inheritdoc
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $request = Yii::$app->request;

        if (!$request->isAjax) {
            $this->getView()->registerJsFile('/js/jQuery.js', ['position' => yii\web\View::POS_HEAD]);
            $this->getView()->registerJsFile('/js/backbone/underscore.js', ['position' => yii\web\View::POS_HEAD]);
            $this->getView()->registerJsFile('/js/backbone/backbone.min.js', ['position' => yii\web\View::POS_HEAD]);
            $this->getView()->registerJsFile('/js/backbone/backbone.radio.js', ['position' => yii\web\View::POS_HEAD]);
            $this->getView()->registerJsFile('/js/backbone/marionette.min.js', ['position' => yii\web\View::POS_HEAD]);
            $this->getView()->registerJsFile('/js/commonApp.js', ['position' => yii\web\View::POS_HEAD]);
        }

        $ba = parent::beforeAction($action);

        return $ba;
    }

}
