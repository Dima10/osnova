<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\ApplEnd;
use app\models\Constant;
use app\models\Pattern;
use app\models\PatternType;
use Yii;
use app\models\ApplBlank;
use app\models\ApplBlankSearch;
use yii\base\DynamicModel;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ApplBlankController implements the CRUD actions for ApplBlank model.
 */
class ApplBlankController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all ApplBlank models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->getView()->registerJsFile('/js/ApplBlankController.js',  ['position' => yii\web\View::POS_END]);

        $searchModel = new ApplBlankSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplBlank model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ApplBlank model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception if not find Pattern
     */
    public function actionCreate()
    {
        $dynModel = DynamicModel::validateData(['date1', 'date2'],
            [
                [['date1', 'date2'], 'string'],
            ]
        );

        if (Yii::$app->request->isPost) {
            $dynModel->load(Yii::$app->request->post());

            ApplBlank::deleteAll(['between', 'applblk_date', $dynModel->date1, $dynModel->date2]);

            $ends = ApplEnd::find()->where(['between', 'apple_date', $dynModel->date1, $dynModel->date2])->all();

            $pattern = Pattern::find()
                ->innerJoin(PatternType::tableName(), 'pat_patt_id = patt_id')
                ->where(['patt_id' => 14])
                ->one();
            if ($pattern == null) {
                throw new HttpException(500, yii::t('app', '$pattern = Pattern::find() not found'));
            }

            foreach ($ends as $key => $end) {
                if (!$model = ApplBlank::find()->where(['applblk_date' => $end->apple_date])->one())
                {
                    $model = new ApplBlank();
                    $model->applblk_date = $end->apple_date;

                    $model->applblk_number = \DateTime::createFromFormat('Y-m-d', $end->apple_date)->format('Ymd') . '-'.$pattern->pat_code;
                }


                if ($end->apple_svdt_id == 1) { //1 - Удостоверение о повышении квалификации
                    $model->applblk_qty_1 = count($end->applEndContents);
                }
                if ($end->apple_svdt_id == 2) { // 2 - Квалификационный аттестат
                    $model->applblk_qty_2 = count($end->applEndContents);
                }
                if ($end->apple_svdt_id == 4) { // 4 - Удостоверение по охране труда
                    $model->applblk_qty_3 = count($end->applEndContents);
                }
                if ($end->apple_svdt_id == 5) { // 5 - Удостоверение по пожарно-техническому минимуму
                    $model->applblk_qty_4 = count($end->applEndContents);
                }
                if ($end->apple_svdt_id == 3) { // 3 - Диплом о профессиональной переподготовке
                    $model->applblk_qty_5 = count($end->applEndContents);
                }

                if (!$model->save()) {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

            }

            foreach (ApplBlank::find()->where(['between', 'applblk_date', $dynModel->date1, $dynModel->date2])->all() as $k => $model) {

                $tmpl_name = Yii::getAlias('@app') . '/storage/' . $pattern->pat_fname;
                $file_name = Yii::getAlias('@app') . '/storage/' . $model->applblk_date . '_' . $pattern->pat_fname;
                file_put_contents($tmpl_name, $pattern->pat_fdata);

                $document = new \PhpOffice\PhpWord\TemplateProcessor($tmpl_name);

                $dateVal = \DateTime::createFromFormat('Y-m-d', $model->applblk_date)->getTimestamp();
                $document->setValue('DOC_DATE_DD', strftime('%d', $dateVal));
                $document->setValue('DOC_DATE_MONTH', Constant::MONTHS[strftime('%B', $dateVal)]);
                $document->setValue('DOC_DATE_YYYY', strftime('%Y', $dateVal));

                $document->setValue('UPK_QTY', $model->applblk_qty_1 ?? 0); //1 - Удостоверение о повышении квалификации
                $document->setValue('KA_QTY', $model->applblk_qty_2 ?? 0); // 2 - Квалификационный аттестат
                $document->setValue('OT_QTY', $model->applblk_qty_3 ?? 0); // 4 - Удостоверение по охране труда
                $document->setValue('PTM_QTY', $model->applblk_qty_4 ?? 0); // 5 - Удостоверение по пожарно-техническому минимуму
                $document->setValue('PP_QTY', $model->applblk_qty_5 ?? 0); // 3 - Диплом о профессиональной переподготовке

                $document->saveAs($file_name);

                $model->applblk_file_name = $model->applblk_date . '_' . $pattern->pat_fname;
                $model->applblk_file_data = file_get_contents($file_name);
                $model->applblk_pat_id = $pattern->pat_id;

                if (!$model->save()) {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }

                unlink($file_name);
                unlink($tmpl_name);

            }

        }

        return $this->render('create_new', ['model' => $dynModel]);

    }

    /**
     * Updates an existing ApplBlank model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->applblk_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ApplBlank model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ApplBlank model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApplBlank the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ApplBlank::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Redirect to download page.
     * @param integer $id
     * @return mixed
     */
    public function actionDownload($id)
    {
        return $this->redirect(['download-f', 'id' => $id]);
    }

    /**
     * Download file from existing Agreement model.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownloadF($id)
    {
        $model = ApplBlank::findOne($id);
        return Yii::$app->response->sendContentAsFile($model->applblk_file_data, $model->applblk_file_name);
    }

}
