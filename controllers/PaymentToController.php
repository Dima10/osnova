<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use Yii;

use app\models\Account;
use app\models\PaymentTo;
use app\models\PaymentToSearch;

use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * PaymentToController implements the CRUD actions for PaymentTo model.
 */
class PaymentToController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all PaymentTo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentToSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentTo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new PaymentTo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentTo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->payt_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PaymentTo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->payt_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentTo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentTo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentTo the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = PaymentTo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }

    /**
     * Updates an existing PaymentTo model quantity.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateQty($id, $value)
    {
        if (!$model = PaymentTo::cacheObject($id)) {
            $model = $this->findModel($id);
        }
        $model->payt_svc_qty = $value;
        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->payt_svc_qty,
        ]);
    }

    /**
     * Updates an existing PaymentTo model price.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdatePrice($id, $value)
    {
        if (!$model = PaymentTo::cacheObject($id)) {
            $model = $this->findModel($id);
        }
        $model->payt_svc_price = $value;
        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->payt_svc_price,
        ]);
    }

    /**
     * Updates an existing PaymentTo model date pay.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateDatePay($id, $value)
    {
        if (!$model = PaymentTo::cacheObject($id)) {
            $model = $this->findModel($id);
        }
        $model->payt_date_pay = $value;
        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_date', [
            'value' => $model->payt_date_pay,
        ]);
    }

    /**
     * Get sum from existing PaymentTo model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateSum($id)
    {
        if (!$model = PaymentTo::cacheObject($id)) {
            $model = $this->findModel($id);
        }
        return $this->renderAjax('_ajax_value', [
            'value' => $model->payt_svc_sum,
        ]);
    }

    /**
     * Updates an existing PaymentTo model account.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateFromAccount($id, $value)
    {
        if (!$model = PaymentTo::cacheObject($id)) {
            $model = $this->findModel($id);
        }

        $model->payt_from_acc_id = (int) $value;
        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_from_account', [
            'account' => ArrayHelper::map($model->fromAccounts, 'acc_id', 'acc_number'),
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaymentTo model account.
     * @param integer $id
     * @param integer $payt_id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateToAccount($id, $payt_id, $value)
    {
        if (!$model = PaymentTo::cacheObject($payt_id)) {
            $model = $this->findModel($payt_id);
        }
        $model->payt_to_acc_id = (int) $value;
        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_to_account', [
            'account' => ArrayHelper::map($model->toAccounts, 'acc_id', 'acc_number'),
            'model' => $model,
            'id' => $id,
        ]);
    }

    /**
     * Updates an existing PaymentTo model account.
     * @param integer $id
     * @param integer $payt_id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateToAb($id, $payt_id, $value)
    {
        if (!$model = PaymentTo::cacheObject($payt_id)) {
            $model = $this->findModel($payt_id);
        }
        $model->payt_to_ab_id = (int) $value;
        $model->payt_to_acc_id = Account::find()->where(['acc_ab_id' => $model->payt_to_ab_id])->one()->acc_id;

        PaymentTo::cacheUpdate($model);

        return $this->renderAjax('_ajax_to_ab', [
            'ab' => ArrayHelper::map($model->toAbs, 'ab_id', 'ab_name'),
            'model' => $model,
            'id' => $id,
        ]);
    }


    /**
     * Updates an existing PaymentTo model account.
     * @param integer $id
     * @param integer $payt_id
     * @param integer $value
     */
    public function actionUpdatePaymentType($id, $payt_id, $value)
    {
        if (!$model = PaymentTo::cacheObject($payt_id)) {
            $model = $this->findModel($payt_id);
        }
        $model->payt_pt_id = (int) $value;

        PaymentTo::cacheUpdate($model);

    }

    /**
     * Get Account from existing PaymentTo model.
     * @param integer $id
     * @param integer $payt_id
     * @return mixed
     */
    public function actionGetToAccount($id, $payt_id)
    {
        if (!$model = PaymentTo::cacheObject($payt_id)) {
            $model = $this->findModel($payt_id);
        }

        return $this->renderAjax('_ajax_to_account', [
            'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $model->payt_to_ab_id])->all(), 'acc_id', 'acc_number'),
            'model' => $model,
            'id' => $id,
        ]);

    }

}
