<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2017-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Agreement;
use app\models\AgreementStatus;
use Yii;
use app\models\AgreementAdd;
use app\models\AgreementAddSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AgreementAddController implements the CRUD actions for AgreementAdd model.
 */
class AgreementAddController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all AgreementAdd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgreementAddSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
        ]);
    }

    /**
     * Displays a single AgreementAdd model.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new AgreementAdd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AgreementAdd();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agadd_file_data')) != null) {
                $model->agadd_file_name = $file->name;
                $model->agadd_file_data = file_get_contents($file->tempName);
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agadd_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /**
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agadd_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
            ]);
        }
         */
    }

    /**
     * Updates an existing AgreementAdd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (($file = UploadedFile::getInstance($model, 'agadd_data')) != null) {
                $model->agadd_file_name = $file->name;
                $model->agadd_file_data = file_get_contents($file->tempName);
            } else {
                $model->agadd_file_name = $model->oldAttributes['agadd_file_name'];
                $model->agadd_file_data = $model->oldAttributes['agadd_file_data'];
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->agadd_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                    'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
                'agreement_status' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ]);
        }

        /**
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->agadd_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'agreement' => ArrayHelper::map(Agreement::find()->select('agr_id, agr_number')->all(), 'agr_id', 'agr_number'),
            ]);
        }
         */
    }

    /**
     * Deletes an existing AgreementAdd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AgreementAdd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgreementAdd the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgreementAdd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
