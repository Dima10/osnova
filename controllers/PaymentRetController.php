<?php
/**
 * @author Ruslan Bondarenko (Dnipro) r.i.bondarenko@gmail.com
 * @copyright Copyright (C) 2016-2017 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 */

namespace app\controllers;

use app\models\Account;
use app\models\Ab;
use app\models\Company;
use Yii;
use app\models\PaymentRet;
use app\models\PaymentRetSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * PaymentRetController implements the CRUD actions for PaymentRet model.
 */
class PaymentRetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' =>
                            function ($rule, $action) {
                                return \app\models\AcAccess::checkAction($action);
                            },
                    ],
                ],
            ],        
        ];
    }

    /**
     * Lists all PaymentRet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentRetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentRet model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new PaymentRet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentRet();

        $ab = [0 => '<>'];
        $comp = ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name');
        $acc = ArrayHelper::map(Account::find()->where(['acc_ab_id' => 0])->all(), 'acc_id', 'acc_number');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ptr_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'ab' => $ab,
                'comp' => $comp,
                'acc' => $acc,
            ]);
        }
    }

    /**
     * Updates an existing PaymentRet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $ab = ArrayHelper::map(Ab::find()->where(['ab_id' => $model->ptr_ab_id])->all(), 'ab_id', 'ab_name');
        $comp = ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name');
        $acc = ArrayHelper::map(Account::find()->where(['acc_ab_id' => $model->ptr_ab_id])->all(), 'acc_id', 'acc_number');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ptr_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'ab' => $ab,
                'comp' => $comp,
                'acc' => $acc,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentRet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentRet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentRet the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = PaymentRet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }


    /**
     * Updates an existing PaymentRet model sum.
     * @param string $id
     * @param integer $ptr_id
     * @param number $value
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateSum($id, $ptr_id, $value)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }
        $model->ptr_svc_sum = $value;
        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->ptr_svc_sum,
        ]);
    }

    /**
     * Updates an existing PaymentRet model tax.
     * @param string $id
     * @param integer $ptr_id
     * @param number $value
     * @return mixed
     */
    public function actionUpdateTax($id, $ptr_id, $value)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }
        $model->ptr_svc_tax = $value;
        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->ptr_svc_tax,
        ]);
    }

    /**
     * Updates an existing PaymentRet model date pay.
     * @param string $id
     * @param integer $ptr_id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateDatePay($id, $ptr_id, $value)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }
        $model->ptr_date_pay = $value;
        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->ptr_date_pay,
        ]);
    }

    /**
     * Updates an existing PaymentRet model account.
     * @param integer $id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateAccount($id, $value)
    {
        if (!$model = PaymentRet::cacheObject($id)) {
            $model = $this->findModel($id);
        }

        $model->ptr_acc_id = (int) $value;
        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_account', [
            'account' => ArrayHelper::map($model->accounts, 'acc_id', 'acc_number'),
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaymentTo model ab_id.
     * @param integer $id
     * @param integer $ptr_id
     * @param integer $value
     * @return mixed
     */
    public function actionUpdateAb($id, $ptr_id, $value)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }
        $model->ptr_ab_id = (int) $value;
        $model->ptr_acc_id = Account::find()->where(['acc_ab_id' => $model->ptr_ab_id])->one()->acc_id;

        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_ab', [
            'ab' => ArrayHelper::map($model->toAbs, 'ab_id', 'ab_name'),
            'model' => $model,
            'id' => $id,
        ]);
    }


    /**
     * Updates an existing PaymentRet model paymentType.
     * @param integer $id
     * @param integer $ptr_id
     * @param integer $value
     */
    public function actionUpdatePaymentType($id, $ptr_id, $value)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }
        $model->ptr_pt_id = $value;

        PaymentRet::cacheUpdate($model);

        return $this->renderAjax('_ajax_value', [
            'value' => $model->ptr_pt_id,
        ]);
    }

    /**
     * Get Account from existing PaymentRet model.
     * @param integer $id
     * @param integer $ptr_id
     * @return mixed
     */
    public function actionGetAccount($id, $ptr_id)
    {
        if (!$model = PaymentRet::cacheObject($ptr_id)) {
            $model = $this->findModel($ptr_id);
        }

        return $this->renderAjax('_ajax_account', [
            'account' => ArrayHelper::map(Account::find()->where(['acc_ab_id' => $model->ptr_ab_id])->all(), 'acc_id', 'acc_number'),
            'model' => $model,
            'id' => $id,
        ]);

    }

}
