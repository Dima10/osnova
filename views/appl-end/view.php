<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEnd */

$this->title = $model->apple_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-end-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->apple_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->apple_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'apple_id',
            'apple_applsx_id',
            'apple_number',
            'apple_date',
            [
                'attribute' => 'applsx_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_create_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_create_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]) ?>

</div>
