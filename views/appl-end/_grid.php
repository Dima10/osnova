<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplEndSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */

?>
<div class="appl-end-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'apple_id',
            [
                'attribute' => 'applsx_number',
                'value' =>
                    function ($data) {
                        return $data->appleApplsx->applsx_number;
                    },
                'label' => Yii::t('app', 'Apple Applsx ID'),
            ],
            'apple_number',
            'apple_date',
            [
                'attribute' => 'apple_trt_id',
                'value' =>
                    function ($data) {
                        return $data->appleTrt->trt_name;
                    },
                'label' => Yii::t('app', 'Apple Trt ID'),
                'filter' => $trt,
            ],
            [
                'attribute' => 'apple_file_name',
                'label' => Yii::t('app', 'Apple File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->apple_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->apple_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'apple_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'apple_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
