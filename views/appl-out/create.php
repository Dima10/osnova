<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplOut */

$this->title = Yii::t('app', 'Create Appl Out');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Outs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-out-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
