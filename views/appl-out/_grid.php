<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */
/* @var $svdt array */

?>
<div class="appl-out-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applout_id',
            'applout_number',
            'applout_date',

            [
                'attribute' => 'applout_trt_id',
                'value' =>
                    function ($data) {
                        return $data->apploutTrt->trt_name;
                    },
                'label' => Yii::t('app', 'Applout Trt ID'),
                'filter' => $trt,
            ],
            [
                'attribute' => 'applout_svdt_id',
                'value' =>
                    function ($data) {
                        return $data->apploutSvdt->svdt_name;
                    },
                'label' => Yii::t('app', 'Applout Svdt ID'),
                'filter' => $svdt,
            ],
            [
                'attribute' => 'applout_file_name',
                'label' => Yii::t('app', 'Applout File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applout_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applout_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'applout_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applout_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
