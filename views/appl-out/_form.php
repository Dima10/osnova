<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-out-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applout_number')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'applout_date')->textInput(); ?>

    <?php echo $form->field($model, 'applout_trt_id')->textInput(); ?>

    <?php echo $form->field($model, 'applout_svdt_id')->textInput(); ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applout_pat_id')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applout_pat_id')->textInput()
    ?>

    <?php echo $form->field($model, 'applout_file_data')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
