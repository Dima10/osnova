<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */
/* @var $svdt array */

$this->title = Yii::t('app', 'Appl Outs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-out-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Out'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'trt' => $trt,
        'svdt' => $svdt,
    ]) ?>

</div>
