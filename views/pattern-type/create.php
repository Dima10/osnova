<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PatternType */

$this->title = Yii::t('app', 'Create Pattern Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
