<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SvcProgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-prog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sprog_id') ?>

    <?= $form->field($model, 'sprog_svc_id') ?>

    <?= $form->field($model, 'sprog_trp_id') ?>

    <?= $form->field($model, 'sprog_create_user') ?>

    <?= $form->field($model, 'sprog_create_time') ?>

    <?php // echo $form->field($model, 'sprog_create_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
