<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcProgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $svc array */
/* @var $prog array */

?>
<div class="svc-prog-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'sprog_id',
            
            [
                'attribute' => 'sprog_svc_id',
                'label' => Yii::t('app', 'Sprog Svc ID'),
                'value' => function ($data) { return $data->sprogSvc->svc_name; },
                'filter' => $svc,
            ],
            [
                'attribute' => 'sprog_trp_id',
                'label' => Yii::t('app', 'Sprog Trp ID'),
                'value' => function ($data) { return $data->sprogTrp->trp_name; },
                'filter' => $prog,
            ],

            [
                'attribute' => 'sprog_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'sprog_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'sprog_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
