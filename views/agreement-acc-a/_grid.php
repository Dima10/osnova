<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccASearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="agreement-acc-a-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'aca_id',
            'aca_aga_id',
            'aca_ana_id',

            'aca_qty',
            'aca_price',
            'aca_tax',

            [
                'attribute' => 'aca_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
