<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAccASearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-acc-a-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'aca_id') ?>

    <?= $form->field($model, 'aca_aga_id') ?>

    <?= $form->field($model, 'aca_svc_id') ?>

    <?= $form->field($model, 'aca_trp_id') ?>

    <?= $form->field($model, 'aca_cost_a') ?>

    <?php // echo $form->field($model, 'aca_price_a') ?>

    <?php // echo $form->field($model, 'aca_qty') ?>

    <?php // echo $form->field($model, 'aca_price') ?>

    <?php // echo $form->field($model, 'aca_tax') ?>

    <?php // echo $form->field($model, 'aca_create_user') ?>

    <?php // echo $form->field($model, 'aca_create_time') ?>

    <?php // echo $form->field($model, 'aca_create_ip') ?>

    <?php // echo $form->field($model, 'aca_update_user') ?>

    <?php // echo $form->field($model, 'aca_update_time') ?>

    <?php // echo $form->field($model, 'aca_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
