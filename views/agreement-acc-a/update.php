<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAccA */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Acc A'),
]) . $model->aca_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Acc As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->aca_id, 'url' => ['view', 'id' => $model->aca_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-acc-a-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
