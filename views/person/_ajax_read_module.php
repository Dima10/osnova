<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $id string */
/* @var $ready boolean */

?>

<?= Html::a(Yii::t('app', 'Ознакомлен'),'#', ['id' => $id, 'class' => ($ready ? 'btn btn-success' : 'btn btn-danger'), 'onClick' => 'readClick(this);']) ?>


