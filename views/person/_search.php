<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
         'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        

    ]); ?>


    <?php //echo $form->field($model, 'prs_id') ?>

    <?= $form->field($model, 'prs_last_name') ?>

    <?= $form->field($model, 'prs_first_name') ?>

    <?= $form->field($model, 'prs_middle_name') ?>

    <?php //echo $form->field($model, 'prs_full_name') ?>

    <?php
        if (!\app\models\User::isSpecAdmin()) {
            echo $form->field($model, 'prs_inn');
        }
    ?>

    <?php // echo $form->field($model, 'prs_birth_date') ?>

    <?php // echo $form->field($model, 'prs_pass_sex') ?>

    <?php // echo $form->field($model, 'prs_pass_serial') ?>

    <?php // echo $form->field($model, 'prs_pass_number') ?>

    <?php // echo $form->field($model, 'prs_pass_issued_by') ?>

    <?php // echo $form->field($model, 'prs_pass_date') ?>

    <?php // echo $form->field($model, 'prs_create_user') ?>

    <?php // echo $form->field($model, 'prs_create_time') ?>

    <?php // echo $form->field($model, 'prs_create_ip') ?>

    <?php // echo $form->field($model, 'prs_update_user') ?>

    <?php // echo $form->field($model, 'prs_update_time') ?>

    <?php // echo $form->field($model, 'prs_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
