<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplCommandContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-trp-grid">

<?php

//Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'class' => 'yii\grid\SerialColumn',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, []);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = yii\helpers\Url::to(['training-index', 'id' => $model->trp_id]);
                            return $url;
                        }
                        return '#';
                    }

            ],

            'trp_id',
            [
                'attribute' => 'applrc_applr_id',
                'label' => Yii::t('app', 'Applrc Applr ID'),
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->applrcApplrId;
                },
            ],
            'trp_name',
            'trp_code',
            'trp_hour',
            //'trp_test_question',
            [
                'attribute' => 'trp_dataA1_name',
                'label' => Yii::t('app', 'Trp Data A1 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataA1_name, yii\helpers\Url::toRoute(['training-prog/download-a1', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataA2_name',
                'label' => Yii::t('app', 'Trp Data A2 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataA2_name, yii\helpers\Url::toRoute(['training-prog/download-a2', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataB1_name',
                'label' => Yii::t('app', 'Trp Data B1 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataB1_name, yii\helpers\Url::toRoute(['training-prog/download-b1', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataB2_name',
                'label' => Yii::t('app', 'Trp Data B2 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataB2_name, yii\helpers\Url::toRoute(['training-prog/download-b2', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

//Pjax::end();


?>

</div>
