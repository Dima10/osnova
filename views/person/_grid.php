<?php

use app\models\Ab;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            'prs_id',
            'prs_full_name',
            'prs_last_name',
            'prs_first_name',
            'prs_middle_name',
            [
                'attribute' => 'prs_pass_sex',
                'value' => function ($data) {
                    if($data->prs_pass_sex === 0){
                        return 'М';
                    } elseif($data->prs_pass_sex === 1) {
                        return 'Ж';
                    } else {
                        return '';
                    }
                },
                'filter' => ["М","Ж"],
                'filterInputOptions' => ['style' => 'width:70px', 'class'=>'form-control']
            ],
            [
                'attribute' => 'prs_birth_date',
                'filterInputOptions' => ['style' => 'width:100px', 'class'=>'form-control'],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_inn',
                'filterInputOptions' => ['style' => 'width:100px', 'class'=>'form-control'],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_pass_serial',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_pass_number',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            //'prs_pass_issued_by',
            //'prs_pass_date',

            'prs_connect_user',
            'prs_connect_pwd',

            [
                'attribute' => 'prs_connect_count',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'stf_id',
                'label' => Yii::t('app', 'Contacts'),
                'format' => 'raw',
                'visible' => !\app\models\User::isSpecAdmin(),
                'value' =>
                    function ($data) {
                        $s = '';
                        foreach ($data->contacts as $key => $value) {
                            switch ($value->conCont->cont_type) {
                                case 0:
                                    $s1 = $value->conCont->cont_name . ' ' . $value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 1:
                                    $s1 = $value->conCont->cont_name . ' ' . Html::mailto($value->con_text);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 2:
                                    $s1 = Html::a($value->conCont->cont_name, 'http://' . str_replace('http://', '', $value->con_text), ['target' => '_blank']);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 3:
                                    $s1 = Html::a($value->conCont->cont_name, 'skype:' . $value->con_text . '?call');
                                    $s .= Html::tag('div', $s1);
                                    break;
                                default:
                                    $s1 = $value->conCont->cont_name . ' ' . $value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                            }

                        }
                        return $s;

                    },
            ],

            [
                'attribute' => 'prs_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],


        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();

