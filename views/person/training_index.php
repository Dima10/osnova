<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $searchModelMod app\models\ApplCommandContentSearch */
/* @var $dataProviderMod yii\data\ActiveDataProvider */
/* @var $searchModelSh app\models\ApplShSearch */
/* @var $dataProviderSh yii\data\ActiveDataProvider */
/* @var $searchModelSheet app\models\ApplSheetSearch */
/* @var $dataProviderSheet yii\data\ActiveDataProvider */
/* @var $prs app\models\Person */
/* @var $sh app\models\ApplSh */
/* @var $sheet app\models\ApplSheet */
/* @var $ready boolean */
/* @var $trp_id integer */



//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity')];
$this->params['breadcrumbs'][] = $this->title;

// https://github.com/yiisoft/yii2/issues/4890
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTabEntity', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTabEntity');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);


?>

<div class="person-trp-grid-index">

<?php

try {
    echo Tabs::widget([
        'navType' => 'nav-tabs',
        'items' => [
            [
                'label' => Yii::t('app', '1. Материал для изучения'),
                'content' =>
                    $this->render('_grid_prog_module.php',
                        [
                            'searchModel' => $searchModelMod,
                            'dataProvider' => $dataProviderMod,
                            'prs' => $prs,
                            'applShPassed' => (bool)(isset($sh) && isset($sh->appls_passed) && $sh->appls_passed == 1),
                        ])
                ,
            ],

            [
                'label' => Yii::t('app', '2. Промежуточное тестирование'),
                'content' =>
                    $this->render('_appl_sh.php',
                        [
                            'dataProvider' => $dataProviderSh,
                            'searchModel' => $searchModelSh,
                            'sh' => $sh,
                            'trp_id' => $trp_id,
                        ])
                ,
                'visible' => (is_null($sh) ? 1 : $sh->appls_passed != 1) && $ready,
            ],


            [
                'label' => Yii::t('app', '3. Итоговое тестирование'),
                'content' =>
                    $this->render('_appl_sheet.php',
                        [
                            'dataProvider' => $dataProviderSheet,
                            'searchModel' => $searchModelSheet,
                            'sheet' => $sheet,
                            'trp_id' => $trp_id,
                        ])
                ,
                'visible' => is_null($sh) ? 0 : $sh->appls_passed == 1,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

</div>
