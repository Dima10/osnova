<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplCommandContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prs app\models\Person */

?>
<div class="person-trp-grid">

<h4>
    Здравствуйте, <?= $prs->prs_full_name ?>.<br>
    Вы находитесь в консоли Вашего аккаунта, здесь Вы можете пройти обучение по выбранным  Вами программам, а также пройти промежуточное и итоговое тестирование.
</h4>

<hr>
    <?= $this->render('_grid_trp', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ])?>
<hr>

    <h4>
        Перед тем, как перейти к обучению по выбранной Вами программе, просим ознакомиться с
        <a href="<?= \yii\helpers\Url::to(['/pattern/download', 'id' => 80]) ?>">Инструкцией пользователя порталом электронного обучения.</a>
    </h4>
</div>
