<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prs app\models\Person */
/* @var $applShPassed bool */
?>

<h4>
    Вы попали на страницу выбранной программы обучения.<br><br>
    Для  того, что бы начать освоение лекционного материала, Вам необходимо пройти по ссылкам, в разделе Модуль. Лекционные материалы можно будет скачать в формате Word или PDF.<br><br>
    После освоения части программы обучения, Вам будет открыт доступ к промежуточному тестированию.<br><br>
    По завершению освоения всего лекционного материала и  успешного прохождения промежуточного тестирования Вам откроется доступ к итоговому тестированию.<br><br>
    В случае возникновения вопросов по лекционным материалам Вы всегда можете обратиться за консультацией к преподавателю, который закреплен за дынной программой обучения. Консультации происходят в формате телефонных переговоров в рабочее время или посредством связи через систему скайп. Для того, что бы осуществить консультацию, слушателю необходимо позвонить по тел. 8-499-372-09-62 или написать письмо на электронную почту: info@pdo-osnova.ru, ответственный сотрудник соединит Вас с преподавателем или передаст Ваши контактные данные ответственному преподавателю.<br><br>

    Уважаемый <?= $prs->prs_full_name ?>, желаем Вам удачи в освоении лекционного материала программы и успешного завершения курса обучения.<br><br>
</h4>


<script type="text/javascript">
    window.applShPassed = <?php echo (int)$applShPassed ?>;
</script>

<?php

$link = \yii\helpers\Url::to(['/person/ajax-read-module']);
$linkR = \yii\helpers\Url::to(['/person/ajax-ready-module-next']);

$js = <<<EOL
    function readClick(obj) {
        $.get("$link",
            {
                trm_id : obj.id
            },
            function (data) {
                obj.outerHTML = data;
            }
        );
        if (window.applShPassed == 0) {
            $.get("$linkR",
                {
                    id : obj.dataset.trpId
                },
                function (data) {
                    if (data.indexOf("true") !== -1) {
                        $('#modal').on('hidden.bs.modal', function () {
                            window.location.reload(true);
                        })
    
                        $('#modal').modal('show');
                        //alert ("Вы готовы пройти промежуточное тестирование");
                        //window.location.reload(true); 
                    }
                }
            );
        }
    }
    
    
EOL;


$this->registerJs($js, yii\web\View::POS_BEGIN);

Modal::begin([
        'id' => 'modal',
        'size' => 'modal-lg',
        'header' => '<h2>Тестирование</h2>',
        //'toggleButton' => ['label' => 'Закрыть'],
]);

echo '<h4>Вы готовы пройти промежуточное тестирование</h4>';

Modal::end();


Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterPosition' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            /*
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]
                            );
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-prog-module', 'id' => $model->trpl_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            'trpl_id',
            */
            'trplTrm.trm_code',
            [
                'attribute' => 'trpl_trm_id',
                'label' => Yii::t('app', 'Trpl Trm ID'),
                'value' => function ($data) {
                    return $data->trplTrm->trm_name;
                },
            ],
            [
                'attribute' => 'trpl_trp_id',
                'label' => Yii::t('app', 'Trm Data A'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trplTrm->trm_dataA_name, yii\helpers\Url::toRoute(['training-module/download-a', 'id' => $data->trpl_trm_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'trpl_trp_id',
                'label' => '',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Yii::t('app', 'Ознакомлен'),'#',
                        [
                            'id' => $data->trpl_trm_id,
                            'class' => count($data->trplTrm->trainingModulePeople) > 0 ? 'btn btn-success' : 'btn btn-danger',
                            'onClick' => 'readClick(this);',
                            'data-trp-id' => $data->trplTrp->trp_id,
                        ]);
                },
            ],

            /*
            [
                'attribute' => 'trpl_trp_id',
                'label' => Yii::t('app', 'Trm Data B'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trplTrm->trm_dataB_name, yii\helpers\Url::toRoute(['training-module/download-b', 'id' => $data->trplTrm->trm_id]), ['target' => '_blank']);
                },
            ],
            */


        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

