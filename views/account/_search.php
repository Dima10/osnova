<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'acc_id') ?>

    <?= $form->field($model, 'acc_ab_id') ?>

    <?= $form->field($model, 'acc_bank_id') ?>

    <?= $form->field($model, 'acc_number') ?>

    <?php // $form->field($model, 'acc_create_user') ?>

    <?php // echo $form->field($model, 'acc_create_time') ?>

    <?php // echo $form->field($model, 'acc_create_ip') ?>

    <?php // echo $form->field($model, 'acc_update_user') ?>

    <?php // echo $form->field($model, 'acc_update_time') ?>

    <?php // echo $form->field($model, 'acc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
