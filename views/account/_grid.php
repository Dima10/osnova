<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $bank array */


Pjax::begin();
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'acc_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'yurLico',
                'label' => Yii::t('app', 'Acc Ab ID'),
                'value' => function ($data) { return $data->accAb->ab_name; },
//                'filter' => $entity,
            ],

            [
                'attribute' => 'bank',
                'label' => Yii::t('app', 'Acc Bank ID'),
                'value' => function ($data) { return $data->accBank->bank_name; },
//                'filter' => $bank,

            ],
            'acc_number',

            [
                'attribute' => 'acc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();
