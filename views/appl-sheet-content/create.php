<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetContent */

$this->title = Yii::t('app', 'Create Appl Sheet Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
