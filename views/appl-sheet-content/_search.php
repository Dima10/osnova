<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sheet-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applsc_id') ?>

    <?= $form->field($model, 'applsc_appls_id') ?>

    <?= $form->field($model, 'applsc_trq_id') ?>

    <?= $form->field($model, 'applsc_trq_question') ?>

    <?= $form->field($model, 'applsc_tra_id') ?>

    <?php // echo $form->field($model, 'applsc_tra_answer') ?>

    <?php // echo $form->field($model, 'applsc_tra_variant') ?>

    <?php // echo $form->field($model, 'applsc_create_user') ?>

    <?php // echo $form->field($model, 'applsc_create_time') ?>

    <?php // echo $form->field($model, 'applsc_create_ip') ?>

    <?php // echo $form->field($model, 'applsc_update_user') ?>

    <?php // echo $form->field($model, 'applsc_update_time') ?>

    <?php // echo $form->field($model, 'applsc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
