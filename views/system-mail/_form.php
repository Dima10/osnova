<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SystemMail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="system-mail-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
        ); 
        echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'sm_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_server')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_port')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'sm_encryption')->dropDownList(['' => '', 'ssl' => 'ssl', 'tls' => 'tls']) ?>

    <?= $form->field($model, 'sm_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_password')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_imap_server')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_imap_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sm_imap_password')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
