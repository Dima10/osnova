<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SystemMail */

$this->title = $model->sm_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'System Mails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-mail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->sm_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->sm_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'sm_id',
                'sm_name',
                'sm_server',
                'sm_encryption',
                'sm_user',
                'sm_create_user',
                'sm_create_time',
                'sm_create_ip',
                'sm_update_user',
                'sm_update_time',
                'sm_update_ip',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    ?>

</div>
