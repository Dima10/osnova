<?php

use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="person-frm-agreement-acc-a-index-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            'aca_id',
            //'aca_aga_id',
            [
                'attribute' => 'aca_ana_id',
                'label' => Yii::t('app', 'Aca Ana ID'),
                'value' =>  function ($data) {
                                return $data->acaAna->anaAgra->agra_number . ' / ' .$data->acaAna->anaSvc->svc_name .' / '.$data->acaAna->anaTrp->trp_name;
                            },
            ],

            'aca_qty',
            'aca_price',
            [
                'attribute' => 'aca_tax',
                'label' => Yii::t('app', 'Aca Tax'),
                'value' => function ($data) { return isset($data->aca_tax) ? $data->aca_tax : Yii::t('app', 'Without Tax'); },
            ],


            [
                'attribute' => 'aca_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'aca_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aca_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);


?>

</div>
