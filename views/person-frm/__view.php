<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->prs_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->prs_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->prs_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'prs_id',
            'prs_full_name',
            'prs_last_name',
            'prs_first_name',
            'prs_middle_name',
            'prs_pass_sex',
            'prs_birth_date',
            'prs_inn',
            'prs_pass_serial',
            'prs_pass_number',
            'prs_pass_issued_by',
            'prs_pass_date',
            'prs_create_user',
            'prs_create_time',
            'prs_create_ip',
            'prs_update_user',
            'prs_update_time',
            'prs_update_ip',
        ],
    ]) ?>

</div>
