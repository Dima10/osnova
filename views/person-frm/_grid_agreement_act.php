<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $person app\models\Person */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

$agreement_status = null;
$agreement = null;
$agreement_annex = null;

echo Html::a(Yii::t('app', 'Create Agreement Act'), ['create-agreement-act', 'prs_id' => $person->prs_id], ['class' => 'btn btn-success']);

Pjax::begin();
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($person){
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement-act', 'prs_id' => $person->prs_id, 'id' => $model->act_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement-act', 'id' => $model->act_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            [
                'attribute' => 'act_id',
                'options' => ['width' => '100'],
            ],

            'act_number',
            'act_date',

            [
                'attribute' => 'act_ast_id',
                'label' => Yii::t('app', 'Act Ast ID'),
                'value' => function ($data) { return $data->actAst->ast_name; },
                'filter' => $agreement_status,
            ],

            [
                'attribute' => 'act_ab_id',
                'label' => Yii::t('app', 'Act Ab ID'),
                'value' => function ($data) { return $data->actAb->ab_name; },
                //'filter' => $person,
            ],

            [
                'attribute' => 'act_comp_id',
                'label' => Yii::t('app', 'Act Comp ID'),
                'value' => function ($data) { return $data->actComp->comp_name; },
                //'filter' => $company,
            ],

            [
                'attribute' => 'act_agr_id',
                'label' => Yii::t('app', 'Act Agr ID'),
                'value' => function ($data) { return $data->actAgr->agr_number; },
                //'filter' => $agreement,
            ],

            [
                'attribute' => 'act_agra_id',
                'label' => Yii::t('app', 'Act Agra ID'),
                'value' => function ($data) { return $data->actAgra->agra_number; },
                //'filter' => $agreement_annex,
            ],

            'act_sum',
            'act_tax',

            [
                'attribute' => 'act_fdata',
                'label' => Yii::t('app', 'Act Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->act_fdata, yii\helpers\Url::toRoute(['download-agreement-act', 'id' => $data->act_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'act_fdata_sign',
                'label' => Yii::t('app', 'Act Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->act_fdata_sign, yii\helpers\Url::toRoute(['download-agreement-act--sign', 'id' => $data->act_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'act_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);
Pjax::end();

