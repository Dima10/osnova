<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
/* @var $contactType array */
/* @var $person array */

?>

<div class="person-frm-contact-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ($model->isNewRecord ?
                                        ['create-contact', 'prs_id' => array_keys($person)[0]]
                                        :
                                        ['update-contact', 'prs_id' => array_keys($person)[0], 'id' => $model->con_id ]
                            ),
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'con_ab_id')->dropDownList($person, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'con_cont_id')->dropDownList($contactType) ?>

    <?= $form->field($model, 'con_text')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
