<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $searchModelContact app\models\ContactSearch */
/* @var $dataProviderContact yii\data\ActiveDataProvider */
/* @var $searchModelRequest app\models\ContactSearch */
/* @var $dataProviderRequest yii\data\ActiveDataProvider */
/* @var $dataProviderAgreement yii\data\ActiveDataProvider */
/* @var $searchModelAgreement app\models\AgreementSearch */
/* @var $dataProviderAgreementAnnex yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAnnex app\models\AgreementAnnexSearch */
/* @var $dataProviderAgreementAcc yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAcc app\models\AgreementAccSearch */
/* @var $dataProviderAgreementAct yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAct app\models\AgreementActSearch */


$this->title = $model->prs_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person')];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="person-frm-view">

    <!--
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->prs_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->prs_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    -->

<?php

// https://github.com/yiisoft/yii2/issues/4890
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTabPerson', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTabPerson');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);


echo Tabs::widget([
    'items' => [
        // Person
        [
            'label' => Yii::t('app', 'Person'),
            'content' =>
                $this->render('_view',
                    [
                    'model' => $model,
                    ])
                ,
        ],

        // Contact
        [
            'label' => Yii::t('app', 'Contacts'),
            'content' =>
                $this->render('_grid_contact',
                    [
                    'dataProvider' => $dataProviderContact,
                    'searchModel' => $searchModelContact,
                    'person' => $model,
                    'addrbook' => null,
                    'contactType' => null,
                    ])
                ,
        ],

        // Company
        [
            'label' => Yii::t('app', 'Company'),
            'content' =>
                $this->render('_grid_company',
                    [
                        'dataProvider' => $dataProviderRequest,
                        'searchModel' => $searchModelRequest,
                    ])
            ,
        ],

        // Documents
        [
            'label' => Yii::t('app', 'Documents'),
            'items' =>
                [
                    [
                        'label' => Yii::t('app', 'Agreements'),
                        'content' => 
                            $this->render('_grid_agreement',
                                [
                                    'dataProvider' => $dataProviderAgreement,
                                    'searchModel' => $searchModelAgreement,
                                    'person' => $model,
                                    //'account' => $account,
                                ]),
                    ],
                    [
                        'label' => Yii::t('app', 'Agreement Annexes'),
                        'content' => 
                            $this->render('_grid_agreement_annex',
                                [
                                    'dataProvider' => $dataProviderAgreementAnnex,
                                    'searchModel' => $searchModelAgreementAnnex,
                                    'person' => $model,
                                ]),
                    ],
                    [
                        'label' => Yii::t('app', 'Agreement Accs'),
                        'content' => 
                            $this->render('_grid_agreement_acc',
                                [
                                    'dataProvider' => $dataProviderAgreementAcc,
                                    'searchModel' => $searchModelAgreementAcc,
                                    'person' => $model,
                                ]),
                    ],
                    [
                        'label' => Yii::t('app', 'Agreement Acts'),
                        'content' => 
                            $this->render('_grid_agreement_act',
                                [
                                    'dataProvider' => $dataProviderAgreementAct,
                                    'searchModel' => $searchModelAgreementAct,
                                    'person' => $model,
                                ]),
                    ],
                ],

        ],

    ],
]);


?>

</div>
