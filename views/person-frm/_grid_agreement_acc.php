<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $person app\models\Person */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

$agreement_status = null;
$company = null;
$agreement = null;
$agreement_annex = null;

echo Html::a(Yii::t('app', 'Create Agreement Acc'), ['create-agreement-acc', 'prs_id' => $person->prs_id], ['class' => 'btn btn-success']);

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {payment}',
                'buttons' => [
                    'update' =>
                        function($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                                ]);
                        },
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    'payment' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'FinanceBook Pay'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($person){
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement-acc', 'prs_id' => $person->prs_id, 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement-acc', 'id' => $model->aga_id]);
                            return $url;
                        }
                        if ($action === 'payment') {
                            //$url = yii\helpers\Url::to(['finance-book/update-pay', 'id' => $model->aga_id]);
                            $url = yii\helpers\Url::to(['update-pay', 'id' => $model->aga_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'aga_id',
            'aga_number',
            'aga_date',

            [
                'attribute' => 'aga_ast_id',
                'label' => Yii::t('app', 'Aga Ast ID'),
                'value' => function ($data) { return $data->agaAst->ast_name; },
                'filter' => $agreement_status,
            ],
            [
                'attribute' => 'aga_ab_id',
                'label' => Yii::t('app', 'Aga Ab ID'),
                'value' => function ($data) { return $data->agaAb->ab_name; },
                'filter' => $person,
            ],

            [
                'attribute' => 'aga_comp_id',
                'label' => Yii::t('app', 'Aga Comp ID'),
                'value' => function ($data) { return $data->agaComp->comp_name; },
                'filter' => $company,
            ],
            
            [
                'attribute' => 'aga_agr_id',
                'label' => Yii::t('app', 'Aga Agr ID'),
                'value' => function ($data) { return $data->agaAgr->agr_number; },
                'filter' => $agreement,
            ],
            
            [
                'attribute' => 'aga_agra_id',
                'label' => Yii::t('app', 'Aga Agra ID'),
                'value' => function ($data) { return $data->agaAgra->agra_number; },
                'filter' => $agreement_annex,
            ],

            'aga_sum',
            'aga_tax',

            [
                'attribute' => 'aga_fdata',
                'label' => Yii::t('app', 'Aga Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->aga_fdata, yii\helpers\Url::toRoute(['download-agreement-acc', 'id' => $data->aga_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'aga_fdata_sign',
                'label' => Yii::t('app', 'Aga Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->aga_fdata_sign, yii\helpers\Url::toRoute(['download-agreement-acc-sign', 'id' => $data->aga_id]), ['target' => '_blank']);
                            },
            ],

            [
                'attribute' => 'aga_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'aga_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

