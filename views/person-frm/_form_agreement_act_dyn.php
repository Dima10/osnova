<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $form yii\widgets\ActiveForm */
/* @var $person array */
/* @var $company array */
/* @var $agreement array */
/* @var $account array */
/* @var $agreement_annex array */
/* @var $pattern array */
/* @var $annex_a array */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>

<div class="person-frm-agreement-act-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement-act', 'ent_id' => array_keys($person)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelA) ? $form->errorSummary($modelA) : '';

    ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])->label(Yii::t('app', 'Act Date'))
    ?>

    <?=  $form->field($model, 'sum')->textInput(['id' => 'sum', 'ReadOnly' => true])->label(Yii::t('app', 'Act Sum'))  ?>

    <?=  $form->field($model, 'tax')->textInput(['id' => 'tax', 'ReadOnly' => true])->label(Yii::t('app', 'Act Tax'))  ?>

    <?= $form->field($model, 'prs_id')->dropDownList($person, ['ReadOnly' => true])->label(Yii::t('app', 'Person')) ?>

    <?= $form->field($model, 'comp_id')->dropDownList($company)->label(Yii::t('app', 'Act Comp ID')) ?>

    <?= $form->field($model, 'agr_id')->dropDownList($agreement, ['id' => 'agr_id'])->label(Yii::t('app', 'Act Agr ID')) ?>

    <?php //= $form->field($model, 'acc_id')->dropDownList($account)->label(Yii::t('app', 'Act Acc ID')) ?>

    <?php //= $form->field($model, 'agra_id')->dropDownList($agreement_annex)->label(Yii::t('app', 'Act Agra ID')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>

    <hr>

    <?= $this->render('_ajax_agreement_act_a_grid', [
        'dataProvider' => $dataProvider,
    ]) ?>


    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
        <div class="col-sm-6"><?= Html::dropDownList('annex', '', $annex_a, ['id' => 'annex_a', 'class'=> 'form-control']) ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Price'), 'price'); ?></div>
        <div class="col-sm-6"><?= Html::textInput('price', '', ['id' => 'price', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Qty'), 'qty'); ?></div>
        <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control']); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6"></div>
        <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add Svc'), '#', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
    </div>

    <hr>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = '
$(function() {
    $("#add_svc").click(function(e) {
        var _id = $("#annex_a").val();
        var _price = $("#price").val();
        var _qty = $("#qty").val();

        $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a-create']).'",
                    {
                      ana_id : _id,
                      price : _price,
                      qty : _qty,
                    },
                    function (data) {
                        $("#grid_act").html(data);
                        $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a-sum']).'",
                            {
                            },
                            function (data) {
                                $("#sum").val(data);
                            }
                        );
                        $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a-tax']).'",
                            {
                            },
                            function (data) {
                                $("#tax").val(data);
                            }
                        );
                    }
                );

    });
    
    $("#agr_id").change(function() {
        var agr_id = this.value;
        $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a']).'",
                    {
                      agr_id : agr_id,
                    },
                    function (data) {
                        $("#annex_a").html(data);
                    }
        );
                
                
        $("#annex_a").change(function() {
            var ana_id = this.value;
            $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a-price']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#price").val(data);
                    }
            );

            $.get("'.\yii\helpers\Url::to(['/person-frm/ajax-agreement-act-a-qty']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#qty").val(data);
                    }
            );
        });
    });
});
';
$this->registerJs($script, yii\web\View::POS_END);

