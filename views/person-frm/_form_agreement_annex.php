<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnex */
/* @var $form yii\widgets\ActiveForm */
/* @var $agreement array */
/* @var $person array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="person-frm-agreement-annex-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);

    ?>

    <?= $form->field($model, 'agra_number')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($model, 'agra_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])
    ?>

    <?= $form->field($model, 'agra_ast_id')->dropDownList($agreement_status) ?>

    <?= $form->field($model, 'agra_agr_id')->dropDownList($agreement, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agra_sum')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_tax')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_data')->fileInput() ?>

    <?= $form->field($model, 'agra_data_sign')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render('_grid_agreement_annex_a', [
        'dataProvider' => $dataProvider,
    ]) ?>


</div>
