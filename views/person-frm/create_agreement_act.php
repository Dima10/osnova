<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $person array */
/* @var $company array */
/* @var $agreement array */
/* @var $account array */
/* @var $agreement_annex array */
/* @var $pattern array */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Create Agreement Act');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person'), 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($person)[0], 'url' => ['view', 'id' => array_keys($person)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-frm-agreement-act-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_act_dyn', [
        'model' => $model,
        'person' => $person,
        'company' => $company,
        'agreement' => $agreement,
        'account' => $account,
        'agreement_annex' => $agreement_annex,
        'pattern' => $pattern,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
