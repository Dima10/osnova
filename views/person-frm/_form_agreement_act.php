<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAct */
/* @var $form yii\widgets\ActiveForm */
/* @var $person array */
/* @var $company array */
/* @var $account array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="person-frm-agreement-act-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);
    ?>

    <?= $form->field($model, 'act_number')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($model, 'act_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])
    ?>

    <?= $form->field($model, 'act_ast_id')->dropDownList($agreement_status) ?>

    <?= $form->field($model, 'act_comp_id')->dropDownList($company, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_agr_id')->dropDownList($agreement, ['ReadOnly' => true]) ?>

    <?php //= $form->field($model, 'act_agra_id')->dropDownList($agreement_annex, ['ReadOnly' => true]) ?>

    <?php //= $form->field($model, 'act_acc_id')->dropDownList($account, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'act_sum')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_tax')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_data')->fileInput() ?>

    <?= $form->field($model, 'act_data_sign')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render('_grid_agreement_act_a', [
        'dataProvider' => $dataProvider,
    ]) ?>


</div>
