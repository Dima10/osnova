<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgSession */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Training Prog Session',
]) . $model->tps_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Prog Sessions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tps_id, 'url' => ['view', 'id' => $model->tps_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-prog-session-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
