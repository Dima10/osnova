<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="training-prog-session-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tps_id',
            'tps_session_id',
            [
                'attribute' => 'trp_name',
                'label' => Yii::t('app', 'Tps Trp ID'),
                'value' => 'tpsTrp.trp_name'
            ],
            [
                'attribute' => 'prs_full_name',
                'label' => Yii::t('app', 'Tps Prs ID'),
                'value' => 'tpsPrs.prs_full_name'
            ],
            [
                'attribute' => 'tps_create_user',
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            'tps_create_time',
            'tps_create_ip',

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
