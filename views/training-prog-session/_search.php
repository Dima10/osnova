<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgSessionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-prog-session-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tps_id') ?>

    <?= $form->field($model, 'tps_session_id') ?>

    <?= $form->field($model, 'tps_trp_id') ?>

    <?= $form->field($model, 'tps_prs_id') ?>

    <?= $form->field($model, 'tps_create_user') ?>

    <?php // echo $form->field($model, 'tps_create_time') ?>

    <?php // echo $form->field($model, 'tps_create_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
