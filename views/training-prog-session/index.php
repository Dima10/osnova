<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Training Prog Sessions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-prog-session-index" id="forTrainingProgSessionController">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--
    <p>
        <?= Html::a(Yii::t('app', 'Create Training Prog Session'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    -->
    <p>
        <?= Html::a(Yii::t('app', 'Export Agreement'), ['export-xml'], ['class' => 'btn btn-info']) ?>
    </p>

    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control', 'min' => 1]) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
