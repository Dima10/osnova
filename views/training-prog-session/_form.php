<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgSession */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-prog-session-form">

    <?php $form = ActiveForm::begin(
            [
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]
    
        );

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'tps_session_id')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'tps_trp_id')->textInput(); ?>

    <?php echo $form->field($model, 'tps_prs_id')->textInput(); ?>

    <?php echo $form->field($model, 'tps_create_user')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'tps_create_time')->textInput(); ?>

    <?php echo $form->field($model, 'tps_create_ip')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
