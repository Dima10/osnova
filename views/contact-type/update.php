<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ContactType */
/* @var $contact_type array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Contact Type'),
]) . $model->cont_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Contact Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cont_id, 'url' => ['view', 'id' => $model->cont_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="contact-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contact_type' => $contact_type,
    ]) ?>

</div>
