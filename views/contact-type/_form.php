<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContactType */
/* @var $form yii\widgets\ActiveForm */
/* @var $contact_type array */
?>

<div class="contact-type-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'cont_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cont_type')->dropDownList($contact_type) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
