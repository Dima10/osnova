<?php

use app\models\AgreementStatus;
use app\models\SvcDocType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="report-listener-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{pager}\n{summary}\n{items}\n{summary}\n{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 20,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'applr_number',
                'label' => Yii::t('app', 'Appl Request'),
                'value' => function ($data) {
                    return empty($data['applr_number']) ? '' : $data['applr_number'];
                }
            ],
            [
                'attribute' => 'applr_date',
                'label' => Yii::t('app', 'Applm Applr Date'),
                'value' => function ($data) {
                    return empty($data['applr_date']) ? '' : $data['applr_date'];
                }
            ],
            [
                'attribute' => 'prs_id',
                'label' => Yii::t('app', 'Prs ID'),
                'value' => function ($data) {
                    return empty($data['prs_id']) ? '' : $data['prs_id'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_connect_user',
                'label' => Yii::t('app', 'Prs Connect User'),
                'value' => function ($data) {
                    return empty($data['prs_connect_user']) ? '' : $data['prs_connect_user'];
                }
            ],
            [
                'attribute' => 'prs_connect_pwd',
                'label' => Yii::t('app', 'Prs Connect Pwd'),
                'value' => function ($data) {
                    return empty($data['prs_connect_pwd']) ? '' : $data['prs_connect_pwd'];
                }
            ],
            [
                'attribute' => 'contacts',
                'format' => 'raw',
                'label' => Yii::t('app', 'Contact'),
                'value' => function ($data) {
                    $rows = array_filter(explode(',', $data['contacts']));
                    foreach ($rows as &$row) {
                        $row = '<span class="text-nowrap">' . $row . '</span>';
                    }
                    return implode('<br>', $rows);
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_full_name',
                'label' => Yii::t('app', 'Prs Full Name'),
                'value' => function ($data) {
                    return empty($data['prs_full_name']) ? '' : $data['prs_full_name'];
                }
            ],
            [
                'attribute' => 'prs_last_name',
                'label' => Yii::t('app', 'Prs Last Name'),
                'value' => function ($data) {
                    return empty($data['prs_last_name']) ? '' : $data['prs_last_name'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_first_name',
                'label' => Yii::t('app', 'Prs First Name'),
                'value' => function ($data) {
                    return empty($data['prs_first_name']) ? '' : $data['prs_first_name'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_middle_name',
                'label' => Yii::t('app', 'Prs Middle Name'),
                'value' => function ($data) {
                    return empty($data['prs_middle_name']) ? '' : $data['prs_middle_name'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'stf_position',
                'label' => Yii::t('app', 'Stf Position'),
                'format' => 'raw',
                'value' => function ($data) {
                    $rows = array_filter(explode(',', $data['stf_position']));
                    return implode(',<br>', $rows);
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ab_name',
                'label' => Yii::t('app', 'Agr Ab ID'),
                'value' => function ($data) {
                    return empty($data['ab_name']) ? '' : $data['ab_name'];
                }
            ],
            [
                'attribute' => 'entc_name',
                'label' => Yii::t('app', 'Entcl Entc ID'),
                'format' => 'raw',
                'value' => function ($data) {
                    $rows = explode(',', $data['entc_name']);
                    foreach ($rows as &$row) {
                        $row = '<span class="text-nowrap">' . $row . '</span>';
                    }
                    return implode(',<br>', $rows);
                }
            ],
            [
                'attribute' => 'agra_comment',
                'label' => Yii::t('app', 'Agra Comment'),
                'value' => function ($data) {
                    return empty($data['agra_comment']) ? '' : $data['agra_comment'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'manager',
                'label' => Yii::t('app', 'Applr Manager ID'),
                'value' => function ($data) {
                    return empty($data['manager']) ? '' : $data['manager'];
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'agr_number',
                'label' => Yii::t('app', 'Agreement'),
                'value' => function ($data) {
                    return empty($data['agr_number']) ? '' : $data['agr_number'];
                }
            ],
            [
                'attribute' => 'agra_number',
                'label' => Yii::t('app', 'Applr Agra ID'),
                'format' => 'raw',
                'value' => function ($data) {
                    return empty($data['agra_number']) ? '' : $data['agra_number'];
                }
            ],
            [
                'attribute' => 'aga_number',
                'label' => Yii::t('app', 'Agreement Acc'),
                'format' => 'raw',
                'value' => function ($data) {
                    $rows = explode(',', $data['aga_number']);
                    natsort($rows);
                    foreach ($rows as &$row) {
                        $row = '<span class="text-nowrap">' . $row . '</span>';
                    }
                    return implode('<br>', $rows);
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'svdt_name',
                'label' => Yii::t('app', 'Svdt Name'),
                'value' => function ($data) {
                    return empty($data['svdt_name']) ? '' : $data['svdt_name'];
                },
                'filter' => ArrayHelper::map(SvcDocType::find()->asArray()->all(), 'svdt_id','svdt_name'),
            ],
            [
                'attribute' => 'svc_name',
                'label' => Yii::t('app', 'Sprog Svc ID'),
                'value' => function ($data) {
                    return empty($data['svc_name']) ? '' : $data['svc_name'];
                }
            ],
            [
                'attribute' => 'trp_name',
                'label' => Yii::t('app', 'Sprog Trp ID'),
                'value' => function ($data) {
                    return empty($data['trp_name']) ? '' : $data['trp_name'];
                }
            ],
            [
                'attribute' => 'svc_hour',
                'label' => Yii::t('app', 'Svc Hour'),
                'value' => function ($data) {
                    return empty($data['svc_hour']) ? '' : $data['svc_hour'];
                }
            ],
            [
                'attribute' => 'applf_cmd_date',
                'label' => Yii::t('app', 'Applf Cmd Date'),
                'value' => function ($data) {
                    return empty($data['applf_cmd_date']) ? '' : $data['applf_cmd_date'];
                }
            ],
            [
                'attribute' => 'applf_end_date',
                'label' => Yii::t('app', 'Applf End Date'),
                'value' => function ($data) {
                    return empty($data['applf_end_date']) ? '' : $data['applf_end_date'];
                }
            ],
            [
                'attribute' => 'applf_number',
                'label' => Yii::t('app', 'Appl Final'),
                'value' => function ($data) {
                    return empty($data['applf_number']) ? '' : $data['applf_number'];
                }
            ],
            [
                'attribute' => 'applr_reestr',
                'label' => Yii::t('app', 'Applr Reestr'),
                'value' => function ($data) {
                    $reestrsArray = \app\models\Constant::reestr_val();
                    return isset($reestrsArray[$data['applr_reestr']]) ? $reestrsArray[$data['applr_reestr']] : $reestrsArray[0];
                },
                'filter' => \app\models\Constant::reestr_val(true),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_flag',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' => function ($data) {
                    return \app\models\Constant::YES_NO[$data['applr_flag'] ?? ''];
                },
                'filter' => \app\models\Constant::yes_no(true),
            ],
            [
                'attribute' => 'ast_name',
                'label' => Yii::t('app', 'Agreement Status'),
                'value' => function ($data) {
                    return empty($data['ast_name']) ? '' : $data['ast_name'];
                },
                'filter' => ArrayHelper::map(AgreementStatus::find()->asArray()->all(), 'ast_id','ast_name'),
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
