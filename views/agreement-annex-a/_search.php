<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexASearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-annex-a-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ana_id') ?>

    <?= $form->field($model, 'ana_agra_id') ?>

    <?= $form->field($model, 'ana_svc_id') ?>

    <?= $form->field($model, 'ana_cost_a') ?>

    <?= $form->field($model, 'ana_price_a') ?>

    <?php // echo $form->field($model, 'ana_qty') ?>

    <?php // echo $form->field($model, 'ana_price') ?>

    <?php // echo $form->field($model, 'ana_tax') ?>

    <?php // echo $form->field($model, 'ana_create_user') ?>

    <?php // echo $form->field($model, 'ana_create_time') ?>

    <?php // echo $form->field($model, 'ana_create_ip') ?>

    <?php // echo $form->field($model, 'ana_update_user') ?>

    <?php // echo $form->field($model, 'ana_update_time') ?>

    <?php // echo $form->field($model, 'ana_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
