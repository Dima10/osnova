<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAnnexASearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $svc array */

?>
<div class="agreement-annex-a-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'ana_id',
            'ana_agra_id',
            [
                'attribute' => 'ana_svc_id',
                'label' => Yii::t('app', 'Ana Svc ID'),
                'value' => function ($data) {
                    return $data->anaSvc->svc_name;
                },
                'filter' => $svc,
            ],
            [
                'attribute' => 'ana_trp_id',
                'label' => Yii::t('app', 'Ana Trp ID'),
                'value' => function ($data) {
                    return isset($data->anaTrp) ? $data->anaTrp > trp_name : '';
                },
            ],


            'ana_cost_a',
            'ana_price_a',
            'ana_qty',
            'ana_price',
            'ana_tax',

            [
                'attribute' => 'ana_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'ana_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
