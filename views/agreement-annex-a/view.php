<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexA */

$this->title = $model->ana_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Annex As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-annex-a-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ana_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ana_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ana_id',
            'ana_agra_id',
            'ana_svc_id',
            'ana_cost_a',
            'ana_price_a',
            'ana_qty',
            'ana_price',
            'ana_tax',
            'ana_create_user',
            'ana_create_time',
            'ana_create_ip',
            'ana_update_user',
            'ana_update_time',
            'ana_update_ip',
        ],
    ]) ?>

</div>
