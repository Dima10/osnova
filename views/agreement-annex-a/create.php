<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnexA */
/* @var $svc array */

$this->title = Yii::t('app', 'Create Agreement Annex A');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Annex As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-annex-a-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'svc' => $svc,
    ]) ?>

</div>
