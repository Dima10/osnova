<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetXContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-sheet-xcontent-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applsxc_id',
            'applsxcApplsx.applsx_number',
            'applsxcPrs.prs_full_name',
            'applsxcTrp.trp_name',
            'applsxc_score',
            'applsxc_passed',

            [
                'attribute' => 'applsxc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsxc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsxc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsxc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsxc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsxc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            
        ],
    ]);

Pjax::end();


?>

</div>
