<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplXxxContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-xxx-content-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applxxxc_id',
            'applxxxcApplxxx.applxxx_number',
            'applxxxcAb.ab_name',
            'applxxxcPrs.prs_full_name',
            'applxxxcTrp.trp_name',
            'applxxxc_position',
            'applxxxc_passed',

            [
                'attribute' => 'applxxxc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applxxxc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applxxxc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applxxxc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applxxxc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applxxxc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            
        ],
    ]);

Pjax::end();


?>

</div>
