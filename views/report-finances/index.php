<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agreement_status array */
/* @var $payment_types array */
/* @var $entc_ids array */
/* @var $fb_types array */
/* @var $svc_svdts array */

$this->title = Yii::t('app', 'Reestr Finances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-acc-index" id="forReportFinancesController">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?php
            echo Html::a(Yii::t('app', 'Export Agreement Acc'), ['export-xml'], ['class' => 'btn btn-info'])
        ?>
    </p>

    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'agreement_status' => $agreement_status,
        'payment_types' => $payment_types,
        'entc_ids' => $entc_ids,
        'fb_types' => $fb_types,
        'svc_svdts' => $svc_svdts,
    ]) ?>

</div>
