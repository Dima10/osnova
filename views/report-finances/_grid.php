<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agreement_status array */
/* @var $payment_types array */
/* @var $entc_ids array */
/* @var $fb_types array */
/* @var $svc_svdts array */

?>
<div class="agreement-acc-index-grid">


<?php

    Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 30,
        ],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ent_name',
            'agent_name',
            [
                'attribute' => 'entc_id',
                'value' => function ($data) use ($entc_ids){
                    return $entc_ids[$data['entc_id']] ?? '';
                },
                'filter' => $entc_ids,
            ],
            [
                'attribute' => 'fb_fbt_id',
                'value' => function ($data) use ($fb_types){
                    return $fb_types[$data['fb_fbt_id']] ?? '';
                },
                'filter' => $fb_types,
            ],
            'svc_name',
            'trp_name',
            [
                'attribute' => 'svc_svdt_id',
                'value' => function ($data) use ($svc_svdts){
                    return $svc_svdts[$data['svc_svdt_id']] ?? '';
                },
                'filter' => $svc_svdts,
            ],
            'agra_number',
            'price',
            'qty',
            'sum',
            'ana_cost_a',
            'sum_ss',
            'marzha',
//            'aga_number',
//            'aga_date',
            [
                'attribute' => 'aga_number',
                'value' => function ($data) {
                    return $data['aga_number'] ?? 'не задано';
                },
            ],
            [
                'attribute' => 'aga_date',
                'value' => function ($data) {
                    return $data['aga_date'] ?? 'не задано';
                },
            ],
            [
                'attribute' => 'aga_ast_id',
                'value' => function ($data) use ($agreement_status) {
                    return $agreement_status[$data['aga_ast_id']] ?? '';
                },
                'filter' => $agreement_status,
            ],
            [
                'attribute' => 'fb_pt_id',
                'value' => function ($data) use ($payment_types){
                    return $payment_types[$data['fb_pt_id']] ?? '';
                },
                'filter' => $payment_types,
            ],
//            'fb_payment',
//            'fb_date',
            [
                'attribute' => 'fb_payment',
                'value' => function ($data) {
                    return $data['fb_payment'] ?? 'не задано';
                },
            ],
            [
                'attribute' => 'fb_date',
                'value' => function ($data) {
                    return $data['fb_date'] ?? 'не задано';
                },
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();

?>

</div>
