<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAccSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agreement_status array */
/* @var $payment_types array */
/* @var $entc_ids array */
/* @var $fb_types array */
/* @var $svc_svdts array */

$this->title = Yii::t('app', 'Reestr Finances');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-acc-index" id="forReportFinancesController">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin();?>
        <div class="form-group">
            <?= Html::submitButton('Сформировать актуальные данные', ['name' => 'refreshReport', 'value' => 'refresh', 'class' => 'btn btn-info']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
