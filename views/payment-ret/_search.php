<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentRetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-ret-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ptr_id') ?>

    <?= $form->field($model, 'ptr_ab_id') ?>

    <?= $form->field($model, 'ptr_comp_id') ?>

    <?= $form->field($model, 'ptr_aga_id') ?>

    <?= $form->field($model, 'ptr_sum') ?>

    <?php // echo $form->field($model, 'ptr_tax') ?>

    <?php // echo $form->field($model, 'ptr_comment') ?>

    <?php // echo $form->field($model, 'ptr_create_user') ?>

    <?php // echo $form->field($model, 'ptr_create_time') ?>

    <?php // echo $form->field($model, 'ptr_create_ip') ?>

    <?php // echo $form->field($model, 'ptr_update_user') ?>

    <?php // echo $form->field($model, 'ptr_update_time') ?>

    <?php // echo $form->field($model, 'ptr_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
