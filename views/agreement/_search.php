<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'agr_id') ?>

    <?= $form->field($model, 'agr_number') ?>

    <?= $form->field($model, 'agr_ab_id') ?>

    <?= $form->field($model, 'agr_comp_id') ?>

    <?= $form->field($model, 'agr_sum') ?>

    <?php // echo $form->field($model, 'agr_data') ?>

    <?php // echo $form->field($model, 'arg_data_sign') ?>

    <?php // echo $form->field($model, 'agr_create_time') ?>

    <?php // echo $form->field($model, 'agr_create_ip') ?>

    <?php // echo $form->field($model, 'agr_update_user') ?>

    <?php // echo $form->field($model, 'agr_update_time') ?>

    <?php // echo $form->field($model, 'agr_update_ip') ?>

    <?php // echo $form->field($model, 'agr_create_user') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
