<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplXxx */

$this->title = Yii::t('app', 'Create Appl Xxx');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Xxxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-xxx-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
