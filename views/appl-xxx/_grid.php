<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplXxxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */

?>
<div class="appl-xxx-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applxxx_id',
            'applxxx_number',
            'applxxx_date',
            [
                'attribute' => 'applxxx_trt_id',
                'value' =>
                    function ($data) {
                        return $data->applxxxTrt->trt_name;
                    },
                'label' => Yii::t('app', 'Applxxx Trt ID'),
                'filter' => $trt,
            ],
            [
                'attribute' => 'applxxx_file_name',
                'label' => Yii::t('app', 'Applxxx File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applxxx_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applxxx_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applxxx_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxx_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxx_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxx_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxx_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applxxx_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
