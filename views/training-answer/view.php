<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingAnswer */

$this->title = $model->tra_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-answer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->tra_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->tra_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tra_id',
            'traTrq.trq_name',
            'tra_answer:ntext',
            'tra_variant',
            'tra_create_user',
            'tra_create_time',
            'tra_create_ip',
            'tra_update_user',
            'tra_update_time',
            'tra_update_ip',
        ],
    ]) ?>

</div>
