<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingAnswer */
/* @var $question array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Training Answer'),
]) . $model->tra_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tra_id, 'url' => ['view', 'id' => $model->tra_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-answer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'question' => $question,

    ]) ?>

</div>
