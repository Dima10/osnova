<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $question array */

?>
<div class="training-answer-index-grid">

<?php Pjax::begin(); ?>

<?php try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'tra_id',
            [
                'attribute' => 'tra_trq_id',
                'value' => 'traTrq.trq_name',
                /*
                'value' =>  function ($data) {
                                return $data->traTrq->trq_name;
                            },
                */
                'filter' => $question,
            ],

            'tra_answer:ntext',
            'tra_variant',

            [
                'attribute' => 'tra_file_name',
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->tra_file_name)) {
                        return Html::img(yii\helpers\Url::to(['image', 'id' => $data->tra_id]));
                    } else {
                        return null;
                    }
                },
            ],

            [
                'attribute' => 'tra_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'tra_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
?>

<?php Pjax::end(); ?>
</div>
