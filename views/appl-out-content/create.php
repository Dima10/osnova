<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplOutContent */

$this->title = Yii::t('app', 'Create Appl Out Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Out Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-out-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
