<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplOutContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-out-content-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'apploutc_applout_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_prs_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_trp_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_ab_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_applcmd_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_applsx_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_apple_id')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_number_upk')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'apploutc_create_user')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'apploutc_create_time')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_create_ip')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'apploutc_update_user')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'apploutc_update_time')->textInput(); ?>

    <?php echo $form->field($model, 'apploutc_update_ip')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
