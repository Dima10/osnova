<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplOutContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-out-content-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'apploutc_id',
            'apploutc_applout_id',
            'apploutc_prs_id',
            'apploutc_trp_id',
            'apploutc_ab_id',
            'apploutc_applcmd_id',
            'apploutc_applsx_id',
            'apploutc_apple_id',
            'apploutc_number_upk',
            'apploutc_create_user',
            'apploutc_create_time',
            'apploutc_create_ip',
            'apploutc_update_user',
            'apploutc_update_time',
            'apploutc_update_ip',

        ],
    ]);

Pjax::end();


?>

</div>
