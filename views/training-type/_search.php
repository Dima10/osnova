<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'trt_id') ?>

    <?= $form->field($model, 'trt_name') ?>

    <?= $form->field($model, 'trt_create_user') ?>

    <?= $form->field($model, 'trt_create_time') ?>

    <?= $form->field($model, 'trt_create_ip') ?>

    <?php // echo $form->field($model, 'trt_update_user') ?>

    <?php // echo $form->field($model, 'trt_update_time') ?>

    <?php // echo $form->field($model, 'trt_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
