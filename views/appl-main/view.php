<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplMain */

$this->title = $model->applm_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-main-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applm_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applm_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applm_id',
            'applm_prs_id',
            'applm_position',
            'applm_svc_id',
            'applm_ab_id',
            'applm_comp_id',
            'applm_trt_id',
            'applm_date_upk',
            'applm_apple_date',
            'applm_number_upk',
            'applm_applsx_date',
            'applm_appls_date',
            'applm_applcmd_date',
            'applm_applr_date',
            'applm_create_user',
            'applm_create_time',
            'applm_create_ip',
            'applm_update_user',
            'applm_update_time',
            'applm_update_ip',
        ],
    ]) ?>

</div>
