<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplMainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-main-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applm_id') ?>

    <?= $form->field($model, 'applm_prs_id') ?>

    <?= $form->field($model, 'applm_position') ?>

    <?= $form->field($model, 'applm_svc_id') ?>

    <?= $form->field($model, 'applm_ab_id') ?>

    <?php // echo $form->field($model, 'applm_comp_id') ?>

    <?php // echo $form->field($model, 'applm_trt_id') ?>

    <?php // echo $form->field($model, 'applm_date_upk') ?>

    <?php // echo $form->field($model, 'applm_apple_date') ?>

    <?php // echo $form->field($model, 'applm_number_upk') ?>

    <?php // echo $form->field($model, 'applm_applsx_date') ?>

    <?php // echo $form->field($model, 'applm_appls_date') ?>

    <?php // echo $form->field($model, 'applm_applcmd_date') ?>

    <?php // echo $form->field($model, 'applm_applr_date') ?>

    <?php // echo $form->field($model, 'applm_create_user') ?>

    <?php // echo $form->field($model, 'applm_create_time') ?>

    <?php // echo $form->field($model, 'applm_create_ip') ?>

    <?php // echo $form->field($model, 'applm_update_user') ?>

    <?php // echo $form->field($model, 'applm_update_time') ?>

    <?php // echo $form->field($model, 'applm_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
