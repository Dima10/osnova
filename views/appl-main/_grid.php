<?php

use app\models\SvcDocType;
use app\models\TrainingType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-main-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {sheet} {sheetX} {command}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pensil"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Update'),
                                ]);
                        },
                    'sheet' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-hand-right"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sheet'),
                                ]);
                        },
                    'sheetX' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-check"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sheet X'),
                                ]);
                        },
                    'command' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Command'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'id' => $model->applm_id]);
                            return $url;
                        }
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update', 'id' => $model->applm_id]);
                            return $url;
                        }
                        if ($action === 'sheet') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-sheet-id', 'id' => $model->applm_prs_id, 'date' => $model->applm_appls_date]);
                            return $url;
                        }
                        if ($action === 'sheetX') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-sheet-x', 'date' => $model->applm_applsx_date]);
                            return $url;
                        }
                        if ($action === 'command') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-command', 'date' => $model->applm_applcmd_date]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'applm_id',
            [
                'attribute' => 'applm_reestr',
                'value' => function ($data) {
                    $reestrsArray = \app\models\Constant::reestr_val();
                    return isset($reestrsArray[$data->applm_reestr]) ? $reestrsArray[$data->applm_reestr] : $reestrsArray[0];
                },
                'label' => Yii::t('app', 'Applm Reestr'),
                'filter' => \app\models\Constant::reestr_val(true),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_prs_id',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'prs_full_name',
                'value' => function ($data) {
                    return $data->applmPrs->prs_full_name ?? null;
                },
                'label' => Yii::t('app', 'Prs Full Name'),

            ],
            'applm_position',
            [
                'attribute' => 'svc_name',
                'value' => function ($data) {
                    return $data->applmSvc->svc_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Svc ID'),

            ],
            [
                'attribute' => 'trp_name',
                'value' => function ($data) {
                    return $data->applmTrp->trp_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Trp ID'),

            ],
            [
                'attribute' => 'ab_name',
                'value' => function ($data) {
                    return $data->applmAb->ab_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Ab ID'),

            ],

            'applm_agr_number',

            [
                'attribute' => 'comp_name',
                'value' => function ($data) {
                    return $data->applmComp->comp_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Comp ID'),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'trt_name',
                'value' => function ($data) {
                    return $data->applmTrt->trt_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Trt ID'),
                'filter' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ],

            [
                'attribute' => 'svdt_name',
                'value' => function ($data) {
                    return $data->applmSvdt->svdt_name ?? null;
                },
                'label' => Yii::t('app', 'Applm Svdt ID'),
                'filter' => ArrayHelper::map(SvcDocType::find()->asArray()->all(), 'svdt_id','svdt_name'),
            ],

            [
                'attribute' => 'applm_date_upk',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applm_apple_date',
            'applm_number_upk',
            'applm_appls_date',
            'applm_applsx_date',
            'applm_appls0_date',
            'applm_appls0x_date',
            'applm_applcmd_date',
            'applm_applr_date',

            [
                'attribute' => 'applm_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applm_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
