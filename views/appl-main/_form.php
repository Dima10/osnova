<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplMain */
/* @var $form yii\widgets\ActiveForm */
/* @var $ab array */
/* @var $comp array */
/* @var $svc array */
/* @var $person array */
/* @var $training_type */
?>

<div class="appl-main-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applm_prs_id')->dropDownList($person); ?>

    <?php echo $form->field($model, 'applm_position')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'applm_svc_id')->dropDownList($svc); ?>

    <?php echo $form->field($model, 'applm_ab_id')->dropDownList($ab); ?>

    <?php echo $form->field($model, 'applm_comp_id')->dropDownList($comp); ?>

    <?php echo $form->field($model, 'applm_trt_id')->dropDownList($training_type); ?>

    <?php echo $form->field($model, 'applm_date_upk')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php echo $form->field($model, 'applm_apple_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php echo $form->field($model, 'applm_number_upk')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'applm_appls_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?php echo $form->field($model, 'applm_applsx_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php echo $form->field($model, 'applm_appls0_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?php echo $form->field($model, 'applm_appls0x_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?php echo $form->field($model, 'applm_applcmd_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php echo $form->field($model, 'applm_applr_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
