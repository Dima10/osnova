<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EntityClass */

$this->title = $model->entc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity Classes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-class-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->entc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->entc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'entc_id',
            'entc_name',
            'entc_sy',
            'entc_create_user',
            'entc_create_time',
            'entc_create_ip',
            'entc_update_user',
            'entc_update_time',
            'entc_update_ip',
        ],
    ]) ?>

</div>
