<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSheet */
/* @var $searchModel app\models\ApplSheetContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Result {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sheet'),
]);// . $model->appls_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->appls_id, 'url' => ['view', 'id' => $model->appls_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sh-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <h2><?= $model->applsPrs->prs_full_name ?></h2>

    <h3>
    Всего правильных ответов: <?= $model->appls_score ?> / <?= $model->appls_score_max ?>
        <br><br>
    Набрано: <?= round(($model->appls_score / $model->appls_score_max) * 100) ?>%
    <br>
    <br>
    <?= $model->appls_passed == 1 ? Yii::t('app', 'Test Is Correct') : Yii::t('app', 'Test Is Incorrect') ?>
    <br>

    <?php if ($model->appls_passed == 1) :?>

    Поздравляем, Вы успешно завершили курс обучения!
    Просим обратиться в администрацию АНО ДПО "СУЦ "Основа"  для разъяснения дальнейших действий по тел. 8-499-372-09-62 или по электронной почте: info@pdo-onova.ru
    </h3>
    <?php else : ?>

    <h3>
    Увы, количество оставшихся попыток прохождения тестирования <?= $model->appls_try ?>.
    </h3>

        <?php if ($model->appls_try == 0) : ?>
        Просим обратиться в администрацию АНО ДПО "СУЦ "Основа" для пояснения дальнейших действия и предоставления справки о пройденном курсе обучения, без выдачи удостоверения по тел. 8-499-372-09-62 или по электронной почте: info@pdo-onova.ru
        <?php endif; ?>

    <?php endif; ?>

    <br>
    <br>

    <!--
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'appls_id',
                'appls_number',
                'appls_date',
                'applsPrs.prs_full_name',
                'applsTrp.trp_name',
                'appls_score_max',
                'appls_score',
                'appls_passed',
            ],
        ]);
    } catch (Exception $e) {
        $e->getMessage();
    } ?>
    -->

    <?= $this->render('_grid_detail_r', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

    <?= Html::a(Yii::t('app', 'Go to home'), ['person/training-program'], ['class' => 'btn btn-success']) ?>

</div>
