<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplSheet */
/* @var $person array */
/* @var $program array */

$this->title = Yii::t('app', 'Create Appl Sheet');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person' => $person,
        'program' => $program,
    ]) ?>

</div>
