<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $sheet app\models\ApplSheet */
/* @var $model yii\base\DynamicModel */
/* @var $question app\models\TrainingQuestion */
/* @var $answer array */
/* @var $id integer */

?>

<div class="appl-sheet-test-form">

    <?php $form = ActiveForm::begin(
        [
            'action' => ['test-next', 'uniq' => $sheet->appls_magic ,'id' => $id],
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
        );
        echo $form->errorSummary($model);
    ?>

    <?php echo Html::textarea('', $question->trq_question, ['rows' => 10, 'cols' => 100]); ?>

    <?php
        if (isset($question->trq_fname) && $question->trq_fname != '') {
            echo Html::img(\yii\helpers\Url::toRoute(['/training-question/image', 'id'=>$question->trq_id]));
        }
    ?>

    <?php echo $form->field($model, 'answer_id')->radioList($answer)->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Next Question'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
