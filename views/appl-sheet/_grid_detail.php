<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplSheetContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-sheet-content-index-grid">

<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            //'applsc_id',
            //'applscAppls.appls_number',
            //'applsc_trq_id',
            'applsc_trq_question:ntext',
            //'applsc_tra_id',
            [
                'attribute' => 'applsc_tra_answer',
                'value' => function ($data) {
                    return isset($data->applsc_tra_id) ? $data->applsc_tra_answer : 'нет ответа';
                },
            ],
            [
                'attribute' => 'applsc_tra_variant',
                'label' => Yii::t('app', 'Result'),
                'value' => function ($data) {
                    return $data->applsc_tra_variant == 1 ? Yii::t('app', 'Correct Answer') : Yii::t('app', 'Incorrect Answer');
                },
            ],

            [
                'attribute' => 'applsc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applsc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

?>

</div>
