<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShXContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sh-xcontent-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applsxc_id') ?>

    <?= $form->field($model, 'applsxc_applsx_id') ?>

    <?= $form->field($model, 'applsxc_prs_id') ?>

    <?= $form->field($model, 'applsxc_trp_id') ?>

    <?= $form->field($model, 'applsxc_score') ?>

    <?php // echo $form->field($model, 'applsxc_passed') ?>

    <?php // echo $form->field($model, 'applsxc_create_user') ?>

    <?php // echo $form->field($model, 'applsxc_create_time') ?>

    <?php // echo $form->field($model, 'applsxc_create_ip') ?>

    <?php // echo $form->field($model, 'applsxc_update_user') ?>

    <?php // echo $form->field($model, 'applsxc_update_time') ?>

    <?php // echo $form->field($model, 'applsxc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
