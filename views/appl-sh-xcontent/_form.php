<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShXContent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sh-xcontent-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applsxc_applsx_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsxc_prs_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsxc_trp_id')->textInput(); ?>

    <?php echo $form->field($model, 'applsxc_score')->textInput(); ?>

    <?php echo $form->field($model, 'applsxc_passed')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
