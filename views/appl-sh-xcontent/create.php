<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplShXContent */

$this->title = Yii::t('app', 'Create Appl Sh Xcontent');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Xcontents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-xcontent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
