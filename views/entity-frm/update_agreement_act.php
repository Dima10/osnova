<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAct */
/* @var $agreement array */
/* @var $account array */
/* @var $agreement_annex array */
/* @var $entity array */
/* @var $company array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Act'),
]) . $model->act_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-agreement-act-update"  id="forUpdateAgreementActController">

    <h1><?= Html::encode($this->title) ?></h1>
    <input id="actIdValue" class="hidden" value="<?php echo $model->act_id; ?>" />
    <?= $this->render('_form_agreement_act', [
        'model' => $model,
        'entity' => $entity,
        'company' => $company,
        'account' => $account,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
