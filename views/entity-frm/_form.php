<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $form yii\widgets\ActiveForm */
/* @var $entityType array */
?>

<div class="entity-frm-form">

    <?php
        $form = ActiveForm::begin([
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->ent_id],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);
        echo $form->errorSummary($model);

    ?>

    <?= $form->field($model, 'ent_entt_id')->dropDownList($entityType) ?>

    <?= $form->field($model, 'ent_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ent_name_short')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ent_orgn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ent_inn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ent_kpp')->textInput(['maxlength' => true]) ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'ent_okpo')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'ent_okpo')->textInput(['maxlength' => true])
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'ent_comment')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'ent_comment')->textarea(['maxlength' => true])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
