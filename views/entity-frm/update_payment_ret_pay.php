<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentRetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\AgreementAcc */
/* @var $entity array */
/* @var $pmodel app\models\PaymentRet */


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Payment Ret'),
]) . Yii::t('app', 'Agreement Acc').' '.$model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="entity-frm-update-payment-ret">


    <h1><?= Html::encode($this->title) ?></h1>

    <input type="button" class="btn btn-success"  value="<?= Yii::t('app', 'Back'); ?>" onclick="window.history.go(-1);"/>

    <?= $this->render('_grid_payment_ret_pay', [
        'dataProvider' => $dataProvider,
	    'searchModel' => null,
    ]) ?>

    <?= Html::a(Yii::t('app', 'Save Payment Ret'), ['save-payment-ret-pay', 'id' => $pmodel->ptr_id] , ['class' => 'btn btn-primary']) ?>


</div>