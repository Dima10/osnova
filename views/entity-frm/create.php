<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $entityType array */

$this->title = Yii::t('app', 'Create Entity');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'entityType' => $entityType,
    ]) ?>

</div>
