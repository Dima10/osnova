<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-agreement-act-a-index-grid">

<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'servicesRows',
            'class' => 'table table-striped table-bordered',
        ],
        'rowOptions' => function ($data, $key, $index, $grid) {
            return [
                'id' => "tr_".$data->acta_id,
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\ActionColumn'],

            [
                'visible' => ($model->act_ast_id == 1) ? true : false,
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['target'=>'_blank', 'title' => Yii::t('yii', 'View'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'deleteService',
                                    'data-acta_id' => $model->acta_id,
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = yii\helpers\Url::toRoute(['svc/view', 'id' => $model->actaAna->anaSvc->svc_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'id' => $key]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            [
                'attribute' => 'acta_id',
                'label' => Yii::t('app', 'Acta ID'),
                'value' => function ($data) {
                    return $data->acta_id;
                },
            ],

            [
                'attribute' => 'acta_ana_id',
                'label' => Yii::t('app', 'Acta Ana ID'),
                'value' => function ($data) {
                    if (isset($data->actaAna)) {

                        return ($data->actaAna->agreementAccAs[0]->acaAga->aga_number ?? '')
                            .' *** '.

                            (isset($data->actaAna->anaAgra) ? $data->actaAna->anaAgra->agra_number : '') . ' / ' .
                            (isset($data->actaAna->anaSvc) ? $data->actaAna->anaSvc->svc_name : '') . ' / ' .
                            (isset($data->actaAna->anaTrp) ? $data->actaAna->anaTrp->trp_name : '');
                    }
                    return '';
                },
            ],

            [
                'attribute' => 'acta_qty',
                'label' => Yii::t('app', 'Ana Qty'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->acta_id.'_priceQty" class="'.$data->acta_id.'_processUpdate">'.$data->acta_qty.'</span>';
                    $input = Html::input('text','acta_qty', $data->acta_qty, ['class'=>'hidden '.$data->acta_id.'_processUpdate processUpdateIput inputQty', 'id'=>$data->acta_id.'_acta_qty', 'data-relate-actaid'=>$data->acta_id, 'size'=>5, 'style'=>'text-align:center']);

                    return isset($data->acta_qty) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'acta_price',
                'label' => Yii::t('app', 'Acta Price'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->acta_id.'_priceLabel" class="'.$data->acta_id.'_processUpdate">'.$data->acta_price.'</span>';
                    $input = Html::input('text','acta_price', $data->acta_price, ['class'=>'hidden '.$data->acta_id.'_processUpdate processUpdateIput', 'id'=>$data->acta_id.'_acta_price', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->acta_price) ? $span.$input : 0;
                },
            ],
            [
                'attribute' => 'acta_tax',
                'label' => Yii::t('app', 'Acta Tax'),
                'format' => 'raw',
                'value' => function ($data) {

                    $span = '<span id="'.$data->acta_id.'_acta_tax_span" class="'.$data->acta_id.'_processTax">'.$data->acta_tax.'</span>';
                    $input = Html::input('text','acta_tax', $data->acta_tax, ['class'=>'hidden '.$data->acta_id.'_processTax', 'readonly'=>'readonly', 'id'=>$data->acta_id.'_acta_tax', 'size'=>8, 'style'=>'text-align:center']);

                    return isset($data->acta_tax) ? $span.$input : Yii::t('app', 'Without Tax');
                },
            ],


            [
                'attribute' => 'acta_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            /*
            [
                'attribute' => 'acta_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'acta_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button type="button" id="exampleModalBtn" class="hidden" data-toggle="modal" data-target="#exampleModal"></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: none !important;">
                <h5 class="modal-title" id="exampleModalLabel">Приложение обновлено</h5>
            </div>
        </div>
    </div>
</div>