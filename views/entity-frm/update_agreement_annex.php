<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnex */
/* @var $agreement array */
/* @var $entity array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Annex'),
]) . $model->agra_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-agreement-annex-update" id="forUpdateAgreementAnnexController">

    <h1><?= Html::encode($this->title) ?></h1>
    <input id="appIdValue" class="hidden" value="<?php echo $model->agra_id; ?>" />
    <?= $this->render('_form_agreement_annex', [
        'model' => $model,
        'agreement' => $agreement,
        'entity' => $entity,
        'agreement_status' => $agreement_status,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
