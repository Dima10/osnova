<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $entityType array */

?>

<div class="entity-frm-update">

    <h1><?= Yii::t('app', 'ID') ?> <?= Html::encode($model->ent_id) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'entityType' => $entityType,
    ]) ?>

</div>
