<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $region array */

?>

<?= Html::dropDownList('region', isset($region[0]) ? $region[0] : '', $region) ?>
