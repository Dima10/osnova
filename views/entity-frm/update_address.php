<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $addressType array */
/* @var $country array */
/* @var $region array */
/* @var $city array */
/* @var $entity array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Address'),
]) . $model->add_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_address', [
        'model' => $model,
        'entity' => $entity,
        'addressType' => $addressType,
        'country' => $country,
        'region' => $region,
        'city' => $city,
    ]) ?>

</div>
