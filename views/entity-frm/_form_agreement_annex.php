<?php

use app\models\Agreement;
use app\models\Svc;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnex */
/* @var $form yii\widgets\ActiveForm */
/* @var $agreement array */
/* @var $entity array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="entity-frm-agreement-annex-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
                'id' => 'agreementAnnexForm'
            ]);

    ?>

    <?= $form->field($model, 'agra_number')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($model, 'agra_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])
    ?>

    <?= $form->field($model, 'agra_ab_id')->dropDownList($entity, ['ReadOnly' => true]); ?>

    <?= $form->field($model, 'agra_ast_id')->dropDownList($agreement_status) ?>

    <?= $form->field($model, 'agra_agr_id')->dropDownList($agreement, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agra_sum')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_tax')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'agra_data')->fileInput() ?>

    <?= $form->field($model, 'agra_data_sign')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary updateApp']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render('_grid_agreement_annex_a', [
        'dataProvider' => $dataProvider,
        'model' => $model,
    ]) ?>

<?php if($model->agra_ast_id == 1){ ?>
    <button type="button" id="showHideServiceId" class="btn btn-success">Добавить услугу</button>
    <div id="serviceForm" class="hidden">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
                <?= Html::textInput('svcText', '', ['id' => 'svcText', 'class'=> 'form-control', 'maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= Html::button(Yii::t('app', 'Search'), ['id' => 'svcSearch']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
            <div class="col-sm-6"><?= Html::dropDownList('svc', [], ArrayHelper::map(Svc::find()->orderBy(['svc_name'=>SORT_ASC])->all(), 'svc_id', 'svc_name'), ['prompt' => '<>', 'id' => 'svc', 'class'=> 'form-control', ]); ?></div>
            <div class="col-sm-4"></div>
        </div>

        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
                <?= Html::textInput('progText', '', ['id' => 'progText', 'class'=> 'form-control', 'maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= Html::button(Yii::t('app', 'Search'), ['id' => 'progSearch', ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Training Prog'), 'prog'); ?></div>
            <div class="col-sm-6"><?= Html::dropDownList('prog', [], [], ['id' => 'prog', 'class'=> 'form-control']); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svc Cost'), 'cost'); ?></div>
            <div class="col-sm-6"><?= Html::textInput('cost', '', ['id' => 'cost', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svc Price'), 'price'); ?></div>
            <div class="col-sm-6"><?= Html::textInput('price_a', '', ['id' => 'price_a', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Ana Price'), 'price'); ?></div>
            <div class="col-sm-6"><?= Html::textInput('price', '', ['id' => 'price', 'class'=> 'form-control', ]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Ana Qty'), 'qty'); ?></div>
            <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control', ]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6"></div>
            <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add just'), '', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
        </div>
    </div>
<?php } ?>
    <br />
</div>

<?php
$script = '
 $(function() {
    
    $("#svc").change(function() {
        var svc = this.value;
        $("#qty").val(1);
        $("#price").val("");
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-cost']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#cost").val(data);
                    }
                );
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-price']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#price_a").val(data);
                    }
                );

        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-trp']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#prog").html(data);
                    }
                );

    });

    $("#svcSearch").click(
            function() {
                var svc = $("#svcText").val();
                $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-svc']).'",
                    {
                      svc_name : svc
                    },
                    function (data) {
                        $("#svc").html(data);
                    }
                );
            }
    );

    $("#progSearch").click(
            function() {
                var trp = $("#progText").val();
                var svc = $("#svc").val();
                $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-trp']).'",
                    {
                      svc_id : svc,
                      trp_name : trp
                    },
                    function (data) {
                        $("#prog").html(data);
                    }
                );
            }
    );
    
});
';
$this->registerJs($script, yii\web\View::POS_END);
?>