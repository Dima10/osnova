<?php

use app\models\AgreementStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAddSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */

echo Html::a(Yii::t('app', 'Create Agreement Add'), ['create-agreement-add', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']);

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement-add', 'ent_id' => $entity->ent_id, 'id' => $model->agadd_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement-add', 'id' => $model->agadd_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'agadd_id',
            'agadd_number',
            'agadd_date',
            [
                'attribute' => 'number',
                'label' => Yii::t('app', 'Agadd Agr ID'),
                'value' => function ($data) {
                    return $data->agaddAgr->agr_number ?? '';
                },
            ],
            [
                'attribute' => 'agadd_ast_id',
                'label' => Yii::t('app', 'Agadd Ast ID'),
                'value' => function ($data) {
                    return $data->agaddAst->ast_name ?? '';
                },
                'filter' => ArrayHelper::map(AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ],
            [
                'attribute' => 'agadd_file_data',
                'label' => Yii::t('app', 'Agadd File Data'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->agadd_file_name, yii\helpers\Url::toRoute(['download-agreement-add', 'id' => $data->agadd_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'agadd_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();





