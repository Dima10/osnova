<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $addressType array */
/* @var $country array */
/* @var $region array */
/* @var $city array */
/* @var $entity array */

$this->title = Yii::t('app', 'Create Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_address', [
        'model' => $model,
        'addressType' => $addressType,
        'entity' => $entity,
        'country' => $country,
        'region' => $region,
        'city' => $city,
    ]) ?>

</div>
