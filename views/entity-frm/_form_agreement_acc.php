<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$annex_a = [];
?>

<div class="agreement-acc-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],
            'id' => 'agreementAccForm'
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'aga_number')->textInput(['maxlength' => true]); ?>

    <?=  $form->field($model, 'aga_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])
    ?>

    <?= $form->field($model, 'aga_ast_id')->dropDownList($agreement_status) ?>

    <?php echo $form->field($model, 'aga_ab_id')->dropDownList($entity, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_comp_id')->dropDownList($company, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_agr_id')->dropDownList($agreement, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_agra_id')->dropDownList($agreement_annex, ['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_sum')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_comment')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'aga_tax')->textInput(['ReadOnly' => true]); ?>

    <?php echo $form->field($model, 'aga_data')->fileInput(); ?>

    <?php echo $form->field($model, 'aga_data_sign')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary updateAcc']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <h3 id="newFileLink"></h3>
    <?= $this->render('_grid_agreement_acc_a', [
        'dataProvider' => $dataProvider,
        'model' => $model,
    ]) ?>


    <?php if($model->aga_ast_id == 1){ ?>
        <button type="button" id="showHideServiceId" class="btn btn-success" style="margin-bottom: 20px">Добавить услугу</button>
    <?php } ?>
    <div id="serviceForm" class="hidden">
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
            <div class="col-sm-6"><?= Html::dropDownList('annex', '', $annex_a, ['id' => 'annex_a', 'class'=> 'form-control']) ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Price'), 'price'); ?></div>
            <div class="col-sm-6"><?= Html::textInput('price', '', ['id' => 'price', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Qty'), 'qty'); ?></div>
            <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control']); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6"></div>
            <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add Svc'), '#', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
        </div>
    </div>
    <br />

</div>

<?php

$script = '
$(function() {    
    $("#agreementacc-aga_agr_id").change(function() {
        var agr_id = this.value;
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a']).'",
                    {
                      agr_id : agr_id,
                    },
                    function (data) {
                        $("#annex_a").html(data);
                    }
        );
                
                
        $("#annex_a").change(function() {
            var ana_id = this.value;
            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-price']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#price").val(data);
                    }
            );

            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-acc-a-qty']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#qty").val(data);
                    }
            );
        });
    });
});
';
$this->registerJs($script, yii\web\View::POS_END);


?>