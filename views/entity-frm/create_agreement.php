<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\Agreement */
/* @var $entity array */
/* @var $account array */
/* @var $company array */
/* @var $pattern array */
/* @var $staff array */
/* @var $address array */

$this->title = Yii::t('app', 'Create Agreement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-agreement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_dyn', [
        'model' => $model,
        'modelA' => $modelA,
        'entity' => $entity,
        'account' => $account,
        'company' => $company,
        'pattern' => $pattern,
        'staff' => $staff,
        'address' => $address,
    ]) ?>

</div>
