<?php

use app\models\TrainingType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplShSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $trt array */
/* @var $trp array */
/* @var $entity app\models\Entity */

?>
<div class="entity-frm-appl-sh-index-grid">

<?= Yii::t('app', 'Appl Sh'); ?>

<div>
    <?= Html::a(Yii::t('app', 'Export Appl Sh'), ['appl-sh-export-xml', 'id' => $entity->ent_id], ['class' => 'btn btn-info']) ?>
</div>

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['appl-sh/generate-file', 'ent_id' => $entity->ent_id, 'id' => $model->appls_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            'appls_id',
            [
                'attribute' => 'applr_number',
                'label' => Yii::t('app', 'Appls Applr ID'),
                'value' =>
                    function ($data) {
                        return $data->applsApplr->applr_number ?? '';
                    },
            ],
            [
                'attribute' => 'prs_full_name',
                'value' =>
                    function ($data) {
                        return $data->applsPrs->prs_full_name;
                    },
                'label' => Yii::t('app', 'Appls Prs ID'),
                //'filter' => $ab,
            ],
            [
                'attribute' => 'prs_connect_user',
                'value' =>
                    function ($data) {
                        return $data->applsPrs->prs_connect_user;
                    },
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            [
                'attribute' => 'trp_name',
                'value' =>
                    function ($data) {
                        return $data->applsTrp->trp_name;
                    },
                'label' => Yii::t('app', 'Appls Trp ID'),
//                'filter' => $trp,
            ],

            'appls_number',
            'appls_date',

            /*
            [
                'attribute' => 'appls_number',
                'label' => Yii::t('app', 'Link'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/test', 'uniq' => $data->appls_magic]);
                        //return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/test', 'uniq' => $data->appls_magic]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/test', 'uniq' => $data->appls_magic]), ['target' => '_blank']);
                    } else {
                        return '';
                    }

                },
            ],
            */
            [
                'attribute' => 'appls_number1',
                'label' => Yii::t('app', 'Result'),
                'format' => 'raw',
                'value' => function ($data) {
                    if (isset($data->appls_magic)) {
                        return Html::a(Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/result', 'id' => $data->appls_id]), Yii::$app->urlManager->createAbsoluteUrl(['appl-sh/result', 'id' => $data->appls_id, 'flag' => 1]), ['target' => '_blank']);
                    } else {
                        return '';
                    }
                },
            ],



            [
                'attribute' => 'appls_passed',
                'label' => Yii::t('app', 'Appls Passed'),
                'value' =>
                    function ($data) {
                        return \app\models\Constant::YES_NO[$data->appls_passed];
                    },
                'filter' => \app\models\Constant::yes_no(true),
            ],

            'appls_score_max',
            'appls_score',

            [
                'attribute' => 'appls_file_name',
                'label' => Yii::t('app', 'Appls File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->appls_file_name, yii\helpers\Url::toRoute(['appl-sh/download', 'id' => $data->appls_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'appls_trt_id',
                'label' => Yii::t('app', 'Appls Trt ID'),
                'value' =>
                    function ($data) {
                        return $data->applsTrt->trt_name;
                    },
                'filter' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ],


            [
                'attribute' => 'applr_flag',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' =>
                    function ($data) {
                        return \app\models\Constant::YES_NO[$data->applsApplr->applr_flag ?? 0];
                    },
                'filter' => \app\models\Constant::yes_no(true),
            ],


            [
                'attribute' => 'appls_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'appls_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
