<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>

<?php
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['id' => 'grid_act'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('', '#',
                                [
                                    'class' => 'glyphicon glyphicon-trash',
                                    'onclick' => '$.post("' . \yii\helpers\Url::toRoute(['/entity-frm/ajax-agreement-act-a-delete', 'id' => $model['ana_id']]) . '",
                                                                        function(data) {
                                                                            $( \'#grid_act\' ).html(data);
                                                                            $.get("' . \yii\helpers\Url::to(['/entity-frm/ajax-agreement-act-a-sum']) . '",
                                                                                {
                                                                                },
                                                                                function (data) {
                                                                                    $("#sum").val(data);
                                                                                    $.get("' . \yii\helpers\Url::to(['/entity-frm/ajax-agreement-act-a-tax']) . '",
                                                                                        {
                                                                                        },
                                                                                        function (data) {
                                                                                            $("#tax").val(data);
                                                                                        }
                                                                                    );
                                                                                 }
                                                                             );
                                                                           
                                                                            
                                                                        });',
                                ]
                            );
                        }

                ],
            ],
            [
                'label' => Yii::t('app', 'Acta ID'),
                'value' => 'ana_id',
            ],
            [
                'label' => Yii::t('app', 'Acta Ana ID'),
                'value' => 'ana_name',
            ],
            [
                'label' => Yii::t('app', 'Acta Qty'),
                'value' => 'qty',
            ],
            [
                'label' => Yii::t('app', 'Acta Price'),
                'value' => 'price',
            ],
            [
                'label' => Yii::t('app', 'Acta Tax'),
                'value' => function ($data) {
                    return $data['tax'] == 0 ? Yii::t('app', 'Without Tax') : $data['tax'];
                },
            ],

        ],
    ]);
} catch (Exception $e) {
}


?>

