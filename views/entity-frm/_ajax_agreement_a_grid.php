<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>

<?php
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['id' => 'grid_svc'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('', '#',
                                [
                                    'class' => 'glyphicon glyphicon-trash',
                                    'onclick' => '$.post("' . \yii\helpers\Url::toRoute(['/entity-frm/ajax-agreement-a-delete', 'id' => $model['svc_id']]) . '",
                                                                        function(data) {
                                                                            $( \'#grid_svc\' ).html(data);
                                                                            $.get("' . \yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-sum']) . '",
                                                                                {
                                                                                },
                                                                                function (data) {
                                                                                    $("#sum").val(data);
                                                                                    $.get("' . \yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-tax']) . '",
                                                                                        {
                                                                                        },
                                                                                        function (data) {
                                                                                            $("#tax").val(data);
                                                                                        }
                                                                                    );
                                                                                 }
                                                                             );
                                                                           
                                                                            
                                                                        });',
                                ]
                            );
                        }

                ],
            ],
            [
                'label' => Yii::t('app', 'Svc ID'),
                'value' => 'svc_id',
            ],
            [
                'label' => Yii::t('app', 'Ana Svc ID'),
                'value' => 'svc_name',
            ],
            [
                'label' => Yii::t('app', 'Ana Trp ID'),
                'value' => 'trp_name',
            ],
            [
                'label' => Yii::t('app', 'Svc Cost'),
                'value' => 'cost_a',
            ],
            [
                'label' => Yii::t('app', 'Svc Price'),
                'value' => 'price_a',
            ],
            [
                'label' => Yii::t('app', 'Ana Qty'),
                'value' => 'qty',
            ],
            [
                'label' => Yii::t('app', 'Ana Price'),
                'value' => 'price',
            ],
            [
                'label' => Yii::t('app', 'Ana Tax'),
                'value' => function ($data) {
                    return $data['tax'] == 0 ? Yii::t('app', 'Without Tax') : $data['tax'];
                },
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}


?>

