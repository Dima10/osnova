<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $agent array */
/* @var $agent_m array */
/* @var $entityClass array */


?>

<div class="entity-frm-update">

    <h1><?= Yii::t('app', 'ID') ?> <?= Html::encode($model->ent_id) ?></h1>

    <?= $this->render('_form_agent', [
        'model' => $model,
        'agent' => $agent,
        'agent_m' => $agent_m,
        'entityClass' => $entityClass,
    ]) ?>

</div>
