<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $entity app\models\Entity */
/* @var $searchModel app\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Account'), ['create-account', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']) ?>
    </p>

<?php

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-account', 'ent_id' => $entity->ent_id, 'id' => $model->acc_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            [
                'attribute' => 'acc_id',
                'options' => ['width' => '100'],
            ],

            //'acc_ab_id',
            [
                'attribute' => 'acc_active',
            ],

            'accBank.bank_name',
            'acc_number',

            [
                'attribute' => 'acc_create_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'acc_create_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'acc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'acc_update_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'acc_update_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'acc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();
