<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\AgreementAnnex */
/* @var $agreement array */
/* @var $account array */
/* @var $address array */
/* @var $staff array */
/* @var $entity array */
/* @var $pattern array */
/* @var $svc array */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Create Agreement Annex');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-agreement-annex-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_annex_dyn', [
        'model' => $model,
        'modelA' => $modelA,
        'entity' => $entity,
        'agreement' => $agreement,
        'account' => $account,
        'address' => $address,
        'staff' => $staff,
        'pattern' => $pattern,
        'svc' => $svc,
        'dataProvider' => $dataProvider,

    ]) ?>


</div>
