<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ListenerSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $entity app\models\Entity */

?>


<?php

echo Yii::t('app', 'Listener');

//Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{add}',
                'buttons' => [
                    'add' =>
                        function ($url, $model) {
                            if (($model['applr_flag'] == 1) && ($model['applr_ast_id'] == 10)) {
                                if (is_null($model['applrc_applf_id'])){
                                    return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, ['title' => Yii::t('yii', 'Insert')]);
                                } else {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Edit')]);
                                }
                            } else {
                                return '';
                            }
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'add') {
                            $url = yii\helpers\Url::to(['create-appl-final', 'ent_id' => $entity->ent_id, 'id' => $model['applrc_id']]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            'prs_last_name',
            'prs_first_name',
            'prs_middle_name',
            'applr_number',
            'applr_date',
            [
                'attribute' => 'pv_time',
                'label' => Yii::t('app', 'Education Time'),
                'value' => function ($data) {
                    return $data['pv_time'];
                },
            ],
            [
                'attribute' => 'applr_ast_id',
                'value' => function ($data) {
                    return $data['applr_ast_name'];
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\AgreementStatus::find()->orderBy('ast_id')->all(), 'ast_id', 'ast_name'),
            ],

            [
                'attribute' => 'applr_reestr',
                'value' => function ($data) {
                    $reestrsArray = \app\models\Constant::reestr_val();
                    return isset($reestrsArray[$data['applr_reestr']]) ? $reestrsArray[$data['applr_reestr']] : $reestrsArray[0];
//                    return \app\models\Constant::reestr_val()[$data['applr_reestr']];
                },
                'filter' => \app\models\Constant::reestr_val(true),
            ],
            [
                'attribute' => 'svc_name',
                'value' => function ($data) {
                    return $data['svc_name'];
                },
//                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Svc::find()->orderBy('svc_name')->all(), 'svc_id', 'svc_name'),
            ],
            [
                'attribute' => 'trp_name',
                'value' => function ($data) {
                    return $data['trp_name'];
                },
//                'filter' => \yii\helpers\ArrayHelper::map(\app\models\TrainingProg::find()->orderBy('trp_name')->all(), 'trp_id', 'trp_name'),
            ],

            [
                'attribute' => 'applr_flag',
                'label' => Yii::t('app', 'Applr Flag'),
                'value' =>
                    function ($data) {
                        try {
                            return \app\models\Constant::YES_NO[$data['applr_flag']];
                        } catch (Exception $e){
                            return $data['applr_flag'];
                        }
                    },
                'filter' => \app\models\Constant::yes_no(true),
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
//Pjax::end();

