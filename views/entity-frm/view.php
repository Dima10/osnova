<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Entity */
/* @var $entityType array */
/* @var $entityClass array */
/* @var $account array */
/* @var $agent array */
/* @var $agent_m array */
/* @var $comp array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $reestr array */
/* @var $training_type array */
/* @var $agreement_status array */

/* @var $searchModelStaff app\models\StaffSearch */
/* @var $dataProviderStaff yii\data\ActiveDataProvider */
/* @var $searchModelContact app\models\ContactSearch */
/* @var $dataProviderContact yii\data\ActiveDataProvider */
/* @var $searchModelAddress app\models\AddressSearch */
/* @var $dataProviderAddress yii\data\ActiveDataProvider */
/* @var $searchModelAccount app\models\AccountSearch */
/* @var $dataProviderAccount yii\data\ActiveDataProvider */
/* @var $searchModelAgreement app\models\AgreementSearch */
/* @var $dataProviderAgreement yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAnnex app\models\AgreementAnnexSearch */
/* @var $dataProviderAgreementAnnex yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAcc app\models\AgreementAccSearch */
/* @var $dataProviderAgreementAcc yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAct app\models\AgreementActSearch */
/* @var $dataProviderAgreementAct yii\data\ActiveDataProvider */
/* @var $searchModelPaymentTo app\models\PaymentToSearch */
/* @var $dataProviderPaymentTo yii\data\ActiveDataProvider */
/* @var $searchModelPaymentRet app\models\PaymentRetSearch */
/* @var $dataProviderPaymentRet yii\data\ActiveDataProvider */
/* @var $searchModelAgreementAdd app\models\AgreementAddSearch */
/* @var $dataProviderAgreementAdd yii\data\ActiveDataProvider */

/* @var $searchModelListener app\models\ListenerSearch */
/* @var $dataProviderListener yii\data\ArrayDataProvider */

/* @var $searchModelApplRequest app\models\ApplRequestSearch */
/* @var $dataProviderApplRequest yii\data\ActiveDataProvider */
/* @var $searchModelApplSh app\models\ApplShSearch */
/* @var $dataProviderApplSh yii\data\ActiveDataProvider */
/* @var $searchModelApplSheet app\models\ApplSheetSearch */
/* @var $dataProviderApplSheet yii\data\ActiveDataProvider */
/* @var $searchModelApplFinal app\models\ApplFinalSearch */
/* @var $dataProviderApplFinal yii\data\ActiveDataProvider */


$this->title = $model->ent_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity')];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-view">

    <h1><?= Html::encode($model->ent_name_short) ?></h1>

    <!--
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ent_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ent_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    -->

<?php

// https://github.com/yiisoft/yii2/issues/4890
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTabEntity', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTabEntity');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

$isAdmin = false;
if(isset(Yii::$app->user->identity)) {
    $isAdmin = Yii::$app->user->identity->isAdmin;
}

// Entity
$items[] = [
    'label' => Yii::t('app', 'Entity'),
    'content' =>
        $this->render('_view',
            [
                'model' => $model,
                'entityType' => $entityType,
            ])
    ,
];

if (isset(Yii::$app->user->identity->accessActions['staff/index'])|| $isAdmin){
    // Staff
    $items[] = [
        'label' => Yii::t('app', 'Staff'),
        'content' =>
            $this->render('_grid_staff',
                [
                    'dataProvider' => $dataProviderStaff,
                    'searchModel' => $searchModelStaff,
                    'entity' => $model,
                    'person' => null,
                ])
        ,
    ];
}

if (isset(Yii::$app->user->identity->accessActions['contact/index']) || $isAdmin){
    // Contact
    $items[] = [
        'label' => Yii::t('app', 'Contacts'),
        'content' =>
            $this->render('_grid_contact',
                [
                    'dataProvider' => $dataProviderContact,
                    'searchModel' => $searchModelContact,
                    'entity' => $model,
                    'addrbook' => null,
                    'contactType' => null,
                ])
        ,
    ];
}

if (isset(Yii::$app->user->identity->accessActions['address/index']) || $isAdmin){
    // Address
    $items[] = [
        'label' => Yii::t('app', 'Addresses'),
        'content' =>
            $this->render('_grid_address',
                [
                    'dataProvider' => $dataProviderAddress,
                    'searchModel' => $searchModelAddress,
                    'entity' => $model,
                    'addrbook' => null,
                    'contactType' => null,
                    'addressType' => null,
                    'country' => null,
                    'region' => null,
                    'city' => null,
                ])
        ,
    ];
}

if (isset(Yii::$app->user->identity->accessActions['account/index']) || $isAdmin){
    // Account
    $items[] = [
        'label' => Yii::t('app', 'Requisites'),
        'content' =>
            $this->render('_grid_account',
                [
                    'dataProvider' => $dataProviderAccount,
                    'searchModel' => $searchModelAccount,
                    'entity' => $model,
                ])
        ,
    ];
}


$documentItems = [];
if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-agreement-in-menu'])|| $isAdmin)){
    $documentItems[] = [
        'label' => Yii::t('app', 'Agreements'),
        'content' =>
            $this->render('_grid_agreement',
                [
                    'dataProvider' => $dataProviderAgreement,
                    'searchModel' => $searchModelAgreement,
                    'entity' => $model,
                    'account' => $account,
                    'agreement_status' => $agreement_status,
                    'addrbook' => null,
                    'comp' => $comp,
                ]),
    ];
}
if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-agreement-add-in-menu'])|| $isAdmin)){
    $documentItems[] = [
        'label' => Yii::t('app', 'Agreement Adds'),
        'content' =>
            $this->render('_grid_agreement_add',
                [
                    'dataProvider' => $dataProviderAgreementAdd,
                    'searchModel' => $searchModelAgreementAdd,
                    'entity' => $model,
                ]),
    ];
}
if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-agreement-annex-in-menu'])|| $isAdmin)){
    $documentItems[] = [
        'label' => Yii::t('app', 'Agreement Annexes'),
        'content' =>
            $this->render('_grid_agreement_annex',
                [
                    'dataProvider' => $dataProviderAgreementAnnex,
                    'searchModel' => $searchModelAgreementAnnex,
                    'entity' => $model,
                    'agreement_status' => $agreement_status,
                    'comp' => $comp,
                    'agreement' => $agreement,
                ]),
    ];
}
if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-account-in-menu'])|| $isAdmin)){
    $documentItems[] = [
        'label' => Yii::t('app', 'Agreement Accs'),
        'content' =>
            $this->render('_grid_agreement_acc',
                [
                    'dataProvider' => $dataProviderAgreementAcc,
                    'searchModel' => $searchModelAgreementAcc,
                    'entity' => $model,
                    'agreement_status' => $agreement_status,
                    'company' => $comp,
                    'agreement' => $agreement,
                    'agreement_annex' => $agreement_annex,
                ]),
    ];
}
if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-agreement-act-in-menu'])|| $isAdmin)){
    $documentItems[] = [
        'label' => Yii::t('app', 'Agreement Acts'),
        'content' =>
            $this->render('_grid_agreement_act',
                [
                    'dataProvider' => $dataProviderAgreementAct,
                    'searchModel' => $searchModelAgreementAct,
                    'entity' => $model,
                    'agreement_status' => $agreement_status,
                    'company' => $comp,
                    'agreement' => $agreement,
                    'agreement_annex' => $agreement_annex,
                ]),
    ];
    $documentItems[] = [
        'label' => '',
        'options' => ['class'=> 'divider' ],
        'content' =>'',
    ];
}

if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-payment-to-in-menu'])|| $isAdmin)) {
    $documentItems[] = [
        'label' => Yii::t('app', 'Payment To'),
        'content' =>
            $this->render('_grid_payment_to',
                [
                    'dataProvider' => $dataProviderPaymentTo,
                    'searchModel' => $searchModelPaymentTo,
                    'entity' => $model,
                ]),
    ];
}

if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-payment-ret-in-menu'])|| $isAdmin)) {
    $documentItems[] = [
        'label' => Yii::t('app', 'Payment Ret'),
        'content' =>
            $this->render('_grid_payment_ret',
                [
                    'dataProvider' => $dataProviderPaymentRet,
                    'searchModel' => $searchModelPaymentRet,
                    'entity' => $model,
                ]),
    ];
}
// Documents

if((isset(Yii::$app->user->identity->accessActions['entity-frm/show-documents-in-navbar'])|| $isAdmin)) {
    $items[] = [
        'label' => Yii::t('app', 'Documents'),
        'items' => $documentItems
    ];
}

if (isset(Yii::$app->user->identity->accessActions['finance-book/index'])|| $isAdmin){
    // Finance
    $items[] = [
        'label' => Yii::t('app', 'Finance'),
        'content' =>  $this->render('_grid_finance_book',
            [
                'dataProvider' => $dataProviderFB,
                'searchModel' => $searchModelFB,
                'addrbook' => null,
                'contactType' => null,
            ])
        ,
    ];
}


    // Listener
    $listenerItems = [];
    if (isset(Yii::$app->user->identity->accessActions['appl-request/index'])|| $isAdmin){
        $listenerItems[] = [
            'label' => Yii::t('app', 'Appl Requests'),
            'content' =>
                $this->render('_grid_appl_request',
                    [
                        'dataProvider' => $dataProviderApplRequest,
                        'searchModel' => $searchModelApplRequest,
                        'entity' => $model,
                        'reestr' => $reestr,
                        'training_type' => $training_type,
                        'agreement_status' => $agreement_status,
                        'comp' => $comp,
                    ])
        ];
    }
    if (isset(Yii::$app->user->identity->accessActions['report-listener/index'])|| $isAdmin){
        $listenerItems[] = [
            'label' => Yii::t('app', 'Listener Reestr'),
            'content' =>
                $this->render('_grid_listener',
                    [
                        'dataProvider' => $dataProviderListener,
                        'searchModel' => $searchModelListener,
                        'entity' => $model,
                    ])
        ];
    }
    if ((isset(Yii::$app->user->identity->accessActions['appl-sh/index'])|| $isAdmin)){
        $listenerItems[] = [
            'label' => Yii::t('app', 'Appl Shs'),
            'content' =>
                $this->render('_grid_appl_sh',
                    [
                        'dataProvider' => $dataProviderApplSh,
                        'searchModel' => $searchModelApplSh,
                        'entity' => $model,
                    ])
        ];
    }
    if ((isset(Yii::$app->user->identity->accessActions['appl-sheet/index'])|| $isAdmin)){
        $listenerItems[] = [
            'label' => Yii::t('app', 'Appl Sheets'),
            'content' =>
                $this->render('_grid_appl_sheet',
                    [
                        'dataProvider' => $dataProviderApplSheet,
                        'searchModel' => $searchModelApplSheet,
                        'entity' => $model,
                        'trt' => null,
                        'trp' => null,
                    ])
        ];
    }
    if ((isset(Yii::$app->user->identity->accessActions['appl-final/index'])|| $isAdmin)){
        $listenerItems[] = [
            'label' => Yii::t('app', 'Appl Finals'),
            'content' =>
                $this->render('_grid_appl_final',
                    [
                        'dataProvider' => $dataProviderApplFinal,
                        'searchModel' => $searchModelApplFinal,
                        'entity' => $model,
                    ])
        ];
    }

    if(count($listenerItems) > 0){
        $items[] = [
            'label' => Yii::t('app', 'Listener'),
            'items' =>$listenerItems,
        ];
    }

// Events
$items[] = [
    'label' => Yii::t('app', 'Events'),
    'content' => ''
    /*
        $this->render('_grid_address',
            [
            'dataProvider' => $dataProviderAddress,
            'searchModel' => $searchModelAddress,
            'addrbook' => null,
            'contactType' => null,
            ])
    */
    ,
];

// Tasks
$items[] = [
    'label' => Yii::t('app', 'Tasks'),
    'content' => ''
    /*
        $this->render('_grid_address',
            [
            'dataProvider' => $dataProviderAddress,
            'searchModel' => $searchModelAddress,
            'addrbook' => null,
            'contactType' => null,
            ])
    */
    ,
];

// Management
$items[] = [
    'label' => Yii::t('app', 'Management'),
    'content' =>
        $this->render('_update_agent',
            [
                'model' => $model,
                'agent' => $agent,
                'agent_m' => $agent_m,
                'entityClass' => $entityClass,
            ])
    ,
];

try {
    echo Tabs::widget([
        'navType' => 'nav-tabs',
        'items' => $items,
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
    var_dump($e->getTrace());
}


?>

</div>
