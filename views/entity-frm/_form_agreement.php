<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $company array */
/* @var $account array */
/* @var $agreement_status array */

?>

<div class="entity-frm-agreement-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            ]);

        echo $form->errorSummary($model);

    ?>

    <?= $form->field($model, 'agr_number')->textInput() ?>

    <?= $form->field($model, 'agr_ast_id')->dropDownList($agreement_status) ?>

    <?= $form->field($model, 'agr_ab_id')->dropDownList($entity) ?>

    <?= $form->field($model, 'agr_acc_id')->dropDownList($account) ?>

    <?= $form->field($model, 'agr_comp_id')->dropDownList($company) ?>

    <?= $form->field($model, 'agr_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agr_sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agr_tax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agr_data')->fileInput() ?>

    <?= $form->field($model, 'agr_data_sign')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
