<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $entity array */

$this->title = Yii::t('app', 'Create Person');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['view', 'id' => array_keys($entity)[0]]];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-person-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_person', [
        'model' => $model,
        'entity' => $entity,
    ]) ?>

</div>
