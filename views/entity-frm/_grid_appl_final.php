<?php

use app\models\SvcDocType;
use app\models\TrainingType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */

?>
<div class="appl-final-index-grid" id="forEntityFrmFinalController">

<?php
echo '<h3>'.Yii::t('app', 'Appl Final').'</h3>';
?>
    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control']) ?>
    </p>
<?php
Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete')]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-appl-final', 'id' => $model->applf_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            'applf_id',
            'applf_name_last',
            'applf_name_first',
            'applf_name_middle',
            'applf_name_full',
            [
                'attribute' => 'applfSvdt',
                'label' => Yii::t('app', 'Applf Svdt ID'),
                'value' =>
                    function ($data) {
                        return $data->applfSvdt->svdt_name;
                    },
                'filter' => ArrayHelper::map(SvcDocType::find()->asArray()->all(), 'svdt_id','svdt_name'),
            ],
            [
                'attribute' => 'applf_trt_id',
                'label' => Yii::t('app', 'Appls Trt ID'),
                'value' =>
                    function ($data) {
                        return $data->applfTrt->trt_name;
                    },
                'filter' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
            ],
            [
                'attribute' => 'applfTrp',
                'value' =>
                    function ($data) {
                        return $data->applfTrp->trp_name;
                    },
                'label' => Yii::t('app', 'Appls Trp ID'),
//                'filter' => $trp,
            ],


            'applf_trp_hour',
            'applf_cmd_date',
            'applf_end_date',
            'applf_number',

            [
                'attribute' => 'applf_file_name',
                'label' => Yii::t('app', 'Applf File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file_name, yii\helpers\Url::toRoute(['appl-final/download', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applf_file0_name',
                'label' => Yii::t('app', 'Applf File0 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file0_name, yii\helpers\Url::toRoute(['appl-final/download0', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applf_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applf_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
