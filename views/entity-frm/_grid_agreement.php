<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */
/* @var $addrbook array */
/* @var $comp array */
/* @var $account array */
/* @var $agreement_status array */


echo Html::a(Yii::t('app', 'Create Agreement'), ['create-agreement', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']);

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-agreement', 'ent_id' => $entity->ent_id, 'id' => $model->agr_id]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-agreement', 'id' => $model->agr_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            [
                'attribute' => 'agr_id',
                'options' => ['width' => '100'],
            ],
            'agr_number',
            'agr_date',
            [
                'attribute' => 'agr_ast_id',
                'label' => Yii::t('app', 'Agr Ast ID'),
                'value' => function ($data) {
                    return $data->agrAst->ast_name;
                },
                'filter' => $agreement_status,
            ],
            /*
            [
                'attribute' => 'agr_ab_id',
                'label' => Yii::t('app', 'Agr Ab ID'),
                'value' => function ($data) {
                    return $data->agrAb->entity->entEntt->entt_name_short . ' ' . $data->agrAb->ab_name;
                },
                'filter' => $addrbook,
            ],
            */
            [
                'attribute' => 'agr_comp_id',
                'label' => Yii::t('app', 'Agr Comp ID'),
                'value' => function ($data) {
                    return $data->agrComp->compEnt->entEntt->entt_name_short . ' ' . $data->agrComp->compEnt->ent_name_short;
                },
                'filter' => $comp,
            ],
            /*
                        [
                            'attribute' => 'agr_acc_id',
                            'label' => Yii::t('app', 'Agr Acc ID'),
                            'value' => function ($data) { return $data->agrAcc->acc_number; },
                            'filter' => $account,
                        ],
            */

            'agr_sum',
            'agr_tax',
            [
                'attribute' => 'agr_comment',
                'value' => function ($data) {
                    return $data->agr_comment;
                },
//                'filter' => $comp,
            ],
            [
                'attribute' => 'agr_comp_id',
                'label' => Yii::t('app', 'Agr Comp ID'),
                'value' => function ($data) {
                    return $data->agrComp->compEnt->entEntt->entt_name_short . ' ' . $data->agrComp->compEnt->ent_name_short;
                },
                'filter' => $comp,
            ],

            /*
                        [
                            'attribute' => 'agr_annex',
                            'label' => Yii::t('app', 'Agr Annex'),
                            'format' => 'raw',
                            'value' =>  function ($data) use ($entity) {
                                            $str = "<ul>";
                                            foreach ($data->agreementAnnexes as $key => $value) {
                                                $str .= "<li>"
                                                    ." ".
                                                    Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                                        yii\helpers\Url::to(['update-agreement-annex', 'ent_id' => $entity->ent_id, 'id' => $value->agra_id]), ['title' => Yii::t('yii', 'Update')]
                                                    )
                                                    ." ".
                                                    Html::a($value->agra_fdata, yii\helpers\Url::to(['download-agreement-annex', 'id' => $value->agra_id]))
                                                    ."<br>".
                                                    Html::a($value->agra_fdata_sign, yii\helpers\Url::to(['download-agreement-annex-sign', 'id' => $value->agra_id]));
                                                $str .= "</li>";
                                            }
                                            $str .= "</ul>";
                                            return $str;
                                        },
                        ],
                        [
                            'attribute' => 'agr_act',
                            'label' => Yii::t('app', 'Agr Act'),
                            'format' => 'raw',
                            'value' =>  function ($data) use ($entity) {
                                            $str = "<ul>";
                                            foreach ($data->agreementActs as $key => $value) {
                                                $str .= "<li>" .
                                                    Yii::t('app', 'Agreement Act')
                                                    ." ".
                                                    Html::a($value->act_number, yii\helpers\Url::to(['update-agreement-act', 'ent_id' => $entity->ent_id, 'id' => $value->act_id]))
                                                    ."<br>".
                                                    Html::a($value->act_fdata, yii\helpers\Url::to(['download-agreement-act', 'id' => $value->act_id]))
                                                    ."<br>".
                                                    Html::a($value->act_fdata_sign, yii\helpers\Url::to(['download-agreement-act-sign', 'id' => $value->act_id]));
                                                $str .= "</li>";
                                            }
                                            $str .= "</ul>";
                                            return $str;
                                        },
                        ],
            */
            [
                'attribute' => 'agr_fdata',
                'label' => Yii::t('app', 'Agr Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->agr_fdata, yii\helpers\Url::toRoute(['download-agreement', 'id' => $data->agr_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'agr_fdata_sign',
                'label' => Yii::t('app', 'Agr Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->agr_fdata_sign, yii\helpers\Url::toRoute(['download-agreement-sign', 'id' => $data->agr_id]), ['target' => '_blank']);
                },
            ],


            [
                'attribute' => 'agr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();
