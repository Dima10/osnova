<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entity-frm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ent_id') ?>

    <?= $form->field($model, 'ent_entt_id') ?>

    <?= $form->field($model, 'ent_name') ?>

    <?= $form->field($model, 'ent_name_short') ?>

    <?= $form->field($model, 'ent_orgn') ?>

    <?php // echo $form->field($model, 'ent_inn') ?>

    <?php // echo $form->field($model, 'ent_kpp') ?>

    <?php // echo $form->field($model, 'ent_okpo') ?>

    <?php // echo $form->field($model, 'ent_create_user') ?>

    <?php // echo $form->field($model, 'ent_create_time') ?>

    <?php // echo $form->field($model, 'ent_create_ip') ?>

    <?php // echo $form->field($model, 'ent_update_user') ?>

    <?php // echo $form->field($model, 'ent_update_time') ?>

    <?php // echo $form->field($model, 'ent_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
