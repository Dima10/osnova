<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */
/* @var $company array */
/* @var $entity array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Acc'),
]) . $model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-agreement-acc-update" id="forUpdateAgreementAccController">

    <h1><?= Html::encode($this->title) ?></h1>
    <input id="accIdValue" class="hidden" value="<?php echo $model->aga_id; ?>" />
    <?= $this->render('_form_agreement_acc', [
        'model' => $model,
        'entity' => $entity,
        'company' => $company,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
