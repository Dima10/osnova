<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentRetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="entity-frm-payment-ret-index-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if (isset($model->financeBook)) {
                return ['class' => 'info'];
            } else {
                return ['class' => 'danger'];
            }
        },

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {pay}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),
                            ]);
                        },
                    'pay' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-euro"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'FinanceBook Pay'),
                                ]);
                        },

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-payment-ret', 'id' => $model->ptr_id]);
                            return $url;
                        }
                        if ($action === 'pay') {
                            $url = yii\helpers\Url::to(['pay-payment-ret', 'id' => $model->ptr_id]);
                            return $url;
                        }
                    }

            ],

            'ptr_id',
            [
                'attribute' => 'ptrAb.ab_name',
                'label' => Yii::t('app', 'Ptr Ab ID'),
            ],
            [
                'attribute' => 'ptrAcc.acc_number',
                'label' => Yii::t('app', 'Ptr Acc ID'),
            ],
            [
                'attribute' => 'ptrFbs.fbs_name',
                'label' => Yii::t('app', 'Ptr Fbs ID'),
            ],
            [
                'attribute' => 'ptrPt.pt_name',
                'label' => Yii::t('app', 'Ptr Pt ID'),
            ],
            [
                'attribute' => 'ptrSvc.svc_name',
                'label' => Yii::t('app', 'Ptr Svc ID'),
            ],

            [
                'attribute' => 'ptr_svc_sum',
                'label' => Yii::t('app', 'Ptr Svc Sum'),
            ],
            [
                'attribute' => 'ptr_svc_tax',
                'label' => Yii::t('app', 'Ptr Svc Tax'),
            ],

            [
                'attribute' => 'ptr_date_pay',
                'label' => Yii::t('app', 'Ptr Date Pay'),
            ],

            [
                'attribute' => 'ptr_create_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_create_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_user',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_time',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],
            [
                'attribute' => 'ptr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 170,
            ],

        ],
    ]);

?>

</div>
