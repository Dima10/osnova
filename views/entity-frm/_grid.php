<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entityType array */
/* @var $entityClass array */
/* @var $agent array */

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        /*
        'pager'        => [
            'class' => 'app\widgets\DropdownPager',
        ],
        */
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
            'maxButtonCount' => 1000,

        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'ent_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'ent_entt_id',
                'label' => Yii::t('app', 'Ent Entt ID'),
                'value' => function ($data) {
                    return $data->entEntt->entt_name_short;
                },
                'filter' => $entityType,
            ],
            [
                'attribute' => 'entc_id',
                'label' => Yii::t('app', 'Entc Name'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = '<ul>';
                        foreach ($data->entityClasses as $key => $obj) {
                            $s .= '<li>';
                            $s .= $obj->entc_name;
                            $s .= '</li>';
                        }
                        return $s;
                    },
                'filter' => $entityClass,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            'ent_name',
            'ent_name_short',

            'ent_orgn',
            'ent_inn',
            'ent_kpp',
            [
                'attribute' => 'ent_okpo',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_comment',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_agent_id',
                'label' => Yii::t('app', 'Ent Agent ID'),
                'value' => function ($data) {
                    return $data->entAgent->ab_name ?? '';
                },
                'filter' => $agent,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'ent_create_user',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_create_time',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_update_user',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_update_time',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'ent_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();
