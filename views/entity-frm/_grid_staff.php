<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity app\models\Entity */
/* @var $person array */

$js = <<<JS
function gridClick(id) {

}

JS;
$this->registerJs($js, yii\web\View::POS_END);

?>
    <p>
        <?= Html::a(Yii::t('app', 'Create Staff'), ['create-staff', 'ent_id' => $entity->ent_id], ['class' => 'btn btn-success']) ?>
    </p>

<?php
Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['id' => $model->stf_id, 'onclick' => 'gridClick(this.id);'];
        },
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
                'template' => '{update}',
                'buttons' => [
                    'update' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update'),
                            ]);
                        }

                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) use ($entity) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update-staff', 'ent_id' => $entity->ent_id, 'id' => $model->stf_id]);
                            return $url;
                        }
                        return '#';
                    }
            ],

            [
                'attribute' => 'stf_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'stf_active',
            ],

            [
                'attribute' => 'stf_ent_id',
                'label' => Yii::t('app', 'Stf Ent ID'),
                'value' => function ($data) {
                    return $data->stfEnt->ent_name;
                },
                'filter' => $entity,
                'visible' => false,
            ],
            [
                'attribute' => 'stf_prs_id',
                'label' => Yii::t('app', 'Stf Prs ID'),
                'value' => function ($data) {
                    return $data->stfPrs->prs_full_name;
                },
                'filter' => $person,
            ],

            'stf_position',


            [
                'attribute' => 'stf_id',
                'label' => Yii::t('app', 'Contacts'),
                'format' => 'raw',
                'value' =>
                    function ($data) {
                        $s = '';
                        foreach ($data->contacts as $key => $value) {
                            switch ($value->conCont->cont_type) {
                                case 0:
                                    $s1 = $value->conCont->cont_name . ' ' . $value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 1:
                                    $s1 = $value->conCont->cont_name . ' ' . Html::mailto($value->con_text);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 2:
                                    $s1 = Html::a($value->conCont->cont_name, 'http://' . str_replace('http://', '', $value->con_text), ['target' => '_blank']);
                                    $s .= Html::tag('div', $s1);
                                    break;
                                case 3:
                                    $s1 = Html::a($value->conCont->cont_name, 'skype:' . $value->con_text . '?call');
                                    $s .= Html::tag('div', $s1);
                                    break;
                                default:
                                    $s1 = $value->conCont->cont_name . ' ' . $value->con_text;
                                    $s .= Html::tag('div', $s1);
                                    break;
                            }

                        }
                        return $s;

                    },
            ],


            [
                'attribute' => 'stf_create_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'stf_create_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'stf_create_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'stf_update_user',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'stf_update_time',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],
            [
                'attribute' => 'stf_update_ip',
                'visible' => Yii::$app->user->identity->level >= 80,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}
Pjax::end();

