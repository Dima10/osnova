<?php

use app\models\Svc;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAct */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $company array */
/* @var $account array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */
/* @var $dataProvider yii\data\ActiveDataProvider */

$annex_a = [];
?>

<div class="entity-frm-agreement-act-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],
            'id' => 'agreementActForm'
            ]);
    ?>

    <?= $form->field($model, 'act_number')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($model, 'act_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])
    ?>

    <?= $form->field($model, 'act_ast_id')->dropDownList($agreement_status) ?>

    <?= $form->field($model, 'act_ab_id')->dropDownList($entity, ['ReadOnly' => true]); ?>

    <?= $form->field($model, 'act_comp_id')->dropDownList($company, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_agr_id')->dropDownList($agreement, ['ReadOnly' => true]) ?>

    <?php //= $form->field($model, 'act_agra_id')->dropDownList($agreement_annex, ['ReadOnly' => true]) ?>

    <?php //= $form->field($model, 'act_acc_id')->dropDownList($account, ['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'act_sum')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_tax')->textInput(['ReadOnly' => true]) ?>

    <?= $form->field($model, 'act_data')->fileInput() ?>

    <?= $form->field($model, 'act_data_sign')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary updateAct']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <h3 id="newFileLink"></h3>
    <?= $this->render('_grid_agreement_act_a', [
        'dataProvider' => $dataProvider,
        'model' => $model,
    ]) ?>

<?php if($model->act_ast_id == 1){ ?>
    <button type="button" id="showHideServiceId" class="btn btn-success" style="margin-bottom: 20px">Добавить услугу</button>
<?php } ?>
    <div id="serviceForm" class="hidden">
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
            <div class="col-sm-6"><?= Html::dropDownList('annex', '', $annex_a, ['id' => 'annex_a', 'class'=> 'form-control']) ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Price'), 'price'); ?></div>
            <div class="col-sm-6"><?= Html::textInput('price', '', ['id' => 'price', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Acta Qty'), 'qty'); ?></div>
            <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control']); ?></div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6"></div>
            <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add Svc'), '#', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
        </div>
    </div>
    <br />
</div>

<?php

$script = '
$(function() {
    
    $("#agreementact-act_agr_id").change(function() {
        var agr_id = this.value;
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-act-a']).'",
                    {
                      agr_id : agr_id,
                    },
                    function (data) {
                        $("#annex_a").html(data);
                    }
        );
                
                
        $("#annex_a").change(function() {
            var ana_id = this.value;
            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-act-a-price']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#price").val(data);
                    }
            );

            $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-act-a-qty']).'",
                    {
                      ana_id : ana_id,
                    },
                    function (data) {
                        $("#qty").val(data);
                    }
            );
        });
    });
});
';
$this->registerJs($script, yii\web\View::POS_END);
