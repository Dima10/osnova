<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $account array */
/* @var $company array */
/* @var $pattern array */
/* @var $staff array */
/* @var $address array */

?>

<div class="entity-frm-agreement-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement', 'ent_id' => array_keys($entity)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelA) ? $form->errorSummary($modelA) : '';

    ?>

    <?php //  $form->field($model, 'number')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Agr Number'))  ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])->label(Yii::t('app', 'Agr Date'))
    ?>

    <?=  $form->field($model, 'sum')->textInput()->label(Yii::t('app', 'Agr Sum'))  ?>

    <?=  $form->field($model, 'tax')->textInput()->label(Yii::t('app', 'Agr Tax'))  ?>

    <?= $form->field($model, 'ent_id')->dropDownList($entity, ['ReadOnly' => true])->label(Yii::t('app', 'Entity')) ?>

    <?= $form->field($model, 'add_id')->dropDownList($address)->label(Yii::t('app', 'Address')) ?>

    <?= $form->field($model, 'comp_id')->dropDownList($company)->label(Yii::t('app', 'Company')) ?>

    <?= $form->field($model, 'acc_id')->dropDownList($account)->label(Yii::t('app', 'Account')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>

    <?= $form->field($model, 'prs_id')->dropDownList($staff)->label(Yii::t('app', 'Staff Signed')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
