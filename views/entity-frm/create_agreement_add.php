<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelAdd app\models\AgreementAdd */
/* @var $entity array */
/* @var $agreement array */
/* @var $pattern array */
/* @var $address array */
/* @var $account array */
/* @var $staff array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Create Agreement Add');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-frm-agreement-add-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_add_dyn', [
        'model' => $model,
        'modelAdd' => $modelAdd,
        'entity' => $entity,
        'agreement' => $agreement,
        'pattern' => $pattern,
        'address' => $address,
        'account' => $account,
        'staff' => $staff,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
