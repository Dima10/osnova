<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agreement */
/* @var $entity array */
/* @var $agreement array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Add'),
]) . $model->agadd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Frm'), 'url' => ['site/frm']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = ['label' => array_keys($entity)[0], 'url' => ['view', 'id' => array_keys($entity)[0]]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entity-frm-agreement-add-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_agreement_add', [
        'model' => $model,
        'entity' => $entity,
        'agreement' => $agreement,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
