<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $modelA app\models\AgreementAnnex */
/* @var $form yii\widgets\ActiveForm */
/* @var $entity array */
/* @var $agreement array */
/* @var $pattern array */
/* @var $svc array */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $account array */
/* @var $address array */
/* @var $staff array */

?>

<div class="entity-frm-agreement-annex-form">



<?php
        $form = ActiveForm::begin([
                'action' => ['create-agreement-annex', 'ent_id' => array_keys($entity)[0]],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo isset($modelA) ? $form->errorSummary($modelA) : '';

    ?>

    <?=  $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ])->label(Yii::t('app', 'Agra Date'))
    ?>

    <?=  $form->field($model, 'sum')->textInput(['id' => 'sum', 'ReadOnly' => true])->label(Yii::t('app', 'Agra Sum'))  ?>

    <?=  $form->field($model, 'tax')->textInput(['id' => 'tax', 'ReadOnly' => true])->label(Yii::t('app', 'Agra Tax'))  ?>

    <?= $form->field($model, 'ent_id')->dropDownList($entity, ['ReadOnly' => true])->label(Yii::t('app', 'Entity')) ?>

    <?= $form->field($model, 'agr_id')->dropDownList($agreement)->label(Yii::t('app', 'Agreement')) ?>

    <?= $form->field($model, 'acc_id')->dropDownList($account)->label(Yii::t('app', 'Account')) ?>

    <?= $form->field($model, 'add_id')->dropDownList($address)->label(Yii::t('app', 'Address')) ?>

    <?= $form->field($model, 'prs_id')->dropDownList($staff)->label(Yii::t('app', 'Staff Signed')) ?>

    <?= $form->field($model, 'pat_id')->dropDownList($pattern)->label(Yii::t('app', 'Pattern')) ?>

    <hr>

    <?= $this->render('_ajax_agreement_a_grid', [
        'dataProvider' => $dataProvider,
    ]) ?>



    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
            <?= Html::textInput('svcText', '', ['id' => 'svcText', 'class'=> 'form-control', 'maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'svcSearch']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svcs'), 'svc'); ?></div>
        <div class="col-sm-6"><?= Html::dropDownList('svc', [], $svc, ['id' => 'svc', 'class'=> 'form-control', ]); ?></div>
        <div class="col-sm-4"></div>
    </div>

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6">
            <?= Html::textInput('progText', '', ['id' => 'progText', 'class'=> 'form-control', 'maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'progSearch', ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Training Prog'), 'prog'); ?></div>
        <div class="col-sm-6"><?= Html::dropDownList('prog', [], [], ['id' => 'prog', 'class'=> 'form-control']); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svc Cost'), 'cost'); ?></div>
        <div class="col-sm-6"><?= Html::textInput('cost', '', ['id' => 'cost', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Svc Price'), 'price'); ?></div>
        <div class="col-sm-6"><?= Html::textInput('price_a', '', ['id' => 'price_a', 'class'=> 'form-control', 'ReadOnly' => true]); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Ana Price'), 'price'); ?></div>
        <div class="col-sm-6"><?= Html::input('number', 'price', '', ['id' => 'price', 'class'=> 'form-control', 'step' => 0.01]); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"><?= Html::label(Yii::t('app', 'Ana Qty'), 'qty'); ?></div>
        <div class="col-sm-6"><?= Html::input('number', 'qty', '', ['id' => 'qty', 'class'=> 'form-control', ]); ?></div>
        <div class="col-sm-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-6"></div>
        <div class="col-sm-4"><?= Html::a(Yii::t('app', 'Add Svc'), '#', ['id' => 'add_svc', 'class' => 'btn btn-success']); ?></div>
    </div>
    <hr>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn btn-primary', 'id' => 'btnCreate']) ?>
    </div>

    <?php ActiveForm::end(); ?>



</div>



<?php

$script = '
 $(function() {
    $("#add_svc").click(function(e) {
        var _svc = $("#svc").val();
        var _prog = $("#prog").val();
        var _price = $("#price").val();
        var _qty = $("#qty").val();
        var _date = $("#dynamicmodel-date").val();

        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-create']).'",
                    {
                      svc_id : _svc,
                      trp_id : _prog,
                      price : _price,
                      qty : _qty,
                      date : _date
                    },
                    function (data) {
                        $("#grid_svc").html(data);
                        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-sum']).'",
                            {
                            },
                            function (data) {
                                $("#sum").val(data);
                            }
                        );
                        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-tax']).'",
                            {
                            },
                            function (data) {
                                $("#tax").val(data);
                            }
                        );
                        
                    }
                );

    });

    $("#svc").change(function() {
        var svc = this.value;
        $("#qty").val(1);
        $("#price").val("");
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-cost']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#cost").val(data);
                    }
                );
        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-price']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#price_a").val(data);
                    }
                );

        $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-trp']).'",
                    {
                      svc_id : svc,
                    },
                    function (data) {
                        $("#prog").html(data);
                    }
                );

    });

    $("#svcSearch").click(
            function() {
                var svc = $("#svcText").val();
                $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-svc']).'",
                    {
                      svc_name : svc
                    },
                    function (data) {
                        $("#svc").html(data);
                    }
                );
            }
    );

    $("#progSearch").click(
            function() {
                var trp = $("#progText").val();
                var svc = $("#svc").val();
                $.get("'.\yii\helpers\Url::to(['/entity-frm/ajax-agreement-a-trp']).'",
                    {
                      svc_id : svc,
                      trp_name : trp
                    },
                    function (data) {
                        $("#prog").html(data);
                    }
                );
            }
    );
    
    $("#btnCreate").click(function(e) {
        setTimeout(function() {
            $(e.currentTarget).prop("disabled", true);
        }, 10);
    });
    
});
';
$this->registerJs($script, yii\web\View::POS_END);


?>