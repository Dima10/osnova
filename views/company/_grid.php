<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */

Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'comp_id',
                'label' => Yii::t('app', 'Comp ID'),
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'entt_name_short',
                'label' => Yii::t('app', 'Entity Type'),
                'value' => function ($data) { return $data->compEnt->entEntt->entt_name_short; },
            ],

            [
                'attribute' => 'comp_ent_id',
                'label' => Yii::t('app', 'Comp Ent ID'),
                'value' => function ($data) { return $data->compEnt->ent_name; },
                'filter' => $entity,
            ],
            'comp_note:ntext',

            [
                'attribute' => 'comp_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'comp_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'comp_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'comp_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'comp_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'comp_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
Pjax::end();
