<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplSh */
/* @var $searchModel app\models\ApplShContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Result {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sh'),
]);// . $model->appls_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Shs'), 'url' => ['person/index']];
$this->params['breadcrumbs'][] = ['label' => $model->appls_id, 'url' => ['view', 'id' => $model->appls_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sh-update">


    <h1><?= Html::encode($this->title) ?></h1>

    <h2><?= $model->applsPrs->prs_full_name ?></h2>

    <h3>
    Всего правильных ответов: <?= $model->appls_score ?> / <?= $model->appls_score_max ?>
        <br><br>
    Набрано: <?= round(($model->appls_score / ($model->appls_score_max <=0 ? 10000 : $model->appls_score_max) * 100)) ?>%
        <br><br>
    <?= $model->appls_passed == 1 ? Yii::t('app', 'Test Is Correct') : Yii::t('app', 'Test Is Incorrect') ?>
    </h3>

    <?= Html::a(Yii::t('app', 'Go to home'), ['person/training-program'], ['class' => 'btn btn-success']) ?>

    <!--
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'appls_id',
                'appls_number',
                'appls_date',
                'applsPrs.prs_full_name',
                'applsTrp.trp_name',
                'appls_score_max',
                'appls_score',
                [
                    'label' => Yii::t('app', 'Appls Passed'),
                    'value' => function () use ($model) {
                        return $model->appls_passed == 1 ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                    },
                ],
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>
    -->

    <?= $this->render('_grid_detail_r', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>


</div>
