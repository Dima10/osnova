<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $sheet app\models\ApplSh */
/* @var $model yii\base\DynamicModel */
/* @var $question app\models\TrainingQuestion */
/* @var $answer array */
/* @var $id integer */

$this->title = Yii::t('app', 'Appl Sh Test');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-test">

    <h1><?= Html::encode($this->title) ?></h1>

    <h4>
        Вопрос <?= $id+1 ?> из <?= $sheet->appls_score_max ?>
    </h4>

    <?= $this->render('_test', [
        'model' => $model,
        'sheet' => $sheet,
        'question' => $question,
        'answer' => $answer,
        'id' => $id,
    ]) ?>

</div>

<p id="timer" style="text-align: center; font-size: 60px;"></p>

<noscript>
    У Вас отключен JavaScript.
    Отображение оставшегося времени недоступно.
</noscript>

<?php
$link = yii::$app->urlManager->createUrl(['appl-sh/timer']);
$js = <<<EOL
function onTimer() {
}

var x = setInterval(function()
{
        $.get(
            '$link',
            {
                id: {$sheet->appls_id}
            },
            function (data) {
                $('#timer').html(data); 
            }
        );
}

, 60000);

EOL;

$this->registerJs($js, \yii\web\View::POS_READY);

?>

<!--
<script>

        // Set the date we're counting down to
        var countDownDate = new Date("<?= $sheet->appls_end ?>").getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();
            // Find the distance between now an the count down date
            var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
            document.getElementById("timer").innerHTML = "Осталось: " +hours + " ч "
                + minutes + " м " + seconds + " с ";

    // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("timer").innerHTML = "Время закончилось";
            }
    }, 1000);
</script>
-->
