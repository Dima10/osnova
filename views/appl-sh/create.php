<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplSh */
/* @var $person array */
/* @var $program array */

$this->title = Yii::t('app', 'Create Appl Sh');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Shs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person' => $person,
        'program' => $program,
    ]) ?>

</div>
