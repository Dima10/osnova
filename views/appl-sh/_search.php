<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sh-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'appls_id') ?>

    <?= $form->field($model, 'appls_number') ?>

    <?= $form->field($model, 'appls_date') ?>

    <?= $form->field($model, 'appls_prs_id') ?>

    <?= $form->field($model, 'appls_trp_id') ?>

    <?php // echo $form->field($model, 'appls_score_max') ?>

    <?php // echo $form->field($model, 'appls_score') ?>

    <?php // echo $form->field($model, 'appls_passed') ?>

    <?php // echo $form->field($model, 'appls_create_user') ?>

    <?php // echo $form->field($model, 'appls_create_time') ?>

    <?php // echo $form->field($model, 'appls_create_ip') ?>

    <?php // echo $form->field($model, 'appls_update_user') ?>

    <?php // echo $form->field($model, 'appls_update_time') ?>

    <?php // echo $form->field($model, 'appls_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
