<?php
/**
 * @author Ruslan Bondarenko r.i.bondarenko@gmail.com (Dnipro)
 * @copyright Copyright (c) 2018 Ruslan Bondarenko (Dnipro)
 * @license http://www.yiiframework.com/license/
 *
 */

/**
 * Created by PhpStorm.
 * User: rb
 * Date: 11.12.2018
 * Time: 14:49
 */

/* @var $this \yii\web\View */
/* @var $hour int */
/* @var $min int */
/* @var $sec int */

if ($hour == 0 && $min == 0) {
    echo "Время закончилось";
} else {
    echo "Осталось: $hour ч $min м";
}
