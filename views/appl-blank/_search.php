<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplBlankSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-blank-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applblk_id') ?>

    <?= $form->field($model, 'applblk_number') ?>

    <?= $form->field($model, 'applblk_date') ?>

    <?= $form->field($model, 'applblk_qty_1') ?>

    <?= $form->field($model, 'applblk_qty_2') ?>

    <?php // echo $form->field($model, 'applblk_qty_3') ?>

    <?php // echo $form->field($model, 'applblk_qty_4') ?>

    <?php // echo $form->field($model, 'applblk_qty_5') ?>

    <?php // echo $form->field($model, 'applout_file_name') ?>

    <?php // echo $form->field($model, 'applblk_file_data') ?>

    <?php // echo $form->field($model, 'applblk_create_user') ?>

    <?php // echo $form->field($model, 'applblk_create_time') ?>

    <?php // echo $form->field($model, 'applblk_create_ip') ?>

    <?php // echo $form->field($model, 'applblk_update_user') ?>

    <?php // echo $form->field($model, 'applblk_update_time') ?>

    <?php // echo $form->field($model, 'applblk_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
