<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplBlank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-blank-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applblk_number')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'applblk_date')->textInput(); ?>

    <?php echo $form->field($model, 'applblk_qty_1')->textInput(); ?>

    <?php
        if (!\app\models\User::isSpecAdmin()) {
            echo $form->field($model, 'applblk_qty_2')->textInput();
        }
    ?>

    <?php echo $form->field($model, 'applblk_qty_3')->textInput(); ?>

    <?php echo $form->field($model, 'applblk_qty_4')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'applblk_qty_5')->textInput(); ?>

    <?php echo $form->field($model, 'applblk_file_data')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
