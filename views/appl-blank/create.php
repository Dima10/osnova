<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplBlank */

$this->title = Yii::t('app', 'Create Appl Blank');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Blanks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-blank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
