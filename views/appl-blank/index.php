<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplBlankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Appl Blanks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-blank-index" id="forApplBlankController">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Blank'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control']) ?>
    </p>
    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
