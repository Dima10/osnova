<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplBlankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-blank-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applblk_id',
            'applblk_number',
            'applblk_date',
            'applblk_qty_1',
            [
                'attribute' => 'applblk_qty_2',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            'applblk_qty_3',
            'applblk_qty_4',
            'applblk_qty_5',
            [
                'attribute' => 'applblk_file_name',
                'label' => Yii::t('app', 'Applout File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applblk_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applblk_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'applblk_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
