<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplBlank */

$this->title = $model->applblk_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Blanks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-blank-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applblk_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applblk_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applblk_id',
            'applblk_number',
            'applblk_date',
            'applblk_qty_1',
            [
                'attribute' => 'applblk_qty_2',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applblk_qty_3',
            'applblk_qty_4',
            'applblk_qty_5',
//            'applout_file_name',
//            'applblk_file_data',
            [
                'attribute' => 'applblk_create_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_create_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_create_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applblk_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]) ?>

</div>
