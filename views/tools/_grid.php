<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

?>
<div class="tools-index-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'action',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data['name'], $data['action']);
                },
            ],

        ],
    ]);


?>

</div>
