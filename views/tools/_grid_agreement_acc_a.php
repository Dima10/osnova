<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="tools-agreement-acc-a-grid">

<?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'aca_id',
            'aca_aga_id',
            'aca_ana_id',

            'aca_qty',
            'aca_price',
            'aca_tax',

        ],
    ]);



?>

</div>
