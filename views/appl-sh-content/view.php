<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShContent */

$this->title = $model->applsc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applsc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applsc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applsc_id',
            'applsc_appls_id',
            'applsc_trq_id',
            'applsc_trq_question:ntext',
            'applsc_tra_id',
            'applsc_tra_answer:ntext',
            'applsc_tra_variant',
            'applsc_create_user',
            'applsc_create_time',
            'applsc_create_ip',
            'applsc_update_user',
            'applsc_update_time',
            'applsc_update_ip',
        ],
    ]) ?>

</div>
