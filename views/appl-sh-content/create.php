<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplShContent */

$this->title = Yii::t('app', 'Create Appl Sh Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
