<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShContent */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sh Content'),
]) . $model->applsc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applsc_id, 'url' => ['view', 'id' => $model->applsc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sh-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
