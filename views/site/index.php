<?php

/* @var $this yii\web\View */

use yii\bootstrap\Collapse;
use yii\helpers\Html;
use \yii\helpers\Url;

$this->title = Yii::t('app', 'Application');

?>

<div class="site-index">
    <div class="body-content">
        <?php if (Yii::$app->user->isGuest) { ?>
            <div class="site-index-header site-index-margin">
                <i>
                    Автономная некоммерческая организация  дополнительного профессионального образования
                    <br>
                    Строительный учебный центр  «Основа»
                </i>
            </div>

            <div class="site-index-address site-index-margin">
                <i>
                    Юр. /факт. адрес: 141401 г. Химки, ул. Академика Грушина, дом 8, помещение 1;
                    <br />
                    ИНН 5047998640, ОГРН 1135000003896, КПП 504701001, ОКПО 18084061
                </i>
            </div>

            <div class="site-index-contact-list site-index-margin">
                <i>
                    тел: 8 (499) 372-09-62
                    <br />
                    E-mail: info@pdo-osnova.ru
                    <br />
                Сайт: pdo-osnova.ru
                </i>
            </div>

            <div class="site-index-greetings">
                Мы рады Вас приветствовать на главной странице образовательного портала.
            </div>

            <div class="site-index-enter-string">
                Для начала обучения Вам необходимо войти в <a href="/index.php?r=site/login">личный кабинет</a>, ввести логин и пароль.
            </div>

            <div class="site-index-tech">
                По всем техническим вопросам, возникающим в процессе обучения, звонить по телефону: 8(499)372-09-62
            </div>
            <div>
                <img class="site-index-img" src="/img/startPageLogo.png" />
            </div>
        <?php } ?>
    </div>
</div>

<style>
    .site-index {
        text-align: center;
    }

    .site-index-header {
        font-weight: bolder;
        font-size: 22px;
    }

    .site-index-contact-list {
        font-size: 16px;
        font-weight: bolder;
    }

    .site-index-address {
        font-size: 16px;
    }

    .site-index-greetings,
    .site-index-enter-string,
    .site-index-tech {
        font-size: 25px;
        font-weight: bold;
    }

    .site-index-margin {
        margin-bottom: 30px;
    }

    .site-index-img {
        height: 550px
    }
</style>