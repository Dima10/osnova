<?php


if (isset(Yii::$app->user->identity) ? Yii::$app->user->identity->level >= 10 : false) {
    echo yii\widgets\Menu::widget([
            'items'=> [
                ['label' => Yii::t('app', 'Entity Frm'), 'url' => ['/entity-frm']],
                ['label' => Yii::t('app', 'Person Frm'), 'url' => ['/person-frm']],
            ],

    ]);
}


