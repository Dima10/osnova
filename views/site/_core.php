<?php


if (isset(Yii::$app->user->identity) ? Yii::$app->user->identity->level >= 70 : false) {
    try {
        echo yii\widgets\Menu::widget([
            'items' => [
                ['label' => Yii::t('app', 'Countries'), 'url' => ['/country']],
                ['label' => Yii::t('app', 'Regions'), 'url' => ['/region']],
                ['label' => Yii::t('app', 'Cities'), 'url' => ['/city']],
                ['label' => Yii::t('app', 'Address Types'), 'url' => ['/address-type']],
                ['label' => Yii::t('app', 'Addresses'), 'url' => ['/address']],
                ['label' => Yii::t('app', 'Contact Types'), 'url' => ['/contact-type']],
                ['label' => Yii::t('app', 'Contacts'), 'url' => ['/contact']],
                ['label' => Yii::t('app', 'Entity Types'), 'url' => ['/entity-type']],
                ['label' => Yii::t('app', 'Entity Classes'), 'url' => ['/entity-class']],
                ['label' => Yii::t('app', 'Entities'), 'url' => ['/entity']],
                ['label' => Yii::t('app', 'Companies'), 'url' => ['/company']],
                ['label' => Yii::t('app', 'Persons'), 'url' => ['/person']],
                ['label' => Yii::t('app', 'Staff'), 'url' => ['/staff']],
            ],

        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}


