<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingQuestion */
/* @var $modelAnswer app\models\TrainingAnswer */
/* @var $module array */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $question array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Training Question'),
]) . $model->trq_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->trq_id, 'url' => ['view', 'id' => $model->trq_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-question-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'module' => $module,
    ]) ?>

    <hr>
    <h3><?=Yii::t('app', 'Training Answers') ?></h3>

    <div class="training-question-training-answer-index-grid">
    <?= $this->render('_grid_tra', [
        'dataProvider' => $dataProvider,
    ]) ?>
    </div>

    <?= $this->render('_form_tra', [
        'model' => $modelAnswer,
        'modelQuestion' => $model,
        'question' => $question,
    ]) ?>


</div>
