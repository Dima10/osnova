<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingAnswer */
/* @var $modelQuestion app\models\TrainingQuestion */
/* @var $form yii\widgets\ActiveForm */
/* @var $question array */


$js = '

// >>> http://stackoverflow.com/a/28309702/7846910
$(document).on("submit", "#tra_form", function(event)
{
    event.preventDefault();
    var url=$(this).attr("action");
// <<< http://stackoverflow.com/a/28309702/7846910

// http://stackoverflow.com/a/28234468/7846910
    $.ajax({
        url: url, 
        type: "POST",             
        data: new FormData(this),
        contentType: false,       
        cache: false,             
        processData:false, 
        success: function(data) {
            $("#tra_grid").html(data);
            $("#tra_variant").prop("checked", false);
        }
    });    


});
';

$this->registerJs($js, \yii\web\View::POS_READY);


?>

<hr>
<h3>
    <?= Yii::t('app', 'Create Training Answer') ?>
</h3>
<hr>

<div class="training-question-training-answer-form">

    <?php
    $form = ActiveForm::begin(
        [
            'id' => 'tra_form',
            'action' => yii\helpers\Url::to(['/training-question/submit-answer']),
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],
        ]

    );
    ?>

    <?php echo Html::hiddenInput('TrainingAnswer[tra_trq_id]', $modelQuestion->trq_id, []); ?>

    <?php echo $form->field($model, 'tra_answer')->textarea(['id' => 'tra_answer', 'rows' => 6]); ?>

    <?php echo $form->field($model, 'tra_variant')->checkbox(['id' => 'tra_variant'], false)->label(Yii::t('app', 'Tra Variant')); ?>

    <?php echo $form->field($model, 'tra_file_data')->fileInput(['id' => 'file_id']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
