<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-book-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fbs_id') ?>

    <?= $form->field($model, 'fbs_name') ?>

    <?= $form->field($model, 'fbs_create_user') ?>

    <?= $form->field($model, 'fbs_create_ip') ?>

    <?= $form->field($model, 'fbs_create_time') ?>

    <?php // echo $form->field($model, 'fbs_update_user') ?>

    <?php // echo $form->field($model, 'fbs_update_ip') ?>

    <?php // echo $form->field($model, 'fbs_update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
