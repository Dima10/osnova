<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookStatus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Finance Book Status'),
]) . $model->fbs_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Book Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fbs_id, 'url' => ['view', 'id' => $model->fbs_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="finance-book-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
