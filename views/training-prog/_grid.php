<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $reestr array */

?>
<div class="training-prog-index-grid">

<?php Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'trp_id',
            [
                'attribute' => 'trp_reestr',
                'value' => function ($data) use ($reestr) {
                    return $reestr[$data->trp_reestr];
                },
                'label' => Yii::t('app', 'Trp Reestr'),
                'filter' => $reestr,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'trp_name',
            'trp_code',
            'trp_hour',
            'trp_test_question',
            [
                'attribute' => 'trp_comment',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_dataA1_name',
                'label' => Yii::t('app', 'Trp Data A1 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataA1_name, yii\helpers\Url::toRoute(['download-a1', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataA2_name',
                'label' => Yii::t('app', 'Trp Data A2 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataA2_name, yii\helpers\Url::toRoute(['download-a2', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataB1_name',
                'label' => Yii::t('app', 'Trp Data B1 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataB1_name, yii\helpers\Url::toRoute(['download-b1', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trp_dataB2_name',
                'label' => Yii::t('app', 'Trp Data B2 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trp_dataB2_name, yii\helpers\Url::toRoute(['download-b2', 'id' => $data->trp_id]), ['target' => '_blank']);
                },
            ],


            [
                'attribute' => 'trp_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trp_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();
?>
</div>
