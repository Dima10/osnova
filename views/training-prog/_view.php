<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProg */
/* @var $reestr array */

?>

<div class="training-prog-update">

    <h1><?= Yii::t('app', 'ID') ?> <?= Html::encode($model->trp_id) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'reestr' => $reestr,

        ]) ?>

</div>
