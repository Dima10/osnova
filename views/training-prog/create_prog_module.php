<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgModule */
/* @var $prog array */
/* @var $module array */

$this->title = Yii::t('app', 'Create Training Prog Module');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Prog Modules'), 'url' => ['view', 'id' => $model->trpl_trp_id]];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="training-prog-module-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_prog_module', [
        'model' => $model,
        'prog' => $prog,
        'module' => $module,
    ]) ?>

</div>
