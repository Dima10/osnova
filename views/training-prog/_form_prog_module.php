<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProgModule */
/* @var $form yii\widgets\ActiveForm */
/* @var $prog array */
/* @var $module array */


$js = '
        $("#moduleSearch").click(
            function() {
                var moduleVal = $("#moduleText").val();
                $.get("'.\yii\helpers\Url::to(['/training-prog/ajax-module-search']).'",
                    {
                      trp_id : '.$model->trpl_trp_id.',
                      text : moduleVal
                    },
                    function (data) {
                        $("#module_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);

?>

<div class="training-prog-module-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); ?>

    <?php echo $form->field($model, 'trpl_trp_id')->dropDownList($prog, ['ReadOnly' => true]); ?>

    <div class="row">
        <div class="col-sm-2">
            <?= Html::label(Yii::t('app', 'Trpl Trm ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'moduleText', '', ['id' => 'moduleText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'moduleSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>


    <?php echo $form->field($model, 'trpl_trm_id')->dropDownList($module, ['id' => 'module_id']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
