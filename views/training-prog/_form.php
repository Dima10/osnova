<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProg */
/* @var $form yii\widgets\ActiveForm */
/* @var $errorSummary boolean|null */
/* @var $reestr array */
?>

<div class="training-prog-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]

    );

    echo $form->errorSummary($model);

    ?>

    <?php echo $form->field($model, 'trp_reestr')->radioList($reestr); ?>

    <?php echo $form->field($model, 'trp_name')->textInput(); ?>

    <?php echo $form->field($model, 'trp_code')->textInput(); ?>

    <?php echo $form->field($model, 'trp_hour')->textInput(['type' => 'number']); ?>

    <?php echo $form->field($model, 'trp_test_question')->textInput(['type' => 'number']); ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'trp_comment')->hiddenInput()->label(false)
        : $form->field($model, 'trp_comment')->textInput()
    ?>

    <?php echo $form->field($model, 'trp_dataA1')->fileInput(); ?>

    <?php echo $form->field($model, 'trp_dataA2')->fileInput(); ?>

    <?php echo $form->field($model, 'trp_dataB1')->fileInput(); ?>

    <?php echo $form->field($model, 'trp_dataB2')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
