<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProg */
/* @var $searchModelProgModule app\models\TrainingProgModuleSearch */
/* @var $dataProviderProgModule yii\data\ActiveDataProvider */
/* @var $prog array */
/* @var $module array */
/* @var $reestr array */

$this->title = $model->trp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-prog-view">

    <!--
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->trp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->trp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    -->
<?php

// https://github.com/yiisoft/yii2/issues/4890
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTabTrainingProg', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTabTrainingProg');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);




echo Tabs::widget([
    'items' => [
        // Prog
        [
            'label' => Yii::t('app', 'Training Prog'),
            'content' =>
                $this->render('_view',
                    [
                        'model' => $model,
                        'reestr' => $reestr,
                    ])
                ,
        ],

        // Plans
        [
            'label' => Yii::t('app', 'Training Prog Module'),
            'content' =>
                $this->render('_grid_prog_module',
                    [
                        'dataProvider' => $dataProviderProgModule,
                        'searchModel' => $searchModelProgModule,
                        'model' => $model,
                        'module' => $module,
                    ])
                ,
        ],

    ],
]);

?>

</div>
