<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingProg */

$this->title = $model->trp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-prog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->trp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->trp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    $attributes = [
        'trp_id',
        'trp_name',
        'trp_code',
        'trp_hour',
        'trp_test_question',
        'trp_dataA1_name',
        'trp_dataA2_name',
        'trp_dataB1_name',
        'trp_dataB2_name',
    ];
    if (!\app\models\User::isSpecAdmin()) {
        $attributes = array_merge(
            $attributes,
            [
                'trp_create_user',
                'trp_create_time',
                'trp_create_ip',
                'trp_update_user',
                'trp_update_time',
                'trp_update_ip',
            ]
        );
    }
    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
