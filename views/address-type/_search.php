<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'addt_id') ?>

    <?= $form->field($model, 'addt_name') ?>

    <?= $form->field($model, 'addt_create_user') ?>

    <?= $form->field($model, 'addt_create_time') ?>

    <?= $form->field($model, 'addt_create_ip') ?>

    <?php // echo $form->field($model, 'addt_update_user') ?>

    <?php // echo $form->field($model, 'addt_update_time') ?>

    <?php // echo $form->field($model, 'addt_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
