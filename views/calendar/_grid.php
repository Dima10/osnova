<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="calendar-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],

            'cal_id',
            'cal_date',
            [
                'attribute' => 'cal_holiday',
//                'label' => 'Номер приложения',
                'value' => function ($data) {
                    return $data->cal_holiday == 0 ? 'Нет' : 'Да';
                },
            'filter' => ['Нет', 'Да']
            ],

            [
                'attribute' => 'cal_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'cal_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'cal_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'cal_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'cal_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'cal_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);

Pjax::end();


?>

</div>
