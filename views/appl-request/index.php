<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Appl Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-index" id="forApplRequestController">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Request'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control']) ?>
    </p>
    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'ab'  => $ab,
        'comp' => $comp,
        'agra' => $agra,
        'reestr' => $reestr,
    ]) ?>

</div>
