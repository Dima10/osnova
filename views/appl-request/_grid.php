<?php

use app\models\AgreementStatus;
use app\models\Company;
use app\models\TrainingType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $reestr array */

?>
<div class="appl-request-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],

            'applr_id',
            [
                'attribute' => 'applr_reestr',
                'value' => function ($data) use ($reestr) {
                    return isset($reestr[$data->applr_reestr]) ? $reestr[$data->applr_reestr] : $reestr[0];
                },
                'label' => Yii::t('app', 'Applr Reestr'),
                'filter' => $reestr,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applr_number',
            'applr_date',
            [
                'attribute' => 'applr_flag',
                'value' =>
                    function ($data) {
                        return \app\models\Constant::YES_NO[$data->applr_flag ?? 0];
                    },
                'filter' => ['Нет', 'Да'],
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'yurLico',
                'value' => function ($data) {
                    return $data->applrAb->ab_name;
                },
                'label' => Yii::t('app', 'Applr Ab ID'),
            ],
            [
                'attribute' => 'applr_comp_id',
                'label' => Yii::t('app', 'Applr Comp ID'),
                'value' => function ($data) {
                    return $data->applrComp->comp_name ?? '';
                },
                'filter' => ArrayHelper::map(Company::find()->all(), 'comp_id', 'comp_name'),
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'agra_number',
                'value' => function ($data) {
                    return $data->applrAgra->agra_number;
                },
                'label' => Yii::t('app', 'Applr Agra ID'),
            ],

            [
                'attribute' => 'applr_trt_id',
                'value' => function ($data) {
                    return $data->applrTrt->trt_name;
                },
                'filter' => ArrayHelper::map(TrainingType::find()->all(), 'trt_id', 'trt_name'),
                'label' => Yii::t('app', 'Applr Trt ID'),
            ],

            [
                'attribute' => 'applr_ast_id',
                'value' => function ($data) {
                    return $data->applrAst->ast_name;
                },
                'filter' => ArrayHelper::map(AgreementStatus::find()->select(['ast_id', 'ast_name'])->orderBy('ast_name')->all(), 'ast_id', 'ast_name'),
                'label' => Yii::t('app', 'Applr Ast ID'),
            ],


            [
                'attribute' => 'applr_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applr_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
