<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applr_id') ?>

    <?= $form->field($model, 'applr_number') ?>

    <?= $form->field($model, 'applr_date') ?>

    <?= $form->field($model, 'applr_ab_id') ?>

    <?= $form->field($model, 'applr_comp_id') ?>

    <?php // echo $form->field($model, 'applr_agra_id') ?>

    <?php // echo $form->field($model, 'applr_create_user') ?>

    <?php // echo $form->field($model, 'applr_create_time') ?>

    <?php // echo $form->field($model, 'applr_create_ip') ?>

    <?php // echo $form->field($model, 'applr_update_user') ?>

    <?php // echo $form->field($model, 'applr_update_time') ?>

    <?php // echo $form->field($model, 'applr_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
