<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
/* @var $applRequest app\models\ApplRequest */
?>

<div class="appl-request-person-form">

    <?php
        $form = ActiveForm::begin([
                'action' => ($model->isNewRecord ?
                                        ['create-person', 'applr_id' => $applRequest->applr_id]
                                        :
                                        ['update-person', 'applr_id' => $applRequest->applr_id, 'id' => $model->prs_id ]
                            ),
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);

        echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'prs_last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_pass_sex')->radioList([0 => 'м', 1 => 'ж']) ?>

    <?php echo $form->field($model, 'prs_birth_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>

    <?= $form->field($model, 'prs_inn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_pass_serial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_pass_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prs_pass_issued_by')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'prs_pass_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
