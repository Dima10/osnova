<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */
/* @var $searchModelC app\models\ApplRequestContentSearch */
/* @var $dataProviderC yii\data\ActiveDataProvider */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $astatus array */
/* @var $trt array */
/* @var $pattern array */
/* @var $reestr array */
/* @var $manager array */
/* @var $ent_id integer */
/* @var $return string */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Request'),
]) . $model->applr_id;
if (isset($ent_id)) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['entity-frm/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => [$return, 'id' => $ent_id]];
    $this->params['breadcrumbs'][] = ['label' => $model->applr_id, 'url' => ['update', 'id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->applr_id, 'url' => ['update', 'id' => $model->applr_id]];
}
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-request-update" id="forApplRequestController">

    <?php if (isset($_SESSION['flash_appl_request_errors']) && !empty($_SESSION['flash_appl_request_errors'])) { ?>
        <div class="alert alert-danger">
            Произошла ошибка IMAP сервера, некотрые письма не были отправлены. Возможные причины:
            <br />
            <ul>
                <?php foreach ($_SESSION['flash_appl_request_errors'] as $error) { ?>
                    <li><?php echo $error; ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <?php unset($_SESSION['flash_appl_request_errors']); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab'  => $ab,
        'comp' => $comp,
        'agra' => $agra,
        'astatus' => $astatus,
        'trt' => $trt,
        'pattern' => $pattern,
        'reestr' => $reestr,
        'manager' => $manager,
    ]) ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Person'), ['create-person', 'applr_id' => $model->applr_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Create Appl Request Content'), ['create-content', 'applr_id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Notify Sends'), ['notify-send', 'applr_id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return], ['class' => 'btn btn-danger']) ?>

        <?= Html::a(Yii::t('app', 'Notify Sends Check 0'), ['notify-send0', 'applr_id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app', 'Notify Sends Check 1'), ['notify-send1', 'applr_id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return], ['class' => 'btn btn-info']) ?>

    </p>


    <?= $this->render('_grid_c', [
        'dataProvider' => $dataProviderC,
        'searchModel' => $searchModelC,
        'applr' => $model,
    ]) ?>


</div>
