<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $applr app\models\ApplRequest */

?>
<div class="appl-request-content-index-grid">

<?php
$link = yii\helpers\Url::to(['notify']);
$js = <<<"JS"
function _sendNotify(id) {
    $.ajax({
    type: "GET",
    url: "$link",
    data: {
        id: id 
        },
    success: function(data) {
    }
    });
}
JS;
$this->registerJs($js, $this::POS_BEGIN);

$link = yii\helpers\Url::to(['notify-check']);
$js = <<<"JS"
function _checkNotify(id) {
    $.ajax({
    type: "GET",
    url: "$link",
    data: {
        id: id 
        },
    success: function(data) {
        $.pjax.reload({container: '#pjax_grid'});
    }
    });
}
JS;

$this->registerJs($js, $this::POS_BEGIN);

Pjax::begin(['id' => 'pjax_grid']);

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-content', 'applrc_id' => $model->applrc_id]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'applrc_id',
            'applrcApplr.applr_number',
            'applrcPrs.prs_full_name',
            [
                'attribute' => 'applrc_notify',
                'format' => 'raw',
                'label' => '',
                'value' => function (\app\models\ApplRequestContent $model) {
                    return Html::checkbox('', $model->applrc_notify,
                        [
                            'title' => Yii::t('yii', 'Check'),
                            'onclick' => "_checkNotify({$model->applrc_id});return false;"
                        ]);

                },

            ],

            'applrcPrs.prs_connect_user',

            [
                'attribute'  => 'applrcTrp.trp_name',
                'label' => Yii::t('app', 'Training Prog'),
            ],
            [
                'attribute'  => 'applrcSvc.svc_name',
                'label' => Yii::t('app', 'Svc'),
            ],
            [
                'attribute'  => 'applrcSvc.svc_hour',
                'label' => Yii::t('app', 'Svc Hour'),
            ],


            [
                'attribute' => 'applrc_date_upk',
                'visible' => ((!empty($applr->applr_flag) ? $applr->applr_flag > 0 : false) && !\app\models\User::isSpecAdmin()),
            ],
            [
                'attribute' => 'applrc_notify',
                'format' => 'raw',
                'value' => function (\app\models\ApplRequestContent $model) {
                    return Html::a('<span class="glyphicon glyphicon-send"></span>', '#',
                        [
                            'title' => Yii::t('yii', 'Send'),
                            'onclick' => "_sendNotify({$model->applrc_id});return false;"
                        ]);

                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],


            /*
            [
                'attribute' => 'applrc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applrc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applrc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applrc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applrc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applrc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            */

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
