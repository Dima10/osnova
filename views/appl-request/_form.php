<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */
/* @var $form yii\widgets\ActiveForm */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $astatus array */
/* @var $trt array */
/* @var $pattern array */
/* @var $reestr array */
/* @var $manager array */

$js = '
        $("#ab_id").change(function() {
            var val = $("#ab_id").val();
            $.get("'.\yii\helpers\Url::to(['/appl-request/ajax-annex-list']).'",
                {
                    ab_id : val,
                    id: "ab_id"
                },
                function (data) {
                    $("#agra_id").html(data);
                }
            );
        });
';

$this->registerJs($js, yii\web\View::POS_READY);


$js = '
        $("#abSearch").click(
            function() {
                var abVal = $("#abText").val();
                $.get("'.\yii\helpers\Url::to(['/entity/ajax-search']).'",
                    {
                      id: "ab_id", 
                      text : abVal
                    },
                    function (data) {
                        $("#ab_id").html(data);
                    }
                );
            }
        );
        $("#abSearchM").click(
            function() {
                var abVal = $("#abTextM").val();
                $.get("'.\yii\helpers\Url::to(['/ab/ajax-search']).'",
                    {
                      id: "ab_id", 
                      text : abVal
                    },
                    function (data) {
                        $("#ab_idM").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, yii\web\View::POS_READY);


?>

<div class="appl-request-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'applr_reestr')->radioList($reestr); ?>

    <?php echo $form->field($model, 'applr_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <?php if (is_null($model->applr_ab_id)) : ?>
    <div class="row">
        <div class="col-sm-2">
            <?php//= Html::label(Yii::t('app', 'Ent Agent ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'abText', '', ['id' => 'abText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'abSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <?php endif; ?>

    <?php echo $form->field($model, 'applr_ab_id')->dropDownList($ab, ['id' => 'ab_id', 'ReadOnly' => !is_null($model->applr_ab_id)]); ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_comp_id')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_comp_id')->dropDownList($comp)
    ?>

    <?php echo $form->field($model, 'applr_agra_id')->dropDownList($agra, ['id' => 'agra_id', 'ReadOnly' => !$model->isNewRecord]); ?>

    <?php echo $form->field($model, 'applr_ast_id')->dropDownList($astatus); ?>

    <?php echo $form->field($model, 'applr_trt_id')->dropDownList($trt); ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_pat_id')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_pat_id')->dropDownList($pattern)
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_flag')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_flag')->checkbox()
    ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_flag_send')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_flag_send')->checkbox()
    ?>

    <div class="row" <?php echo (\app\models\User::isSpecAdmin()) ? 'style="display:none"' : '' ?>>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'abTextM', '', ['id' => 'abTextM']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'abSearchM']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_manager_id')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_manager_id')->dropDownList($manager, ['id' => 'ab_idM'])
    ?>
    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_curator_name')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_curator_name')->textInput(['maxlength' => true])
    ?>
    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'applr_curator_addr')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'applr_curator_addr')->textInput(['maxlength' => true])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'createUpdateBtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
