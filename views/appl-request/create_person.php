<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $applRequest app\models\ApplRequest */
/* @var $ent_id integer */
/* @var $return string */

$this->title = Yii::t('app', 'Create Person');
if (isset($ent_id)) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['entity-frm/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => [$return, 'id' => $ent_id]];
    $this->params['breadcrumbs'][] = ['label' => $applRequest->applr_id, 'url' => ['update', 'id' => $applRequest->applr_id, 'ent_id' => $ent_id, 'return' => $return]];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['update', 'id' => $applRequest->applr_id, 'ent_id' => $ent_id, 'return' => $return]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $applRequest->applr_id, 'url' => ['update', 'id' => $applRequest->applr_id]];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Staff'), 'url' => ['update', 'id' => $applRequest->applr_id]];
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-person-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_person', [
        'model' => $model,
        'applRequest' => $applRequest,
    ]) ?>

</div>
