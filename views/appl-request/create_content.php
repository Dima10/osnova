<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */
/* @var $applRequest app\models\ApplRequest */
/* @var $person array */
/* @var $svc array */
/* @var $prog array */
/* @var $applRequests array */
/* @var $ent_id integer */
/* @var $request_row_is_declined string */
/* @var $return string */

$this->title = Yii::t('app', 'Create Appl Request Content');
if (isset($ent_id)) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['entity-frm/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => [$return, 'id' => $ent_id]];
    $this->params['breadcrumbs'][] = ['label' => $applRequest->applr_id, 'url' => ['update', 'id' => $applRequest->applr_id, 'ent_id' => $ent_id, 'return' => $return]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request Contents'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request'), 'url' => ['update', 'id' => $applRequest->applr_id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-content-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3>
        <?php if(isset($request_row_is_declined) && !empty($request_row_is_declined)){
            echo $request_row_is_declined;
        } elseif(Yii::$app->request->post() && (!isset($request_row_is_declined) || empty($request_row_is_declined))) {
            echo '<div class="alert alert-danger">Несоответствие реестра Заявки и Программы!</div>';
        } else {} ?>
    </h3>

    <?= $this->render('_form_content', [
        'model' => $model,
        'person' => $person,
        'svc' => $svc,
        'prog' => $prog,
        'applRequests' => $applRequests,
    ]) ?>

</div>
