<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplRequest */
/* @var $ab array */
/* @var $comp array */
/* @var $agra array */
/* @var $astatus array */
/* @var $trt array */
/* @var $pattern array */
/* @var $reestr array */
/* @var $manager array */
/* @var $ent_id integer */
/* @var $return string */


$this->title = Yii::t('app', 'Create Appl Request');
if (isset($ent_id)) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entities'), 'url' => ['entity-frm/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Entity'), 'url' => [$return, 'id' => $ent_id]];
    $this->params['breadcrumbs'][] = ['label' => $model->applr_id, 'url' => ['update', 'id' => $model->applr_id, 'ent_id' => $ent_id, 'return' => $return]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ab'  => $ab,
        'comp' => $comp,
        'agra' => $agra,
        'astatus' => $astatus,
        'trt' => $trt,
        'pattern' => $pattern,
        'reestr' => $reestr,
        'manager' => $manager,
    ]) ?>

</div>
