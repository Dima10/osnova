<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShX */
/* @var $searchModel app\models\ApplShContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Sh X'),
]) . $model->applsx_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applsx_id, 'url' => ['view', 'id' => $model->applsx_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-sh-x-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('_grid_detail', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>

</div>
