<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplShXSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $trt array */

?>
<div class="appl-sh-x-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applsx_id',
            'applsx_number',
            'applsx_date',
            [
                'attribute' => 'applsx_trt_id',
                'value' =>
                    function ($data) {
                        return $data->applsxTrt->trt_name;
                    },
                'label' => Yii::t('app', 'Applsx Trt ID'),
                'filter' => $trt,
            ],
            [
                'attribute' => 'applsx_file_name',
                'label' => Yii::t('app', 'Applsx File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applsx_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applsx_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applsx_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
