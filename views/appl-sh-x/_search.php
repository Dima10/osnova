<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShXSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-sh-x-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applsx_id') ?>

    <?= $form->field($model, 'applsx_number') ?>

    <?= $form->field($model, 'applsx_date') ?>

    <?= $form->field($model, 'applsx_create_user') ?>

    <?= $form->field($model, 'applsx_create_time') ?>

    <?php // echo $form->field($model, 'applsx_create_ip') ?>

    <?php // echo $form->field($model, 'applsx_update_user') ?>

    <?php // echo $form->field($model, 'applsx_update_time') ?>

    <?php // echo $form->field($model, 'applsx_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
