<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplShX */

$this->title = $model->applsx_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sh-x-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applsx_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applsx_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applsx_id',
            'applsx_number',
            'applsx_date',
            [
                'attribute' => 'applsx_create_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_create_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_create_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'applsx_update_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_update_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applsx_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]) ?>

</div>
