<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAcc */
/* @var $entity array */
/* @var $company array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Acc'),
]) . $model->aga_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Accs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->aga_id, 'url' => ['view', 'id' => $model->aga_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-acc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

        'entity' => $entity,
        'company' => $company,
        'agreement' => $agreement,
        'agreement_annex' => $agreement_annex,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
