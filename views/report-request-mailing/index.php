<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplRequestContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \yii\base\DynamicModel */

$this->title = Yii::t('app', 'Request Mailing');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-listener-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div>
        <?= Html::a(Yii::t('app', 'Export Agreement Acc'), ['export-xml'], ['class' => 'btn btn-info']) ?>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]) ?>


</div>
