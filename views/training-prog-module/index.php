<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prog array */
/* @var $module array */

$this->title = Yii::t('app', 'Training Prog Modules');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-prog-module-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Training Prog Module'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'prog' => $prog,
        'module' => $module,
    ]) ?>

</div>
