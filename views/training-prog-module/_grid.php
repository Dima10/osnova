<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingProgModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prog array */
/* @var $module array */

?>
<div class="training-prog-module-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'trpl_id',

            [
                'attribute' => 'trpl_trm_id',
                'label' => Yii::t('app', 'Trpl Trm ID'),
                'value' => function ($data) {
                    return $data->trplTrm->trm_name;
                },
                'filter' => $module,
            ],

            [
                'attribute' => 'trpl_trp_id',
                'label' => Yii::t('app', 'Trpl Trp ID'),
                'value' => function ($data) {
                    return $data->trplTrp->trp_name;
                },
                'filter' => $prog,
            ],

            [
                'attribute' => 'trpl_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trpl_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trpl_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
