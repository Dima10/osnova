<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAddSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agreement-add-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'agadd_id') ?>

    <?= $form->field($model, 'agadd_number') ?>

    <?= $form->field($model, 'agadd_date') ?>

    <?= $form->field($model, 'agadd_agr_id') ?>

    <?= $form->field($model, 'agadd_create_user') ?>

    <?php // echo $form->field($model, 'agadd_create_time') ?>

    <?php // echo $form->field($model, 'agadd_create_ip') ?>

    <?php // echo $form->field($model, 'agadd_update_user') ?>

    <?php // echo $form->field($model, 'agadd_update_time') ?>

    <?php // echo $form->field($model, 'agadd_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
