<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementAddSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agreement_status array */

?>
<div class="agreement-add-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'agadd_id',
            'agadd_number',
            'agadd_date',
            [
                'attribute' => 'number',
                'label' => Yii::t('app', 'Agadd Agr ID'),
                'value' => function ($data) {
                    return $data->agaddAgr->agr_number ?? '';
                },
            ],
            [
                'attribute' => 'agadd_ast_id',
                'label' => Yii::t('app', 'Agadd Ast ID'),
                'value' => function ($data) {
                    return $data->agaddAst->ast_name ?? '';
                },
                'filter' => $agreement_status,
            ],

            [
                'attribute' => 'agadd_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'agadd_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
