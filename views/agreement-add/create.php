<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AgreementAdd */
/* @var $agreement array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Create Agreement Add');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Adds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-add-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'agreement' => $agreement,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
