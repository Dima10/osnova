<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAdd */

$this->title = $model->agadd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Adds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-add-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->agadd_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->agadd_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'agadd_id',
                'agadd_number',
                'agadd_date',
                [
                    'label' => Yii::t('app', 'Agadd Agr ID'),
                    'value' => $model->agaddAgr->agr_number,
                ],
                [
                    'label' => Yii::t('app', 'Agadd Ast ID'),
                    'value' => $model->agaddAst->ast_name ?? '',
                ],
                'agadd_create_user',
                'agadd_create_time',
                'agadd_create_ip',
                'agadd_update_user',
                'agadd_update_time',
                'agadd_update_ip',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
