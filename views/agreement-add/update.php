<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAdd */
/* @var $agreement array */
/* @var $agreement_status array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Agreement Add'),
]) . $model->agadd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Adds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->agadd_id, 'url' => ['view', 'id' => $model->agadd_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="agreement-add-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'agreement' => $agreement,
        'agreement_status' => $agreement_status,
    ]) ?>

</div>
