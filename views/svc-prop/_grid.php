<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcPropSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="svc-prop-index-grid">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'svcp_id',
            'svcp_name',
            'svcp_create_user',
            'svcp_create_time',
            'svcp_create_ip',
            'svcp_update_user',
            'svcp_update_time',
            'svcp_update_ip',

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
