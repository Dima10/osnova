<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AgreementActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $agreement array */
/* @var $agreement_annex array */
/* @var $agreement_status array */

Pjax::begin();
try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pager'=>[
            'maxButtonCount' => 25,
            'firstPageLabel' => ' «« В Начало',
            'lastPageLabel'  => ' В Конец »» '
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'act_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'yurLico',
                'label' => Yii::t('app', 'Act Ab ID'),
                'value' => function ($data) {
                    return $data->actAb->ab_name;
                },
            ],

            [
                'attribute' => 'act_ast_id',
                'label' => Yii::t('app', 'Act Ast ID'),
                'value' => function ($data) {
                    return $data->actAst->ast_name;
                },
                'filter' => $agreement_status,
            ],

            'act_number',
            'act_date',

            [
                'attribute' => 'dogovor',
                'label' => 'Договор',
                'value' => function ($data) {
                    return $data->actAgr->agr_number;
                },
            ],
            [
                'attribute' => 'appNumber',
                'label' => 'Номер приложения',
                'value' => function ($data) {
                    if($data->actAgra != null){
                        return $data->actAgra->agra_number;
                    }
                },
            ],

            //'act_agr_id',
            //'act_agra_id',
            'act_sum',
            'act_tax',

            [
                'attribute' => 'act_fdata',
                'label' => Yii::t('app', 'Act Fdata'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->act_fdata, yii\helpers\Url::toRoute(['download', 'id' => $data->act_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'act_fdata_sign',
                'label' => Yii::t('app', 'Act Fdata Sign'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->act_fdata_sign, yii\helpers\Url::toRoute(['download-sign', 'id' => $data->act_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'act_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'act_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}
Pjax::end();
