<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEndContent */

$this->title = $model->applec_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl End Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-end-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applec_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applec_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applec_id',
            'applec_apple_id',
            'applec_prs_id',
            'applec_trp_id',
            'applec_number',
            'applec_create_user',
            'applec_create_time',
            'applec_create_ip',
            'applec_update_user',
            'applec_update_time',
            'applec_update_ip',
        ],
    ]) ?>

</div>
