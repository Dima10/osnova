<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEndContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-end-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applec_id') ?>

    <?= $form->field($model, 'applec_apple_id') ?>

    <?= $form->field($model, 'applec_prs_id') ?>

    <?= $form->field($model, 'applec_trp_id') ?>

    <?= $form->field($model, 'applec_number') ?>

    <?php // echo $form->field($model, 'applec_create_user') ?>

    <?php // echo $form->field($model, 'applec_create_time') ?>

    <?php // echo $form->field($model, 'applec_create_ip') ?>

    <?php // echo $form->field($model, 'applec_update_user') ?>

    <?php // echo $form->field($model, 'applec_update_time') ?>

    <?php // echo $form->field($model, 'applec_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
