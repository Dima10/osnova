<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplEndContent */

$this->title = Yii::t('app', 'Create Appl End Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl End Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-end-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
