<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SvcDocTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-doc-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'svdt_id') ?>

    <?= $form->field($model, 'svdt_name') ?>

    <?= $form->field($model, 'svdt_create_user') ?>

    <?= $form->field($model, 'svdt_create_time') ?>

    <?= $form->field($model, 'svdt_create_ip') ?>

    <?php // echo $form->field($model, 'svdt_update_user') ?>

    <?php // echo $form->field($model, 'svdt_update_time') ?>

    <?php // echo $form->field($model, 'svdt_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
