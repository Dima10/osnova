<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */
/* @var $person array */
/* @var $svc array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Appl Request Content'),
]) . $model->applrc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->applrc_id, 'url' => ['view', 'id' => $model->applrc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="appl-request-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'person' => $person,
        'svc' => $svc,
    ]) ?>

</div>
