<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-request-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applrc_id') ?>

    <?= $form->field($model, 'applrc_applr_id') ?>

    <?= $form->field($model, 'applrc_prs_id') ?>

    <?= $form->field($model, 'applrc_trp_id') ?>

    <?= $form->field($model, 'applrc_create_user') ?>

    <?php // echo $form->field($model, 'applrc_create_time') ?>

    <?php // echo $form->field($model, 'applrc_create_ip') ?>

    <?php // echo $form->field($model, 'applrc_update_user') ?>

    <?php // echo $form->field($model, 'applrc_update_time') ?>

    <?php // echo $form->field($model, 'applrc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
