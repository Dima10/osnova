<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplRequestContent */

$this->title = $model->applrc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Request Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-request-content-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->applrc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->applrc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'applrc_id',
            'applrc_applr_id',
            'applrc_prs_id',
            'applrc_svc_id',
            'applrc_create_user',
            'applrc_create_time',
            'applrc_create_ip',
            'applrc_update_user',
            'applrc_update_time',
            'applrc_update_ip',
        ],
    ]) ?>

</div>
