<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */

$this->title = $model->taskt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->taskt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->taskt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'taskt_id',
            'taskt_name',
            'taskt_note:ntext',
            'taskt_create_user',
            'taskt_create_time',
            'taskt_create_ip',
            'taskt_update_user',
            'taskt_update_time',
            'taskt_update_ip',
        ],
    ]) ?>

</div>
