<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Task Type',
]) . $model->taskt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->taskt_id, 'url' => ['view', 'id' => $model->taskt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="task-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
