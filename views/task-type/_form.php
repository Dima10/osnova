<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'taskt_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taskt_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'taskt_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taskt_create_time')->textInput() ?>

    <?= $form->field($model, 'taskt_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taskt_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'taskt_update_time')->textInput() ?>

    <?= $form->field($model, 'taskt_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
