<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Svc */
/* @var $prog array */
/* @var $doc_type array */
/* @var $ab array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Svc'),
]) . $model->svc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svcs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->svc_id, 'url' => ['view', 'id' => $model->svc_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="svc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'doc_type' => $doc_type,
        'ab' => $ab,
    ]) ?>

</div>
