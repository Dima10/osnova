<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Svc */
/* @var $doc_type array */
/* @var $ab array */
/* @var $prog array */

$this->title = Yii::t('app', 'Create Svc');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svcs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'doc_type' => $doc_type,
        'ab' => $ab,
        //'prog' => $prog,
    ]) ?>

</div>
