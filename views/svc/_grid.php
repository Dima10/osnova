<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $doc_type array */
/* @var $ab array */

Pjax::begin();
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'svc_id',
                'options' => ['width' => '100'],
            ],

            'svc_name',
            [
                'attribute' => 'svc_svdt_id',
                'label' => Yii::t('app', 'Svc Svdt ID'),
                'value' => function ($data) { return isset($data->svcSvdt) ? $data->svcSvdt->svdt_name : Yii::t('yii', '(not set)'); },
                'filter' => $doc_type,
            ],

            'svc_skill',
            'svc_hour',
            'svc_price',
            'svc_cost',
            'svc_cost_ext',
            [
                'attribute' => 'svc_tax_flag',
                'value' => function ($data) {
                    return \app\models\Constant::YES_NO[$data->svc_tax_flag ?? ''];
                },
                'filter' => [
                    0 => 'Нет',
                    1 => 'Да',
                ],
            ],
            'svc_payment_flag',

            [
                'attribute' => 'svc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'svc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'svc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'svc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'svc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'svc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

            
        ],
    ]);
Pjax::end();
