<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Svc */
/* @var $searchModelSvcProg app\models\SvcProgSearch */
/* @var $dataProviderSvcProg yii\data\ActiveDataProvider */
/* @var $svc array */
/* @var $prog array */
/* @var $doc_type array */
/* @var $ab array */

$this->title = $model->svc_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svcs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->svc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->svc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

<?php

// https://github.com/yiisoft/yii2/issues/4890
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTabSvcProg', $(e.target).attr('href'));
        });

        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTabSvcProg');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);




echo Tabs::widget([
    'items' => [
        // Svc
        [
            'label' => Yii::t('app', 'Svc'),
            'content' =>
                $this->render('_view',
                    [
                        'model' => $model,
                        'doc_type' => $doc_type,
                        'ab' => $ab,
                    ])
                ,
        ],

        // Progs
        [
            'label' => Yii::t('app', 'Training Prog'),
            'content' =>
                $this->render('_grid_svc_prog',
                    [
                        'dataProvider' => $dataProviderSvcProg,
                        'searchModel' => $searchModelSvcProg,
                        'model' => $model,
                        'svc' => $svc,
                        'prog' => $prog,
                    ])
                ,
        ],

    ],
]);

?>

</div>
