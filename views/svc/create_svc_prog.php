<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SvcProg */
/* @var $svc array */
/* @var $prog array */

$this->title = Yii::t('app', 'Create Svc Prog');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Svc Progs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-svc-prog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_svc_prog', [
        'model' => $model,
        'svc' => $svc,
        'prog' => $prog,
    ]) ?>

</div>
