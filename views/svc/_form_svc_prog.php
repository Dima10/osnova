<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SvcProg */
/* @var $form yii\widgets\ActiveForm */
/* @var $svc array */
/* @var $prog array */

$js = '
        $("#progSearch").click(
            function() {
                var progVal = $("#progText").val();
                $.get("'.\yii\helpers\Url::to(['/svc/ajax-svc-prog-search']).'",
                    {
                      svc_id : '.$model->sprog_svc_id.',
                      text : progVal
                    },
                    function (data) {
                        $("#prog_id").html(data);
                    }
                );
            }
        );
';

$this->registerJs($js, \yii\web\View::POS_READY);


?>

<div class="svc-svc-prog-form">

<?php
    $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

    echo $form->errorSummary($model);
?>

    <?php echo $form->field($model, 'sprog_svc_id')->dropDownList($svc, ['ReadOnly' => true]); ?>

    <div class="row">
        <div class="col-sm-2">
            <?= Html::label(Yii::t('app', 'Sprog Trp ID')) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::input('text', 'prog', '', ['id' => 'progText']) ?>
            <?= Html::button(Yii::t('app', 'Search'), ['id' => 'progSearch']) ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>

    <?php echo $form->field($model, 'sprog_trp_id')->dropDownList($prog, ['id' => 'prog_id']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
