<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $id string */
/* @var $prog array */
?>

<?= Html::dropDownList($id, isset($prog[0]) ? $prog[0] : '', $prog, ['id' => $id]) ?>
