<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingModule */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="training-module-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); ?>

    <?php echo $form->field($model, 'trm_name')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'trm_code')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'trm_test_question')->textInput(['type' => 'number']); ?>

    <?php echo $form->field($model, 'trm_dataA')->fileInput(); ?>

    <?= (\app\models\User::isSpecAdmin())
        ? $form->field($model, 'trm_dataB')->hiddenInput(['maxlength' => true])->label(false)
        : $form->field($model, 'trm_dataB')->fileInput()
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
