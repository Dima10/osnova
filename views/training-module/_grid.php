<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TrainingModuleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $module array */

?>
<div class="training-module-index-grid">

<?php Pjax::begin(); ?>
<?php

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'trm_id',
            'trm_name',
            'trm_code',
            'trm_test_question',
            [
                'attribute' => 'trm_trm_id',
                'label' => Yii::t('app', 'Trm Trm ID'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(isset($data->trmTrm) ? $data->trmTrm->trm_name : '', yii\helpers\Url::toRoute(['view', 'id' => isset($data->trmTrm) ? $data->trmTrm->trm_id : -1]));
                },
                'filter' => $module,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'trm_dataA_name',
                'label' => Yii::t('app', 'Trm Data A Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trm_dataA_name, yii\helpers\Url::toRoute(['download-a', 'id' => $data->trm_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'trm_dataB_name',
                'label' => Yii::t('app', 'Trm Data B Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->trm_dataB_name, yii\helpers\Url::toRoute(['download-b', 'id' => $data->trm_id]), ['target' => '_blank']);
                },
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'trm_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
} ?>
<?php Pjax::end(); ?>
</div>
