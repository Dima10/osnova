<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingModule */

$this->title = $model->trm_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service'), 'url' => ['site/svc']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-module-view">

    <h1><?= Html::encode($this->title).': '.Html::encode($model->trm_name) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->trm_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->trm_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>&nbsp;
        <?= Html::a(Yii::t('app', 'Create Trm Trm ID'), ['create-child', 'pid' => $model->trm_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'trm_id',
            'trm_name',
            'trm_code',
            'trm_test_question',
            'trm_dataA_name',

            [
                'attribute' => 'trm_dataB_name',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_create_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_create_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_create_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_user',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_time',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'trm_update_ip',
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
        ],
    ]) ?>

</div>
