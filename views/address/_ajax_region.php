<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $region array */

?>

<?= Html::dropDownList('region_id', isset($region[0]) ? $region[0] : '', $region) ?>

<!--

    $s = '';
    foreach ($region as $key => $value ) {
        $s .= '<option value="'.$key.'">'.$value.'</option>';
    }

    if ($s==='') {
        echo '<option value=""></option>';
    } else {
        echo $s;
   }
-->