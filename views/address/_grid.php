<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $addressType array */
/* @var $country array */
/* @var $region array */
/* @var $city array */

Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'contragent',
                'label' => Yii::t('app', 'Add Ab ID'),
                'value' => function ($data) { return $data->addAb->ab_name ?? ''; },
            ],
            
            [
                'attribute' => 'add_addt_id',
                'label' => Yii::t('app', 'Add Addt ID'),
                'value' => function ($data) { return $data->addAddt->addt_name ?? ''; },
                'filter' => $addressType,
            ],
            [
                'attribute' => 'add_cou_id',
                'label' => Yii::t('app', 'Add Cou ID'),
                'value' => function ($data) { return $data->addCou->cou_name ?? ''; },
                'filter' => $country,
            ],
            [
                'attribute' => 'add_reg_id',
                'label' => Yii::t('app', 'Add Reg ID'),
                'value' => function ($data) { return $data->addReg->reg_name ?? ''; },
                'filter' => $region,
            ],
            [
                'attribute' => 'add_city_id',
                'label' => Yii::t('app', 'Add City ID'),
                'value' => function ($data) { return $data->addCity->city_name ?? ''; },
                'filter' => $city,
            ],

            'add_index',
            'add_data',

            [
                'attribute' => 'add_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'add_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'add_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'add_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'add_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'add_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();

