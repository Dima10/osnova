<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $addressType array */
/* @var $addrbook array */
/* @var $country array */
/* @var $region array */
/* @var $city array */

$this->title = Yii::t('app', 'Addresses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Address'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    echo $this->render('_grid',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'addressType' => $addressType,
                'addrbook' => $addrbook,
                'country' => $country,
                'region' => $region,
                'city' => $city,
            ]
         );
    
    
    ?>

</div>
