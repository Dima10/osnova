<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Address */
/* @var $addressType array */
/* @var $addrbook array */
/* @var $country array */
/* @var $region array */
/* @var $city array */

$this->title = Yii::t('app', 'Create Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addresses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'addrbook' => $addrbook,
        'addressType' => $addressType,
        'country' => $country,
        'region' => $region,
        'city' => $city,
    ]) ?>

</div>
