<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'add_id') ?>

    <?= $form->field($model, 'add_ab_id') ?>

    <?= $form->field($model, 'add_addt_id') ?>

    <?= $form->field($model, 'add_reg_id') ?>

    <?= $form->field($model, 'add_city_id') ?>

    <?php // echo $form->field($model, 'add_data') ?>

    <?php // echo $form->field($model, 'add_create_user') ?>

    <?php // echo $form->field($model, 'add_create_time') ?>

    <?php // echo $form->field($model, 'add_create_ip') ?>

    <?php // echo $form->field($model, 'add_update_user') ?>

    <?php // echo $form->field($model, 'add_update_time') ?>

    <?php // echo $form->field($model, 'add_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
