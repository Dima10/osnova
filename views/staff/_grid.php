<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $entity array */
/* @var $person array */

Pjax::begin(); 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'stf_id',
                'options' => ['width' => '100'],
            ],

            [
                'attribute' => 'yurLico',
                'label' => Yii::t('app', 'Stf Ent ID'),
                'value' => function ($data) { return $data->stfEnt->ent_name; },
//                'filter' => $entity,
            ],
            [
                'attribute' => 'fizLico',
                'label' => Yii::t('app', 'Stf Prs ID'),
                'value' => function ($data) { return $data->stfPrs->prs_full_name; },
//                'filter' => '<input type="text" class="form-control" name="StaffSearch[fizLico]" value="">',
            ],

            'stf_position',

            [
                'attribute' => 'stf_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'stf_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'stf_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'stf_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'stf_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'stf_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
Pjax::end();

