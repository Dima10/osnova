<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommandContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-command-content-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applcmdc_id') ?>

    <?= $form->field($model, 'applcmdc_applcmd_id') ?>

    <?= $form->field($model, 'applcmdc_prs_id') ?>

    <?= $form->field($model, 'applcmdc_trp_id') ?>

    <?= $form->field($model, 'applcmdc_create_user') ?>

    <?php // echo $form->field($model, 'applcmdc_create_time') ?>

    <?php // echo $form->field($model, 'applcmdc_create_ip') ?>

    <?php // echo $form->field($model, 'applcmdc_update_user') ?>

    <?php // echo $form->field($model, 'applcmdc_update_time') ?>

    <?php // echo $form->field($model, 'applcmdc_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
