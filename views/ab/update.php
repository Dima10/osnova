<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ab */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Ab'),
]) . $model->ab_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Abs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ab_id, 'url' => ['view', 'id' => $model->ab_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ab-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
