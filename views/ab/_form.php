<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ab */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ab-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'ab_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ab_type')->textInput() ?>

    <?= $form->field($model, 'ab_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ab_create_time')->textInput() ?>

    <?= $form->field($model, 'ab_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ab_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ab_update_time')->textInput() ?>

    <?= $form->field($model, 'ab_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
