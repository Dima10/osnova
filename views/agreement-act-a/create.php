<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AgreementActA */

$this->title = Yii::t('app', 'Create Agreement Act A');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Act As'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-act-a-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
