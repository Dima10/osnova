<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplCommandContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="appl-command-content-index-grid">

<?php



Pjax::begin();

    echo GridView::widget([
        'id' => 'applcmdc',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete-content', 'cmd_id' => $model->applcmdc_applcmd_id, 'id' => $model->applcmdc_id]);
                            return $url;
                        }
                        return '#';
                    },

            ],

            'applcmdc_id',
            'applcmdcPrs.prs_full_name',
            'applcmdcTrp.trp_code',
            'applcmdcTrp.trp_name',
            'applcmdcTrp.trp_hour',

            [
                'attribute' => 'applcmdc_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmdc_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmdc_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmdc_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmdc_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmdc_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
