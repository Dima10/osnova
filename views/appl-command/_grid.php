<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplCommandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $comp array */
/* @var $trt array */
/* @var $svdt array */
/* @var $reestr array */

?>
<div class="appl-command-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}{summary}{items}{summary}{pager}",
        'pager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel' => Yii::t('app', 'Last'),
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                /*
                'template' => '{update} {sh} {sheet}',
                'buttons' => [
                  'update' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pensil"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Update'),
                                ]);
                        },
                    'sh' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-hand-right"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sh'),
                                ]);
                        },
                    'sheet' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-hand-left"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sheet'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update', 'applcmd_id' => $model->applcmd_id]);
                            return $url;
                        }
                        if ($action === 'sh') {
                            $url = yii\helpers\Url::to(['appl-sh/generate', 'id' => $model->applcmd_prs_id, 'date' => $model->applm_appls_date]);
                            return $url;
                        }
                        if ($action === 'sheetX') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-sheet-x', 'date' => $model->applm_applsx_date]);
                            return $url;
                        }
                        if ($action === 'command') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-command', 'date' => $model->applm_applcmd_date]);
                            return $url;
                        }
                        return '#';
                    },
                */

            ],

            'applcmd_id',
            [
                'attribute' => 'applcmd_reestr',
                'value' => function ($data) use ($reestr) {
                    return $reestr[$data->applcmd_reestr];
                },
                'label' => Yii::t('app', 'Applcmd Reestr'),
                'filter' => $reestr,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],
            'applcmd_number',
            'applcmd_date',
            /*
            [
                'attribute' => 'applcmd_ab_id',
                'label' => Yii::t('app', 'Applcmd Ab ID'),
                'value' =>
                    function ($data) {
                        return isset($data->applcmdAb) ? $data->applcmdAb->ab_name : '';
                    },
                'filter' => $ab,
            ],
            */
            [
                'attribute' => 'applcmd_comp_id',
                'label' => Yii::t('app', 'Applcmd Comp ID'),
                'value' =>
                    function ($data) {
                        return isset($data->applcmdComp) ? $data->applcmdComp->comp_name : '';
                    },
                'filter' => $comp,
                'visible' => !\app\models\User::isSpecAdmin(),
            ],

            [
                'attribute' => 'applcmd_trt_id',
                'label' => Yii::t('app', 'Applcmd Trt ID'),
                'value' =>
                    function ($data) {
                        return $data->applcmdTrt->trt_name;
                    },
                'filter' => $trt,
            ],
            [
                'attribute' => 'applcmd_svdt_id',
                'label' => Yii::t('app', 'Applcmd Svdt ID'),
                'value' =>
                    function ($data) {
                        return $data->applcmdSvdt->svdt_name;
                    },
                'filter' => $svdt,
            ],
            [
                'attribute' => 'applsx_file_name',
                'label' => Yii::t('app', 'Applcmd File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applcmd_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applcmd_id]), ['target' => '_blank']);
                },
            ],

            [
                'attribute' => 'applcmd_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmd_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmd_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmd_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmd_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applcmd_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
