<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplCommandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $comp array */
/* @var $trt array */
/* @var $svdt array */
/* @var $reestr array */

$this->title = Yii::t('app', 'Appl Commands');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-command-index" id="forApplCommandController">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Appl Command'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::button('К странице', ['id'=>'toPageBtn', 'class'=> 'btn btn-info', 'style'=>'float:left']) ?>
        <?= Html::input('number', 'toPageInput', 1,['id'=>'toPageInput', 'style'=>'text-align:center; width:100px', 'class'=> 'form-control']) ?>
    </p>
    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'comp' => $comp,
        'trt' => $trt,
        'svdt' => $svdt,
        'reestr' => $reestr,
    ]) ?>

</div>
