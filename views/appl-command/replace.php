<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\ReplaceFiles */
/* @var $form yii\widgets\ActiveForm */

/* @var $replaceTypes array */

$this->title = 'Замена документов';
?>

<div class="pattern-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pattern-form">

        <?php if (!empty($error)){ ?>
            <div class="alert alert-danger"><?php echo $error ?></div>
        <?php } ?>

        <?php if (!empty($success)){ ?>
            <div class="alert alert-success"><?php echo $success ?></div>
        <?php } ?>

        <?php
        $form = ActiveForm::begin([
                  'fieldConfig' => [
                      'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                  ],
              ]);

        $form->errorSummary($model);
        ?>

        <?php
            $replaceTypes[0] = 'Тип документа';
            ksort($replaceTypes);
        ?>


        <?= $form->field($model, 'replaceTypeId')->dropDownList($replaceTypes) ?>

        <?= $form->field($model, 'idDocument')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'fileData')->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Replace'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>



</div>
