<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tasks_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tasks_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tasks_create_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tasks_create_time')->textInput() ?>

    <?= $form->field($model, 'tasks_create_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tasks_update_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tasks_update_time')->textInput() ?>

    <?= $form->field($model, 'tasks_update_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
