<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAnnex */

$this->title = $model->agra_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance'), 'url' => ['site/finance']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agreement Annexes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-annex-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->agra_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->agra_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'agra_id',
            'agra_number',
            'agraEnt.ent_name',
            'agraAgr.agr_number',
            'agra_sum',
            'agra_tax',
            'agra_fdata',
            'agra_fdata_sign',
            'agra_create_user',
            'agra_create_time',
            'agra_create_ip',
            'agra_update_user',
            'agra_update_time',
            'agra_update_ip',
        ],
    ]) ?>

</div>
