<?php

use app\models\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="finance-book-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'pager'=>[
            'maxButtonCount' => 25,
            'firstPageLabel' => ' «« В Начало',
            'lastPageLabel'  => ' В Конец »» '
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],

            [
                'attribute' => 'fb_id',
                'filterInputOptions' => ['style' => 'width:50px', 'class'=>'form-control']
            ],
            [
                'attribute' => 'svc_name',
                'label' => Yii::t('app', 'Svc'),
            ],
            [
                'attribute' => 'fb_fbt_id',
                'label' => Yii::t('app', 'Fb Fbt ID'),
                'filter' => ArrayHelper::map(\app\models\FinanceBookType::find()->all(), 'fbt_id', 'fbt_name'),
                'value' => function ($data) {
                    return $data->fbFbt->fbt_name;
                },
            ],
            'ab_name',
            [
                'attribute' => 'fb_comp_id',
                'label' => Yii::t('app', 'Fb Comp ID'),
                'value' => function ($data) {
                    return $data->fbComp->comp_name;
                },
                'filter' => ArrayHelper::map(\app\models\Company::find()->all(), 'comp_id', 'comp_name'),
            ],
            'fb_sum',
            'fb_tax',
            'aga_number',
            [
                'attribute' => 'acc_number',
                'label' => Yii::t('app', 'Acc Number'),
                'value' => function ($data) {
                    if(isset($data->fbAcc)){
                        return $data->fbAcc->acc_number;
                    } else {
                        return '';
                    }
                },
            ],
            [
                'attribute' => 'fb_fbs_id',
                'label' => Yii::t('app', 'Fb Fbs ID'),
                'filter' => ArrayHelper::map(\app\models\FinanceBookStatus::find()->all(), 'fbs_id', 'fbs_name'),
                'value' => function ($data) {
                    return $data->fbFbs->fbs_name;
                },
            ],
            [
                'attribute' => 'pt_id',
                'label' => Yii::t('app', 'Fb Pt ID'),
                'filter' => ArrayHelper::map(\app\models\PaymentType::find()->all(), 'pt_id', 'pt_name'),
                'value' => function ($data) {
                    return $data->fbPt->pt_name;
                },
            ],

            'fb_date',
            'fb_payment',
            'fb_comment',

            [
                'attribute' => 'fb_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'fb_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
