<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fb_id') ?>

    <?= $form->field($model, 'fb_svc_id') ?>

    <?= $form->field($model, 'fb_fbt_id') ?>

    <?= $form->field($model, 'fb_sum') ?>

    <?= $form->field($model, 'fb_tax') ?>

    <?php // echo $form->field($model, 'fb_aga_id') ?>

    <?php // echo $form->field($model, 'fb_acc_id') ?>

    <?php // echo $form->field($model, 'fb_fbs_id') ?>

    <?php // echo $form->field($model, 'fb_date') ?>

    <?php // echo $form->field($model, 'fb_payment') ?>

    <?php // echo $form->field($model, 'fb_comment') ?>

    <?php // echo $form->field($model, 'fb_create_user') ?>

    <?php // echo $form->field($model, 'fb_create_ip') ?>

    <?php // echo $form->field($model, 'fb_create_time') ?>

    <?php // echo $form->field($model, 'fb_update_user') ?>

    <?php // echo $form->field($model, 'fb_update_id') ?>

    <?php // echo $form->field($model, 'fb_update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
