<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-to-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'payt_from_ab_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_from_acc_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_to_ab_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_to_acc_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_fbs_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_svc_id')->textInput(); ?>

    <?php echo $form->field($model, 'payt_svc_price')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'payt_svc_qty')->textInput(); ?>

    <?php echo $form->field($model, 'payt_svc_sum')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'payt_date_pay')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
