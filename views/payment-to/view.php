<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTo */

$this->title = $model->payt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Tos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-to-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->payt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->payt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'payt_id',
            'payt_from_ab_id',
            'payt_from_acc_id',
            'payt_to_ab_id',
            'payt_to_acc_id',
            'payt_fbs_id',
            'payt_svc_id',
            'payt_svc_price',
            'payt_svc_qty',
            'payt_svc_sum',
            'payt_date_pay',
            'payt_create_user',
            'payt_create_time',
            'payt_create_ip',
            'payt_update_user',
            'payt_update_time',
            'payt_update_ip',
        ],
    ]) ?>

</div>
