<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentTo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Payment To'),
]) . $model->payt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payment Tos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payt_id, 'url' => ['view', 'id' => $model->payt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-to-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
