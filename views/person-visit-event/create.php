<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonVisitEvent */

$this->title = Yii::t('app', 'Create Person Visit Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visit Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-visit-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
