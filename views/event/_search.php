<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'event_id') ?>

    <?= $form->field($model, 'event_ab_id') ?>

    <?= $form->field($model, 'event_evt_id') ?>

    <?= $form->field($model, 'event_evrt_id') ?>

    <?= $form->field($model, 'event_date') ?>

    <?php // echo $form->field($model, 'event_user_id') ?>

    <?php // echo $form->field($model, 'event_note') ?>

    <?php // echo $form->field($model, 'event_create_user') ?>

    <?php // echo $form->field($model, 'event_create_time') ?>

    <?php // echo $form->field($model, 'event_create_ip') ?>

    <?php // echo $form->field($model, 'event_update_user') ?>

    <?php // echo $form->field($model, 'event_update_time') ?>

    <?php // echo $form->field($model, 'event_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
