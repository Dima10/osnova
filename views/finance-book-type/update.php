<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Finance Book Type',
]) . $model->fbt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Book Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fbt_id, 'url' => ['view', 'id' => $model->fbt_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="finance-book-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
