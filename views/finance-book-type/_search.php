<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-book-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fbt_id') ?>

    <?= $form->field($model, 'fbt_name') ?>

    <?= $form->field($model, 'fbt_create_user') ?>

    <?= $form->field($model, 'fbt_create_ip') ?>

    <?= $form->field($model, 'fbt_create_time') ?>

    <?php // echo $form->field($model, 'fbt_update_user') ?>

    <?php // echo $form->field($model, 'fbt_update_ip') ?>

    <?php // echo $form->field($model, 'fbt_update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
