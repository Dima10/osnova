<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBookType */

$this->title = $model->fbt_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Book Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-book-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->fbt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->fbt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fbt_id',
            'fbt_name',
            'fbt_create_user',
            'fbt_create_ip',
            'fbt_create_time',
            'fbt_update_user',
            'fbt_update_ip',
            'fbt_update_time',
        ],
    ]) ?>

</div>
