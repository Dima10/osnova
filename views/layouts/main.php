<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\AcUserRole;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<?php if ($prs = app\models\Person::findOne(['prs_connect_user' => Yii::$app->user->identity->username ?? null])): ?>

<script>
    var distance = 120;
    var x = setInterval(
        function()
        {
            $.get('<?= yii::$app->urlManager->createUrl(['site/time-on']) ?>');
            distance = distance - 1;
            if (distance < 0) {
                clearInterval(x);
            }
        }
        , 60000);
</script>

<?php endif; ?>

<div class="wrap">
    <?php
    $isAdmin = false;
    if(isset(Yii::$app->user->identity)){
        $isAdmin = Yii::$app->user->identity->isAdmin || AcUserRole::find()->where(['acur_user_id' => Yii::$app->user->identity->id, 'acur_acr_id' => 0])->exists();
    }
    NavBar::begin([
        'brandLabel' => Yii::t('app', 'My Company'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    try {

        $coreItems = [
            (isset(Yii::$app->user->identity->accessActions['country/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Countries'), 'url' => ['/country']] : false,
            (isset(Yii::$app->user->identity->accessActions['region/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Regions'), 'url' => ['/region']] : false,
            (isset(Yii::$app->user->identity->accessActions['city/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Cities'), 'url' => ['/city']] : false,
            (isset(Yii::$app->user->identity->accessActions['city/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['address-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Address Types'), 'url' => ['/address-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['address/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Addresses'), 'url' => ['/address']] : false,
            (isset(Yii::$app->user->identity->accessActions['address/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['contact-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Contact Types'), 'url' => ['/contact-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['contact/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Contacts'), 'url' => ['/contact']] : false,
            (isset(Yii::$app->user->identity->accessActions['contact/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['entity-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Entity Types'), 'url' => ['/entity-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['entity-class/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Entity Classes'), 'url' => ['/entity-class']] : false,
            (isset(Yii::$app->user->identity->accessActions['entity/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Entities'), 'url' => ['/entity']] : false,
            (isset(Yii::$app->user->identity->accessActions['company/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Companies'), 'url' => ['/company']] : false,
            (isset(Yii::$app->user->identity->accessActions['company/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['person/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Persons'), 'url' => ['/person']] : false,
            (isset(Yii::$app->user->identity->accessActions['staff/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Staff'), 'url' => ['/staff']] : false,
        ];
        $coreItems = array_filter($coreItems);

        $financeItems = [
            (isset(Yii::$app->user->identity->accessActions['bank/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Banks'), 'url' => ['/bank']] : false,
            (isset(Yii::$app->user->identity->accessActions['account/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Accounts'), 'url' => ['/account']] : false,
            (isset(Yii::$app->user->identity->accessActions['account/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['agreement/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreements'), 'url' => ['/agreement']] : false,
            (isset(Yii::$app->user->identity->accessActions['agreement-add/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Adds'), 'url' => ['/agreement-add']] : false,
            (isset(Yii::$app->user->identity->accessActions['agreement-annex/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Annexes'), 'url' => ['/agreement-annex']] : false,
            (isset(Yii::$app->user->identity->accessActions['agreement-acc/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Accs'), 'url' => ['/agreement-acc']] : false,
            (isset(Yii::$app->user->identity->accessActions['agreement-act/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Acts'), 'url' => ['/agreement-act']] : false,
            (isset(Yii::$app->user->identity->accessActions['agreement-act/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['calendar/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar']] : false,
            (isset(Yii::$app->user->identity->accessActions['finance-book-status/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Finance Book Statuses'), 'url' => ['/finance-book-status']] : false,
            (isset(Yii::$app->user->identity->accessActions['finance-book/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Finance Books'), 'url' => ['/finance-book']] : false,
        ];
        $financeItems = array_filter($financeItems);

        $serviceItems = [
            (isset(Yii::$app->user->identity->accessActions['svc-doc-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Svc Doc Types'), 'url' => ['/svc-doc-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Training Types'), 'url' => ['/training-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['svc/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Svcs'), 'url' => ['/svc']] : false,
            (isset(Yii::$app->user->identity->accessActions['svc-prop/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Svc Props'), 'url' => ['/svc-prop']] : false,
            (isset(Yii::$app->user->identity->accessActions['svc-value/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Svc Values'), 'url' => ['/svc-value']] : false,
        ];
        $serviceItems = array_filter($serviceItems);

        $educationItems = [
            (isset(Yii::$app->user->identity->accessActions['training-prog/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Training Progs'), 'url' => ['/training-prog']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-module/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Training Modules'), 'url' => ['/training-module']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-question/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Training Questions'), 'url' => ['/training-question']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-question/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['appl-request/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Requests'), 'url' => ['/appl-request']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-command/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Commands'), 'url' => ['/appl-command']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-sh/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Shs'), 'url' => ['/appl-sh']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-sh-x/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Sh Xes'), 'url' => ['/appl-sh-x']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-sheet/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Sheets'), 'url' => ['/appl-sheet']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-sheet-x/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Sheet Xes'), 'url' => ['/appl-sheet-x']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-xxx/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Xxxes'), 'url' => ['/appl-xxx']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-end/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Ends'), 'url' => ['/appl-end']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-final/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Finals'), 'url' => ['/appl-final']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-final/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['appl-blank/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Blanks'), 'url' => ['/appl-blank']] : false,
            (isset(Yii::$app->user->identity->accessActions['appl-out/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Outs'), 'url' => ['/appl-out']] : false,
        ];
        $educationItems = array_filter($educationItems);

        $reportsItems = [
            (isset(Yii::$app->user->identity->accessActions['report-finances/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Reestr Finances'), 'url' => ['/report-finances']] : false,
            (isset(Yii::$app->user->identity->accessActions['report-listener/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Reestr Listener'), 'url' => ['/report-listener']] : false,
            (isset(Yii::$app->user->identity->accessActions['person-visit/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Person Visits'), 'url' => ['/person-visit']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-prog-session/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Training Prog Sessions'), 'url' => ['/training-prog-session']] : false,
            (isset(Yii::$app->user->identity->accessActions['training-prog-session/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['report-request-mailing/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Request Mailing'), 'url' => ['/report-request-mailing']] : false,
        ];
        $reportsItems = array_filter($reportsItems);

        $settingItems = [
            (isset(Yii::$app->user->identity->accessActions['agreement-status/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Statuses'), 'url' => ['/agreement-status']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['/pattern-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Patterns'), 'url' => ['/pattern']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['system-mail/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Системная почта'), 'url' => ['/system-mail']] : false,
            (isset(Yii::$app->user->identity->accessActions['user-mail/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Управление уведомлениями'), 'url' => ['/user-mail']] : false,
            (isset(Yii::$app->user->identity->accessActions['user-mail/index'])|| $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['appl-main/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['/appl-main']] : false,
        ];
        $settingItems = array_filter($settingItems);

        $adminSettingItems = [
            //['label' => Yii::t('app', 'Tags'), 'url' => ['/tag']],
            (isset(Yii::$app->user->identity->accessActions['agreement-status/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Agreement Statuses'), 'url' => ['/agreement-status']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern-type/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Pattern Types'), 'url' => ['/pattern-type']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Patterns'), 'url' => ['/pattern']] : false,
            (isset(Yii::$app->user->identity->accessActions['pattern/index'])|| $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['system-mail/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Системная почта'), 'url' => ['/system-mail']] : false,
            (isset(Yii::$app->user->identity->accessActions['user-mail/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Управление уведомлениями'), 'url' => ['/user-mail']] : false,
            (isset(Yii::$app->user->identity->accessActions['user-mail/index'])|| $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['user/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Users'), 'url' => ['/user']] : false,
            (isset(Yii::$app->user->identity->accessActions['ac-role/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Ac Roles'), 'url' => ['/ac-role']] : false,
            (isset(Yii::$app->user->identity->accessActions['ac-role/index']) || $isAdmin) ? '<li class="divider"></li>' : false,
            (isset(Yii::$app->user->identity->accessActions['appl-main/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Appl Mains'), 'url' => ['/appl-main']] : false,
//                        (isset(Yii::$app->user->identity->accessActions['ac-func/index'])|| $isAdmin) ? ['label' => Yii::t('app', 'Ac Funcs'), 'url' => ['/ac-func']] : false,
        ];
        $adminSettingItems = array_filter($adminSettingItems);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => Yii::t('app', 'Home'), 'url' => ['/site/index']],
                //['label' => Yii::t('app', 'About'), 'url' => ['/site/about']],
                //['label' => Yii::t('app', 'Contact'), 'url' => ['/site/contact']],

                ['label' => Yii::t('app', 'Core'),
                    'visible' => count($coreItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => $coreItems,
                ],

                ['label' => Yii::t('app', 'Finance'),
                    'visible' => count($financeItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => $financeItems,
                ],

                ['label' => Yii::t('app', 'Service'),
                    'visible' => count($serviceItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => $serviceItems,

                ],

                ['label' => Yii::t('app', 'Education'),
                    'visible' => count($educationItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => $educationItems,

                ],

                ['label' => Yii::t('app', 'Reports'),
                    'visible' => count($reportsItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0),
                    'items' => $reportsItems,
                ],

                ['label' => Yii::t('app', 'Setting'),
                    'visible' =>count($settingItems) > 0 && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && !Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0 && Yii::$app->user->identity->level < 70),
                    'items' => $settingItems,
                ],

                // для админов
                ['label' => Yii::t('app', 'Setting'),
                    'visible' => count($adminSettingItems) > 0  && isset(Yii::$app->user->identity->roles) && count(Yii::$app->user->identity->roles)>0 && isset(Yii::$app->user->identity) ? Yii::$app->user->identity->level >= 70 : false,
                    'items' => $adminSettingItems,
                ],


                Yii::$app->user->isGuest ? (
                ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                    . Html::submitButton(
                        Yii::t('app', 'Logout  ({user})', ['user' => Yii::$app->user->identity->username]),
                        ['class' => 'btn btn-link']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    NavBar::end();
    ?>


    <div class="container">

        <div class="">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">

                <?php if (!Yii::$app->user->isGuest && (Yii::$app->user->identity->level > 0)) : ?>

                <?= Html::a(Html::img("@web/img/Company3.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/entity-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/person2.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/person-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/kav.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/appl-gen/create-appl-all'])); ?><br>
                <?= Html::a(Html::img("@web/img/kav20.png", ["class"=>"img-rounded", 'style' => 'margin-bottom: 5px']), Url::toRoute(['/appl-gen/create-appl-all-20'])); ?><br>
                <?= Html::a(Html::img("@web/img/kav2.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/appl-gen-new/create-appl-all'])); ?><br>

                <?php endif; ?>

            </div>

            <?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index' && Yii::$app->user->isGuest) { ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php } else { ?>
                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">
            <?php } ?>
                <?php
                try {
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
                } catch (Exception $e) {
                    echo $e->getMessage();
                } ?>

                <?= Yii::$app->session->getFlash('PersonVisit') ?>

                <?= $content ?>

            </div>

        </div>

<?php
/*

        <table style="float: left; width: 100%;">
            <tr>
                <td style="vertical-align: top;">
                <?= Html::a(Html::img("@web/img/Company3.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/entity-frm'])); ?><br>
                <?= Html::a(Html::img("@web/img/person2.jpg", ["class"=>"img-rounded"]), Url::toRoute(['/person'])); ?><br>
                </td>
                <td style="vertical-align: top;">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>


                <?= $content ?>
                </td>
            </tr>
        </table>
*/
?>

    </div>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::t('app', 'My Company') ?> <?= date('Y') ?></p>
    </div>
</footer>

<img src="/img/ajax-loader.gif" id="loading" style="display:none; position:fixed; top:50%; left:50%" />

<?php
$js = <<<EOL
$( document ).ajaxStart(function() {
  $( "#loading" ).show();
}).ajaxStop(function() {
  $( "#loading" ).hide();
});
EOL;

$this->registerJs($js, \yii\web\View::POS_READY);

?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
