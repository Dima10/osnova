<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModelTPS app\models\TrainingProgSessionSearch */
/* @var $dataProviderTPS yii\data\ActiveDataProvider */

?>
<?php

try {
    echo GridView::widget([
        'id' => 'session',
        'dataProvider' => $dataProviderTPS,
        'filterModel' => $searchModelTPS,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tps_id',
            [
                'attribute' => 'tps_session_id',
                'filter' => false
            ],
            [
                'attribute' => 'trp_name',
                'label' => Yii::t('app', 'Tps Trp ID'),
                'value' => 'tpsTrp.trp_name'
            ],
            [
                'attribute' => 'prs_full_name',
                'label' => Yii::t('app', 'Tps Prs ID'),
                'value' => 'tpsPrs.prs_full_name'
            ],
            [
                'attribute' => 'tps_create_user',
                'label' => Yii::t('app', 'Prs Connect User'),
            ],
            'tps_create_time',
            'tps_create_ip',

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

?>
