<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonVisitSearch */
/* @var $searchModelTPS app\models\TrainingProgSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderTPS yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Person Visits');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-visit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--
    <p>
        <?= Html::a(Yii::t('app', 'Create Person Visit'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    -->

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'dataProviderTPS' => $dataProviderTPS,
        'searchModelTPS' => $searchModelTPS
    ]) ?>

</div>
