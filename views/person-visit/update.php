<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisit */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Person Visit',
]) . $model->pv_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Person Visits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pv_id, 'url' => ['view', 'id' => $model->pv_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="person-visit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
