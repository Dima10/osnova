<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplFinal */

$this->title = Yii::t('app', 'Create Appl Final');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Finals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-final-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
