<?php

use app\models\SvcDocType;
use app\models\TrainingProg;
use app\models\TrainingType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplFinalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $reestr array */

?>
<div class="appl-final-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'applf_id',
            [
                'attribute' => 'applf_reestr',
                'value' => function ($data) use ($reestr) {
                    if ($data->applf_reestr !== null){
                        return $reestr[$data->applf_reestr];
                    } else {
                        return '';
                    }

                },
                'label' => Yii::t('app', 'Applcmd Reestr'),
                'filter' => $reestr,
            ],
            'applf_name_last',
            'applf_name_first',
            'applf_name_middle',
            'applf_name_full',
            [
                'attribute' => 'applfSvdt',
                'label' => Yii::t('app', 'Applf Svdt ID'),
                'value' => 'applfSvdt.svdt_name',
                'filter' => ArrayHelper::map(SvcDocType::find()->asArray()->all(), 'svdt_id','svdt_name'),
            ],
            [
                'attribute' => 'applfTrt',
                'label' => Yii::t('app', 'Applf Trt ID'),
                'value' => 'applfTrt.trt_name',
                'filter' => ArrayHelper::map(TrainingType::find()->asArray()->all(), 'trt_id','trt_name'),
            ],
            [
                'attribute' => 'applfTrp',
                'label' => Yii::t('app', 'Applf Trp ID'),
                'value' => 'applfTrp.trp_name',
            ],


            'applf_trp_hour',
            'applf_cmd_date',
            'applf_end_date',
            'applf_number',

            [
                'attribute' => 'applf_file_name',
                'label' => Yii::t('app', 'Applf File Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file_name, yii\helpers\Url::toRoute(['download', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
            ],
            [
                'attribute' => 'applf_file0_name',
                'label' => Yii::t('app', 'Applf File0 Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->applf_file0_name, yii\helpers\Url::toRoute(['download0', 'id' => $data->applf_id]), ['target' => '_blank']);
                },
                'visible' => !\app\models\User::isSpecAdmin()
            ],
            [
                'attribute' => 'applf_create_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_create_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_update_user',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_update_time',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],
            [
                'attribute' => 'applf_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70 && !\app\models\User::isSpecAdmin(),
            ],

        ],
    ]);

Pjax::end();


?>

</div>
