<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $form yii\widgets\ActiveForm */
/* @var $country array */
/* @var $region array */

$script  = <<< 'EOD'
function getState(val) {
    $.ajax({
    type: "POST",
    url: "index.php?r=city/ajax-region&id="+val,
    data:'id='+val,
    success: function(data){
        $("#region").html(data);
    }
    });
}
EOD;

$this->registerJs($script, \yii\web\View::POS_END);

?>

<div class="city-form">

    <?php
        $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
                ],        
            ]);
    ?>

    <?= $form->field($model, 'city_cou_id')->dropDownList($country, ['id' => 'country', 'onChange' => 'getState(this.value);']) ?>

    <?= $form->field($model, 'city_reg_id')->dropDownList($region, ['id' => 'region']) ?>

    <?= $form->field($model, 'city_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
