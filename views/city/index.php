<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $country array */
/* @var $region array */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create City'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php
echo $this->render('_grid',
        [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'country' => $country,
            'region' => $region,
        ]
    );
?>
</div>
