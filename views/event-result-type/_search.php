<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventResultTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-result-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'evrt_id') ?>

    <?= $form->field($model, 'evrt_name') ?>

    <?= $form->field($model, 'evrt_note') ?>

    <?= $form->field($model, 'evrt_create_user') ?>

    <?= $form->field($model, 'evrt_create_time') ?>

    <?php // echo $form->field($model, 'evrt_create_ip') ?>

    <?php // echo $form->field($model, 'evrt_update_user') ?>

    <?php // echo $form->field($model, 'evrt_update_time') ?>

    <?php // echo $form->field($model, 'evrt_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
