<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserMailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-mail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'um_id') ?>

    <?= $form->field($model, 'um_name') ?>

    <?= $form->field($model, 'um_subj') ?>

    <?= $form->field($model, 'um_text') ?>

    <?= $form->field($model, 'um_password') ?>

    <?php // echo $form->field($model, 'um_sm_id') ?>

    <?php // echo $form->field($model, 'um_to') ?>

    <?php // echo $form->field($model, 'um_create_user') ?>

    <?php // echo $form->field($model, 'um_create_time') ?>

    <?php // echo $form->field($model, 'um_create_ip') ?>

    <?php // echo $form->field($model, 'um_update_user') ?>

    <?php // echo $form->field($model, 'um_update_time') ?>

    <?php // echo $form->field($model, 'um_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
