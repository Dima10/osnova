<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserMailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sysmail array */
/* @var $reestr array */
/* @var $type array */

?>
<div class="user-mail-index-grid">

<?php

Pjax::begin();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'um_id',
            'um_name',
            [
                'attribute' => 'um_umt_id',
                'value' => function ($data) {return $data->umUmt->umt_name ?? '';},
                'filter' => $type,
            ],
            [
                'attribute' => 'um_reestr',
                'value' => function ($data) use ($reestr) {
                    return $reestr[$data->um_reestr];
                },
                'filter' => $reestr,
            ],
            [
                'attribute' => 'um_flag',
                'value' => function ($data)  {
                    return \app\models\Constant::YES_NO[$data->um_flag];
                },
                'filter' => \app\models\Constant::YES_NO,
            ],

            [
                'attribute' => 'um_sm_id',
                'value' => function ($data) {return $data->umSm->sm_name;},
                'filter' => $sysmail,
            ],
            //'um_to',
            [
                'attribute' => 'um_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'um_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'um_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'um_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'um_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'um_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);
} catch (Exception $e) {
    echo $e->getMessage();
}

Pjax::end();


?>

</div>
