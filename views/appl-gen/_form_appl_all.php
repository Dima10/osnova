<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model yii\base\DynamicModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-gen-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'date1')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])->label(Yii::t('app', 'Date Begin'));
    ?>
    <?php echo $form->field($model, 'date2')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ])->label(Yii::t('app', 'Date End'));
    ?>

    <?= $form->field($model, 'obj1')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Appl Command')) ?>
    <?= $form->field($model, 'obj2')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Appl Sheet')) ?>
    <?= $form->field($model, 'obj3')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Appl Sheet X')) ?>
    <?= $form->field($model, 'obj4')->textInput(['ReadOnly' => true])->label(Yii::t('app', 'Appl End')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['name' => 'update', 'value' => 'update', 'class' => 'btn btn-warning', 'onclick' => 'return confirm("'.Yii::t('app', 'Are your sure?').'")']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Delete'), ['name' => 'delete', 'value' => 'delete', 'class' => 'btn btn-danger', 'onclick' => 'return confirm("'.Yii::t('app', 'Are your sure?').'")']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
