<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommandSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-command-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'applcmd_id') ?>

    <?= $form->field($model, 'applcmd_number') ?>

    <?= $form->field($model, 'applcmd_date') ?>

    <?= $form->field($model, 'applcmd_ab_id') ?>

    <?= $form->field($model, 'applcmd_comp_id') ?>

    <?php // echo $form->field($model, 'applcmd_create_user') ?>

    <?php // echo $form->field($model, 'applcmd_create_time') ?>

    <?php // echo $form->field($model, 'applcmd_create_ip') ?>

    <?php // echo $form->field($model, 'applcmd_update_user') ?>

    <?php // echo $form->field($model, 'applcmd_update_time') ?>

    <?php // echo $form->field($model, 'applcmd_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
