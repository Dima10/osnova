<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplEnd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appl-end-form">

    <?php $form = ActiveForm::begin(
            [
                'action' => $model->isNewRecord ? ['appl-end/create'] : ['appl-end/update', 'id' => $model->apple_id],
                'fieldConfig' => [
                    'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); 

        echo $form->errorSummary($model);
    ?>

    <?php echo $form->field($model, 'apple_applsx_id')->textInput(); ?>

    <?php echo $form->field($model, 'apple_number')->textInput(['maxlength' => true]); ?>

    <?php echo $form->field($model, 'apple_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
