<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplCommand */
/* @var $ab array */
/* @var $comp array */

$this->title = Yii::t('app', 'Create Appl Gen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Gens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-gen-create">

    <h1><?= Html::encode((isset($headTextGen20) && !empty($headTextGen20)) ? $headTextGen20 : $this->title) ?></h1>
    <h4 style="color: red"><?= Html::encode((isset($dateIsInvalidForThisGenerationMessage) && !empty($dateIsInvalidForThisGenerationMessage)) ? $dateIsInvalidForThisGenerationMessage : '') ?></h4>

    <?= $this->render('_form_appl_all', [
        'model' => $model,
    ]) ?>

    <?php if(!empty($applmArray)) { ?>
        <br/>
        <br/>
        Коды строк из таблицы итоговых документов которые не сгенерировались:
        <br/>
        <?php
        foreach ($applmArray as $item) {
            echo $item . '<br />';
        }
    }
    ?>

</div>
