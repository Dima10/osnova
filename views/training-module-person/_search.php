<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingModulePersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-module-person-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tmp_id') ?>

    <?= $form->field($model, 'tmp_prs_id') ?>

    <?= $form->field($model, 'tmp_trm_id') ?>

    <?= $form->field($model, 'tmp_create_user') ?>

    <?= $form->field($model, 'tmp_create_time') ?>

    <?php // echo $form->field($model, 'tmp_create_ip') ?>

    <?php // echo $form->field($model, 'tmp_update_user') ?>

    <?php // echo $form->field($model, 'tmp_update_time') ?>

    <?php // echo $form->field($model, 'tmp_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
