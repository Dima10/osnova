<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingModulePerson */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Training Module Person',
]) . $model->tmp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Module People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tmp_id, 'url' => ['view', 'id' => $model->tmp_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="training-module-person-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
