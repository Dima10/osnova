<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrainingModulePerson */

$this->title = $model->tmp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Training Module People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-module-person-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->tmp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->tmp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tmp_id',
            'tmp_prs_id',
            'tmp_trm_id',
            'tmp_create_user',
            'tmp_create_time',
            'tmp_create_ip',
            'tmp_update_user',
            'tmp_update_time',
            'tmp_update_ip',
        ],
    ]) ?>

</div>
