<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pattern */
/* @var $pattern_type array */
/* @var $pattern_svdt array */
/* @var $pattern_trt array */

$this->title = Yii::t('app', 'Create Pattern');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patterns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pattern_type' => $pattern_type,
        'pattern_svdt' => $pattern_svdt,
        'pattern_trt' => $pattern_trt,
    ]) ?>

</div>
