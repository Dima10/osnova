<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PatternSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $pattern_type array */
/* @var $pattern_svdt array */
/* @var $pattern_trt array */

$this->title = Yii::t('app', 'Patterns');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['site/setting']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pattern'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\SerialColumn']['width']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ['width' => Yii::$app->params['yii\grid\ActionColumn']['width']],
            ],

            [
                'attribute' => 'pat_id',
                'options' => ['width' => '100'],
            ],

            'pat_name',

            [   
                'attribute' => 'pat_patt_id',
                'label' => Yii::t('app', 'Pat Patt ID'),
                'value' => function ($data) { return $data->patPatt->patt_name; },
                'filter' => $pattern_type,
            ],
            [
                'attribute' => 'pat_svdt_id',
                'label' => Yii::t('app', 'Pat Svdt ID'),
                'value' => function ($data) { return isset($data->patSvdt) ? $data->patSvdt->svdt_name : ''; },
                'filter' => $pattern_svdt,
            ],
            [
                'attribute' => 'pat_trt_id',
                'label' => Yii::t('app', 'Pat Trt ID'),
                'value' => function ($data) { return isset($data->patTrt) ? $data->patTrt->trt_name : ''; },
                'filter' => $pattern_trt,
            ],

            'pat_code',

            [
                'attribute' => 'pat_fname',
                'label' => Yii::t('app', 'Pat Fname'),
                'format' => 'raw',
                'value' => function ($data) {
                                return Html::a($data->pat_fname, yii\helpers\Url::toRoute(['download', 'id' => $data->pat_id]), ['target' => '_blank']);
                            },
            ],

            'pat_note:ntext',

            [
                'attribute' => 'pat_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'pat_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'pat_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'pat_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'pat_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'pat_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
