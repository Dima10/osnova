<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PatternSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pattern-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pat_id') ?>

    <?= $form->field($model, 'pat_name') ?>

    <?= $form->field($model, 'pat_patt_id') ?>

    <?= $form->field($model, 'pat_fname') ?>

    <?php // echo $form->field($model, 'pat_fdata') ?>

    <?php // echo $form->field($model, 'pat_note') ?>

    <?php // echo $form->field($model, 'pat_create_user') ?>

    <?php // echo $form->field($model, 'pat_create_time') ?>

    <?php // echo $form->field($model, 'pat_create_ip') ?>

    <?php // echo $form->field($model, 'pat_update_user') ?>

    <?php // echo $form->field($model, 'pat_update_time') ?>

    <?php // echo $form->field($model, 'pat_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
