<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SvcValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $svc array */
/* @var $svc_prop array */

$this->title = Yii::t('app', 'Svc Values');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Core'), 'url' => ['site/core']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="svc-value-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Svc Value'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_grid', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
        'svc' => $svc,
        'svc_prop' => $svc_prop,
    ]) ?>

</div>
