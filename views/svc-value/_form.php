<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SvcValue */
/* @var $form yii\widgets\ActiveForm */
/* @var $svc array */
/* @var $svc_prop array */
?>

<div class="svc-value-form">

    <?php $form = ActiveForm::begin(
        [
            'fieldConfig' => [
                'template' => '<div class="row"><div class="col-sm-2">{label}</div><div class="col-sm-6">{input}</div><div class="col-sm-4">{error}</div></div>',
            ],        
        ]
    
    ); ?>

    <?php echo $form->field($model, 'svcv_svc_id')->dropDownList($svc); ?>

    <?php echo $form->field($model, 'svcv_svcp_id')->dropDownList($svc_prop); ?>

    <?php echo $form->field($model, 'svcv_value')->textInput(['maxlength' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
