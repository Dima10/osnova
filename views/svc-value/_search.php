<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SvcValueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="svc-value-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'svcv_id') ?>

    <?= $form->field($model, 'svcv_svc_id') ?>

    <?= $form->field($model, 'svcv_svcp_id') ?>

    <?= $form->field($model, 'svcv_value') ?>

    <?= $form->field($model, 'svcv_create_user') ?>

    <?php // echo $form->field($model, 'svcv_create_time') ?>

    <?php // echo $form->field($model, 'svcv_create_ip') ?>

    <?php // echo $form->field($model, 'svcv_update_user') ?>

    <?php // echo $form->field($model, 'svcv_update_time') ?>

    <?php // echo $form->field($model, 'svcv_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
