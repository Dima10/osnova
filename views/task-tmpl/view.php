<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskTmpl */

$this->title = $model->tasktp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Task Tmpls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-tmpl-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->tasktp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->tasktp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tasktp_id',
            'tasktp_event_id',
            'tasktp_taskt_id',
            'tasktp_date_add',
            'tasktp_note:ntext',
            'tasktp_create_user',
            'tasktp_create_time',
            'tasktp_create_ip',
            'tasktp_update_user',
            'tasktp_update_time',
            'tasktp_update_ip',
        ],
    ]) ?>

</div>
