<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskTmplSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-tmpl-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tasktp_id') ?>

    <?= $form->field($model, 'tasktp_event_id') ?>

    <?= $form->field($model, 'tasktp_taskt_id') ?>

    <?= $form->field($model, 'tasktp_date_add') ?>

    <?= $form->field($model, 'tasktp_note') ?>

    <?php // echo $form->field($model, 'tasktp_create_user') ?>

    <?php // echo $form->field($model, 'tasktp_create_time') ?>

    <?php // echo $form->field($model, 'tasktp_create_ip') ?>

    <?php // echo $form->field($model, 'tasktp_update_user') ?>

    <?php // echo $form->field($model, 'tasktp_update_time') ?>

    <?php // echo $form->field($model, 'tasktp_update_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
