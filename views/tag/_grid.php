<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="tag-index-grid">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'tag_id',
            'tag_name',
            'tag_comment',
            'tag_create_user',
            'tag_create_time',
            'tag_create_ip',
            'tag_update_user',
            'tag_update_time',
            'tag_update_ip',

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
