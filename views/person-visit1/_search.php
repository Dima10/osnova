<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonVisit1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-visit1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pv1_id') ?>

    <?= $form->field($model, 'pv1_pv_id') ?>

    <?= $form->field($model, 'pv1_pve_id') ?>

    <?= $form->field($model, 'pv1_create_user') ?>

    <?= $form->field($model, 'pv1_create_time') ?>

    <?php // echo $form->field($model, 'pv1_create_ip') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
