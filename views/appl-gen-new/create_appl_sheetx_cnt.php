<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetXContent */

$this->title = Yii::t('app', 'Create Appl Sheet Xcontent');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Xcontents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-xcontent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_appl_sheetx_cnt', [
        'model' => $model,
    ]) ?>

</div>
