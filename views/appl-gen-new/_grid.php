<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="appl-main-index-grid">

<?php

Pjax::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {sheet} {sheetX} {command}',
                'buttons' => [
                    'delete' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    'update' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pensil"></span>', $url,
                                [
                                    'title' => Yii::t('yii', 'Update'),
                                ]);
                        },
                    'sheet' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-hand-right"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sheet'),
                                ]);
                        },
                    'sheetX' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-check"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Sheet X'),
                                ]);
                        },
                    'command' =>
                        function($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Create Appl Command'),
                                ]);
                        },
                ],
                'urlCreator' =>
                    function ($action, $model, $key, $index) {
                        if ($action === 'delete') {
                            $url = yii\helpers\Url::to(['delete', 'applm_id' => $model->applm_id]);
                            return $url;
                        }
                        if ($action === 'update') {
                            $url = yii\helpers\Url::to(['update', 'applm_id' => $model->applm_id]);
                            return $url;
                        }
                        if ($action === 'sheet') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-sheet-id', 'id' => $model->applm_prs_id, 'date' => $model->applm_appls_date]);
                            return $url;
                        }
                        if ($action === 'sheetX') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-sheet-x', 'date' => $model->applm_applsx_date]);
                            return $url;
                        }
                        if ($action === 'command') {
                            $url = yii\helpers\Url::to(['appl-gen/create-appl-command', 'date' => $model->applm_applcmd_date]);
                            return $url;
                        }
                        return '#';
                    },
            ],

            'applm_id',
            [
                'attribute' => 'applmPrs.prs_full_name',
                'label' => Yii::t('app', 'Applm Prs ID'),

            ],
            'applm_position',
            [
                'attribute' => 'applmSvc.svc_name',
                'label' => Yii::t('app', 'Applm Svc ID'),

            ],
            [
                'attribute' => 'applmTrp.trp_name',
                'label' => Yii::t('app', 'Applm Trp ID'),

            ],
            [
                'attribute' => 'applmAb.ab_name',
                'label' => Yii::t('app', 'Applm Ab ID'),

            ],

            [
                'attribute' => 'applmComp.comp_name',
                'label' => Yii::t('app', 'Applm Comp ID'),

            ],

            [
                'attribute' => 'applmTrt.trt_name',
                'label' => Yii::t('app', 'Applm Trt ID'),

            ],

            [
                'attribute' => 'applmSvdt.svdt_name',
                'label' => Yii::t('app', 'Applm Svdt ID'),

            ],

            'applm_date_upk',
            'applm_apple_date',
            'applm_number_upk',
            'applm_applsx_date',
            'applm_appls_date',
            'applm_applcmd_date',
            'applm_applr_date',

            [
                'attribute' => 'applm_create_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applm_create_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applm_create_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applm_update_user',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applm_update_time',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],
            [
                'attribute' => 'applm_update_ip',
                'visible' => Yii::$app->user->identity->level >= 70,
            ],

        ],
    ]);

Pjax::end();


?>

</div>
