<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplSheetX */

$this->title = Yii::t('app', 'Create Appl Sheet X');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Sheet Xes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-sheet-x-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_appl_sheetx', [
        'model' => $model,
    ]) ?>

</div>
