<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplCommandContent */
/* @var $person array */
/* @var $program array */

$this->title = Yii::t('app', 'Create Appl Command Content');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Appl Command Contents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appl-command-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_appl_command_cnt', [
        'model' => $model,
        'person'  => $person,
        'program' => $program,
    ]) ?>

</div>
