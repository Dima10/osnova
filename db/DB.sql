-- MySQL dump 10.13  Distrib 5.5.40, for Win64 (x86)
--
-- Host: emoye.mysql.ukraine.com.ua    Database: emoye_osnova
-- ------------------------------------------------------
-- Server version	5.6.27-75.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `emoye_osnova`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `emoye_osnova` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `emoye_osnova`;

--
-- Table structure for table `ab`
--

DROP TABLE IF EXISTS `ab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ab` (
  `ab_id` int(11) NOT NULL AUTO_INCREMENT,
  `ab_name` varchar(128) NOT NULL,
  `ab_type` int(11) NOT NULL,
  `ab_create_user` varchar(64) DEFAULT NULL,
  `ab_create_time` datetime DEFAULT NULL,
  `ab_create_ip` varchar(64) DEFAULT NULL,
  `ab_update_user` varchar(64) DEFAULT NULL,
  `ab_update_time` datetime DEFAULT NULL,
  `ab_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3947 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ac_func`
--

DROP TABLE IF EXISTS `ac_func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_func` (
  `acf_id` int(11) NOT NULL AUTO_INCREMENT,
  `acf_name` varchar(64) NOT NULL,
  `acf_controller` varchar(128) NOT NULL,
  `acf_action` varchar(128) NOT NULL,
  `acf_create_user` varchar(32) DEFAULT NULL,
  `acf_create_time` datetime DEFAULT NULL,
  `acf_update_user` varchar(32) DEFAULT NULL,
  `acf_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`acf_id`),
  UNIQUE KEY `acf_controller_acf_action` (`acf_controller`,`acf_action`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ac_role`
--

DROP TABLE IF EXISTS `ac_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_role` (
  `acr_id` int(11) NOT NULL AUTO_INCREMENT,
  `acr_name` varchar(64) NOT NULL,
  `acr_desc` varchar(256) NOT NULL,
  `acr_create_user` varchar(32) DEFAULT NULL,
  `acr_create_time` datetime DEFAULT NULL,
  `acr_update_user` varchar(32) DEFAULT NULL,
  `acr_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`acr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ac_role_func`
--

DROP TABLE IF EXISTS `ac_role_func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_role_func` (
  `acrf_id` int(11) NOT NULL AUTO_INCREMENT,
  `acrf_acr_id` int(11) NOT NULL,
  `acrf_acf_id` int(11) NOT NULL,
  `acrf_create_user` varchar(32) DEFAULT NULL,
  `acrf_create_time` datetime DEFAULT NULL,
  `acrf_update_user` varchar(32) DEFAULT NULL,
  `acrf_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`acrf_id`),
  KEY `acrf_acf_id` (`acrf_acf_id`),
  KEY `acrf_acr_id` (`acrf_acr_id`),
  CONSTRAINT `ac_role_func_ibfk_1` FOREIGN KEY (`acrf_acf_id`) REFERENCES `ac_func` (`acf_id`) ON UPDATE CASCADE,
  CONSTRAINT `ac_role_func_ibfk_2` FOREIGN KEY (`acrf_acr_id`) REFERENCES `ac_role` (`acr_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ac_user_role`
--

DROP TABLE IF EXISTS `ac_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_user_role` (
  `acur_id` int(11) NOT NULL AUTO_INCREMENT,
  `acur_user_id` int(11) NOT NULL,
  `acur_acr_id` int(11) NOT NULL,
  `acur_create_user` varchar(32) DEFAULT NULL,
  `acur_create_time` datetime DEFAULT NULL,
  `acur_update_user` varchar(32) DEFAULT NULL,
  `acur_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`acur_id`),
  KEY `acur_acr_id` (`acur_acr_id`),
  KEY `acur_user_id` (`acur_user_id`),
  CONSTRAINT `ac_user_role_ibfk_1` FOREIGN KEY (`acur_acr_id`) REFERENCES `ac_role` (`acr_id`) ON UPDATE CASCADE,
  CONSTRAINT `ac_user_role_ibfk_2` FOREIGN KEY (`acur_user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `acc_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_ab_id` int(11) NOT NULL,
  `acc_bank_id` int(11) NOT NULL,
  `acc_number` varchar(32) NOT NULL,
  `acc_create_user` varchar(64) DEFAULT NULL,
  `acc_create_time` datetime DEFAULT NULL,
  `acc_create_ip` varchar(64) DEFAULT NULL,
  `acc_update_user` varchar(64) DEFAULT NULL,
  `acc_update_time` datetime DEFAULT NULL,
  `acc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`acc_id`),
  KEY `acc_ab_id` (`acc_ab_id`),
  KEY `acc_bank_id` (`acc_bank_id`),
  KEY `acc_number` (`acc_number`),
  CONSTRAINT `account_ibfk_1` FOREIGN KEY (`acc_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `account_ibfk_2` FOREIGN KEY (`acc_bank_id`) REFERENCES `bank` (`bank_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=725 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_ab_id` int(11) NOT NULL,
  `add_addt_id` int(11) NOT NULL,
  `add_cou_id` int(11) DEFAULT NULL,
  `add_reg_id` int(11) DEFAULT NULL,
  `add_city_id` int(11) DEFAULT NULL,
  `add_index` varchar(16) DEFAULT NULL,
  `add_data` varchar(256) NOT NULL,
  `add_create_user` varchar(64) DEFAULT NULL,
  `add_create_time` datetime DEFAULT NULL,
  `add_create_ip` varchar(64) DEFAULT NULL,
  `add_update_user` varchar(64) DEFAULT NULL,
  `add_update_time` datetime DEFAULT NULL,
  `add_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`add_id`),
  KEY `add_ab_id` (`add_ab_id`),
  KEY `add_city_id` (`add_city_id`),
  KEY `add_addt_id` (`add_addt_id`),
  KEY `add_reg_id` (`add_reg_id`),
  KEY `add_cou_id` (`add_cou_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`add_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `address_ibfk_2` FOREIGN KEY (`add_city_id`) REFERENCES `city` (`city_id`) ON UPDATE CASCADE,
  CONSTRAINT `address_ibfk_3` FOREIGN KEY (`add_addt_id`) REFERENCES `address_type` (`addt_id`) ON UPDATE CASCADE,
  CONSTRAINT `address_ibfk_4` FOREIGN KEY (`add_reg_id`) REFERENCES `region` (`reg_id`) ON UPDATE CASCADE,
  CONSTRAINT `address_ibfk_5` FOREIGN KEY (`add_cou_id`) REFERENCES `country` (`cou_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=739 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `address_type`
--

DROP TABLE IF EXISTS `address_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_type` (
  `addt_id` int(11) NOT NULL AUTO_INCREMENT,
  `addt_name` varchar(64) NOT NULL,
  `addt_create_user` varchar(64) DEFAULT NULL,
  `addt_create_time` datetime DEFAULT NULL,
  `addt_create_ip` varchar(64) DEFAULT NULL,
  `addt_update_user` varchar(64) DEFAULT NULL,
  `addt_update_time` datetime DEFAULT NULL,
  `addt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`addt_id`),
  UNIQUE KEY `addt_name` (`addt_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement`
--

DROP TABLE IF EXISTS `agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement` (
  `agr_id` int(11) NOT NULL AUTO_INCREMENT,
  `agr_number` varchar(64) NOT NULL,
  `agr_date` date NOT NULL,
  `agr_ab_id` int(11) NOT NULL,
  `agr_comp_id` int(11) NOT NULL,
  `agr_acc_id` int(11) DEFAULT NULL,
  `agr_pat_id` int(11) DEFAULT NULL,
  `agr_prs_id` int(11) DEFAULT NULL,
  `agr_sum` decimal(15,4) DEFAULT NULL,
  `agr_tax` decimal(15,4) DEFAULT NULL,
  `agr_ast_id` int(11) DEFAULT NULL,
  `agr_comment` varchar(1024) DEFAULT NULL,
  `agr_fdata` varchar(256) DEFAULT NULL,
  `agr_data` longblob,
  `agr_fdata_sign` varchar(256) DEFAULT NULL,
  `agr_data_sign` longblob,
  `agr_create_time` datetime DEFAULT NULL,
  `agr_create_ip` varchar(64) DEFAULT NULL,
  `agr_update_user` varchar(64) DEFAULT NULL,
  `agr_update_time` datetime DEFAULT NULL,
  `agr_update_ip` varchar(64) DEFAULT NULL,
  `agr_create_user` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`agr_id`),
  UNIQUE KEY `agr_number_agr_comp_id` (`agr_number`,`agr_comp_id`),
  KEY `agr_ab_id` (`agr_ab_id`),
  KEY `agr_comp_id` (`agr_comp_id`),
  KEY `agr_acc_id` (`agr_acc_id`),
  KEY `agr_prs_id` (`agr_prs_id`),
  KEY `arg_ast_id` (`agr_ast_id`),
  CONSTRAINT `agreement_ibfk_1` FOREIGN KEY (`agr_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_ibfk_2` FOREIGN KEY (`agr_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_ibfk_3` FOREIGN KEY (`agr_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_ibfk_4` FOREIGN KEY (`agr_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_ibfk_5` FOREIGN KEY (`agr_ast_id`) REFERENCES `agreement_status` (`ast_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1221 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_acc`
--

DROP TABLE IF EXISTS `agreement_acc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_acc` (
  `aga_id` int(11) NOT NULL AUTO_INCREMENT,
  `aga_number` varchar(32) NOT NULL,
  `aga_date` date NOT NULL,
  `aga_ab_id` int(11) NOT NULL,
  `aga_comp_id` int(11) NOT NULL,
  `aga_agr_id` int(11) DEFAULT NULL,
  `aga_agra_id` int(11) DEFAULT NULL,
  `aga_sum` decimal(15,4) DEFAULT NULL,
  `aga_tax` decimal(15,4) DEFAULT NULL,
  `aga_pat_id` int(11) DEFAULT NULL,
  `aga_ast_id` int(11) DEFAULT NULL,
  `aga_comment` varchar(1024) DEFAULT NULL,
  `aga_data` longblob,
  `aga_fdata` varchar(256) DEFAULT NULL,
  `aga_data_sign` longblob,
  `aga_fdata_sign` varchar(256) DEFAULT NULL,
  `aga_create_user` varchar(64) DEFAULT NULL,
  `aga_create_time` datetime DEFAULT NULL,
  `aga_create_ip` varchar(64) DEFAULT NULL,
  `aga_update_user` varchar(64) DEFAULT NULL,
  `aga_update_time` datetime DEFAULT NULL,
  `aga_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`aga_id`),
  KEY `aga_pat_id` (`aga_pat_id`),
  KEY `aga_ast_id` (`aga_ast_id`),
  KEY `aga_ab_id` (`aga_ab_id`),
  KEY `aga_comp_id` (`aga_comp_id`),
  KEY `aga_arg_id` (`aga_agr_id`),
  KEY `aga_agra_id` (`aga_agra_id`),
  CONSTRAINT `agreement_acc_ibfk_2` FOREIGN KEY (`aga_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_ibfk_3` FOREIGN KEY (`aga_agr_id`) REFERENCES `agreement` (`agr_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_ibfk_4` FOREIGN KEY (`aga_agra_id`) REFERENCES `agreement_annex` (`agra_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_ibfk_5` FOREIGN KEY (`aga_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_ibfk_6` FOREIGN KEY (`aga_ast_id`) REFERENCES `agreement_status` (`ast_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_ibfk_7` FOREIGN KEY (`aga_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1818 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_acc_a`
--

DROP TABLE IF EXISTS `agreement_acc_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_acc_a` (
  `aca_id` int(11) NOT NULL AUTO_INCREMENT,
  `aca_aga_id` int(11) NOT NULL,
  `aca_ana_id` int(11) NOT NULL,
  `aca_qty` int(11) DEFAULT NULL,
  `aca_price` decimal(18,4) DEFAULT NULL,
  `aca_tax` decimal(18,4) DEFAULT NULL,
  `aca_create_user` varchar(64) DEFAULT NULL,
  `aca_create_time` datetime DEFAULT NULL,
  `aca_create_ip` varchar(64) DEFAULT NULL,
  `aca_update_user` varchar(64) DEFAULT NULL,
  `aca_update_time` datetime DEFAULT NULL,
  `aca_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`aca_id`),
  KEY `aca_ana_id` (`aca_ana_id`),
  KEY `aca_aga_id` (`aca_aga_id`),
  CONSTRAINT `agreement_acc_a_ibfk_1` FOREIGN KEY (`aca_ana_id`) REFERENCES `agreement_annex_a` (`ana_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_acc_a_ibfk_2` FOREIGN KEY (`aca_aga_id`) REFERENCES `agreement_acc` (`aga_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3015 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_act`
--

DROP TABLE IF EXISTS `agreement_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_act` (
  `act_id` int(11) NOT NULL AUTO_INCREMENT,
  `act_number` varchar(64) NOT NULL,
  `act_date` date NOT NULL,
  `act_ab_id` int(11) NOT NULL,
  `act_comp_id` int(11) NOT NULL,
  `act_agr_id` int(11) DEFAULT NULL,
  `act_agra_id` int(11) DEFAULT NULL,
  `act_acc_id` int(11) DEFAULT NULL,
  `act_sum` decimal(15,4) DEFAULT NULL,
  `act_tax` decimal(15,4) DEFAULT NULL,
  `act_ast_id` int(11) DEFAULT NULL,
  `act_comment` varchar(1024) DEFAULT NULL,
  `act_fdata` varchar(256) DEFAULT NULL,
  `act_data` longblob,
  `act_fdata_sign` varchar(256) DEFAULT NULL,
  `act_data_sign` longblob,
  `act_pat_id` int(11) DEFAULT NULL,
  `act_create_user` varchar(64) DEFAULT NULL,
  `act_create_time` datetime DEFAULT NULL,
  `act_create_ip` varchar(64) DEFAULT NULL,
  `act_update_user` varchar(64) DEFAULT NULL,
  `act_update_time` datetime DEFAULT NULL,
  `act_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`act_id`),
  UNIQUE KEY `act_number_act_comp_id` (`act_number`,`act_comp_id`),
  KEY `act_agr_id` (`act_agr_id`),
  KEY `act_agra_id` (`act_agra_id`),
  KEY `act_ab_id` (`act_ab_id`),
  KEY `act_pat_id` (`act_pat_id`),
  KEY `act_acc_id` (`act_acc_id`),
  KEY `act_comp_id` (`act_comp_id`),
  KEY `act_ast_id` (`act_ast_id`),
  CONSTRAINT `agreement_act_ibfk_1` FOREIGN KEY (`act_agr_id`) REFERENCES `agreement` (`agr_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_2` FOREIGN KEY (`act_agra_id`) REFERENCES `agreement_annex` (`agra_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_4` FOREIGN KEY (`act_ab_id`) REFERENCES `entity` (`ent_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_5` FOREIGN KEY (`act_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_6` FOREIGN KEY (`act_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_7` FOREIGN KEY (`act_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_ibfk_8` FOREIGN KEY (`act_ast_id`) REFERENCES `agreement_status` (`ast_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=944 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_act_a`
--

DROP TABLE IF EXISTS `agreement_act_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_act_a` (
  `acta_id` int(11) NOT NULL AUTO_INCREMENT,
  `acta_act_id` int(11) NOT NULL,
  `acta_ana_id` int(11) NOT NULL,
  `acta_qty` int(11) DEFAULT NULL,
  `acta_price` decimal(18,4) DEFAULT NULL,
  `acta_tax` decimal(18,4) DEFAULT NULL,
  `acta_create_user` varchar(64) DEFAULT NULL,
  `acta_create_time` datetime DEFAULT NULL,
  `acta_create_ip` varchar(64) DEFAULT NULL,
  `acta_update_user` varchar(64) DEFAULT NULL,
  `acta_update_time` datetime DEFAULT NULL,
  `acta_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`acta_id`),
  KEY `acta_act_id` (`acta_act_id`),
  KEY `acat_ana_id` (`acta_ana_id`),
  CONSTRAINT `agreement_act_a_ibfk_1` FOREIGN KEY (`acta_act_id`) REFERENCES `agreement_act` (`act_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_act_a_ibfk_2` FOREIGN KEY (`acta_ana_id`) REFERENCES `agreement_annex_a` (`ana_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2218 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_annex`
--

DROP TABLE IF EXISTS `agreement_annex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_annex` (
  `agra_id` int(11) NOT NULL AUTO_INCREMENT,
  `agra_ab_id` int(11) NOT NULL,
  `agra_comp_id` int(11) NOT NULL,
  `agra_number` varchar(64) NOT NULL,
  `agra_date` date NOT NULL,
  `agra_agr_id` int(11) NOT NULL,
  `agra_acc_id` int(11) DEFAULT NULL,
  `agra_prs_id` int(11) DEFAULT NULL,
  `agra_sum` decimal(15,4) DEFAULT NULL,
  `agra_tax` decimal(15,4) DEFAULT NULL,
  `agra_ast_id` int(11) DEFAULT NULL,
  `agra_comment` varchar(1024) DEFAULT NULL,
  `agra_fdata` varchar(256) DEFAULT NULL,
  `agra_data` longblob,
  `agra_fdata_sign` varchar(256) DEFAULT NULL,
  `agra_data_sign` longblob,
  `agra_pat_id` int(11) DEFAULT NULL,
  `agra_create_user` varchar(64) DEFAULT NULL,
  `agra_create_time` datetime DEFAULT NULL,
  `agra_create_ip` varchar(64) DEFAULT NULL,
  `agra_update_user` varchar(64) DEFAULT NULL,
  `agra_update_time` datetime DEFAULT NULL,
  `agra_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`agra_id`),
  KEY `arga_agr_id` (`agra_agr_id`),
  KEY `agra_ent_id` (`agra_ab_id`),
  KEY `agra_pat_id` (`agra_pat_id`),
  KEY `agra_acc_id` (`agra_acc_id`),
  KEY `agra_comp_id` (`agra_comp_id`),
  KEY `agra_prs_id` (`agra_prs_id`),
  KEY `agra_ast_id` (`agra_ast_id`),
  CONSTRAINT `agreement_annex_ibfk_1` FOREIGN KEY (`agra_agr_id`) REFERENCES `agreement` (`agr_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_2` FOREIGN KEY (`agra_ab_id`) REFERENCES `entity` (`ent_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_3` FOREIGN KEY (`agra_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_4` FOREIGN KEY (`agra_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_5` FOREIGN KEY (`agra_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_6` FOREIGN KEY (`agra_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_ibfk_7` FOREIGN KEY (`agra_ast_id`) REFERENCES `agreement_status` (`ast_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2039 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_annex_a`
--

DROP TABLE IF EXISTS `agreement_annex_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_annex_a` (
  `ana_id` int(11) NOT NULL AUTO_INCREMENT,
  `ana_agra_id` int(11) NOT NULL,
  `ana_svc_id` int(11) NOT NULL,
  `ana_trp_id` int(11) DEFAULT NULL,
  `ana_cost_a` decimal(18,4) DEFAULT NULL,
  `ana_price_a` decimal(18,4) DEFAULT NULL,
  `ana_qty` int(11) DEFAULT NULL,
  `ana_price` decimal(18,4) DEFAULT NULL,
  `ana_tax` decimal(18,4) DEFAULT NULL,
  `ana_create_user` varchar(64) DEFAULT NULL,
  `ana_create_time` datetime DEFAULT NULL,
  `ana_create_ip` varchar(64) DEFAULT NULL,
  `ana_update_user` varchar(64) DEFAULT NULL,
  `ana_update_time` datetime DEFAULT NULL,
  `ana_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ana_id`),
  KEY `ana_agra_id` (`ana_agra_id`),
  KEY `ana_svc_id` (`ana_svc_id`),
  KEY `ana_trp_id` (`ana_trp_id`),
  CONSTRAINT `agreement_annex_a_ibfk_1` FOREIGN KEY (`ana_agra_id`) REFERENCES `agreement_annex` (`agra_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_a_ibfk_2` FOREIGN KEY (`ana_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `agreement_annex_a_ibfk_3` FOREIGN KEY (`ana_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3266 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agreement_status`
--

DROP TABLE IF EXISTS `agreement_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement_status` (
  `ast_id` int(11) NOT NULL AUTO_INCREMENT,
  `ast_name` varchar(64) NOT NULL,
  `ast_sys` varchar(64) DEFAULT NULL,
  `ast_create_time` datetime DEFAULT NULL,
  `ast_create_ip` varchar(64) DEFAULT NULL,
  `ast_update_user` varchar(64) DEFAULT NULL,
  `ast_update_time` datetime DEFAULT NULL,
  `ast_update_ip` varchar(64) DEFAULT NULL,
  `ast_create_user` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ast_id`),
  UNIQUE KEY `ast_name` (`ast_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_command`
--

DROP TABLE IF EXISTS `appl_command`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_command` (
  `applcmd_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Приказ о зачислении',
  `applcmd_number` varchar(64) NOT NULL,
  `applcmd_date` date NOT NULL,
  `applcmd_ab_id` int(11) DEFAULT NULL,
  `applcmd_comp_id` int(11) DEFAULT NULL,
  `applcmd_trt_id` int(11) DEFAULT NULL,
  `applcmd_svdt_id` int(11) DEFAULT NULL,
  `applcmd_pat_id` int(11) DEFAULT NULL,
  `applcmd_file_name` varchar(256) DEFAULT NULL,
  `applcmd_file_data` longblob,
  `applcmd_create_user` varchar(64) DEFAULT NULL,
  `applcmd_create_time` datetime DEFAULT NULL,
  `applcmd_create_ip` varchar(64) DEFAULT NULL,
  `applcmd_update_user` varchar(64) DEFAULT NULL,
  `applcmd_update_time` datetime DEFAULT NULL,
  `applcmd_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applcmd_id`),
  KEY `applcmd_ab_id` (`applcmd_ab_id`),
  KEY `applcmd_comp_id` (`applcmd_comp_id`),
  KEY `applcmd_trt_id` (`applcmd_trt_id`),
  KEY `applcmd_pat_id` (`applcmd_pat_id`),
  KEY `applcmd_svdt_id` (`applcmd_svdt_id`),
  CONSTRAINT `appl_command_ibfk_1` FOREIGN KEY (`applcmd_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_ibfk_2` FOREIGN KEY (`applcmd_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_ibfk_3` FOREIGN KEY (`applcmd_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_ibfk_4` FOREIGN KEY (`applcmd_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_ibfk_5` FOREIGN KEY (`applcmd_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_command_content`
--

DROP TABLE IF EXISTS `appl_command_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_command_content` (
  `applcmdc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applcmdc_applcmd_id` int(11) NOT NULL,
  `applcmdc_prs_id` int(11) NOT NULL,
  `applcmdc_trp_id` int(11) NOT NULL,
  `applcmdc_create_user` varchar(64) DEFAULT NULL,
  `applcmdc_create_time` datetime DEFAULT NULL,
  `applcmdc_create_ip` varchar(64) DEFAULT NULL,
  `applcmdc_update_user` varchar(64) DEFAULT NULL,
  `applcmdc_update_time` datetime DEFAULT NULL,
  `applcmdc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applcmdc_id`),
  KEY `applcmdc_prs_id` (`applcmdc_prs_id`),
  KEY `applcmdc_trp_id` (`applcmdc_trp_id`),
  KEY `applcmdc_applcmd_id` (`applcmdc_applcmd_id`),
  CONSTRAINT `appl_command_content_ibfk_2` FOREIGN KEY (`applcmdc_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_content_ibfk_3` FOREIGN KEY (`applcmdc_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_command_content_ibfk_4` FOREIGN KEY (`applcmdc_applcmd_id`) REFERENCES `appl_command` (`applcmd_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=678 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_end`
--

DROP TABLE IF EXISTS `appl_end`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_end` (
  `apple_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Приказ о окончании',
  `apple_applsx_id` int(11) DEFAULT NULL,
  `apple_number` varchar(64) NOT NULL,
  `apple_date` date NOT NULL,
  `apple_pat_id` int(11) DEFAULT NULL,
  `apple_trt_id` int(11) DEFAULT NULL,
  `apple_svdt_id` int(11) DEFAULT NULL,
  `apple_file_name` varchar(256) DEFAULT NULL,
  `apple_file_data` longblob,
  `apple_create_user` varchar(64) DEFAULT NULL,
  `apple_create_time` datetime DEFAULT NULL,
  `apple_create_ip` varchar(64) DEFAULT NULL,
  `apple_update_user` varchar(64) DEFAULT NULL,
  `apple_update_time` datetime DEFAULT NULL,
  `apple_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`apple_id`),
  UNIQUE KEY `apple_number` (`apple_number`),
  KEY `apple_applsx_id` (`apple_applsx_id`),
  KEY `apple_pat_id` (`apple_pat_id`),
  KEY `apple_trt_id` (`apple_trt_id`),
  KEY `apple_svdt_id` (`apple_svdt_id`),
  CONSTRAINT `appl_end_ibfk_1` FOREIGN KEY (`apple_applsx_id`) REFERENCES `appl_sheet_x` (`applsx_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_end_ibfk_2` FOREIGN KEY (`apple_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_end_ibfk_3` FOREIGN KEY (`apple_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_end_ibfk_4` FOREIGN KEY (`apple_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_end_content`
--

DROP TABLE IF EXISTS `appl_end_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_end_content` (
  `applec_id` int(11) NOT NULL AUTO_INCREMENT,
  `applec_apple_id` int(11) DEFAULT NULL,
  `applec_prs_id` int(11) NOT NULL,
  `applec_trp_id` int(11) NOT NULL,
  `applec_number` varchar(64) DEFAULT NULL,
  `applec_create_user` varchar(64) DEFAULT NULL,
  `applec_create_time` datetime DEFAULT NULL,
  `applec_create_ip` varchar(64) DEFAULT NULL,
  `applec_update_user` varchar(64) DEFAULT NULL,
  `applec_update_time` datetime DEFAULT NULL,
  `applec_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applec_id`),
  KEY `applec_apple_id` (`applec_apple_id`),
  KEY `applec_trp_id` (`applec_trp_id`),
  KEY `applec_prs_id` (`applec_prs_id`),
  CONSTRAINT `appl_end_content_ibfk_1` FOREIGN KEY (`applec_apple_id`) REFERENCES `appl_end` (`apple_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_end_content_ibfk_2` FOREIGN KEY (`applec_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_end_content_ibfk_3` FOREIGN KEY (`applec_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1290 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_main`
--

DROP TABLE IF EXISTS `appl_main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_main` (
  `applm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Учет обучения',
  `applm_applrc_id` int(11) DEFAULT NULL COMMENT 'Заявка',
  `applm_prs_id` int(11) NOT NULL COMMENT 'Физ. лицо',
  `applm_position` varchar(64) DEFAULT NULL COMMENT 'Должность',
  `applm_svc_id` int(11) DEFAULT NULL COMMENT 'Услуга',
  `applm_trp_id` int(11) DEFAULT NULL COMMENT 'Программа',
  `applm_ab_id` int(11) DEFAULT NULL COMMENT 'Юр. лицо',
  `applm_comp_id` int(11) DEFAULT NULL COMMENT 'Наша компания',
  `applm_trt_id` int(11) DEFAULT NULL COMMENT 'Вид обучения',
  `applm_svdt_id` int(11) DEFAULT NULL COMMENT 'Тип выдаваемого документа',
  `applm_date_upk` date DEFAULT NULL COMMENT 'Желаемая дата выдачи документа (УПК)',
  `applm_date_end` date DEFAULT NULL COMMENT 'Приказ об окончании обучения',
  `applm_number_upk` varchar(64) DEFAULT NULL COMMENT '№ документа (УПК)',
  `applm_appls_date` date DEFAULT NULL COMMENT 'Протокол сдачи тестов',
  `applm_applsx_date` date DEFAULT NULL COMMENT 'Ведомость сдачи тестов',
  `applm_appls0_date` date DEFAULT NULL COMMENT 'Протокол промежуточного тестирования',
  `applm_appls0x_date` date DEFAULT NULL COMMENT 'Ведомость промежуточного тестирования',
  `applm_applc_date` date DEFAULT NULL COMMENT 'Приказ о зачислении',
  `applm_applr_date` date DEFAULT NULL COMMENT 'Дата Заявления',
  `applm_create_user` varchar(64) DEFAULT NULL,
  `applm_create_time` datetime DEFAULT NULL,
  `applm_create_ip` varchar(64) DEFAULT NULL,
  `applm_update_user` varchar(64) DEFAULT NULL,
  `applm_update_time` datetime DEFAULT NULL,
  `applm_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applm_id`),
  KEY `applm_svc_id` (`applm_svc_id`),
  KEY `applm_prs_id` (`applm_prs_id`),
  KEY `applm_ab_id` (`applm_ab_id`),
  KEY `applm_comp_id` (`applm_comp_id`),
  KEY `applm_trt_id` (`applm_trt_id`),
  KEY `applm_applrc_id` (`applm_applrc_id`),
  KEY `applm_svdt_id` (`applm_svdt_id`),
  CONSTRAINT `appl_main_ibfk_1` FOREIGN KEY (`applm_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_2` FOREIGN KEY (`applm_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_3` FOREIGN KEY (`applm_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_4` FOREIGN KEY (`applm_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_5` FOREIGN KEY (`applm_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_6` FOREIGN KEY (`applm_applrc_id`) REFERENCES `appl_request_content` (`applrc_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_main_ibfk_7` FOREIGN KEY (`applm_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7869 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_request`
--

DROP TABLE IF EXISTS `appl_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_request` (
  `applr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Заявка на обучение',
  `applr_number` varchar(64) NOT NULL,
  `applr_date` date NOT NULL,
  `applr_ab_id` int(11) NOT NULL,
  `applr_comp_id` int(11) NOT NULL,
  `applr_agra_id` int(11) DEFAULT NULL,
  `applr_appls_date` date DEFAULT NULL,
  `applr_apple_date` date DEFAULT NULL,
  `applr_applsx_date` date DEFAULT NULL,
  `applr_applc_date` date DEFAULT NULL,
  `applr_trt_id` int(11) DEFAULT NULL,
  `applr_ast_id` int(11) DEFAULT NULL,
  `applr_create_user` varchar(64) DEFAULT NULL,
  `applr_create_time` datetime DEFAULT NULL,
  `applr_create_ip` varchar(64) DEFAULT NULL,
  `applr_update_user` varchar(64) DEFAULT NULL,
  `applr_update_time` datetime DEFAULT NULL,
  `applr_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applr_id`),
  UNIQUE KEY `applr_number` (`applr_number`),
  KEY `applr_ab_id` (`applr_ab_id`),
  KEY `applr_comp_id` (`applr_comp_id`),
  KEY `applr_agra_id` (`applr_agra_id`),
  KEY `applr_trt_id` (`applr_trt_id`),
  KEY `applr_ast_id` (`applr_ast_id`),
  CONSTRAINT `appl_request_ibfk_1` FOREIGN KEY (`applr_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_ibfk_2` FOREIGN KEY (`applr_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_ibfk_3` FOREIGN KEY (`applr_agra_id`) REFERENCES `agreement_annex` (`agra_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_ibfk_4` FOREIGN KEY (`applr_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_ibfk_5` FOREIGN KEY (`applr_ast_id`) REFERENCES `agreement_status` (`ast_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_request_content`
--

DROP TABLE IF EXISTS `appl_request_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_request_content` (
  `applrc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applrc_applr_id` int(11) NOT NULL,
  `applrc_prs_id` int(11) NOT NULL,
  `applrc_svc_id` int(11) NOT NULL,
  `applrc_trp_id` int(11) NOT NULL,
  `applrc_date_upk` date DEFAULT NULL,
  `applrc_create_user` varchar(64) DEFAULT NULL,
  `applrc_create_time` datetime DEFAULT NULL,
  `applrc_create_ip` varchar(64) DEFAULT NULL,
  `applrc_update_user` varchar(64) DEFAULT NULL,
  `applrc_update_time` datetime DEFAULT NULL,
  `applrc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applrc_id`),
  KEY `applrc_applr_id` (`applrc_applr_id`),
  KEY `applrc_svc_id` (`applrc_svc_id`),
  KEY `applrc_trp_id` (`applrc_trp_id`),
  CONSTRAINT `appl_request_content_ibfk_1` FOREIGN KEY (`applrc_applr_id`) REFERENCES `appl_request` (`applr_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_content_ibfk_2` FOREIGN KEY (`applrc_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_request_content_ibfk_3` FOREIGN KEY (`applrc_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sh`
--

DROP TABLE IF EXISTS `appl_sh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sh` (
  `appls_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Протокол промежуточной сдачи',
  `appls_number` varchar(64) NOT NULL,
  `appls_date` date NOT NULL,
  `appls_prs_id` int(11) NOT NULL,
  `appls_trp_id` int(11) NOT NULL,
  `appls_score_max` int(11) NOT NULL,
  `appls_score` int(11) NOT NULL,
  `appls_passed` int(11) DEFAULT NULL,
  `appls_trt_id` int(11) DEFAULT NULL,
  `appls_svdt_id` int(11) DEFAULT NULL,
  `appls_pat_id` int(11) DEFAULT NULL,
  `appls_file_name` varchar(256) DEFAULT NULL,
  `appls_file_data` longblob,
  `appls_create_user` varchar(64) DEFAULT NULL,
  `appls_create_time` datetime DEFAULT NULL,
  `appls_create_ip` varchar(64) DEFAULT NULL,
  `appls_update_user` varchar(64) DEFAULT NULL,
  `appls_update_time` datetime DEFAULT NULL,
  `appls_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`appls_id`),
  UNIQUE KEY `appls_number` (`appls_number`),
  KEY `appls_prs_id` (`appls_prs_id`),
  KEY `appls_trp_id` (`appls_trp_id`),
  KEY `appls_pat_id` (`appls_pat_id`),
  KEY `appls_trt_id` (`appls_trt_id`),
  KEY `appls_svdt_id` (`appls_svdt_id`),
  CONSTRAINT `appl_sh_ibfk_1` FOREIGN KEY (`appls_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_ibfk_2` FOREIGN KEY (`appls_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_ibfk_3` FOREIGN KEY (`appls_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_ibfk_4` FOREIGN KEY (`appls_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_ibfk_5` FOREIGN KEY (`appls_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sh_content`
--

DROP TABLE IF EXISTS `appl_sh_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sh_content` (
  `applsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applsc_appls_id` int(11) NOT NULL,
  `applsc_trq_id` int(11) NOT NULL,
  `applsc_trq_question` text,
  `applsc_tra_id` int(11) NOT NULL,
  `applsc_tra_answer` text,
  `applsc_tra_variant` int(11) NOT NULL,
  `applsc_create_user` varchar(64) DEFAULT NULL,
  `applsc_create_time` datetime DEFAULT NULL,
  `applsc_create_ip` varchar(64) DEFAULT NULL,
  `applsc_update_user` varchar(64) DEFAULT NULL,
  `applsc_update_time` datetime DEFAULT NULL,
  `applsc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsc_id`),
  KEY `applsc_appls_id` (`applsc_appls_id`),
  KEY `applsc_tra_id` (`applsc_tra_id`),
  KEY `applsc_trq_id` (`applsc_trq_id`),
  CONSTRAINT `appl_sh_content_ibfk_2` FOREIGN KEY (`applsc_tra_id`) REFERENCES `training_answer` (`tra_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_content_ibfk_3` FOREIGN KEY (`applsc_trq_id`) REFERENCES `training_question` (`trq_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_content_ibfk_4` FOREIGN KEY (`applsc_appls_id`) REFERENCES `appl_sh` (`appls_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5836 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sh_x`
--

DROP TABLE IF EXISTS `appl_sh_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sh_x` (
  `applsx_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ведомость тестов промежуточная',
  `applsx_number` varchar(64) NOT NULL,
  `applsx_date` date NOT NULL,
  `applsx_trt_id` int(11) DEFAULT NULL,
  `applsx_svdt_id` int(11) DEFAULT NULL,
  `applsx_pat_id` int(11) DEFAULT NULL,
  `applsx_file_name` varchar(256) DEFAULT NULL,
  `applsx_file_data` longblob,
  `applsx_create_user` varchar(64) DEFAULT NULL,
  `applsx_create_time` datetime DEFAULT NULL,
  `applsx_create_ip` varchar(64) DEFAULT NULL,
  `applsx_update_user` varchar(64) DEFAULT NULL,
  `applsx_update_time` datetime DEFAULT NULL,
  `applsx_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsx_id`),
  UNIQUE KEY `applsx_number` (`applsx_number`),
  KEY `applsx_trt_id` (`applsx_trt_id`),
  KEY `applsx_pat_id` (`applsx_pat_id`),
  KEY `applsx_svdt_id` (`applsx_svdt_id`),
  CONSTRAINT `appl_sh_x_ibfk_1` FOREIGN KEY (`applsx_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_x_ibfk_2` FOREIGN KEY (`applsx_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_x_ibfk_3` FOREIGN KEY (`applsx_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sh_x_content`
--

DROP TABLE IF EXISTS `appl_sh_x_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sh_x_content` (
  `applsxc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applsxc_applsx_id` int(11) NOT NULL,
  `applsxc_prs_id` int(11) NOT NULL,
  `applsxc_trp_id` int(11) NOT NULL,
  `applsxc_score` int(11) NOT NULL,
  `applsxc_passed` int(11) NOT NULL,
  `applsxc_create_user` varchar(64) DEFAULT NULL,
  `applsxc_create_time` datetime DEFAULT NULL,
  `applsxc_create_ip` varchar(64) DEFAULT NULL,
  `applsxc_update_user` varchar(64) DEFAULT NULL,
  `applsxc_update_time` datetime DEFAULT NULL,
  `applsxc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsxc_id`),
  KEY `applsxc_prs_id` (`applsxc_prs_id`),
  KEY `applsxc_applsx_id` (`applsxc_applsx_id`),
  KEY `applsxc_trp_id` (`applsxc_trp_id`),
  CONSTRAINT `appl_sh_x_content_ibfk_1` FOREIGN KEY (`applsxc_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_x_content_ibfk_2` FOREIGN KEY (`applsxc_applsx_id`) REFERENCES `appl_sh_x` (`applsx_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sh_x_content_ibfk_3` FOREIGN KEY (`applsxc_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sheet`
--

DROP TABLE IF EXISTS `appl_sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sheet` (
  `appls_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Протокол сдачи',
  `appls_number` varchar(64) NOT NULL,
  `appls_date` date NOT NULL,
  `appls_prs_id` int(11) NOT NULL,
  `appls_trp_id` int(11) NOT NULL,
  `appls_score_max` int(11) NOT NULL,
  `appls_score` int(11) NOT NULL,
  `appls_passed` int(11) DEFAULT NULL,
  `appls_trt_id` int(11) DEFAULT NULL,
  `appls_svdt_id` int(11) DEFAULT NULL,
  `appls_pat_id` int(11) DEFAULT NULL,
  `appls_file_name` varchar(256) DEFAULT NULL,
  `appls_file_data` longblob,
  `appls_create_user` varchar(64) DEFAULT NULL,
  `appls_create_time` datetime DEFAULT NULL,
  `appls_create_ip` varchar(64) DEFAULT NULL,
  `appls_update_user` varchar(64) DEFAULT NULL,
  `appls_update_time` datetime DEFAULT NULL,
  `appls_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`appls_id`),
  UNIQUE KEY `appls_number` (`appls_number`),
  KEY `appls_prs_id` (`appls_prs_id`),
  KEY `appls_trp_id` (`appls_trp_id`),
  KEY `appls_pat_id` (`appls_pat_id`),
  KEY `appls_trt_id` (`appls_trt_id`),
  KEY `appls_svdt_id` (`appls_svdt_id`),
  CONSTRAINT `appl_sheet_ibfk_1` FOREIGN KEY (`appls_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_ibfk_2` FOREIGN KEY (`appls_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_ibfk_3` FOREIGN KEY (`appls_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_ibfk_4` FOREIGN KEY (`appls_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_ibfk_5` FOREIGN KEY (`appls_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sheet_content`
--

DROP TABLE IF EXISTS `appl_sheet_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sheet_content` (
  `applsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applsc_appls_id` int(11) NOT NULL,
  `applsc_trq_id` int(11) NOT NULL,
  `applsc_trq_question` text,
  `applsc_tra_id` int(11) NOT NULL,
  `applsc_tra_answer` text,
  `applsc_tra_variant` int(11) NOT NULL,
  `applsc_create_user` varchar(64) DEFAULT NULL,
  `applsc_create_time` datetime DEFAULT NULL,
  `applsc_create_ip` varchar(64) DEFAULT NULL,
  `applsc_update_user` varchar(64) DEFAULT NULL,
  `applsc_update_time` datetime DEFAULT NULL,
  `applsc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsc_id`),
  KEY `applsc_appls_id` (`applsc_appls_id`),
  KEY `applsc_tra_id` (`applsc_tra_id`),
  KEY `applsc_trq_id` (`applsc_trq_id`),
  CONSTRAINT `appl_sheet_content_ibfk_1` FOREIGN KEY (`applsc_appls_id`) REFERENCES `appl_sheet` (`appls_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_content_ibfk_2` FOREIGN KEY (`applsc_tra_id`) REFERENCES `training_answer` (`tra_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_content_ibfk_3` FOREIGN KEY (`applsc_trq_id`) REFERENCES `training_question` (`trq_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27546 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sheet_x`
--

DROP TABLE IF EXISTS `appl_sheet_x`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sheet_x` (
  `applsx_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ведомость тестов',
  `applsx_number` varchar(64) NOT NULL,
  `applsx_date` date NOT NULL,
  `applsx_trt_id` int(11) DEFAULT NULL,
  `applsx_svdt_id` int(11) DEFAULT NULL,
  `applsx_pat_id` int(11) DEFAULT NULL,
  `applsx_file_name` varchar(256) DEFAULT NULL,
  `applsx_file_data` longblob,
  `applsx_create_user` varchar(64) DEFAULT NULL,
  `applsx_create_time` datetime DEFAULT NULL,
  `applsx_create_ip` varchar(64) DEFAULT NULL,
  `applsx_update_user` varchar(64) DEFAULT NULL,
  `applsx_update_time` datetime DEFAULT NULL,
  `applsx_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsx_id`),
  UNIQUE KEY `applsx_number` (`applsx_number`),
  KEY `applsx_trt_id` (`applsx_trt_id`),
  KEY `applsx_pat_id` (`applsx_pat_id`),
  KEY `applsx_svdt_id` (`applsx_svdt_id`),
  CONSTRAINT `appl_sheet_x_ibfk_1` FOREIGN KEY (`applsx_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_x_ibfk_2` FOREIGN KEY (`applsx_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_x_ibfk_3` FOREIGN KEY (`applsx_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_sheet_x_content`
--

DROP TABLE IF EXISTS `appl_sheet_x_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_sheet_x_content` (
  `applsxc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applsxc_applsx_id` int(11) NOT NULL,
  `applsxc_prs_id` int(11) NOT NULL,
  `applsxc_trp_id` int(11) NOT NULL,
  `applsxc_score` int(11) NOT NULL,
  `applsxc_passed` int(11) NOT NULL,
  `applsxc_create_user` varchar(64) DEFAULT NULL,
  `applsxc_create_time` datetime DEFAULT NULL,
  `applsxc_create_ip` varchar(64) DEFAULT NULL,
  `applsxc_update_user` varchar(64) DEFAULT NULL,
  `applsxc_update_time` datetime DEFAULT NULL,
  `applsxc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applsxc_id`),
  KEY `applsxc_prs_id` (`applsxc_prs_id`),
  KEY `applsxc_applsx_id` (`applsxc_applsx_id`),
  KEY `applsxc_trp_id` (`applsxc_trp_id`),
  CONSTRAINT `appl_sheet_x_content_ibfk_1` FOREIGN KEY (`applsxc_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_x_content_ibfk_2` FOREIGN KEY (`applsxc_applsx_id`) REFERENCES `appl_sheet_x` (`applsx_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_sheet_x_content_ibfk_3` FOREIGN KEY (`applsxc_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1613 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_xxx`
--

DROP TABLE IF EXISTS `appl_xxx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_xxx` (
  `applxxx_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Протокол',
  `applxxx_number` varchar(64) NOT NULL,
  `applxxx_date` date NOT NULL,
  `applxxx_trt_id` int(11) DEFAULT NULL,
  `applxxx_svdt_id` int(11) DEFAULT NULL,
  `applxxx_pat_id` int(11) DEFAULT NULL,
  `applxxx_file_name` varchar(256) DEFAULT NULL,
  `applxxx_file_data` longblob,
  `applxxx_create_user` varchar(64) DEFAULT NULL,
  `applxxx_create_time` datetime DEFAULT NULL,
  `applxxx_create_ip` varchar(64) DEFAULT NULL,
  `applxxx_update_user` varchar(64) DEFAULT NULL,
  `applxxx_update_time` datetime DEFAULT NULL,
  `applxxx_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applxxx_id`),
  UNIQUE KEY `applxxx_number` (`applxxx_number`),
  KEY `applxxx_trt_id` (`applxxx_trt_id`),
  KEY `applxxx_pat_id` (`applxxx_pat_id`),
  KEY `applxxx_svdt_id` (`applxxx_svdt_id`),
  CONSTRAINT `appl_xxx_ibfk_1` FOREIGN KEY (`applxxx_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_xxx_ibfk_2` FOREIGN KEY (`applxxx_pat_id`) REFERENCES `pattern` (`pat_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_xxx_ibfk_3` FOREIGN KEY (`applxxx_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `appl_xxx_content`
--

DROP TABLE IF EXISTS `appl_xxx_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appl_xxx_content` (
  `applxxxc_id` int(11) NOT NULL AUTO_INCREMENT,
  `applxxxc_applxxx_id` int(11) NOT NULL,
  `applxxxc_ab_id` int(11) DEFAULT NULL,
  `applxxxc_prs_id` int(11) NOT NULL,
  `applxxxc_trp_id` int(11) NOT NULL,
  `applxxxc_position` varchar(64) DEFAULT NULL,
  `applxxxc_passed` int(11) NOT NULL,
  `applxxxc_create_user` varchar(64) DEFAULT NULL,
  `applxxxc_create_time` datetime DEFAULT NULL,
  `applxxxc_create_ip` varchar(64) DEFAULT NULL,
  `applxxxc_update_user` varchar(64) DEFAULT NULL,
  `applxxxc_update_time` datetime DEFAULT NULL,
  `applxxxc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`applxxxc_id`),
  KEY `applxxxc_ab_id` (`applxxxc_ab_id`),
  KEY `applxxxc_prs_id` (`applxxxc_prs_id`),
  KEY `applxxxc_applxxx_id` (`applxxxc_applxxx_id`),
  KEY `applxxxc_trp_id` (`applxxxc_trp_id`),
  CONSTRAINT `appl_xxx_content_ibfk_1` FOREIGN KEY (`applxxxc_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_xxx_content_ibfk_2` FOREIGN KEY (`applxxxc_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_xxx_content_ibfk_3` FOREIGN KEY (`applxxxc_applxxx_id`) REFERENCES `appl_xxx` (`applxxx_id`) ON UPDATE CASCADE,
  CONSTRAINT `appl_xxx_content_ibfk_4` FOREIGN KEY (`applxxxc_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(128) NOT NULL,
  `bank_bic` varchar(16) NOT NULL,
  `bank_account` varchar(32) DEFAULT NULL,
  `bank_address` varchar(256) DEFAULT NULL,
  `bank_create_user` varchar(64) DEFAULT NULL,
  `bank_create_time` datetime DEFAULT NULL,
  `bank_create_ip` varchar(64) DEFAULT NULL,
  `bank_update_user` varchar(64) DEFAULT NULL,
  `bank_update_time` datetime DEFAULT NULL,
  `bank_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`bank_id`),
  UNIQUE KEY `bank_bic` (`bank_bic`)
) ENGINE=InnoDB AUTO_INCREMENT=3208 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `cal_id` int(11) NOT NULL AUTO_INCREMENT,
  `cal_date` date NOT NULL,
  `cal_holiday` int(11) NOT NULL,
  `cal_create_user` varchar(64) DEFAULT NULL,
  `cal_create_time` datetime DEFAULT NULL,
  `cal_create_ip` varchar(64) DEFAULT NULL,
  `cal_update_user` varchar(64) DEFAULT NULL,
  `cal_update_time` datetime DEFAULT NULL,
  `cal_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`cal_id`),
  UNIQUE KEY `cal_date` (`cal_date`)
) ENGINE=InnoDB AUTO_INCREMENT=732 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_cou_id` int(11) NOT NULL,
  `city_reg_id` int(11) NOT NULL,
  `city_name` varchar(128) NOT NULL,
  `city_create_user` varchar(64) DEFAULT NULL,
  `city_create_time` datetime DEFAULT NULL,
  `city_create_ip` varchar(64) DEFAULT NULL,
  `city_update_user` varchar(64) DEFAULT NULL,
  `city_update_time` datetime DEFAULT NULL,
  `city_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_reg_id_city_name` (`city_reg_id`,`city_name`),
  KEY `city_cou_id` (`city_cou_id`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`city_reg_id`) REFERENCES `region` (`reg_id`) ON UPDATE CASCADE,
  CONSTRAINT `city_ibfk_2` FOREIGN KEY (`city_cou_id`) REFERENCES `country` (`cou_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_ent_id` int(11) NOT NULL,
  `comp_name` varchar(192) NOT NULL,
  `comp_note` text,
  `comp_create_user` varchar(64) DEFAULT NULL,
  `comp_create_time` datetime DEFAULT NULL,
  `comp_create_ip` varchar(64) DEFAULT NULL,
  `comp_update_user` varchar(64) DEFAULT NULL,
  `comp_update_time` datetime DEFAULT NULL,
  `comp_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`comp_id`),
  KEY `comp_ent_id` (`comp_ent_id`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`comp_ent_id`) REFERENCES `entity` (`ent_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `con_id` int(11) NOT NULL AUTO_INCREMENT,
  `con_ab_id` int(11) NOT NULL,
  `con_cont_id` int(11) NOT NULL,
  `con_text` text,
  `con_create_user` varchar(64) DEFAULT NULL,
  `con_create_time` datetime DEFAULT NULL,
  `con_create_ip` varchar(64) DEFAULT NULL,
  `con_update_user` varchar(64) DEFAULT NULL,
  `con_update_time` datetime DEFAULT NULL,
  `con_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`con_id`),
  KEY `con_cont_id` (`con_cont_id`),
  KEY `con_ab_id` (`con_ab_id`),
  CONSTRAINT `contact_ibfk_2` FOREIGN KEY (`con_cont_id`) REFERENCES `contact_type` (`cont_id`) ON UPDATE CASCADE,
  CONSTRAINT `contact_ibfk_3` FOREIGN KEY (`con_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=555 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_type`
--

DROP TABLE IF EXISTS `contact_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_type` (
  `cont_id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_name` varchar(64) NOT NULL,
  `cont_type` int(11) NOT NULL,
  `cont_create_user` varchar(64) DEFAULT NULL,
  `cont_create_time` datetime DEFAULT NULL,
  `cont_create_ip` varchar(64) DEFAULT NULL,
  `cont_update_user` varchar(64) DEFAULT NULL,
  `cont_update_time` datetime DEFAULT NULL,
  `cont_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`cont_id`),
  UNIQUE KEY `cont_name` (`cont_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `cou_id` int(11) NOT NULL AUTO_INCREMENT,
  `cou_name` varchar(128) NOT NULL,
  `cou_create_user` varchar(64) DEFAULT NULL,
  `cou_create_time` datetime DEFAULT NULL,
  `cou_create_ip` varchar(64) DEFAULT NULL,
  `cou_update_user` varchar(64) DEFAULT NULL,
  `cou_update_time` datetime DEFAULT NULL,
  `cou_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`cou_id`),
  UNIQUE KEY `cou_name` (`cou_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `ent_id` int(11) NOT NULL AUTO_INCREMENT,
  `ent_entt_id` int(11) NOT NULL,
  `ent_name` varchar(192) NOT NULL,
  `ent_name_short` varchar(128) NOT NULL,
  `ent_orgn` varchar(16) DEFAULT NULL,
  `ent_inn` varchar(16) DEFAULT NULL,
  `ent_kpp` varchar(16) DEFAULT NULL,
  `ent_okpo` varchar(16) DEFAULT NULL,
  `ent_agent_id` int(11) DEFAULT NULL,
  `ent_create_user` varchar(64) DEFAULT NULL,
  `ent_create_time` datetime DEFAULT NULL,
  `ent_create_ip` varchar(64) DEFAULT NULL,
  `ent_update_user` varchar(64) DEFAULT NULL,
  `ent_update_time` datetime DEFAULT NULL,
  `ent_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ent_id`),
  KEY `ent_entt_id` (`ent_entt_id`),
  KEY `ent_agent` (`ent_agent_id`),
  KEY `ent_name_short` (`ent_name_short`),
  KEY `ent_name` (`ent_name`),
  CONSTRAINT `entity_ibfk_1` FOREIGN KEY (`ent_entt_id`) REFERENCES `entity_type` (`entt_id`) ON UPDATE CASCADE,
  CONSTRAINT `entity_ibfk_2` FOREIGN KEY (`ent_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `entity_ibfk_3` FOREIGN KEY (`ent_agent_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3946 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity_class`
--

DROP TABLE IF EXISTS `entity_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_class` (
  `entc_id` int(11) NOT NULL AUTO_INCREMENT,
  `entc_name` varchar(128) NOT NULL,
  `entc_sy` varchar(64) DEFAULT NULL,
  `entc_create_user` varchar(64) DEFAULT NULL,
  `entc_create_time` datetime DEFAULT NULL,
  `entc_create_ip` varchar(64) DEFAULT NULL,
  `entc_update_user` varchar(64) DEFAULT NULL,
  `entc_update_time` datetime DEFAULT NULL,
  `entc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`entc_id`),
  UNIQUE KEY `entc_name` (`entc_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity_class_lnk`
--

DROP TABLE IF EXISTS `entity_class_lnk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_class_lnk` (
  `entcl_id` int(11) NOT NULL AUTO_INCREMENT,
  `entcl_ent_id` int(11) NOT NULL,
  `entcl_entc_id` int(11) NOT NULL,
  `entcl_create_user` varchar(64) DEFAULT NULL,
  `entcl_create_time` datetime DEFAULT NULL,
  `entcl_create_ip` varchar(64) DEFAULT NULL,
  `entcl_update_user` varchar(64) DEFAULT NULL,
  `entcl_update_time` datetime DEFAULT NULL,
  `entcl_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`entcl_id`),
  KEY `entcl_ent_id` (`entcl_ent_id`),
  KEY `entcl_entc_id` (`entcl_entc_id`),
  CONSTRAINT `entity_class_lnk_ibfk_1` FOREIGN KEY (`entcl_ent_id`) REFERENCES `entity` (`ent_id`) ON UPDATE CASCADE,
  CONSTRAINT `entity_class_lnk_ibfk_2` FOREIGN KEY (`entcl_entc_id`) REFERENCES `entity_class` (`entc_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entity_type`
--

DROP TABLE IF EXISTS `entity_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_type` (
  `entt_id` int(11) NOT NULL AUTO_INCREMENT,
  `entt_name` varchar(128) NOT NULL,
  `entt_name_short` varchar(64) NOT NULL,
  `entt_create_user` varchar(64) DEFAULT NULL,
  `entt_create_time` datetime DEFAULT NULL,
  `entt_create_ip` varchar(64) DEFAULT NULL,
  `entt_update_user` varchar(64) DEFAULT NULL,
  `entt_update_time` datetime DEFAULT NULL,
  `entt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`entt_id`),
  UNIQUE KEY `entt_name` (`entt_name`),
  UNIQUE KEY `entt_name_short` (`entt_name_short`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_ab_id` int(11) NOT NULL,
  `event_evt_id` int(11) NOT NULL,
  `event_evrt_id` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `event_user_id` int(11) DEFAULT NULL,
  `event_note` text,
  `event_create_user` varchar(64) DEFAULT NULL,
  `event_create_time` datetime DEFAULT NULL,
  `event_create_ip` varchar(64) DEFAULT NULL,
  `event_update_user` varchar(64) DEFAULT NULL,
  `event_update_time` datetime DEFAULT NULL,
  `event_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `event_ab_id` (`event_ab_id`),
  KEY `event_evt_id` (`event_evt_id`),
  KEY `event_evrt_id` (`event_evrt_id`),
  KEY `event_user_id` (`event_user_id`),
  CONSTRAINT `event_ibfk_3` FOREIGN KEY (`event_evrt_id`) REFERENCES `event_result_type` (`evrt_id`) ON UPDATE CASCADE,
  CONSTRAINT `event_ibfk_5` FOREIGN KEY (`event_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `event_ibfk_7` FOREIGN KEY (`event_user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `event_ibfk_9` FOREIGN KEY (`event_evt_id`) REFERENCES `event_type` (`evt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_result_type`
--

DROP TABLE IF EXISTS `event_result_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_result_type` (
  `evrt_id` int(11) NOT NULL AUTO_INCREMENT,
  `evrt_name` varchar(64) NOT NULL,
  `evrt_note` text,
  `evrt_create_user` varchar(64) DEFAULT NULL,
  `evrt_create_time` datetime DEFAULT NULL,
  `evrt_create_ip` varchar(64) DEFAULT NULL,
  `evrt_update_user` varchar(64) DEFAULT NULL,
  `evrt_update_time` datetime DEFAULT NULL,
  `evrt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`evrt_id`),
  UNIQUE KEY `evrt_result` (`evrt_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `evt_id` int(11) NOT NULL AUTO_INCREMENT,
  `evt_name` varchar(64) NOT NULL,
  `evt_note` text,
  `evt_create_user` varchar(64) DEFAULT NULL,
  `evt_create_time` datetime DEFAULT NULL,
  `evt_create_ip` varchar(64) DEFAULT NULL,
  `evt_update_user` varchar(64) DEFAULT NULL,
  `evt_update_time` datetime DEFAULT NULL,
  `evt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`evt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `finance_book`
--

DROP TABLE IF EXISTS `finance_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_book` (
  `fb_id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_fb_id` int(11) DEFAULT NULL,
  `fb_aca_id` int(11) DEFAULT NULL COMMENT 'Строка счета',
  `fb_payt_id` int(11) DEFAULT NULL COMMENT 'Расходный ордер',
  `fb_ptr_id` int(11) DEFAULT NULL COMMENT 'Строка отката',
  `fb_svc_id` int(11) NOT NULL COMMENT 'Услуга',
  `fb_fbt_id` int(11) NOT NULL COMMENT 'Тип фин. книги',
  `fb_ab_id` int(11) NOT NULL COMMENT 'Контрагент',
  `fb_comp_id` int(11) NOT NULL COMMENT 'Наша компания',
  `fb_sum` decimal(16,4) NOT NULL,
  `fb_tax` decimal(16,4) NOT NULL,
  `fb_pay` decimal(16,4) DEFAULT NULL COMMENT 'Сумма оплаты',
  `fb_aga_id` int(11) NOT NULL COMMENT 'Счет',
  `fb_acc_id` int(11) DEFAULT NULL COMMENT 'Счет (Банк)',
  `fb_fbs_id` int(11) NOT NULL COMMENT 'Статус',
  `fb_pt_id` int(11) NOT NULL COMMENT 'Тип оплаты',
  `fb_date` date DEFAULT NULL COMMENT 'Дата платежа',
  `fb_payment` varchar(256) DEFAULT NULL COMMENT 'Платежка',
  `fb_comment` varchar(1024) DEFAULT NULL COMMENT 'Комментарий',
  `fb_create_user` varchar(64) DEFAULT NULL,
  `fb_create_ip` varchar(64) DEFAULT NULL,
  `fb_create_time` datetime DEFAULT NULL,
  `fb_update_user` varchar(64) DEFAULT NULL,
  `fb_update_ip` varchar(64) DEFAULT NULL,
  `fb_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fb_id`),
  KEY `fb_fbs_id` (`fb_fbs_id`),
  KEY `fb_svc_id` (`fb_svc_id`),
  KEY `fb_aga_id` (`fb_aga_id`),
  KEY `fb_acc_id` (`fb_acc_id`),
  KEY `fb_fbt_id` (`fb_fbt_id`),
  KEY `fb_ab_id` (`fb_ab_id`),
  KEY `fb_comp_id` (`fb_comp_id`),
  KEY `fb_aca_id` (`fb_aca_id`),
  KEY `fb_payt_id` (`fb_payt_id`),
  KEY `fb_pt_id` (`fb_pt_id`),
  KEY `fb_ptr_id` (`fb_ptr_id`),
  CONSTRAINT `finance_book_ibfk_1` FOREIGN KEY (`fb_fbs_id`) REFERENCES `finance_book_status` (`fbs_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_10` FOREIGN KEY (`fb_pt_id`) REFERENCES `payment_type` (`pt_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_12` FOREIGN KEY (`fb_ptr_id`) REFERENCES `payment_ret` (`ptr_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_2` FOREIGN KEY (`fb_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_3` FOREIGN KEY (`fb_aga_id`) REFERENCES `agreement_acc` (`aga_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_4` FOREIGN KEY (`fb_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_5` FOREIGN KEY (`fb_fbt_id`) REFERENCES `finance_book_type` (`fbt_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_6` FOREIGN KEY (`fb_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_7` FOREIGN KEY (`fb_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_8` FOREIGN KEY (`fb_aca_id`) REFERENCES `agreement_acc_a` (`aca_id`) ON UPDATE CASCADE,
  CONSTRAINT `finance_book_ibfk_9` FOREIGN KEY (`fb_payt_id`) REFERENCES `payment_to` (`payt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `finance_book_status`
--

DROP TABLE IF EXISTS `finance_book_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_book_status` (
  `fbs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fbs_name` varchar(64) NOT NULL,
  `fbs_create_user` varchar(64) DEFAULT NULL,
  `fbs_create_ip` varchar(64) DEFAULT NULL,
  `fbs_create_time` datetime DEFAULT NULL,
  `fbs_update_user` varchar(64) DEFAULT NULL,
  `fbs_update_ip` varchar(64) DEFAULT NULL,
  `fbs_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fbs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `finance_book_type`
--

DROP TABLE IF EXISTS `finance_book_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_book_type` (
  `fbt_id` int(11) NOT NULL AUTO_INCREMENT,
  `fbt_name` varchar(64) NOT NULL,
  `fbt_create_user` varchar(64) DEFAULT NULL,
  `fbt_create_ip` varchar(64) DEFAULT NULL,
  `fbt_create_time` datetime DEFAULT NULL,
  `fbt_update_user` varchar(64) DEFAULT NULL,
  `fbt_update_ip` varchar(64) DEFAULT NULL,
  `fbt_update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`fbt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pattern`
--

DROP TABLE IF EXISTS `pattern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pattern` (
  `pat_id` int(11) NOT NULL AUTO_INCREMENT,
  `pat_name` varchar(128) NOT NULL,
  `pat_patt_id` int(11) NOT NULL,
  `pat_svdt_id` int(11) DEFAULT NULL,
  `pat_trt_id` int(11) DEFAULT NULL,
  `pat_code` varchar(16) DEFAULT NULL,
  `pat_fname` varchar(256) DEFAULT NULL,
  `pat_fdata` longblob,
  `pat_note` text,
  `pat_create_user` varchar(64) DEFAULT NULL,
  `pat_create_time` datetime DEFAULT NULL,
  `pat_create_ip` varchar(64) DEFAULT NULL,
  `pat_update_user` varchar(64) DEFAULT NULL,
  `pat_update_time` datetime DEFAULT NULL,
  `pat_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pat_id`),
  KEY `pat_patt_id` (`pat_patt_id`),
  KEY `pat_svdt_id` (`pat_svdt_id`),
  KEY `pat_trt_id` (`pat_trt_id`),
  CONSTRAINT `pattern_ibfk_1` FOREIGN KEY (`pat_patt_id`) REFERENCES `pattern_type` (`patt_id`) ON UPDATE CASCADE,
  CONSTRAINT `pattern_ibfk_2` FOREIGN KEY (`pat_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE,
  CONSTRAINT `pattern_ibfk_3` FOREIGN KEY (`pat_trt_id`) REFERENCES `training_type` (`trt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pattern_type`
--

DROP TABLE IF EXISTS `pattern_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pattern_type` (
  `patt_id` int(11) NOT NULL AUTO_INCREMENT,
  `patt_name` varchar(64) NOT NULL,
  `patt_note` text,
  `patt_create_user` varchar(64) DEFAULT NULL,
  `patt_create_time` datetime DEFAULT NULL,
  `patt_create_ip` varchar(64) DEFAULT NULL,
  `patt_update_user` varchar(64) DEFAULT NULL,
  `patt_update_time` datetime DEFAULT NULL,
  `patt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`patt_id`),
  UNIQUE KEY `patt_name` (`patt_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_inv_id` int(11) NOT NULL,
  `pay_inv_sum` decimal(15,4) NOT NULL,
  `pay_pays_id` int(11) NOT NULL,
  `pay_date_plan` date NOT NULL,
  `pay_date_fact` date DEFAULT NULL,
  `pay_create_user` varchar(64) DEFAULT NULL,
  `pay_create_time` datetime DEFAULT NULL,
  `pay_create_ip` varchar(64) DEFAULT NULL,
  `pay_update_user` varchar(64) DEFAULT NULL,
  `pay_update_time` datetime DEFAULT NULL,
  `pay_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pay_id`),
  KEY `pay_inv_id` (`pay_inv_id`),
  KEY `pay_pays_id` (`pay_pays_id`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`pay_inv_id`) REFERENCES `agreement_acc` (`aga_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`pay_pays_id`) REFERENCES `payment_status` (`pays_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_ret`
--

DROP TABLE IF EXISTS `payment_ret`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_ret` (
  `ptr_id` int(11) NOT NULL AUTO_INCREMENT,
  `ptr_ab_id` int(11) NOT NULL,
  `ptr_acc_id` int(11) DEFAULT NULL,
  `ptr_comp_id` int(11) NOT NULL,
  `ptr_aga_id` int(11) DEFAULT NULL COMMENT 'Счет',
  `ptr_aca_id` int(11) DEFAULT NULL COMMENT 'Строка счета',
  `ptr_fb_id` int(11) DEFAULT NULL COMMENT 'Запись в финкниге',
  `ptr_fbs_id` int(11) NOT NULL COMMENT 'Статус',
  `ptr_pt_id` int(11) NOT NULL COMMENT 'Тип оплаты',
  `ptr_svc_id` int(11) NOT NULL,
  `ptr_svc_sum` decimal(15,4) NOT NULL,
  `ptr_svc_tax` decimal(15,4) NOT NULL,
  `ptr_date_pay` date NOT NULL,
  `ptr_comment` varchar(1024) DEFAULT NULL,
  `ptr_create_user` varchar(64) DEFAULT NULL,
  `ptr_create_time` datetime DEFAULT NULL,
  `ptr_create_ip` varchar(64) DEFAULT NULL,
  `ptr_update_user` varchar(64) DEFAULT NULL,
  `ptr_update_time` datetime DEFAULT NULL,
  `ptr_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ptr_id`),
  KEY `ptr_ab_id` (`ptr_ab_id`),
  KEY `ptr_comp_id` (`ptr_comp_id`),
  KEY `ptr_acc_id` (`ptr_acc_id`),
  KEY `ptr_fb_id` (`ptr_fb_id`),
  KEY `ptr_fbs_id` (`ptr_fbs_id`),
  KEY `ptr_pt_id` (`ptr_pt_id`),
  KEY `ptr_aga_id` (`ptr_aga_id`),
  KEY `ptr_aca_id` (`ptr_aca_id`),
  KEY `ptr_svc_id` (`ptr_svc_id`),
  CONSTRAINT `payment_ret_ibfk_1` FOREIGN KEY (`ptr_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_10` FOREIGN KEY (`ptr_aca_id`) REFERENCES `agreement_acc_a` (`aca_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_11` FOREIGN KEY (`ptr_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_2` FOREIGN KEY (`ptr_comp_id`) REFERENCES `company` (`comp_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_4` FOREIGN KEY (`ptr_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_7` FOREIGN KEY (`ptr_fbs_id`) REFERENCES `finance_book_status` (`fbs_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_8` FOREIGN KEY (`ptr_pt_id`) REFERENCES `payment_type` (`pt_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_ret_ibfk_9` FOREIGN KEY (`ptr_aga_id`) REFERENCES `agreement_acc` (`aga_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_status`
--

DROP TABLE IF EXISTS `payment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_status` (
  `pays_id` int(11) NOT NULL AUTO_INCREMENT,
  `pays_name` varchar(64) NOT NULL,
  `pays_create_user` varchar(64) DEFAULT NULL,
  `pays_create_time` datetime DEFAULT NULL,
  `pays_create_ip` varchar(64) DEFAULT NULL,
  `pays_update_user` varchar(64) DEFAULT NULL,
  `pays_update_time` datetime DEFAULT NULL,
  `pays_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pays_id`),
  UNIQUE KEY `pays_name` (`pays_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_to`
--

DROP TABLE IF EXISTS `payment_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_to` (
  `payt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Код',
  `payt_aca_id` int(11) NOT NULL COMMENT 'Строки счета',
  `payt_aga_id` int(11) NOT NULL COMMENT 'Счет',
  `payt_from_ab_id` int(11) NOT NULL COMMENT 'Контрагент Откуда',
  `payt_from_acc_id` int(11) DEFAULT NULL COMMENT 'Счет Откуда',
  `payt_to_ab_id` int(11) NOT NULL COMMENT 'Контрагент Куда',
  `payt_to_acc_id` int(11) DEFAULT NULL COMMENT 'Счет Куда',
  `payt_fbs_id` int(11) NOT NULL COMMENT 'Статус',
  `payt_pt_id` int(11) NOT NULL COMMENT 'тип оплаты',
  `payt_fb_id` int(11) DEFAULT NULL COMMENT 'Запись в Финкниге',
  `payt_svc_id` int(11) NOT NULL COMMENT 'Услуга',
  `payt_svc_price` decimal(18,4) NOT NULL COMMENT 'Цена',
  `payt_svc_qty` int(11) NOT NULL COMMENT 'Кол-во',
  `payt_svc_sum` decimal(18,4) NOT NULL COMMENT 'Сумма',
  `payt_date_pay` datetime NOT NULL COMMENT 'Дата платежа',
  `payt_create_user` varchar(64) DEFAULT NULL,
  `payt_create_time` datetime DEFAULT NULL,
  `payt_create_ip` varchar(64) DEFAULT NULL,
  `payt_update_user` varchar(64) DEFAULT NULL,
  `payt_update_time` datetime DEFAULT NULL,
  `payt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`payt_id`),
  KEY `payt_from_ab_id` (`payt_from_ab_id`),
  KEY `payt_to_ab_id` (`payt_to_ab_id`),
  KEY `payt_from_acc_id` (`payt_from_acc_id`),
  KEY `payt_to_acc_id` (`payt_to_acc_id`),
  KEY `payt_fbs_id` (`payt_fbs_id`),
  KEY `payt_svc_id` (`payt_svc_id`),
  KEY `payt_aga_id` (`payt_aga_id`),
  KEY `payt_pt_id` (`payt_pt_id`),
  CONSTRAINT `payment_to_ibfk_2` FOREIGN KEY (`payt_from_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_3` FOREIGN KEY (`payt_to_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_4` FOREIGN KEY (`payt_from_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_5` FOREIGN KEY (`payt_to_acc_id`) REFERENCES `account` (`acc_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_6` FOREIGN KEY (`payt_fbs_id`) REFERENCES `finance_book_status` (`fbs_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_7` FOREIGN KEY (`payt_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_8` FOREIGN KEY (`payt_aga_id`) REFERENCES `agreement_acc` (`aga_id`) ON UPDATE CASCADE,
  CONSTRAINT `payment_to_ibfk_9` FOREIGN KEY (`payt_pt_id`) REFERENCES `payment_type` (`pt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_type`
--

DROP TABLE IF EXISTS `payment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_type` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_name` varchar(64) NOT NULL,
  `pt_create_user` varchar(64) DEFAULT NULL,
  `pt_create_time` datetime DEFAULT NULL,
  `pt_create_ip` varchar(64) DEFAULT NULL,
  `pt_update_user` varchar(64) DEFAULT NULL,
  `pt_update_time` datetime DEFAULT NULL,
  `pt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pt_id`),
  UNIQUE KEY `pt_name` (`pt_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `prs_id` int(11) NOT NULL AUTO_INCREMENT,
  `prs_first_name` varchar(64) NOT NULL,
  `prs_last_name` varchar(64) NOT NULL,
  `prs_middle_name` varchar(64) DEFAULT NULL,
  `prs_full_name` varchar(256) DEFAULT NULL,
  `prs_inn` varchar(16) DEFAULT NULL,
  `prs_birth_date` date DEFAULT NULL,
  `prs_pass_sex` int(11) DEFAULT NULL,
  `prs_pass_serial` varchar(4) DEFAULT NULL,
  `prs_pass_number` varchar(8) DEFAULT NULL,
  `prs_pass_issued_by` varchar(256) DEFAULT NULL,
  `prs_pass_date` date DEFAULT NULL,
  `prs_connect_link` varchar(256) DEFAULT NULL,
  `prs_create_user` varchar(64) DEFAULT NULL,
  `prs_create_time` datetime DEFAULT NULL,
  `prs_create_ip` varchar(64) DEFAULT NULL,
  `prs_update_user` varchar(64) DEFAULT NULL,
  `prs_update_time` datetime DEFAULT NULL,
  `prs_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`prs_id`),
  CONSTRAINT `person_ibfk_1` FOREIGN KEY (`prs_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3947 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `price_effect`
--

DROP TABLE IF EXISTS `price_effect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_effect` (
  `pef_id` int(11) NOT NULL AUTO_INCREMENT,
  `pef_ab_id` int(11) NOT NULL,
  `pef_pt_id` int(11) NOT NULL,
  `pef_create_user` varchar(64) DEFAULT NULL,
  `pef_create_time` datetime DEFAULT NULL,
  `pef_create_ip` varchar(64) DEFAULT NULL,
  `pef_update_user` varchar(64) DEFAULT NULL,
  `pef_update_time` datetime DEFAULT NULL,
  `pef_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pef_id`),
  KEY `pef_ab_id` (`pef_ab_id`),
  KEY `pef_pt_id` (`pef_pt_id`),
  CONSTRAINT `price_effect_ibfk_1` FOREIGN KEY (`pef_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `price_effect_ibfk_2` FOREIGN KEY (`pef_pt_id`) REFERENCES `price_type` (`pt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `price_list`
--

DROP TABLE IF EXISTS `price_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_list` (
  `pl_id` int(11) NOT NULL AUTO_INCREMENT,
  `pl_svc_id` int(11) NOT NULL,
  `pl_pt_id` int(11) NOT NULL,
  `pl_price` decimal(15,4) NOT NULL,
  `pl_create_user` varchar(64) DEFAULT NULL,
  `pl_create_time` datetime DEFAULT NULL,
  `pl_create_ip` varchar(64) DEFAULT NULL,
  `pl_update_user` varchar(64) DEFAULT NULL,
  `pl_update_time` datetime DEFAULT NULL,
  `pl_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pl_id`),
  KEY `pl_svc_id` (`pl_svc_id`),
  KEY `pl_pt_id` (`pl_pt_id`),
  CONSTRAINT `price_list_ibfk_1` FOREIGN KEY (`pl_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `price_list_ibfk_2` FOREIGN KEY (`pl_pt_id`) REFERENCES `price_type` (`pt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `price_type`
--

DROP TABLE IF EXISTS `price_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price_type` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_name` varchar(64) NOT NULL,
  `pt_note` text,
  `pt_create_user` varchar(64) DEFAULT NULL,
  `pt_create_time` datetime DEFAULT NULL,
  `pt_create_ip` varchar(64) DEFAULT NULL,
  `pt_update_user` varchar(64) DEFAULT NULL,
  `pt_update_time` datetime DEFAULT NULL,
  `pt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`pt_id`),
  UNIQUE KEY `pt_name` (`pt_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_cou_id` int(11) NOT NULL,
  `reg_name` varchar(128) NOT NULL,
  `reg_create_user` varchar(64) DEFAULT NULL,
  `reg_create_time` datetime DEFAULT NULL,
  `reg_create_ip` varchar(64) DEFAULT NULL,
  `reg_update_user` varchar(64) DEFAULT NULL,
  `reg_update_time` datetime DEFAULT NULL,
  `reg_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`reg_id`),
  UNIQUE KEY `reg_cou_id_reg_name` (`reg_cou_id`,`reg_name`),
  CONSTRAINT `region_ibfk_1` FOREIGN KEY (`reg_cou_id`) REFERENCES `country` (`cou_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `stf_id` int(11) NOT NULL AUTO_INCREMENT,
  `stf_ent_id` int(11) NOT NULL,
  `stf_prs_id` int(11) NOT NULL,
  `stf_position` varchar(128) DEFAULT NULL,
  `stf_create_user` varchar(64) DEFAULT NULL,
  `stf_create_time` datetime DEFAULT NULL,
  `stf_create_ip` varchar(64) DEFAULT NULL,
  `stf_update_user` varchar(64) DEFAULT NULL,
  `stf_update_time` datetime DEFAULT NULL,
  `stf_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`stf_id`),
  UNIQUE KEY `stf_ent_id_stf_prs_id` (`stf_ent_id`,`stf_prs_id`),
  KEY `stf_prs_id` (`stf_prs_id`),
  CONSTRAINT `staff_ibfk_2` FOREIGN KEY (`stf_prs_id`) REFERENCES `person` (`prs_id`) ON UPDATE CASCADE,
  CONSTRAINT `staff_ibfk_3` FOREIGN KEY (`stf_ent_id`) REFERENCES `entity` (`ent_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=788 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svc`
--

DROP TABLE IF EXISTS `svc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svc` (
  `svc_id` int(11) NOT NULL AUTO_INCREMENT,
  `svc_name` varchar(128) NOT NULL,
  `svc_svdt_id` int(11) DEFAULT NULL,
  `svc_skill` varchar(64) DEFAULT NULL,
  `svc_hour` varchar(16) DEFAULT NULL,
  `svc_price` decimal(18,4) DEFAULT NULL,
  `svc_cost` decimal(18,4) DEFAULT NULL,
  `svc_cost_ext` decimal(18,4) DEFAULT NULL,
  `svc_payment_flag` int(11) DEFAULT NULL,
  `svc_tax_flag` int(11) DEFAULT NULL,
  `svc_ab_id` int(11) DEFAULT NULL,
  `svc_create_user` varchar(64) DEFAULT NULL,
  `svc_create_time` datetime DEFAULT NULL,
  `svc_create_ip` varchar(64) DEFAULT NULL,
  `svc_update_user` varchar(64) DEFAULT NULL,
  `svc_update_time` datetime DEFAULT NULL,
  `svc_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`svc_id`),
  UNIQUE KEY `scv_name` (`svc_name`),
  KEY `svc_svdt_id` (`svc_svdt_id`),
  KEY `svc_ab_id` (`svc_ab_id`),
  CONSTRAINT `svc_ibfk_1` FOREIGN KEY (`svc_svdt_id`) REFERENCES `svc_doc_type` (`svdt_id`) ON UPDATE CASCADE,
  CONSTRAINT `svc_ibfk_2` FOREIGN KEY (`svc_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svc_doc_type`
--

DROP TABLE IF EXISTS `svc_doc_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svc_doc_type` (
  `svdt_id` int(11) NOT NULL AUTO_INCREMENT,
  `svdt_name` varchar(128) NOT NULL,
  `svdt_create_user` varchar(64) DEFAULT NULL,
  `svdt_create_time` datetime DEFAULT NULL,
  `svdt_create_ip` varchar(64) DEFAULT NULL,
  `svdt_update_user` varchar(64) DEFAULT NULL,
  `svdt_update_time` datetime DEFAULT NULL,
  `svdt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`svdt_id`),
  UNIQUE KEY `scv_name` (`svdt_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svc_prog`
--

DROP TABLE IF EXISTS `svc_prog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svc_prog` (
  `sprog_id` int(11) NOT NULL AUTO_INCREMENT,
  `sprog_svc_id` int(11) NOT NULL,
  `sprog_trp_id` int(11) NOT NULL,
  `sprog_create_user` varchar(64) DEFAULT NULL,
  `sprog_create_time` datetime DEFAULT NULL,
  `sprog_create_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`sprog_id`),
  KEY `sprog_svc_id` (`sprog_svc_id`),
  KEY `sprog_trp_id` (`sprog_trp_id`),
  CONSTRAINT `svc_prog_ibfk_1` FOREIGN KEY (`sprog_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `svc_prog_ibfk_2` FOREIGN KEY (`sprog_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=937 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svc_prop`
--

DROP TABLE IF EXISTS `svc_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svc_prop` (
  `svcp_id` int(11) NOT NULL AUTO_INCREMENT,
  `svcp_name` varchar(64) NOT NULL,
  `svcp_create_user` varchar(64) DEFAULT NULL,
  `svcp_create_time` datetime DEFAULT NULL,
  `svcp_create_ip` varchar(64) DEFAULT NULL,
  `svcp_update_user` varchar(64) DEFAULT NULL,
  `svcp_update_time` datetime DEFAULT NULL,
  `svcp_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`svcp_id`),
  UNIQUE KEY `svcp_name` (`svcp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `svc_value`
--

DROP TABLE IF EXISTS `svc_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `svc_value` (
  `svcv_id` int(11) NOT NULL AUTO_INCREMENT,
  `svcv_svc_id` int(11) NOT NULL,
  `svcv_svcp_id` int(11) NOT NULL,
  `svcv_value` varchar(256) NOT NULL,
  `svcv_create_user` varchar(64) DEFAULT NULL,
  `svcv_create_time` datetime DEFAULT NULL,
  `svcv_create_ip` varchar(64) DEFAULT NULL,
  `svcv_update_user` varchar(64) DEFAULT NULL,
  `svcv_update_time` datetime DEFAULT NULL,
  `svcv_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`svcv_id`),
  KEY `svd_svc_id` (`svcv_svc_id`),
  KEY `svd_svcp_id` (`svcv_svcp_id`),
  CONSTRAINT `svc_value_ibfk_1` FOREIGN KEY (`svcv_svc_id`) REFERENCES `svc` (`svc_id`) ON UPDATE CASCADE,
  CONSTRAINT `svc_value_ibfk_2` FOREIGN KEY (`svcv_svcp_id`) REFERENCES `svc_prop` (`svcp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(64) NOT NULL,
  `tag_comment` varchar(256) NOT NULL,
  `tag_create_user` varchar(64) DEFAULT NULL,
  `tag_create_time` datetime DEFAULT NULL,
  `tag_create_ip` varchar(64) DEFAULT NULL,
  `tag_update_user` varchar(64) DEFAULT NULL,
  `tag_update_time` datetime DEFAULT NULL,
  `tag_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_ab_id` int(11) NOT NULL,
  `task_taskt_id` int(11) NOT NULL,
  `task_date_end` date NOT NULL,
  `task_tasks_id` int(11) NOT NULL,
  `task_user_id` int(11) NOT NULL,
  `task_note` text NOT NULL,
  `task_event_id` int(11) DEFAULT NULL,
  `task_create_user` varchar(64) DEFAULT NULL,
  `task_create_time` datetime DEFAULT NULL,
  `task_create_ip` varchar(64) DEFAULT NULL,
  `task_update_user` varchar(64) DEFAULT NULL,
  `task_update_time` datetime DEFAULT NULL,
  `task_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  KEY `task_taskt_id` (`task_taskt_id`),
  KEY `task_ab_id` (`task_ab_id`),
  KEY `task_tasks_id` (`task_tasks_id`),
  KEY `task_user_id` (`task_user_id`),
  KEY `task_event_id` (`task_event_id`),
  CONSTRAINT `task_ibfk_2` FOREIGN KEY (`task_taskt_id`) REFERENCES `task_type` (`taskt_id`) ON UPDATE CASCADE,
  CONSTRAINT `task_ibfk_3` FOREIGN KEY (`task_ab_id`) REFERENCES `ab` (`ab_id`) ON UPDATE CASCADE,
  CONSTRAINT `task_ibfk_4` FOREIGN KEY (`task_tasks_id`) REFERENCES `task_status` (`tasks_id`) ON UPDATE CASCADE,
  CONSTRAINT `task_ibfk_5` FOREIGN KEY (`task_user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `task_ibfk_6` FOREIGN KEY (`task_event_id`) REFERENCES `event` (`event_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_status`
--

DROP TABLE IF EXISTS `task_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_status` (
  `tasks_id` int(11) NOT NULL AUTO_INCREMENT,
  `tasks_name` varchar(64) NOT NULL,
  `tasks_note` text NOT NULL,
  `tasks_create_user` varchar(64) DEFAULT NULL,
  `tasks_create_time` datetime DEFAULT NULL,
  `tasks_create_ip` varchar(64) DEFAULT NULL,
  `tasks_update_user` varchar(64) DEFAULT NULL,
  `tasks_update_time` datetime DEFAULT NULL,
  `tasks_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tasks_id`),
  UNIQUE KEY `tasks_name` (`tasks_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_tmpl`
--

DROP TABLE IF EXISTS `task_tmpl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_tmpl` (
  `tasktp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tasktp_event_id` int(11) NOT NULL,
  `tasktp_taskt_id` int(11) NOT NULL,
  `tasktp_date_add` int(11) NOT NULL,
  `tasktp_note` text,
  `tasktp_create_user` varchar(64) DEFAULT NULL,
  `tasktp_create_time` datetime DEFAULT NULL,
  `tasktp_create_ip` varchar(64) DEFAULT NULL,
  `tasktp_update_user` varchar(64) DEFAULT NULL,
  `tasktp_update_time` datetime DEFAULT NULL,
  `tasktp_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tasktp_id`),
  KEY `tasktp_event_id` (`tasktp_event_id`),
  KEY `tasktp_taskt_id` (`tasktp_taskt_id`),
  CONSTRAINT `task_tmpl_ibfk_1` FOREIGN KEY (`tasktp_event_id`) REFERENCES `event` (`event_id`) ON UPDATE CASCADE,
  CONSTRAINT `task_tmpl_ibfk_2` FOREIGN KEY (`tasktp_taskt_id`) REFERENCES `task_type` (`taskt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `task_type`
--

DROP TABLE IF EXISTS `task_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_type` (
  `taskt_id` int(11) NOT NULL AUTO_INCREMENT,
  `taskt_name` varchar(64) NOT NULL,
  `taskt_note` text NOT NULL,
  `taskt_create_user` varchar(64) DEFAULT NULL,
  `taskt_create_time` datetime DEFAULT NULL,
  `taskt_create_ip` varchar(64) DEFAULT NULL,
  `taskt_update_user` varchar(64) DEFAULT NULL,
  `taskt_update_time` datetime DEFAULT NULL,
  `taskt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`taskt_id`),
  UNIQUE KEY `tskt_name` (`taskt_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_answer`
--

DROP TABLE IF EXISTS `training_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_answer` (
  `tra_id` int(11) NOT NULL AUTO_INCREMENT,
  `tra_trq_id` int(11) NOT NULL,
  `tra_answer` text NOT NULL,
  `tra_variant` int(11) NOT NULL,
  `tra_file_name` varchar(256) DEFAULT NULL,
  `tra_file_data` longblob,
  `tra_create_user` varchar(64) DEFAULT NULL,
  `tra_create_time` datetime DEFAULT NULL,
  `tra_create_ip` varchar(64) DEFAULT NULL,
  `tra_update_user` varchar(64) DEFAULT NULL,
  `tra_update_time` datetime DEFAULT NULL,
  `tra_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tra_id`),
  KEY `tra_trq_id` (`tra_trq_id`),
  CONSTRAINT `training_answer_ibfk_1` FOREIGN KEY (`tra_trq_id`) REFERENCES `training_question` (`trq_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4673 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_lecture`
--

DROP TABLE IF EXISTS `training_lecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_lecture` (
  `tlec_id` int(11) NOT NULL AUTO_INCREMENT,
  `tlec_name` varchar(128) NOT NULL,
  `tlec_data` blob NOT NULL,
  `tlec_trm_id` int(11) NOT NULL,
  `tlec_create_user` varchar(64) DEFAULT NULL,
  `tlec_create_time` datetime DEFAULT NULL,
  `tlec_create_ip` varchar(64) DEFAULT NULL,
  `tlec_update_user` varchar(64) DEFAULT NULL,
  `tlec_update_time` datetime DEFAULT NULL,
  `tlec_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`tlec_id`),
  KEY `tlec_trm_id` (`tlec_trm_id`),
  CONSTRAINT `training_lecture_ibfk_1` FOREIGN KEY (`tlec_trm_id`) REFERENCES `training_module` (`trm_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_module`
--

DROP TABLE IF EXISTS `training_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_module` (
  `trm_id` int(11) NOT NULL AUTO_INCREMENT,
  `trm_trm_id` int(11) DEFAULT NULL,
  `trm_name` varchar(512) NOT NULL,
  `trm_code` varchar(128) NOT NULL,
  `trm_dataA` longblob,
  `trm_dataA_name` varchar(256) DEFAULT NULL,
  `trm_dataB` longblob,
  `trm_dataB_name` varchar(256) DEFAULT NULL,
  `trm_test_question` int(11) DEFAULT NULL,
  `trm_create_user` varchar(64) DEFAULT NULL,
  `trm_create_time` datetime DEFAULT NULL,
  `trm_create_ip` varchar(64) DEFAULT NULL,
  `trm_update_user` varchar(64) DEFAULT NULL,
  `trm_update_time` datetime DEFAULT NULL,
  `trm_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`trm_id`),
  KEY `trm_trm_id` (`trm_trm_id`),
  CONSTRAINT `training_module_ibfk_1` FOREIGN KEY (`trm_trm_id`) REFERENCES `training_module` (`trm_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_prog`
--

DROP TABLE IF EXISTS `training_prog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_prog` (
  `trp_id` int(11) NOT NULL AUTO_INCREMENT,
  `trp_name` varchar(512) NOT NULL,
  `trp_code` varchar(128) NOT NULL,
  `trp_hour` int(11) NOT NULL,
  `trp_dataA1` longblob,
  `trp_dataA2` longblob,
  `trp_dataA1_name` varchar(256) DEFAULT NULL,
  `trp_dataA2_name` varchar(256) DEFAULT NULL,
  `trp_dataB1` longblob,
  `trp_dataB2` longblob,
  `trp_dataB1_name` varchar(256) DEFAULT NULL,
  `trp_dataB2_name` varchar(256) DEFAULT NULL,
  `trp_test_question` int(11) DEFAULT NULL,
  `trp_create_user` varchar(64) DEFAULT NULL,
  `trp_create_time` datetime DEFAULT NULL,
  `trp_create_ip` varchar(64) DEFAULT NULL,
  `trp_update_user` varchar(64) DEFAULT NULL,
  `trp_update_time` datetime DEFAULT NULL,
  `trp_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`trp_id`),
  UNIQUE KEY `trp_code` (`trp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=678 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_prog_module`
--

DROP TABLE IF EXISTS `training_prog_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_prog_module` (
  `trpl_id` int(11) NOT NULL AUTO_INCREMENT,
  `trpl_trm_id` int(11) NOT NULL,
  `trpl_trp_id` int(11) NOT NULL,
  `trpl_create_user` varchar(64) DEFAULT NULL,
  `trpl_create_time` datetime DEFAULT NULL,
  `trpl_create_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`trpl_id`),
  KEY `trpl_trm_id` (`trpl_trm_id`),
  KEY `trpl_trp_id` (`trpl_trp_id`),
  CONSTRAINT `training_prog_module_ibfk_1` FOREIGN KEY (`trpl_trm_id`) REFERENCES `training_module` (`trm_id`) ON UPDATE CASCADE,
  CONSTRAINT `training_prog_module_ibfk_2` FOREIGN KEY (`trpl_trp_id`) REFERENCES `training_prog` (`trp_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2600 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_question`
--

DROP TABLE IF EXISTS `training_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_question` (
  `trq_id` int(11) NOT NULL AUTO_INCREMENT,
  `trq_question` text NOT NULL,
  `trq_fname` varchar(256) DEFAULT NULL,
  `trq_fdata` longblob,
  `trq_trm_id` int(11) NOT NULL,
  `trq_create_user` varchar(64) DEFAULT NULL,
  `trq_create_time` datetime DEFAULT NULL,
  `trq_create_ip` varchar(64) DEFAULT NULL,
  `trq_update_user` varchar(64) DEFAULT NULL,
  `trq_update_time` datetime DEFAULT NULL,
  `trq_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`trq_id`),
  KEY `trq_trm_id` (`trq_trm_id`),
  CONSTRAINT `training_question_ibfk_1` FOREIGN KEY (`trq_trm_id`) REFERENCES `training_module` (`trm_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1203 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_type`
--

DROP TABLE IF EXISTS `training_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_type` (
  `trt_id` int(11) NOT NULL AUTO_INCREMENT,
  `trt_name` varchar(64) NOT NULL,
  `trt_create_user` varchar(64) DEFAULT NULL,
  `trt_create_time` datetime DEFAULT NULL,
  `trt_create_ip` varchar(64) DEFAULT NULL,
  `trt_update_user` varchar(64) DEFAULT NULL,
  `trt_update_time` datetime DEFAULT NULL,
  `trt_update_ip` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`trt_id`),
  UNIQUE KEY `trp_name` (`trt_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(128) NOT NULL,
  `user_pwd` varchar(128) NOT NULL,
  `user_real` varchar(128) NOT NULL,
  `user_level` int(11) NOT NULL,
  `user_authKey` varchar(128) NOT NULL,
  `user_accessToken` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-16 17:33:19
