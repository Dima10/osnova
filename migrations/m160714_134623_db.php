<?php

use yii\db\Migration;

class m160714_134623_db extends Migration
{
    public function up()
    {
        return true;

        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text(),
        ]);

    }

    public function down()
    {
        return true;
        echo "m160714_134623_db cannot be reverted.\n";

        return false;

         $this->dropTable('news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
