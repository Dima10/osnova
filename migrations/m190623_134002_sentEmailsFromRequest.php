<?php

use yii\db\Migration;

/**
 * Class m190623_134002_sentEmailsFromRequest
 */
class m190623_134002_sentEmailsFromRequest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `sent_emails_from_request` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `applr_id` int(11) NOT NULL DEFAULT '0',
                              `applrc_prs_id` int(11) NOT NULL DEFAULT '0',
                              `fio` varchar(256) DEFAULT NULL,
                              `login` varchar(256) DEFAULT NULL,
                              `applrc_trp_id` int(11) NOT NULL DEFAULT '0',
                              `applr_ab_id` int(11) NOT NULL DEFAULT '0',
                              `applr_manager_id` int(11) NOT NULL DEFAULT '0',
                              `recipient_email` varchar(256) DEFAULT NULL,
                              `sender_email` varchar(256) DEFAULT NULL,
                              `status` int(3) NOT NULL DEFAULT '0',
                              `send_date` datetime NOT NULL,
                              `applr_reestr` int(11) NOT NULL DEFAULT '0',
                              `applr_flag` int(11) NOT NULL DEFAULT '0',
                              `portal` datetime NOT NULL,
                              PRIMARY KEY (`id`),
                              KEY `prs_id` (`applrc_prs_id`),
                              KEY `prs_login` (`login`(255))
                            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8"
        );
}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sent_emails_from_request');
    }

}
