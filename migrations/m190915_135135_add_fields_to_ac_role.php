<?php

use yii\db\Migration;

/**
 * Class m190915_135135_add_fields_to_ac_role
 */
class m190915_135135_add_fields_to_ac_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `ac_role`
            ADD COLUMN `acr_create_ip` VARCHAR(64),
            ADD COLUMN `acr_update_ip` VARCHAR(64);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE `ac_role`
            DROP COLUMN `acr_create_ip`,
            DROP COLUMN `acr_update_ip`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190915_135135_add_fields_to_ac_role cannot be reverted.\n";

        return false;
    }
    */
}
